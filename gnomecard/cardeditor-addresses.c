/* GNOME Card Editor Addresses Object
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <glade/glade.h>

#include <mimedir/mimedir-vcard.h>
#include <mimedir/mimedir-vcard-address.h>

#include "cardeditor-addresses.h"


struct _CardEditorAddressesPriv {
	MIMEDirVCard	*vcard;

	GtkWidget	*treeview;
	GtkListStore	*model;

	GtkWidget	*freeform_check;
	GtkWidget	*address_notebook;

	GtkWidget	*pobox_entry;
	GtkWidget	*extended_entry;
	GtkWidget	*street_entry;
	GtkWidget	*locality_entry;
	GtkWidget	*region_entry;
	GtkWidget	*pcode_entry;
	GtkWidget	*country_entry;

	GtkWidget	*freeform_text;

	GtkWidget	*domestic_check;
	GtkWidget	*intl_check;
	GtkWidget	*postal_check;
	GtkWidget	*parcel_check;
	GtkWidget	*home_check;
	GtkWidget	*work_check;
	GtkWidget	*pref_check;
};


static GtkHBoxClass *parent_class = NULL;


static void		card_editor_addresses_class_init	(CardEditorAddressesClass *klass);
static void		card_editor_addresses_init		(CardEditorAddresses *addresses);
static void		card_editor_addresses_dispose		(GObject *object);

static gboolean		card_editor_addresses_setup		(CardEditorAddresses *addresses);
static GtkTreeIter	card_editor_addresses_internal_append	(CardEditorAddresses *addresses, MIMEDirVCardAddress *add);
static MIMEDirVCardAddress *
			card_editor_addresses_get_current_address
								(CardEditorAddresses *addresses);

static void		card_editor_addresses_address_changed	(CardEditorAddresses *addresses, MIMEDirVCardAddress *address);

/*
 * Class and Object Management
 */

GType
card_editor_addresses_get_type (void)
{
	static GType card_editor_addresses_type = 0;

	if (!card_editor_addresses_type) {
		static const GTypeInfo card_editor_addresses_info = {
			sizeof (CardEditorAddressesClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) card_editor_addresses_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CardEditorAddresses),
			1,    /* n_preallocs */
			(GInstanceInitFunc) card_editor_addresses_init,
		};

		card_editor_addresses_type = g_type_register_static (CARD_EDITOR_TYPE_EDITLIST,
								     "CardEditorAddresses",
								     &card_editor_addresses_info,
								     0);
	}

	return card_editor_addresses_type;
}


static void
card_editor_addresses_class_init (CardEditorAddressesClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CARD_EDITOR_IS_ADDRESSES_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = card_editor_addresses_dispose;

	parent_class = g_type_class_peek_parent (klass);
}


static void
card_editor_addresses_init (CardEditorAddresses *widget)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARD_EDITOR_IS_ADDRESSES (widget));

	widget->priv = g_new0 (CardEditorAddressesPriv, 1);

	card_editor_addresses_setup (widget);
}


static void
card_editor_addresses_dispose (GObject *object)
{
	CardEditorAddresses *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CARD_EDITOR_IS_ADDRESSES (object));

	list = CARD_EDITOR_ADDRESSES (object);

	if (list->priv) {
		if (list->priv->vcard)
			g_object_unref (G_OBJECT (list->priv->vcard));
		g_object_unref (G_OBJECT (list->priv->model));

		g_free (list->priv);
		list->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Callbacks
 */

static void
card_editor_addresses_add_item (CardEditorAddresses *addresses, CardEditorEditList *list)
{
	MIMEDirVCardAddress *add;
	GtkTreeIter iter;
	GtkTreeSelection *selection;

	add = mimedir_vcard_address_new ();

	mimedir_vcard_append_address (addresses->priv->vcard, add);

	iter = card_editor_addresses_internal_append (addresses, add);

	g_object_unref (G_OBJECT (add));

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (addresses->priv->treeview));
	gtk_tree_selection_select_iter (selection, &iter);

	gtk_widget_grab_focus (addresses->priv->pobox_entry);
}


static void
card_editor_addresses_remove_item (CardEditorAddresses *addresses, CardEditorEditList *list)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (addresses->priv->treeview));

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardAddress *address;

		gtk_tree_model_get (model, &iter, 0, &address, -1);

		g_assert (address != NULL && MIMEDIR_IS_VCARD_ADDRESS (address));

		g_signal_handlers_disconnect_by_func (G_OBJECT (address),
						      card_editor_addresses_address_changed,
						      addresses);

		if (gtk_list_store_remove (GTK_LIST_STORE (model), &iter)) {
			gtk_tree_selection_select_iter (selection, &iter);
		} else {
			/* Select last element */

			gint n;

			n = gtk_tree_model_iter_n_children (model, NULL);
			if (n > 0) {
				gtk_tree_model_iter_nth_child (model, &iter, NULL, n - 1);
				gtk_tree_selection_select_iter (selection, &iter);
			}
		}
		mimedir_vcard_remove_address (addresses->priv->vcard, address);

		gtk_widget_grab_focus (addresses->priv->pobox_entry);
	}
}


static void
card_editor_addresses_render_item_cb (GtkTreeViewColumn *column,
				      GtkCellRenderer *renderer,
				      GtkTreeModel *model,
				      GtkTreeIter *iter,
				      gpointer data)
{
	MIMEDirVCardAddress *address;
	gchar *title;

	gtk_tree_model_get (model, iter, 0, &address, -1);
	g_assert (address != NULL && MIMEDIR_IS_VCARD_ADDRESS (address));

	g_object_get (G_OBJECT (address), "title", &title, NULL);
	g_object_set (G_OBJECT (renderer), "text", title ? title : "", NULL);
	g_free (title);
}


static void
card_editor_addresses_entry_updated (CardEditorAddresses *addresses, GtkEntry *entry, const gchar *prop)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (addresses->priv->treeview));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardAddress *address;
		const gchar *text;

		text = gtk_entry_get_text (entry);

		gtk_tree_model_get (model, &iter, 0, &address, -1);
		g_assert (address != NULL && MIMEDIR_IS_VCARD_ADDRESS (address));
		g_object_set (G_OBJECT (address), prop, text, NULL);
	}
}


static void
card_editor_addresses_set_freeform_view (CardEditorAddresses *addresses, GtkToggleButton *button)
{
	CardEditorAddressesPriv *priv;
	gboolean act;

	priv = addresses->priv;

	act = gtk_toggle_button_get_active (button);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (priv->address_notebook),
				       act ? 1 : 0);
}


static void
card_editor_addresses_freeform_toggled (CardEditorAddresses *addresses, GtkToggleButton *button)
{
	CardEditorAddressesPriv *priv;
	MIMEDirVCardAddress *address;
	gboolean act;

	priv = addresses->priv;

	address = card_editor_addresses_get_current_address (addresses);
	g_assert (address != NULL);

	act = gtk_toggle_button_get_active (button);
	if (act) {
		GtkTextBuffer *buffer;
		GtkTextIter start, end;
		gchar *text;

		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->freeform_text));
		gtk_text_buffer_get_start_iter (buffer, &start);
		gtk_text_buffer_get_end_iter (buffer, &end);
		text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);

		g_object_set (G_OBJECT (address),
			      "full",     text,

			      "pobox",    NULL,
			      "extended", NULL,
			      "street",   NULL,
			      "locality", NULL,
			      "region",   NULL,
			      "pcode",    NULL,
			      "country",  NULL,

			      NULL);

		g_free (text);
	} else {
		const gchar *pobox, *extended, *street, *locality, *region, *pcode, *country;

		pobox    = gtk_entry_get_text (GTK_ENTRY (priv->pobox_entry));
		extended = gtk_entry_get_text (GTK_ENTRY (priv->extended_entry));
		street   = gtk_entry_get_text (GTK_ENTRY (priv->street_entry));
		locality = gtk_entry_get_text (GTK_ENTRY (priv->locality_entry));
		region   = gtk_entry_get_text (GTK_ENTRY (priv->region_entry));
		pcode    = gtk_entry_get_text (GTK_ENTRY (priv->pcode_entry));
		country  = gtk_entry_get_text (GTK_ENTRY (priv->country_entry));

		g_object_set (G_OBJECT (address),
			      "full",     NULL,

			      "pobox",    pobox,
			      "extended", extended,
			      "street",   street,
			      "locality", locality,
			      "region",   region,
			      "pcode",    pcode,
			      "country",  country,

			      NULL);
	}
}


static void
card_editor_addresses_pobox_updated (CardEditorAddresses *addresses, GtkEntry *entry)
{
	card_editor_addresses_entry_updated (addresses, entry, "pobox");
}


static void
card_editor_addresses_extended_updated (CardEditorAddresses *addresses, GtkEntry *entry)
{
	card_editor_addresses_entry_updated (addresses, entry, "extended");
}


static void
card_editor_addresses_street_updated (CardEditorAddresses *addresses, GtkEntry *entry)
{
	card_editor_addresses_entry_updated (addresses, entry, "street");
}


static void
card_editor_addresses_locality_updated (CardEditorAddresses *addresses, GtkEntry *entry)
{
	card_editor_addresses_entry_updated (addresses, entry, "locality");
}


static void
card_editor_addresses_region_updated (CardEditorAddresses *addresses, GtkEntry *entry)
{
	card_editor_addresses_entry_updated (addresses, entry, "region");
}


static void
card_editor_addresses_pcode_updated (CardEditorAddresses *addresses, GtkEntry *entry)
{
	card_editor_addresses_entry_updated (addresses, entry, "pcode");
}


static void
card_editor_addresses_country_updated (CardEditorAddresses *addresses, GtkEntry *entry)
{
	card_editor_addresses_entry_updated (addresses, entry, "country");
}


static void
card_editor_addresses_check_updated (CardEditorAddresses *addresses, GtkToggleButton *button, const gchar *prop)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (addresses->priv->treeview));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardAddress *address;
		gboolean b;

		b = gtk_toggle_button_get_active (button);

		gtk_tree_model_get (model, &iter, 0, &address, -1);
		g_assert (address != NULL && MIMEDIR_IS_VCARD_ADDRESS (address));
		g_object_set (G_OBJECT (address), prop, b, NULL);
	}
}


static void
card_editor_addresses_domestic_updated (CardEditorAddresses *addresses, GtkToggleButton *button)
{
	card_editor_addresses_check_updated (addresses, button, "domestic");
}


static void
card_editor_addresses_intl_updated (CardEditorAddresses *addresses, GtkToggleButton *button)
{
	card_editor_addresses_check_updated (addresses, button, "international");
}


static void
card_editor_addresses_postal_updated (CardEditorAddresses *addresses, GtkToggleButton *button)
{
	card_editor_addresses_check_updated (addresses, button, "postal");
}


static void
card_editor_addresses_parcel_updated (CardEditorAddresses *addresses, GtkToggleButton *button)
{
	card_editor_addresses_check_updated (addresses, button, "parcel");
}


static void
card_editor_addresses_home_updated (CardEditorAddresses *addresses, GtkToggleButton *button)
{
	card_editor_addresses_check_updated (addresses, button, "home");
}


static void
card_editor_addresses_work_updated (CardEditorAddresses *addresses, GtkToggleButton *button)
{
	card_editor_addresses_check_updated (addresses, button, "work");
}


static void
card_editor_addresses_pref_updated (CardEditorAddresses *addresses, GtkToggleButton *button)
{
	card_editor_addresses_check_updated (addresses, button, "preferred");
}


static gboolean
card_editor_addresses_address_changed_fe (GtkTreeModel *model,
					  GtkTreePath  *path,
					  GtkTreeIter  *iter,
					  gpointer      data)
{
	MIMEDirVCardAddress *address;

	g_return_val_if_fail (MIMEDIR_IS_VCARD_ADDRESS (data), FALSE);

	gtk_tree_model_get (model, iter, 0, &address, -1);
	g_assert (MIMEDIR_IS_VCARD_ADDRESS (address));

	if (address == data)
		gtk_tree_model_row_changed (model, path, iter);

	return FALSE;
}


static void
card_editor_addresses_address_changed (CardEditorAddresses *addresses, MIMEDirVCardAddress *address)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL (addresses->priv->model), card_editor_addresses_address_changed_fe, address);
}


static void
card_editor_addresses_update_editor (CardEditorAddresses *addresses, CardEditorEditList *list)
{
	CardEditorAddressesPriv *priv;

	GtkTreeSelection *selection;
	GtkTreeIter iter;
	GtkTreeModel *model;
	MIMEDirVCardAddress *address;

	priv = addresses->priv;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (addresses->priv->treeview));

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		GtkTextBuffer *buffer;
		gchar *full;
		gchar *pobox, *extended, *street, *locality, *region, *pcode, *country;
		gboolean domestic, intl, postal, parcel, home, work, pref;

		gtk_tree_model_get (model, &iter, 0, &address, -1);
		g_assert (MIMEDIR_IS_VCARD_ADDRESS (address));

		g_signal_handlers_block_by_func (priv->freeform_check, card_editor_addresses_freeform_toggled, addresses);

		g_signal_handlers_block_by_func (priv->pobox_entry,    card_editor_addresses_pobox_updated,    addresses);
		g_signal_handlers_block_by_func (priv->extended_entry, card_editor_addresses_extended_updated, addresses);
		g_signal_handlers_block_by_func (priv->street_entry,   card_editor_addresses_street_updated,   addresses);
		g_signal_handlers_block_by_func (priv->locality_entry, card_editor_addresses_locality_updated, addresses);
		g_signal_handlers_block_by_func (priv->region_entry,   card_editor_addresses_region_updated,   addresses);
		g_signal_handlers_block_by_func (priv->pcode_entry,    card_editor_addresses_pcode_updated,    addresses);
		g_signal_handlers_block_by_func (priv->country_entry,  card_editor_addresses_country_updated,  addresses);

		g_signal_handlers_block_by_func (priv->domestic_check, card_editor_addresses_domestic_updated, addresses);
		g_signal_handlers_block_by_func (priv->intl_check,     card_editor_addresses_intl_updated,     addresses);
		g_signal_handlers_block_by_func (priv->postal_check,   card_editor_addresses_postal_updated,   addresses);
		g_signal_handlers_block_by_func (priv->parcel_check,   card_editor_addresses_parcel_updated,   addresses);
		g_signal_handlers_block_by_func (priv->home_check,     card_editor_addresses_home_updated,     addresses);
		g_signal_handlers_block_by_func (priv->work_check,     card_editor_addresses_work_updated,     addresses);
		g_signal_handlers_block_by_func (priv->pref_check,     card_editor_addresses_pref_updated,     addresses);

		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->freeform_text));

		g_object_get (G_OBJECT (address),
			      "full",          &full,

			      "pobox",         &pobox,
			      "extended",      &extended,
			      "street",        &street,
			      "locality",      &locality,
			      "region",        &region,
			      "pcode",         &pcode,
			      "country",       &country,

			      "domestic",      &domestic,
			      "international", &intl,
			      "postal",        &postal,
			      "parcel",        &parcel,
			      "home",          &home,
			      "work",          &work,
			      "preferred",     &pref,

			      NULL);

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->freeform_check), full != NULL);

		gtk_text_buffer_set_text (buffer, full ? full : "", -1);

		gtk_entry_set_text (GTK_ENTRY (priv->pobox_entry),    pobox    ? pobox    : "");
		gtk_entry_set_text (GTK_ENTRY (priv->extended_entry), extended ? extended : "");
		gtk_entry_set_text (GTK_ENTRY (priv->street_entry),   street   ? street   : "");
		gtk_entry_set_text (GTK_ENTRY (priv->locality_entry), locality ? locality : "");
		gtk_entry_set_text (GTK_ENTRY (priv->region_entry),   region   ? region   : "");
		gtk_entry_set_text (GTK_ENTRY (priv->pcode_entry),    pcode    ? pcode    : "");
		gtk_entry_set_text (GTK_ENTRY (priv->country_entry),  country  ? country  : "");

		g_free (full);
		g_free (pobox);
		g_free (extended);
		g_free (street);
		g_free (locality);
		g_free (region);
		g_free (pcode);
		g_free (country);

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->domestic_check), domestic);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->intl_check),     intl);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->postal_check),   postal);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->parcel_check),   parcel);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->home_check),     home);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->work_check),     work);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->pref_check),     pref);

		g_signal_handlers_unblock_by_func (priv->freeform_check, card_editor_addresses_freeform_toggled, addresses);

		g_signal_handlers_unblock_by_func (priv->pobox_entry,    card_editor_addresses_pobox_updated,    addresses);
		g_signal_handlers_unblock_by_func (priv->extended_entry, card_editor_addresses_extended_updated, addresses);
		g_signal_handlers_unblock_by_func (priv->street_entry,   card_editor_addresses_street_updated,   addresses);
		g_signal_handlers_unblock_by_func (priv->locality_entry, card_editor_addresses_locality_updated, addresses);
		g_signal_handlers_unblock_by_func (priv->region_entry,   card_editor_addresses_region_updated,   addresses);
		g_signal_handlers_unblock_by_func (priv->pcode_entry,    card_editor_addresses_pcode_updated,    addresses);
		g_signal_handlers_unblock_by_func (priv->country_entry,  card_editor_addresses_country_updated,  addresses);

		g_signal_handlers_unblock_by_func (priv->domestic_check, card_editor_addresses_domestic_updated, addresses);
		g_signal_handlers_unblock_by_func (priv->intl_check,     card_editor_addresses_intl_updated,     addresses);
		g_signal_handlers_unblock_by_func (priv->postal_check,   card_editor_addresses_postal_updated,   addresses);
		g_signal_handlers_unblock_by_func (priv->parcel_check,   card_editor_addresses_parcel_updated,   addresses);
		g_signal_handlers_unblock_by_func (priv->home_check,     card_editor_addresses_home_updated,     addresses);
		g_signal_handlers_unblock_by_func (priv->work_check,     card_editor_addresses_work_updated,     addresses);
		g_signal_handlers_unblock_by_func (priv->pref_check,     card_editor_addresses_pref_updated,     addresses);
	}
}

static void
card_editor_addresses_reordered_cb (CardEditorAddresses *addresses, GtkTreePath *path, GtkTreeIter *iter, gpointer arg3, GtkTreeModel *model)
{
	/* FIXME */
}

/*
 * Internal Methods
 */

static gboolean
card_editor_addresses_setup (CardEditorAddresses *addresses)
{
	CardEditorAddressesPriv *priv;
	GladeXML *xml;
	GtkWidget *w, *tree;
	GtkCellRenderer *renderer;

	priv = addresses->priv;

	/* Internal signals */

	g_signal_connect_swapped (G_OBJECT (addresses), "selection-changed", G_CALLBACK (card_editor_addresses_update_editor), addresses);
	g_signal_connect_swapped (G_OBJECT (addresses), "add-item", G_CALLBACK (card_editor_addresses_add_item), addresses);
	g_signal_connect_swapped (G_OBJECT (addresses), "remove-item", G_CALLBACK (card_editor_addresses_remove_item), addresses);

	/* Tree view */

	priv->model = gtk_list_store_new (2, G_TYPE_OBJECT, G_TYPE_ULONG);
	g_signal_connect_swapped (G_OBJECT (priv->model), "rows-reordered",
				  G_CALLBACK (card_editor_addresses_reordered_cb), addresses);

	tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (priv->model));
	priv->treeview = tree;
	gtk_tree_view_set_reorderable (GTK_TREE_VIEW (tree), TRUE);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree), FALSE);
	gtk_widget_show (tree);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (tree),
						    0, NULL, renderer,
						    card_editor_addresses_render_item_cb, addresses, NULL);

	card_editor_editlist_set_list (CARD_EDITOR_EDITLIST (addresses), GTK_TREE_VIEW (tree));

	/* Editor */

	xml = glade_xml_new (GLADEDIR "/card-editor.glade", "address_box", GETTEXT_PACKAGE);
	if (!xml) {
		g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/card-editor.glade");
		return FALSE;
	}

	glade_xml_signal_autoconnect (xml);

	w = glade_xml_get_widget (xml, "address_box");

	priv->freeform_check   = glade_xml_get_widget (xml, "freeform_check");
	priv->address_notebook = glade_xml_get_widget (xml, "address_notebook");

	priv->pobox_entry      = glade_xml_get_widget (xml, "pobox_entry");
	priv->extended_entry   = glade_xml_get_widget (xml, "extended_entry");
	priv->street_entry     = glade_xml_get_widget (xml, "street_entry");
	priv->locality_entry   = glade_xml_get_widget (xml, "locality_entry");
	priv->region_entry     = glade_xml_get_widget (xml, "region_entry");
	priv->pcode_entry      = glade_xml_get_widget (xml, "pcode_entry");
	priv->country_entry    = glade_xml_get_widget (xml, "country_entry");

	priv->freeform_text    = glade_xml_get_widget (xml, "freeform_text");

	priv->domestic_check   = glade_xml_get_widget (xml, "domestic_check");
	priv->intl_check       = glade_xml_get_widget (xml, "intl_check");
	priv->postal_check     = glade_xml_get_widget (xml, "postal_check");
	priv->parcel_check     = glade_xml_get_widget (xml, "parcel_check");
	priv->home_check       = glade_xml_get_widget (xml, "home_check");
	priv->work_check       = glade_xml_get_widget (xml, "work_check");
	priv->pref_check       = glade_xml_get_widget (xml, "pref_check");

	if (!w ||
	    !priv->freeform_check || !priv->address_notebook ||
	    !priv->pobox_entry || !priv->extended_entry || !priv->street_entry || !priv->locality_entry || !priv->region_entry || !priv->pcode_entry || !priv->country_entry ||
	    !priv->freeform_text ||
	    !priv->domestic_check || !priv->intl_check || !priv->postal_check || !priv->parcel_check || !priv->home_check || !priv->work_check || !priv->pref_check) {
		g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
		return FALSE;
	}

	/* FIXME: listen to changes of freeform_text */

	g_signal_connect_swapped (priv->freeform_check, "toggled", G_CALLBACK (card_editor_addresses_set_freeform_view), addresses);
	g_signal_connect_swapped (priv->freeform_check, "toggled", G_CALLBACK (card_editor_addresses_freeform_toggled), addresses);

	g_signal_connect_swapped (priv->pobox_entry,    "changed", G_CALLBACK (card_editor_addresses_pobox_updated),    addresses);
	g_signal_connect_swapped (priv->extended_entry, "changed", G_CALLBACK (card_editor_addresses_extended_updated), addresses);
	g_signal_connect_swapped (priv->street_entry,   "changed", G_CALLBACK (card_editor_addresses_street_updated),   addresses);
	g_signal_connect_swapped (priv->locality_entry, "changed", G_CALLBACK (card_editor_addresses_locality_updated), addresses);
	g_signal_connect_swapped (priv->region_entry,   "changed", G_CALLBACK (card_editor_addresses_region_updated),   addresses);
	g_signal_connect_swapped (priv->pcode_entry,    "changed", G_CALLBACK (card_editor_addresses_pcode_updated),    addresses);
	g_signal_connect_swapped (priv->country_entry,  "changed", G_CALLBACK (card_editor_addresses_country_updated),  addresses);

	g_signal_connect_swapped (priv->domestic_check, "toggled", G_CALLBACK (card_editor_addresses_domestic_updated), addresses);
	g_signal_connect_swapped (priv->intl_check,     "toggled", G_CALLBACK (card_editor_addresses_intl_updated),     addresses);
	g_signal_connect_swapped (priv->postal_check,   "toggled", G_CALLBACK (card_editor_addresses_postal_updated),   addresses);
	g_signal_connect_swapped (priv->parcel_check,   "toggled", G_CALLBACK (card_editor_addresses_parcel_updated),   addresses);
	g_signal_connect_swapped (priv->home_check,     "toggled", G_CALLBACK (card_editor_addresses_home_updated),     addresses);
	g_signal_connect_swapped (priv->work_check,     "toggled", G_CALLBACK (card_editor_addresses_work_updated),     addresses);
	g_signal_connect_swapped (priv->pref_check,     "toggled", G_CALLBACK (card_editor_addresses_pref_updated),     addresses);

	card_editor_editlist_set_editor (CARD_EDITOR_EDITLIST (addresses), w);
	g_object_unref (G_OBJECT (xml));

	gtk_widget_grab_focus (priv->pobox_entry);

	return TRUE;
}


static GtkTreeIter
card_editor_addresses_internal_append (CardEditorAddresses *addresses, MIMEDirVCardAddress *add)
{
	GtkTreeIter iter;

	g_signal_connect_swapped (G_OBJECT (add), "changed", G_CALLBACK (card_editor_addresses_address_changed), addresses);

	gtk_list_store_append (addresses->priv->model, &iter);
	gtk_list_store_set (addresses->priv->model, &iter, 0, add, -1);

	return iter;
}


static MIMEDirVCardAddress *
card_editor_addresses_get_current_address (CardEditorAddresses *addresses)
{
	MIMEDirVCardAddress *address;
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (addresses->priv->treeview));
	if (!gtk_tree_selection_get_selected (selection, &model, &iter))
		return NULL;

	gtk_tree_model_get (model, &iter, 0, &address, -1);
	g_assert (address != NULL && MIMEDIR_IS_VCARD_ADDRESS (address));
	return address;
}

/*
 * External Methods
 */

GtkWidget *
card_editor_addresses_new (void)
{
	CardEditorAddresses *addresses;

	addresses = g_object_new (CARD_EDITOR_TYPE_ADDRESSES, NULL);

	return GTK_WIDGET (addresses);
}


void
card_editor_addresses_edit_vcard (CardEditorAddresses *addresses, MIMEDirVCard *vcard)
{
	CardEditorAddressesPriv *priv;
	GSList *list, *l;

	g_return_if_fail (addresses != NULL);
	g_return_if_fail (CARD_EDITOR_IS_ADDRESSES (addresses));
	g_return_if_fail (vcard != NULL);
	g_return_if_fail (MIMEDIR_IS_VCARD (vcard));

	priv = addresses->priv;

	if (priv->vcard)
		g_object_unref (G_OBJECT (priv->vcard));
	g_object_ref (vcard);
	priv->vcard = vcard;

	g_object_get (G_OBJECT (vcard), "address_list", &list, NULL);

	for (l = list; l != NULL; l = g_slist_next (l)) {
		MIMEDirVCardAddress *add;

		add = MIMEDIR_VCARD_ADDRESS (l->data);
		card_editor_addresses_internal_append (addresses, add);
	}

	if (list) {
		GtkTreeSelection *selection;
		GtkTreePath *path;

		selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->treeview));

		path = gtk_tree_path_new_from_string ("0");
		gtk_tree_selection_select_path (selection, path);
		gtk_tree_path_free (path);
	}
}
