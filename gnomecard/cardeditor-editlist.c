/* GNOME Card Editor Edit List Object
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include "cardeditor-editlist.h"


enum {
	SIGNAL_SELECTION_CHANGED,
	SIGNAL_ADD_ITEM,
	SIGNAL_REMOVE_ITEM,
	SIGNAL_LAST
};

struct _CardEditorEditListPriv {
	GtkWidget *editor_frame;
	GtkWidget *editor;
	GtkWidget *list_window;
	GtkWidget *list;
	GtkWidget *remove_button;
};


static GtkHBoxClass *parent_class = NULL;

static gint card_editor_editlist_signals[SIGNAL_LAST] = { 0, 0, 0 };


static void card_editor_editlist_class_init	(CardEditorEditListClass *klass);
static void card_editor_editlist_init		(CardEditorEditList *list);
static void card_editor_editlist_dispose	(GObject *object);

static void card_editor_editlist_setup		(CardEditorEditList *list);

/*
 * Class and Object Management
 */

GType
card_editor_editlist_get_type (void)
{
	static GType card_editor_editlist_type = 0;

	if (!card_editor_editlist_type) {
		static const GTypeInfo card_editor_editlist_info = {
			sizeof (CardEditorEditListClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) card_editor_editlist_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CardEditorEditList),
			1,    /* n_preallocs */
			(GInstanceInitFunc) card_editor_editlist_init,
		};

		card_editor_editlist_type = g_type_register_static (GTK_TYPE_HBOX,
								    "CardEditorEditList",
								    &card_editor_editlist_info,
								    0);
	}

	return card_editor_editlist_type;
}


static void
card_editor_editlist_class_init (CardEditorEditListClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CARD_EDITOR_IS_EDITLIST_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = card_editor_editlist_dispose;

	parent_class = g_type_class_peek_parent (klass);

	/* Signals */

	card_editor_editlist_signals[SIGNAL_SELECTION_CHANGED] =
		g_signal_new ("selection-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CardEditorEditListClass, selection_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	card_editor_editlist_signals[SIGNAL_ADD_ITEM] =
		g_signal_new ("add-item",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CardEditorEditListClass, add_item),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	card_editor_editlist_signals[SIGNAL_REMOVE_ITEM] =
		g_signal_new ("remove-item",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CardEditorEditListClass, remove_item),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
}


static void
card_editor_editlist_init (CardEditorEditList *widget)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARD_EDITOR_IS_EDITLIST (widget));

	widget->priv = g_new0 (CardEditorEditListPriv, 1);

	card_editor_editlist_setup (widget);
}


static void
card_editor_editlist_dispose (GObject *object)
{
	CardEditorEditList *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CARD_EDITOR_IS_EDITLIST (object));

	list = CARD_EDITOR_EDITLIST (object);

	g_free (list->priv);
	list->priv = NULL;

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Callbacks
 */

static void
card_editor_editlist_selection_changed (CardEditorEditList *list, GtkTreeSelection *selection)
{
	gboolean selected;

	selected = gtk_tree_selection_get_selected (selection, NULL, NULL);

	/* Send signal */

	g_signal_emit (G_OBJECT (list), card_editor_editlist_signals[SIGNAL_SELECTION_CHANGED], 0);

	/* Change sensibility */

	gtk_widget_set_sensitive (list->priv->remove_button, selected);
	if (list->priv->editor)
		gtk_widget_set_sensitive (list->priv->editor, selected);

}


static void
card_editor_editlist_add_cb (CardEditorEditList *list, GtkButton *button)
{
	g_signal_emit (G_OBJECT (list), card_editor_editlist_signals[SIGNAL_ADD_ITEM], 0);
}


static void
card_editor_editlist_remove_cb (CardEditorEditList *list, GtkButton *button)
{
	g_signal_emit (G_OBJECT (list), card_editor_editlist_signals[SIGNAL_REMOVE_ITEM], 0);
}

/*
 * Internal Methods
 */

static void
card_editor_editlist_setup (CardEditorEditList *list)
{
	GtkWidget *w, *box, *box2, *sw;

	gtk_box_set_spacing (GTK_BOX (list), 18);

	box = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (box);
	gtk_box_pack_start (GTK_BOX (list), box, TRUE, TRUE, 0);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);
	gtk_widget_show (sw);
	gtk_box_pack_start (GTK_BOX (box), sw, TRUE, TRUE, 0);
	list->priv->list_window = sw;

	box2 = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (box2), GTK_BUTTONBOX_EDGE);
	gtk_widget_show (box2);
	gtk_box_pack_start (GTK_BOX (box), box2, FALSE, FALSE, 0);

	w = gtk_button_new_from_stock (GTK_STOCK_NEW);
	g_signal_connect_swapped (G_OBJECT (w), "clicked", G_CALLBACK (card_editor_editlist_add_cb), list);
	gtk_widget_show (w);
	gtk_box_pack_start (GTK_BOX (box2), w, FALSE, FALSE, 0);

	w = gtk_button_new_from_stock (GTK_STOCK_DELETE);
	g_signal_connect_swapped (G_OBJECT (w), "clicked", G_CALLBACK (card_editor_editlist_remove_cb), list);
	gtk_widget_set_sensitive (w, FALSE);
	gtk_widget_show (w);
	gtk_box_pack_start (GTK_BOX (box2), w, FALSE, FALSE, 0);
	list->priv->remove_button = w;
}

/*
 * External Methods
 */

void
card_editor_editlist_set_editor (CardEditorEditList *list, GtkWidget *editor)
{
	CardEditorEditListPriv *priv;

	g_return_if_fail (list != NULL);
	g_return_if_fail (CARD_EDITOR_IS_EDITLIST (list));
	g_return_if_fail (editor != NULL);
	g_return_if_fail (GTK_IS_WIDGET (editor));

	priv = list->priv;

	if (priv->editor)
		gtk_container_remove (GTK_CONTAINER (list), priv->editor);
	gtk_box_pack_start (GTK_BOX (list), editor, TRUE, TRUE, 0);
	priv->editor = editor;

	if (priv->list) {
		GtkTreeSelection *selection;

		selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->list));
		card_editor_editlist_selection_changed (list, selection);
	}
}


void
card_editor_editlist_set_list (CardEditorEditList *list, GtkTreeView *view)
{
	CardEditorEditListPriv *priv;
	GtkTreeSelection *selection;

	g_return_if_fail (list != NULL);
	g_return_if_fail (CARD_EDITOR_IS_EDITLIST (list));
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_TREE_VIEW (view));

	priv = list->priv;

	if (priv->list)
		gtk_container_remove (GTK_CONTAINER (priv->list_window), priv->list);
	gtk_container_add (GTK_CONTAINER (priv->list_window), GTK_WIDGET (view));
	priv->list = GTK_WIDGET (view);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
	g_signal_connect_swapped (G_OBJECT (selection), "changed", G_CALLBACK (card_editor_editlist_selection_changed), list);

	if (priv->editor)
		card_editor_editlist_selection_changed (list, selection);
}
