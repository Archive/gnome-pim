/* GNOME Card Editor E-Mail Object
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <glade/glade.h>

#include <mimedir/mimedir-vcard.h>
#include <mimedir/mimedir-vcard-email.h>

#include "cardeditor-email.h"


/* FIXME: list reorderable */


struct _CardEditorEMailPriv {
	MIMEDirVCard	*vcard;

	GtkWidget	*treeview;
	GtkListStore	*model;

	GtkWidget	*address_entry;

	GtkWidget	*internet_radio;
	GtkWidget	*x400_radio;
	GtkWidget	*pref_check;
};


static GtkHBoxClass *parent_class = NULL;


static void		card_editor_email_class_init		(CardEditorEMailClass *klass);
static void		card_editor_email_init			(CardEditorEMail *email);
static void		card_editor_email_dispose		(GObject *object);

static gboolean		card_editor_email_setup			(CardEditorEMail *email);
static GtkTreeIter	card_editor_email_internal_append	(CardEditorEMail *email, MIMEDirVCardEMail *em);

static void		card_editor_email_address_changed	(CardEditorEMail *email, MIMEDirVCardEMail *em);

/*
 * Class and Object Management
 */

GType
card_editor_email_get_type (void)
{
	static GType card_editor_email_type = 0;

	if (!card_editor_email_type) {
		static const GTypeInfo card_editor_email_info = {
			sizeof (CardEditorEMailClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) card_editor_email_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CardEditorEMail),
			1,    /* n_preallocs */
			(GInstanceInitFunc) card_editor_email_init,
		};

		card_editor_email_type = g_type_register_static (CARD_EDITOR_TYPE_EDITLIST,
								 "CardEditorEMail",
								 &card_editor_email_info,
								 0);
	}

	return card_editor_email_type;
}


static void
card_editor_email_class_init (CardEditorEMailClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CARD_EDITOR_IS_EMAIL_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = card_editor_email_dispose;

	parent_class = g_type_class_peek_parent (klass);
}


static void
card_editor_email_init (CardEditorEMail *widget)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARD_EDITOR_IS_EMAIL (widget));

	widget->priv = g_new0 (CardEditorEMailPriv, 1);

	card_editor_email_setup (widget);
}


static void
card_editor_email_dispose (GObject *object)
{
	CardEditorEMail *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CARD_EDITOR_IS_EMAIL (object));

	list = CARD_EDITOR_EMAIL (object);

	if (list->priv) {
		if (list->priv->vcard)
			g_object_unref (G_OBJECT (list->priv->vcard));
		g_object_unref (G_OBJECT (list->priv->model));

		g_free (list->priv);
		list->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Callbacks
 */

static void
card_editor_email_update_editor (CardEditorEMail *email, CardEditorEditList *list)
{
	CardEditorEMailPriv *priv;

	GtkTreeSelection *selection;
	GtkTreeIter iter;
	GtkTreeModel *model;

	priv = email->priv;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (email->priv->treeview));

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardEMail *em;
		gchar *address;
		MIMEDirVCardEMailType type;
		gboolean pref;

		gtk_tree_model_get (model, &iter, 0, &em, -1);
		g_assert (MIMEDIR_IS_VCARD_EMAIL (em));

		g_object_get (G_OBJECT (em),
			      "address",   &address,
			      "type",      &type,
			      "preferred", &pref,
			      NULL);

		gtk_entry_set_text (GTK_ENTRY (priv->address_entry), address ? address : "");
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->pref_check), pref);

		switch (type) {
		case MIMEDIR_VCARD_EMAIL_TYPE_UNKNOWN:
			/* fall through */
		case MIMEDIR_VCARD_EMAIL_TYPE_INTERNET:
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->internet_radio), TRUE);
			break;
		case MIMEDIR_VCARD_EMAIL_TYPE_X400:
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->x400_radio), TRUE);
			break;
		}

		g_free (address);
	}
}


static void
card_editor_email_add_item (CardEditorEMail *email, CardEditorEditList *list)
{
	MIMEDirVCardEMail *em;
	GtkTreeIter iter;
	GtkTreeSelection *selection;

	em = mimedir_vcard_email_new ();

	mimedir_vcard_append_email (email->priv->vcard, em);

	iter = card_editor_email_internal_append (email, em);

	g_object_unref (G_OBJECT (em));

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (email->priv->treeview));
	gtk_tree_selection_select_iter (selection, &iter);

	gtk_widget_grab_focus (email->priv->address_entry);
}


static void
card_editor_email_remove_item (CardEditorEMail *email, CardEditorEditList *list)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (email->priv->treeview));

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardEMail *em;

		gtk_tree_model_get (model, &iter, 0, &em, -1);

		g_assert (em != NULL && MIMEDIR_IS_VCARD_EMAIL (em));

		g_signal_handlers_disconnect_by_func (G_OBJECT (em),
						      card_editor_email_address_changed,
						      email);

		if (gtk_list_store_remove (GTK_LIST_STORE (model), &iter)) {
			gtk_tree_selection_select_iter (selection, &iter);
		} else {
			/* Select last element */

			gint n;

			n = gtk_tree_model_iter_n_children (model, NULL);
			if (n > 0) {
				gtk_tree_model_iter_nth_child (model, &iter, NULL, n - 1);
				gtk_tree_selection_select_iter (selection, &iter);
			}
		}
		mimedir_vcard_remove_email (email->priv->vcard, em);

		gtk_widget_grab_focus (email->priv->address_entry);
	}
}


static void
card_editor_email_render_item_cb (GtkTreeViewColumn *column,
				  GtkCellRenderer *renderer,
				  GtkTreeModel *model,
				  GtkTreeIter *iter,
				  gpointer data)
{
	MIMEDirVCardEMail *email;
	gchar *address;

	gtk_tree_model_get (model, iter, 0, &email, -1);
	g_assert (email != NULL && MIMEDIR_IS_VCARD_EMAIL (email));

	g_object_get (G_OBJECT (email), "address", &address, NULL);
	g_object_set (G_OBJECT (renderer), "text", address, NULL);
	g_free (address);
}


static void
card_editor_email_entry_updated (CardEditorEMail *email, GtkEntry *entry, const gchar *prop)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (email->priv->treeview));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardEMail *email;
		const gchar *text;

		text = gtk_entry_get_text (entry);

		gtk_tree_model_get (model, &iter, 0, &email, -1);
		g_assert (email != NULL && MIMEDIR_IS_VCARD_EMAIL (email));
		g_object_set (G_OBJECT (email), prop, text, NULL);
	}
}


static void
card_editor_email_address_updated (CardEditorEMail *email, GtkEntry *entry)
{
	card_editor_email_entry_updated (email, entry, "address");
}


static void
card_editor_email_check_updated (CardEditorEMail *email, GtkToggleButton *button, const gchar *prop)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (email->priv->treeview));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardEMail *email;
		gboolean b;

		b = gtk_toggle_button_get_active (button);

		gtk_tree_model_get (model, &iter, 0, &email, -1);
		g_assert (email != NULL && MIMEDIR_IS_VCARD_EMAIL (email));
		g_object_set (G_OBJECT (email), prop, b, NULL);
	}
}


static void
card_editor_email_pref_updated (CardEditorEMail *email, GtkToggleButton *button)
{
	card_editor_email_check_updated (email, button, "preferred");
}


static void
card_editor_email_type_updated (CardEditorEMail *email, GtkToggleButton *button)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (email->priv->treeview));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardEMail *em;
		const gchar *type;

		gtk_tree_model_get (model, &iter, 0, &em, -1);
		g_assert (email != NULL && MIMEDIR_IS_VCARD_EMAIL (em));

		type = (const gchar *) g_object_get_data (G_OBJECT (button), "type");
		g_object_set (G_OBJECT (em), "type", type, NULL);
	}
}


static gboolean
card_editor_email_address_changed_fe (GtkTreeModel *model,
				     GtkTreePath  *path,
				     GtkTreeIter  *iter,
				     gpointer      data)
{
	MIMEDirVCardEMail *email;

	g_return_val_if_fail (MIMEDIR_IS_VCARD_EMAIL (data), FALSE);

	gtk_tree_model_get (model, iter, 0, &email, -1);
	g_assert (MIMEDIR_IS_VCARD_EMAIL (email));

	if (email == data)
		gtk_tree_model_row_changed (model, path, iter);

	return FALSE;
}


static void
card_editor_email_address_changed (CardEditorEMail *email, MIMEDirVCardEMail *em)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL (email->priv->model), card_editor_email_address_changed_fe, em);
}

/*
 * Internal Methods
 */

static gboolean
card_editor_email_setup (CardEditorEMail *email)
{
	CardEditorEMailPriv *priv;
	GladeXML *xml;
	GtkWidget *w, *tree;
	GtkCellRenderer *renderer;

	priv = email->priv;

	/* Internal signals */

	g_signal_connect_swapped (G_OBJECT (email), "selection-changed", G_CALLBACK (card_editor_email_update_editor), email);
	g_signal_connect_swapped (G_OBJECT (email), "add-item", G_CALLBACK (card_editor_email_add_item), email);
	g_signal_connect_swapped (G_OBJECT (email), "remove-item", G_CALLBACK (card_editor_email_remove_item), email);

	/* Tree view */

	priv->model = gtk_list_store_new (2, G_TYPE_OBJECT, G_TYPE_ULONG);
	tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (priv->model));
	priv->treeview = tree;
	gtk_tree_view_set_reorderable (GTK_TREE_VIEW (tree), TRUE);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree), FALSE);
	gtk_widget_show (tree);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (tree),
						    0, NULL, renderer,
						    card_editor_email_render_item_cb, email, NULL);

	card_editor_editlist_set_list (CARD_EDITOR_EDITLIST (email), GTK_TREE_VIEW (tree));

	/* Editor */

	xml = glade_xml_new (GLADEDIR "/card-editor.glade", "email_box", GETTEXT_PACKAGE);
	if (!xml) {
		g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/card-editor.glade");
		return FALSE;
	}

	glade_xml_signal_autoconnect (xml);

	w = glade_xml_get_widget (xml, "email_box");

	priv->address_entry  = glade_xml_get_widget (xml, "address_entry");

	priv->internet_radio = glade_xml_get_widget (xml, "internet_radio");
	priv->x400_radio     = glade_xml_get_widget (xml, "x400_radio");
	priv->pref_check     = glade_xml_get_widget (xml, "pref_check");

	if (!w || !priv->address_entry ||
	    !priv->internet_radio || !priv->x400_radio || !priv->pref_check) {
		g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
		return FALSE;
	}

	g_object_set_data (G_OBJECT (priv->internet_radio), "type", GINT_TO_POINTER (MIMEDIR_VCARD_EMAIL_TYPE_INTERNET));
	g_object_set_data (G_OBJECT (priv->x400_radio),     "type", GINT_TO_POINTER (MIMEDIR_VCARD_EMAIL_TYPE_X400));

	g_signal_connect_swapped (priv->address_entry,  "changed", G_CALLBACK (card_editor_email_address_updated),  email);

	g_signal_connect_swapped (priv->internet_radio, "toggled", G_CALLBACK (card_editor_email_type_updated),     email);
	g_signal_connect_swapped (priv->x400_radio,     "toggled", G_CALLBACK (card_editor_email_type_updated),     email);
	g_signal_connect_swapped (priv->pref_check,     "toggled", G_CALLBACK (card_editor_email_pref_updated),     email);

	card_editor_editlist_set_editor (CARD_EDITOR_EDITLIST (email), w);
	g_object_unref (G_OBJECT (xml));

	gtk_widget_grab_focus (priv->address_entry);

	return TRUE;
}


static GtkTreeIter
card_editor_email_internal_append (CardEditorEMail *email, MIMEDirVCardEMail *em)
{
	GtkTreeIter iter;

	g_signal_connect_swapped (G_OBJECT (em), "changed", G_CALLBACK (card_editor_email_address_changed), email);

	gtk_list_store_append (email->priv->model, &iter);
	gtk_list_store_set (email->priv->model, &iter, 0, em, -1);

	return iter;
}

/*
 * External Methods
 */

GtkWidget *
card_editor_email_new (void)
{
	CardEditorEMail *email;

	email = g_object_new (CARD_EDITOR_TYPE_EMAIL, NULL);

	return GTK_WIDGET (email);
}

void
card_editor_email_edit_vcard (CardEditorEMail *email, MIMEDirVCard *vcard)
{
	CardEditorEMailPriv *priv;
	GSList *list, *l;

	g_return_if_fail (email != NULL);
	g_return_if_fail (CARD_EDITOR_IS_EMAIL (email));
	g_return_if_fail (vcard != NULL);
	g_return_if_fail (MIMEDIR_IS_VCARD (vcard));

	priv = email->priv;

	if (priv->vcard)
		g_object_unref (G_OBJECT (priv->vcard));
	g_object_ref (vcard);
	priv->vcard = vcard;

	g_object_get (G_OBJECT (vcard), "email_list", &list, NULL);

	for (l = list; l != NULL; l = g_slist_next (l)) {
		MIMEDirVCardEMail *em;

		em = MIMEDIR_VCARD_EMAIL (l->data);
		card_editor_email_internal_append (email, em);
	}

	if (list) {
		GtkTreeSelection *selection;
		GtkTreePath *path;

		selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->treeview));

		path = gtk_tree_path_new_from_string ("0");
		gtk_tree_selection_select_path (selection, path);
		gtk_tree_path_free (path);
	}
}
