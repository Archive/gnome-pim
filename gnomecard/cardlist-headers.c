/* GNOME Card List Header List
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include "cardlist-headers.h"


CardListHeader cardlist_header_list[] = {
	{ CLH_NAME,		"CardName",	"name",		N_("Card Name")		},
	{ CLH_GIVENNAME,	"GivenName",	"givenname",	N_("Given Name")	},
	{ CLH_MIDDLENAME,	"MiddleName",	"middlename",	N_("Middle Name")	},
	{ CLH_FAMILYNAME,	"FamilyName",	"familyname",	N_("Family Name")	},
	{ CLH_PREFIX,		"Prefix",	"prefix",	N_("Prefix")		},
	{ CLH_SUFFIX,		"Suffix",	"suffix",	N_("Suffix")		},
	{ CLH_ORGANIZATION,	"Organization",	"organization",	N_("Organization")	},
	{ CLH_TITLE,		"Title",	"jobtitle",	N_("Job Title")		},
	{ CLH_EMAIL,		"EMail",	"email",	N_("E-Mail")		},
	{ CLH_URL,		"URL",		"url",		N_("WWW")		},
	{ CLH_PHONE,		"Phone",	"phone",	N_("Phone")		},
	{ 0,			NULL,		NULL,		NULL			}
};
