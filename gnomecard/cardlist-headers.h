#ifndef __CARDLIST_HEADERS_H__
#define __CARDLIST_HEADERS_H__

/* GNOME Card List Header List
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

typedef enum _CardListHeaderId CardListHeaderId;
enum _CardListHeaderId {
	CLH_NAME = 1,
	CLH_GIVENNAME,
	CLH_MIDDLENAME,
	CLH_FAMILYNAME,
	CLH_PREFIX,
	CLH_SUFFIX,
	CLH_ORGANIZATION,
	CLH_TITLE,
	CLH_EMAIL,
	CLH_URL,
	CLH_PHONE
};

typedef struct _CardListHeader CardListHeader;
struct _CardListHeader {
	CardListHeaderId id;
	gchar *name;
	gchar *property;
	gchar *title;
};

extern CardListHeader cardlist_header_list[];

#endif /* __CARDLIST_HEADERS_H__ */
