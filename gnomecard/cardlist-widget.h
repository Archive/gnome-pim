#ifndef __CARDLIST_WIDGET_H__
#define __CARDLIST_WIDGET_H__

/* GNOME Card List Widget Object
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <gtk/gtktreeview.h>

#include <mimedir/mimedir-vcard.h>

#include "cardlist-headers.h"


#define CARDLIST_TYPE_WIDGET		(cardlist_widget_get_type())
#define CARDLIST_WIDGET(obj)		(GTK_CHECK_CAST ((obj), CARDLIST_TYPE_WIDGET, CardListWidget))
#define CARDLIST_WIDGET_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CARDLIST_TYPE_WIDGET))
#define CARDLIST_IS_WIDGET(obj)		(GTK_CHECK_TYPE ((obj), CARDLIST_TYPE_WIDGET))
#define CARDLIST_IS_WIDGET_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CARDLIST_TYPE_WIDGET))

typedef struct _CardListWidget		CardListWidget;
typedef struct _CardListWidgetClass	CardListWidgetClass;
typedef struct _CardListWidgetPriv	CardListWidgetPriv;

struct _CardListWidget
{
	GtkTreeView parent;

	CardListWidgetPriv *priv;
};

struct _CardListWidgetClass
{
	GtkTreeViewClass parent_class;

	void (*vcard_changed) (MIMEDirVCard *vcard);
};

GType		 cardlist_widget_get_type	(void);
GtkWidget	*cardlist_widget_new		(void);

MIMEDirVCard	*cardlist_widget_get_current_card
						(CardListWidget *widget);

void		 cardlist_widget_clear		(CardListWidget *widget);
void		 cardlist_widget_set_list	(CardListWidget *widget, GList *list);
void		 cardlist_widget_append_list	(CardListWidget *widget, GList *list);
void		 cardlist_widget_append_card	(CardListWidget *widget, MIMEDirVCard *card);
void		 cardlist_widget_remove_card	(CardListWidget *widget, MIMEDirVCard *card);

gboolean	 cardlist_widget_open_file	(CardListWidget *widget, const gchar *filename, GError **error);
gboolean	 cardlist_widget_append_file	(CardListWidget *widget, const gchar *filename, GError **error);
gboolean	 cardlist_widget_save		(CardListWidget *widget, GError **error);
gboolean	 cardlist_widget_save_file	(CardListWidget *widget, const gchar *filename, GError **error);

const gchar	*cardlist_widget_get_filename	(CardListWidget *widget);

void		 cardlist_widget_sort		(CardListWidget *widget, CardListHeaderId id);
void		 cardlist_widget_find		(CardListWidget *widget, const gchar *string, gboolean case_sensitive, gboolean backwards);

#endif /* __CARDLIST_WIDGET_H__ */
