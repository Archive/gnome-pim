#ifndef __GNOMECARD_PREFS_H__
#define __GNOMECARD_PREFS_H__

/* GNOME Card Preferences Dialog Object
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <gtk/gtkdialog.h>

#define GNCARD_TYPE_PREFS		(gncard_prefs_get_type())
#define GNCARD_PREFS(obj)		(GTK_CHECK_CAST ((obj), GNCARD_TYPE_PREFS, GncardPrefs))
#define GNCARD_PREFS_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNCARD_TYPE_PREFS))
#define GNCARD_IS_PREFS(obj)		(GTK_CHECK_TYPE ((obj), GNCARD_TYPE_PREFS))
#define GNCARD_IS_PREFS_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GNCARD_TYPE_PREFS))

typedef struct _GncardPrefs		GncardPrefs;
typedef struct _GncardPrefsClass	GncardPrefsClass;
typedef struct _GncardPrefsPriv		GncardPrefsPriv;

struct _GncardPrefs
{
	GtkDialog parent;

	GncardPrefsPriv *priv;
};

struct _GncardPrefsClass
{
	GtkDialogClass parent_class;
};

GType		 gncard_prefs_get_type	(void);
GtkWidget	*gncard_prefs_new	(GtkWindow *parent);

#endif /* __GNOMECARD_PREFS_H__ */
