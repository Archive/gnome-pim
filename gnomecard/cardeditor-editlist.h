#ifndef __CARDEDITOR_EDITLIST_H__
#define __CARDEDITOR_EDITLIST_H__

/* GNOME Card Editor Edit List Object
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <gtk/gtkbox.h>
#include <gtk/gtkwidget.h>


#define CARD_EDITOR_TYPE_EDITLIST		(card_editor_editlist_get_type())
#define CARD_EDITOR_EDITLIST(obj)		(GTK_CHECK_CAST ((obj), CARD_EDITOR_TYPE_EDITLIST, CardEditorEditList))
#define CARD_EDITOR_EDITLIST_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CARD_EDITOR_TYPE_EDITLIST))
#define CARD_EDITOR_IS_EDITLIST(obj)		(GTK_CHECK_TYPE ((obj), CARD_EDITOR_TYPE_EDITLIST))
#define CARD_EDITOR_IS_EDITLIST_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CARD_EDITOR_TYPE_EDITLIST))

typedef struct _CardEditorEditList	CardEditorEditList;
typedef struct _CardEditorEditListClass	CardEditorEditListClass;
typedef struct _CardEditorEditListPriv	CardEditorEditListPriv;

struct _CardEditorEditList
{
	GtkHBox parent;

	CardEditorEditListPriv *priv;
};

struct _CardEditorEditListClass
{
	GtkHBoxClass parent_class;

	void (* selection_changed)	(CardEditorEditList *list);
	void (* add_item)		(CardEditorEditList *list);
	void (* remove_item)		(CardEditorEditList *list);
};


GType	card_editor_editlist_get_type	(void);

void	card_editor_editlist_set_editor	(CardEditorEditList *list, GtkWidget *editor);
void	card_editor_editlist_set_list	(CardEditorEditList *list, GtkTreeView *view);

#endif /* __CARD_EDITOR_EDITLIST_H__ */
