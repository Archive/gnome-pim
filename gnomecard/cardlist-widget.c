/* Card List Widget Object
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include <gnome.h>

#include <mimedir/mimedir-vcard.h>

#include "cardlist-headers.h"
#include "cardlist-widget.h"
#include "gnomecard-config.h"


/* FIXME: drag & drop, save sort type in session */


enum {
	SIGNAL_VCARD_CHANGED,
	SIGNAL_LAST
};

struct _CardListWidgetPriv {
	gchar *filename;

	guint notify;
};


static GtkTreeViewClass *parent_class = NULL;

static gint cardlist_widget_signals[SIGNAL_LAST] = { 0 };


static void cardlist_widget_class_init		(CardListWidgetClass *klass);
static void cardlist_widget_init		(CardListWidget *widget);
static void cardlist_widget_dispose		(GObject *object);

static void cardlist_widget_setup		(CardListWidget *widget);

static void cardlist_widget_card_changed	(MIMEDirVCard *card, gpointer data);

/*
 * Class and Object Management
 */

GType
cardlist_widget_get_type (void)
{
	static GType cardlist_widget_type = 0;

	if (!cardlist_widget_type) {
		static const GTypeInfo cardlist_widget_info = {
			sizeof (CardListWidgetClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) cardlist_widget_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CardListWidget),
			1,    /* n_preallocs */
			(GInstanceInitFunc) cardlist_widget_init,
		};

		cardlist_widget_type = g_type_register_static (GTK_TYPE_TREE_VIEW,
							       "CardListWidget",
							       &cardlist_widget_info,
							       0);
	}

	return cardlist_widget_type;
}

static void
cardlist_widget_class_init (CardListWidgetClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = cardlist_widget_dispose;

	parent_class = g_type_class_peek_parent (klass);

	/* Signals */

	cardlist_widget_signals[SIGNAL_VCARD_CHANGED] =
		g_signal_new ("vcard-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CardListWidgetClass, vcard_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, MIMEDIR_TYPE_VCARD);
}

static void
cardlist_widget_init (CardListWidget *widget)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET (widget));

	widget->priv = g_new0 (CardListWidgetPriv, 1);

	cardlist_widget_setup (widget);
}

static void
cardlist_widget_dispose (GObject *object)
{
	CardListWidget *widget;
	GtkTreeModel *model;
	GtkTreeIter iter;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET (object));

	widget = CARDLIST_WIDGET (object);

	/* Disconnect from vCard "changed" signals */

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (object));
	if (model && gtk_tree_model_get_iter_first (model, &iter)) {
		do {
			GObject *object;

			gtk_tree_model_get (model, &iter, 0, &object, -1);
			g_signal_handlers_disconnect_by_func (object,
							      cardlist_widget_card_changed,
							      object);
		} while (gtk_tree_model_iter_next (model, &iter));
	}

	if (widget->priv) {
		g_free (widget->priv->filename);

		/* Remove GConf notification */

		if (widget->priv->notify) {
			GConfClient *gconf;

			gconf = gconf_client_get_default ();

			gconf_client_notify_remove (gconf, widget->priv->notify);
			widget->priv->notify = 0;

			g_object_unref (G_OBJECT (gconf));
		}

		/* Free the private structure */

		g_free (widget->priv);
		widget->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Utility Functions
 */

static gboolean
_gtk_tree_model_iter_prev (GtkTreeModel *model, GtkTreeIter *iter)
{
	GtkTreePath *path;

	path = gtk_tree_model_get_path (model, iter);
	if (!gtk_tree_path_prev (path)) {
		gtk_tree_path_free (path);
		return FALSE;
	}

	return gtk_tree_model_get_iter (model, iter, path);
}

static gboolean
_gtk_tree_model_get_iter_last (GtkTreeModel *model, GtkTreeIter *iter)
{
	GtkTreeIter parent;
	gint n;
	gboolean has_parent;

	has_parent = gtk_tree_model_iter_parent (model, &parent, iter);
	n = gtk_tree_model_iter_n_children (model, has_parent ? &parent : NULL);
	if (n == 0)
		return FALSE;
	return gtk_tree_model_iter_nth_child (model, iter, NULL, n - 1);
}

/*
 * Internal Methods
 */

static void
cardlist_widget_hide_all_columns (CardListWidget *widget)
{
	CardListHeader *header;
	gint i;

	for (i = 0, header = cardlist_header_list; header->name != NULL; i++, header++) {
		GtkTreeViewColumn *column;
		column = gtk_tree_view_get_column (GTK_TREE_VIEW (widget), i);
		gtk_tree_view_column_set_visible (column, FALSE);
	}
}

static void
cardlist_widget_show_column_by_name (CardListWidget *widget, const gchar *col)
{
	GtkTreeViewColumn *tree_column;
	CardListHeader *header;
	gint i, column = -1;

	for (i = 0, header = cardlist_header_list; header->name != NULL; i++, header++) {
		if (!g_ascii_strcasecmp (col, header->name)) {
			column = i;
			break;
		}
	}

	if (column < 0) {
		g_warning (_("'%s' is not a valid column name"), col);
		return;
	}

	tree_column = gtk_tree_view_get_column (GTK_TREE_VIEW (widget), column);
	gtk_tree_view_column_set_visible (tree_column, TRUE);
}

static void
cardlist_widget_show_columns (CardListWidget *widget, GSList *list)
{
	GSList *item;

	/* Hide all columns */

	cardlist_widget_hide_all_columns (widget);

	/* Show selected columns */

	if (list) {
		for (item = list; item != NULL; item = g_slist_next (item))
			cardlist_widget_show_column_by_name (widget, (const gchar *) item->data);
	} else
		cardlist_widget_show_column_by_name (widget, "CardName");
}


static GtkTreeIter
cardlist_widget_internal_append (CardListWidget *widget, MIMEDirVCard *vcard)
{
	GtkTreeModel *model;
	GtkTreeIter   iter;
	gulong signal;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));

	signal = g_signal_connect (G_OBJECT (vcard), "changed", G_CALLBACK (cardlist_widget_card_changed), widget);

	gtk_list_store_append (GTK_LIST_STORE (model), &iter);
	gtk_list_store_set (GTK_LIST_STORE (model), &iter, 0, vcard, -1);

	return iter;
}


static void
cardlist_widget_internal_remove (CardListWidget *widget, MIMEDirVCard *vcard)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));

	if (gtk_tree_model_get_iter_first (model, &iter)) {
		do {
			MIMEDirVCard *vc;

			gtk_tree_model_get (model, &iter, 0, &vc, -1);
			g_assert (vc != NULL && MIMEDIR_IS_VCARD (vc));

			if (vcard == vc) {
				g_signal_handlers_disconnect_by_func (G_OBJECT (widget), G_CALLBACK (cardlist_widget_card_changed), widget);

				gtk_list_store_remove (GTK_LIST_STORE (model), &iter);

				return;
			}

		} while (gtk_tree_model_iter_next (model, &iter));
	}
}

/*
 * Utility Functions
 */

static gboolean
utf8_find (const gchar *s, const gunichar *f, glong fsize, gboolean _case)
{
	gchar *tmps1 = NULL, *tmps2;
	gunichar *us;
	glong i, sz;
	gboolean found = FALSE;

	if (!_case) {
		tmps1 = g_utf8_casefold (s, -1);
		tmps2 = g_utf8_normalize (tmps1, -1, G_NORMALIZE_ALL);
	} else
		tmps2 = g_utf8_normalize (s, -1, G_NORMALIZE_ALL);
	us = g_utf8_to_ucs4_fast (tmps2, -1, &sz);
	g_free (tmps1);
	g_free (tmps2);

	if (sz < fsize) {
		g_free (us);
		return FALSE;
	}

	for (i = 0; i <= sz - fsize; i++) {
		glong j;

		found = TRUE;
		for (j = 0; j < fsize; j++) {
			if (us[i + j] != f[j]) {
				found = FALSE;
				break;
			}
		}

		if (found)
			break;
	}

	g_free (us);

	return found;
}

/*
 * Callbacks
 */

static void
cardlist_widget_render (GtkTreeViewColumn *tree_column,
			GtkCellRenderer   *renderer,
			GtkTreeModel      *model,
			GtkTreeIter       *iter,
			gpointer           data)
{
	MIMEDirVCard *card;
	const gchar *property;
	gchar *string;

	g_return_if_fail (data != NULL);

	property = (const gchar *) data;

	gtk_tree_model_get (model, iter, 0, &card, -1);

	g_object_get (G_OBJECT (card), property, &string, NULL);
	g_object_set (G_OBJECT (renderer), "text", string ? string : "", NULL);
	g_free (string);
}

static void
cardlist_widget_config_changed (GConfClient *gconf,
				guint        cnxn_id,
				GConfEntry  *entry,
				gpointer     data)
{
	CardListWidget *widget;
        const gchar *key;

        g_return_if_fail (entry != NULL);
        g_return_if_fail (data != NULL);
        g_return_if_fail (CARDLIST_IS_WIDGET (data));

	widget = CARDLIST_WIDGET (data);

        key = gconf_entry_get_key (entry);

	if (!strcmp (key, CONFIG_UI_COLUMN_HEADERS)) {
		GConfValue *value = gconf_entry_get_value (entry);

		if (value->type == GCONF_VALUE_LIST && gconf_value_get_list_type (value) == GCONF_VALUE_STRING) {
			GSList *list, *new_list = NULL;

			/* Show selected columns */

			list = gconf_value_get_list (value);
			for (; list != NULL; list = g_slist_next (list)) {
				const gchar *s;
				s = gconf_value_get_string ((GConfValue *) list->data);
				new_list = g_slist_append (new_list, (gpointer) s);
			}
			cardlist_widget_show_columns (widget, new_list);
			g_slist_free (new_list);
		}
	}
}


static gboolean
cardlist_widget_card_changed_fe (GtkTreeModel *model,
				 GtkTreePath  *path,
				 GtkTreeIter  *iter,
				 gpointer      data)
{
	MIMEDirVCard *card, *curitem;

	g_return_val_if_fail (data != NULL, TRUE);
	g_return_val_if_fail (MIMEDIR_IS_VCARD (data), TRUE);

	card = MIMEDIR_VCARD (data);

        gtk_tree_model_get (model, iter, 0, &curitem, -1);

        g_assert (curitem != NULL);
        g_assert (MIMEDIR_IS_VCARD (curitem));

        if (card == curitem) {
                gtk_tree_model_row_changed (model, path, iter);

                return TRUE;
        }

        return FALSE;
}


static void
cardlist_widget_card_changed (MIMEDirVCard *vcard, gpointer data)
{
	CardListWidget *widget;
	GtkTreeModel *model;

	g_return_if_fail (vcard != NULL);
	g_return_if_fail (MIMEDIR_IS_VCARD (vcard));
	g_return_if_fail (data != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET (data));

	widget = CARDLIST_WIDGET (data);
        model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));

	gtk_tree_model_foreach (model, cardlist_widget_card_changed_fe, vcard);

	g_signal_emit (G_OBJECT (widget), cardlist_widget_signals[SIGNAL_VCARD_CHANGED], 0, vcard);
}

/* Sorting callbacks */

static gint
cardlist_widget_sort_string (GtkTreeModel *model, GtkTreeIter *iter1, GtkTreeIter *iter2, gpointer data)
{
	gint res;
	MIMEDirVCard *card1, *card2;
	gchar *s1, *s2, *c1, *c2;

	gtk_tree_model_get (model, iter1, 0, &card1, -1);
	gtk_tree_model_get (model, iter2, 0, &card2, -1);

	g_object_get (G_OBJECT (card1), (const gchar *) data, &s1, NULL);
	g_object_get (G_OBJECT (card2), (const gchar *) data, &s2, NULL);

	if (!s1 && !s2)
		return 0;
	else if (!s1 && s2) {
		g_free (s2);
		return 1;
	} else if (s1 && !s2) {
		g_free (s1);
		return -1;
	}

	c1 = g_utf8_casefold (s1, -1);
	c2 = g_utf8_casefold (s2, -1);

	res = g_utf8_collate (c1, c2);

	g_free (s1);
	g_free (s2);
	g_free (c1);
	g_free (c2);

	return res;
}

/*
 * Setup
 */

static void
cardlist_widget_setup_gconf (CardListWidget *widget)
{
	CardListWidgetPriv *priv = widget->priv;
	GConfClient *gconf;
	GSList *list, *tmp;

	cardlist_widget_hide_all_columns (widget);

	gconf = gconf_client_get_default ();

	gconf_client_add_dir (gconf, CONFIG_UI_DIR, GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);

	list = gconf_client_get_list (gconf, CONFIG_UI_COLUMN_HEADERS,
				      GCONF_VALUE_STRING, NULL);
	cardlist_widget_show_columns (widget, list);
	for (tmp = list; tmp != NULL; tmp = g_slist_next (tmp))
		g_free (tmp->data);
	g_slist_free (list);

	/* Notification */

	priv->notify = gconf_client_notify_add (gconf, CONFIG_UI_COLUMN_HEADERS,
						cardlist_widget_config_changed, widget,
						NULL, NULL);

	g_object_unref (G_OBJECT (gconf));
}

static void
cardlist_widget_setup (CardListWidget *widget)
{
	CardListWidgetPriv *priv;
	GtkListStore *store;
	GtkCellRenderer *renderer;
	CardListHeader *header;
	gint i;

	priv = widget->priv;

	/* Tree model */

	store = gtk_list_store_new (1, G_TYPE_OBJECT);

	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_NAME,
					 cardlist_widget_sort_string,
					 "name", NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_GIVENNAME,
					 cardlist_widget_sort_string,
					 "givenname", NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_MIDDLENAME,
					 cardlist_widget_sort_string,
					 "middlename", NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_FAMILYNAME,
					 cardlist_widget_sort_string,
					 "familyname", NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_PREFIX,
					 cardlist_widget_sort_string,
					 "prefix", NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_SUFFIX,
					 cardlist_widget_sort_string,
					 "suffix", NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_ORGANIZATION,
					 cardlist_widget_sort_string,
					 "organization", NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_TITLE,
					 cardlist_widget_sort_string,
					 "title", NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_EMAIL,
					 cardlist_widget_sort_string,
					 "email", NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_URL,
					 cardlist_widget_sort_string,
					 "url", NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 CLH_PHONE,
					 cardlist_widget_sort_string,
					 "phone", NULL);

	gtk_tree_view_set_model (GTK_TREE_VIEW (widget), GTK_TREE_MODEL (store));

	/* Tree columns */

	renderer = gtk_cell_renderer_text_new ();

	for (i = 0, header = cardlist_header_list; header->name != NULL; i++, header++) {
		GtkTreeViewColumn *column;

		column = gtk_tree_view_column_new_with_attributes (_(header->title), renderer, NULL);
		gtk_tree_view_column_set_cell_data_func (column,
							 renderer,
							 cardlist_widget_render,
							 header->property, NULL);
		gtk_tree_view_column_set_sort_column_id (column, header->id);
		gtk_tree_view_insert_column (GTK_TREE_VIEW (widget), column, i);
	}

	/* GConf */

	cardlist_widget_setup_gconf (widget);
}

static gboolean
cardlist_widget_match_card (MIMEDirVCard *card, const gchar *string, gboolean _case)
{
	gint i;
	gchar *s[12], *tmps1 = NULL, *tmps2;
	gunichar *us;
	glong sz;
	gboolean found = FALSE;

	g_object_get (G_OBJECT (card),
		      "name",         &s[0],
		      "familyname",   &s[1],
		      "givenname",    &s[2],
		      "middlename",   &s[3],
		      "prefix",       &s[4],
		      "suffix",       &s[5],
		      "jobtitle",     &s[6],
		      "role",         &s[7],
		      "categories",   &s[8],
		      "note",         &s[9],
		      "url",          &s[10],
		      "organization", &s[11],
		      NULL);

	if (!_case) {
		tmps1 = g_utf8_casefold (string, -1);
		tmps2 = g_utf8_normalize (tmps1, -1, G_NORMALIZE_ALL);
	} else
		tmps2 = g_utf8_normalize (string, -1, G_NORMALIZE_ALL);
	us = g_utf8_to_ucs4_fast (tmps2, -1, &sz);
	g_free (tmps1);
	g_free (tmps2);

	for (i = 0; i < G_N_ELEMENTS (s); i++) {
		if (s[i]) {
			if (utf8_find (s[i], us, sz, _case)) {
				found = TRUE;
				break;
			}
		}
	}

	g_free (us);

	for (i = 0; i < G_N_ELEMENTS (s); i++)
		g_free (s[i]);

	return found;
}

/*
 * Public Methods
 */

GtkWidget *
cardlist_widget_new (void)
{
	CardListWidget *widget;

	widget = g_object_new (CARDLIST_TYPE_WIDGET, NULL);

	return GTK_WIDGET (widget);
}


MIMEDirVCard *
cardlist_widget_get_current_card (CardListWidget *widget)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;
	MIMEDirVCard *vcard;

	g_return_val_if_fail (widget != NULL, NULL);
	g_return_val_if_fail (CARDLIST_IS_WIDGET (widget), NULL);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (widget));
	if (!gtk_tree_selection_get_selected (selection, &model, &iter))
		return NULL;

	gtk_tree_model_get (model, &iter, 0, &vcard, -1);
	g_assert (vcard != NULL && MIMEDIR_IS_VCARD (vcard));

	return vcard;
}


void
cardlist_widget_clear (CardListWidget *widget)
{
	GtkTreeModel *model;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET (widget));

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));

	gtk_list_store_clear (GTK_LIST_STORE (model));
}


void
cardlist_widget_set_list (CardListWidget *widget, GList *list)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET (widget));

	cardlist_widget_clear (widget);
	cardlist_widget_append_list (widget, list);
}


void
cardlist_widget_append_list (CardListWidget *widget, GList *list)
{
	GtkTreeModel *model;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET (widget));

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));

	for (; list != NULL; list = g_list_next (list)) {
		if (MIMEDIR_IS_VCARD (list->data))
			cardlist_widget_internal_append (widget, MIMEDIR_VCARD (list->data));
		else
			g_critical ("List item is not of type MIMEDirVCard");
	}
}


void
cardlist_widget_append_card (CardListWidget *widget, MIMEDirVCard *card)
{
	GtkTreeModel     *model;
	GtkTreePath      *path;
	GtkTreeIter       iter;
	GtkTreeSelection *selection;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET (widget));
	g_return_if_fail (card != NULL);
	g_return_if_fail (MIMEDIR_IS_VCARD (card));

	model     = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (widget));

	iter = cardlist_widget_internal_append (widget, card);

	path = gtk_tree_model_get_path (model, &iter);
	gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (widget),
				      path, NULL,
				      TRUE, 0.5, 0.0);

	gtk_tree_selection_select_path (selection, path);
	gtk_tree_path_free (path);
}


void
cardlist_widget_remove_card (CardListWidget *widget, MIMEDirVCard *card)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET (widget));
	g_return_if_fail (card != NULL);
	g_return_if_fail (MIMEDIR_IS_VCARD (card));

	cardlist_widget_internal_remove (widget, card);
}


gboolean
cardlist_widget_open_file (CardListWidget *widget, const gchar *filename, GError **error)
{
	GtkTreeSelection *selection;
	GtkTreePath *path;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (CARDLIST_IS_WIDGET (widget), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);

	/* Load file */

	cardlist_widget_clear (widget);

	if (!cardlist_widget_append_file (widget, filename, error))
		return FALSE;

	/* Select first item */

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (widget));
	path = gtk_tree_path_new_from_string ("0");
	gtk_tree_selection_select_path (selection, path);
	gtk_tree_path_free (path);

	/* Store file name */

	g_free (widget->priv->filename);
	widget->priv->filename = g_strdup (filename);

	return TRUE;
}


gboolean
cardlist_widget_append_file (CardListWidget *widget, const gchar *filename, GError **error)
{
	GError *err = NULL;
	GtkTreeModel *model;
	GList *list, *current;
        
	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (CARDLIST_IS_WIDGET (widget), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);

	list = mimedir_vcard_read_file (filename, &err);
	if (err) {
		g_propagate_error (error, err);
		return FALSE;
	}

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));

	for (current = list; current != NULL; current = g_list_next (current)) {
		g_assert (current->data != NULL && MIMEDIR_IS_VCARD (current->data));
		cardlist_widget_internal_append (widget, MIMEDIR_VCARD (current->data));
	}

	mimedir_vcard_free_list (list);

	return TRUE;
}


gboolean
cardlist_widget_save (CardListWidget *widget, GError **error)
{
	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (CARDLIST_IS_WIDGET (widget), FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
	g_return_val_if_fail (widget->priv->filename != NULL, FALSE);

	return cardlist_widget_save_file (widget, widget->priv->filename, error);
}


gboolean
cardlist_widget_save_file (CardListWidget *widget, const gchar *filename, GError **error)
{
	CardListWidgetPriv *priv;
	GError *err = NULL;
	GList *list = NULL;
	GtkTreeIter iter;
	GtkTreeModel *model;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (CARDLIST_IS_WIDGET (widget), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	priv = widget->priv;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));

	if (gtk_tree_model_get_iter_first (model, &iter)) {
		do {
			MIMEDirVCard *vcard;

			gtk_tree_model_get (model, &iter, 0, &vcard, -1);
			g_assert (vcard != NULL && MIMEDIR_IS_VCARD (vcard));

			list = g_list_append (list, vcard);
		} while (gtk_tree_model_iter_next (model, &iter));
	}

	if (!mimedir_vcard_write_list (filename, list, &err)) {
		g_propagate_error (error, err);
		return FALSE;
	}

	if (priv->filename != filename) {
		g_free (priv->filename);
		priv->filename = g_strdup (filename);
	}

	return TRUE;
}

const gchar *
cardlist_widget_get_filename (CardListWidget *widget)
{
	g_return_val_if_fail (widget != NULL, NULL);
	g_return_val_if_fail (CARDLIST_IS_WIDGET (widget), NULL);

	return widget->priv->filename;
}

void
cardlist_widget_sort (CardListWidget *widget, CardListHeaderId id)
{
	GtkTreeModel *model;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET (widget));
	g_return_if_fail (id >= CLH_NAME && id <= CLH_PHONE);

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (widget));
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model), id, GTK_SORT_ASCENDING);
}

void
cardlist_widget_find (CardListWidget *widget, const gchar *string, gboolean case_sensitive, gboolean backwards)
{
	GtkTreeModel *model;
	GtkTreeSelection *selection;
	GtkTreeIter iter;
	GtkTreePath *start_path;
	gboolean wrapped = FALSE, stop = FALSE;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARDLIST_IS_WIDGET (widget));
	g_return_if_fail (string != NULL);

	/* Get the first row to search */

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (widget));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		/* Get the currently selected row +- 1 */

		start_path = gtk_tree_model_get_path (model, &iter);

		if (!backwards) {
			if (!gtk_tree_model_iter_next (model, &iter)) {
				if (!gtk_tree_model_get_iter_first (model, &iter)) {
					gtk_tree_path_free (start_path);
					return;
				}
			}
		} else {
			if (!_gtk_tree_model_iter_prev (model, &iter)) {
				if (!_gtk_tree_model_get_iter_last (model, &iter)) {
					gtk_tree_path_free (start_path);
					return;
				}
			}
		}
	} else {
		/* Get the first/last row */

		if (!backwards) {
			if (!gtk_tree_model_get_iter_first (model, &iter))
				return;
		} else {
			if (!_gtk_tree_model_get_iter_last (model, &iter))
				return;
		}

		start_path = gtk_tree_model_get_path (model, &iter);
	}

	do {
		gboolean do_next;

		do {
			MIMEDirVCard *card;

			if (wrapped) {
				GtkTreePath *path;
				gint cmp;

				path = gtk_tree_model_get_path (model, &iter);

				cmp = gtk_tree_path_compare (start_path, path);
				gtk_tree_path_free (path);
				if (cmp == 0) {
					GtkWidget *dialog;

					dialog = gtk_message_dialog_new (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (widget))),
									 0,
									 GTK_MESSAGE_INFO,
									 GTK_BUTTONS_OK,
									 _("No matching card found."));
					g_signal_connect (G_OBJECT (dialog), "response",
							  G_CALLBACK (gtk_widget_destroy), NULL);
					gtk_widget_show (dialog);

					stop = TRUE;
					break;
				}
			}

			gtk_tree_model_get (model, &iter, 0, &card, -1);

			if (cardlist_widget_match_card (card, string, case_sensitive)) {
				gtk_tree_selection_select_iter (selection, &iter);
				stop = TRUE;
				break;
			}

			if (!backwards)
				do_next = gtk_tree_model_iter_next (model, &iter);
			else
				do_next = _gtk_tree_model_iter_prev (model, &iter);
		} while (do_next);

		if (!wrapped && !stop) {
			GtkWidget *dialog;
			const gchar *msg;
			gint response;

			if (!backwards)
				msg = _("The end of the card list was reached, but the card was not found. Continue search from the beginning?");
			else
				msg = _("The start of the card list was reached, but the card was not found. Continue search from the end?");

			dialog = gtk_message_dialog_new (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (widget))),
							 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
							 GTK_MESSAGE_QUESTION,
							 GTK_BUTTONS_NONE,
							 msg);
			gtk_dialog_add_buttons (GTK_DIALOG (dialog),
						GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
						_("Continue"),    GTK_RESPONSE_OK,
						NULL);
			gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

			response = gtk_dialog_run (GTK_DIALOG (dialog));

			switch (response) {
			case GTK_RESPONSE_CANCEL:
				gtk_widget_destroy (dialog);
				// fall through
			case GTK_RESPONSE_DELETE_EVENT:
				stop = TRUE;
				break;
			case GTK_RESPONSE_OK:
				gtk_widget_destroy (dialog);
				if (!backwards)
					gtk_tree_model_get_iter_first (model, &iter);
				else
					_gtk_tree_model_get_iter_last (model, &iter);
				break;
			default:
				g_return_if_reached ();
			}

			wrapped = TRUE;
		} else
			stop = TRUE;
	} while (!stop);

	gtk_tree_path_free (start_path);
}
