#ifndef __CARD_EDITOR_ADDRESSES_H__
#define __CARD_EDITOR_ADDRESSES_H__

/* GNOME Card Editor Addresses Object
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <mimedir/mimedir-vcard.h>

#include "cardeditor-editlist.h"


#define CARD_EDITOR_TYPE_ADDRESSES		(card_editor_addresses_get_type())
#define CARD_EDITOR_ADDRESSES(obj)		(GTK_CHECK_CAST ((obj), CARD_EDITOR_TYPE_ADDRESSES, CardEditorAddresses))
#define CARD_EDITOR_ADDRESSES_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CARD_EDITOR_TYPE_ADDRESSES))
#define CARD_EDITOR_IS_ADDRESSES(obj)		(GTK_CHECK_TYPE ((obj), CARD_EDITOR_TYPE_ADDRESSES))
#define CARD_EDITOR_IS_ADDRESSES_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CARD_EDITOR_TYPE_ADDRESSES))

typedef struct _CardEditorAddresses		CardEditorAddresses;
typedef struct _CardEditorAddressesClass	CardEditorAddressesClass;
typedef struct _CardEditorAddressesPriv		CardEditorAddressesPriv;

struct _CardEditorAddresses
{
	CardEditorEditList parent;

	CardEditorAddressesPriv *priv;
};

struct _CardEditorAddressesClass
{
	CardEditorEditListClass parent_class;
};


GType		 card_editor_addresses_get_type		(void);
GtkWidget	*card_editor_addresses_new		(void);

void		 card_editor_addresses_edit_vcard	(CardEditorAddresses *addresses, MIMEDirVCard *vcard);

#endif /* __CARD_EDITOR_ADDRESSES_H__ */
