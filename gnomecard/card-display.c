/* Card Display Object
 * Copyright (C) 2003, 2004  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * Based on the original GnomeCard code, which is:
 *
 * Copyright (C) 1999 The Free Software Foundation
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Changed in 2002 and 2003 by Sebastian Rittau <srittau@jroger.in-berlin.de>
 * for the GNOME 2 port.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <gnome.h>

#include <mimedir/mimedir-vcard.h>

#include "card-display.h"


#define CANVAS_WIDTH		225
#define CANVAS_HEIGHT		350

#define COLOR_HEADER_BOX	"khaki"
#define COLOR_LABEL_BOX		"dark khaki"

#define COLOR_HEADER_TEXT	"black"
#define COLOR_LABEL_TEXT	"black"
#define COLOR_BODY_TEXT		"black"

#define FONT_CARDNAME		"Sans 18"
#define FONT_TITLE		"Monospace 14"
#define FONT_COMMENT		"Monospace 10"
#define FONT_CANVAS		"Monospace 12"
#define FONT_ADDR		"Monospace 10"


struct _CardDisplayPriv {
	MIMEDirVCard *card;

	GnomeCanvasItem *background_box;
	GnomeCanvasItem *cardname, *cardname_box;
	GnomeCanvasItem *body;
};


static GnomeCanvasClass *parent_class = NULL;


static void card_display_class_init	(CardDisplayClass *klass);
static void card_display_init		(CardDisplay *display);
static void card_display_dispose	(GObject *object);

static void card_display_setup		(CardDisplay *display);

/*
 * Class and Object Management
 */

GType
card_display_get_type (void)
{
	static GType card_display_type = 0;

	if (!card_display_type) {
		static const GTypeInfo card_display_info = {
			sizeof (CardDisplayClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) card_display_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CardDisplay),
			0,    /* n_preallocs */
			(GInstanceInitFunc) card_display_init,
		};

		card_display_type = g_type_register_static (GNOME_TYPE_CANVAS,
							    "CardDisplay",
							    &card_display_info,
							    0);
	}

	return card_display_type;
}

static void
card_display_class_init (CardDisplayClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CARD_IS_DISPLAY_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = card_display_dispose;

	parent_class = g_type_class_peek_parent (klass);
}

static void
card_display_init (CardDisplay *display)
{
	g_return_if_fail (display != NULL);
	g_return_if_fail (CARD_IS_DISPLAY (display));

	display->priv = g_new0 (CardDisplayPriv, 1);

	card_display_setup (display);
}

static void
card_display_dispose (GObject *object)
{
	CardDisplay *display;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CARD_IS_DISPLAY (object));

	display = CARD_DISPLAY (object);

	if (display->priv) {
		if (display->priv->card)
			g_object_unref (G_OBJECT (display->priv->card));

		g_free (display->priv);
		display->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Callbacks
 */

enum {
	TARGET_VCARD,
	TARGET_EMAIL,
	TARGET_TEXT,
};

static GtkTargetEntry
card_display_drag_types [] = {
	{ "text/x-vcard",          0, TARGET_VCARD },
	{ "x-application/x-email", 0, TARGET_EMAIL },
	{ "text/plain",            0, TARGET_TEXT },
};

static void
card_display_drag_data_get (GtkWidget        *widget,
			    GdkDragContext   *context,
			    GtkSelectionData *selection_data,
			    guint            info,
			    guint32          time)
{
	CardDisplay *display;
	MIMEDirVCard *card;
	gchar *data = NULL;

	display = CARD_DISPLAY (widget);

	if (!display->priv->card)
		return;

	card = display->priv->card;

	switch (info) {
	case TARGET_TEXT: 
		data = mimedir_vcard_get_as_string (card);
		break;
                
	case TARGET_EMAIL: {
		gchar *name, *email;

		g_object_get (G_OBJECT (card),
			      "name",  &name,
			      "email", &email,
			      NULL);

		if (email && *email) {
			if (name && *name)
				data = g_strdup_printf ("%s <%s>", name, email);
			else
				data = g_strdup_printf("<%s>", email);
		}

		g_free (name);
		g_free (email);

		break;
	}
                
	case TARGET_VCARD:
		data = mimedir_vcard_write_to_string (card);
		break;
	}

	if (!data)
		return;
        
	gtk_selection_data_set (selection_data, selection_data->target, 8,
				data, strlen (data) + 1);

	g_free (data);
}

/*
 * Internal Methods
 */

static void
card_display_setup (CardDisplay *display)
{
	CardDisplayPriv *priv = display->priv;
	GnomeCanvasGroup *root;
	GtkObject *adj;

	root = gnome_canvas_root (GNOME_CANVAS (display));

	gtk_widget_set_size_request (GTK_WIDGET (display), CANVAS_WIDTH, CANVAS_HEIGHT);
	adj = gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	gtk_layout_set_hadjustment (GTK_LAYOUT (display), GTK_ADJUSTMENT (adj));
	adj = gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	gtk_layout_set_vadjustment (GTK_LAYOUT (display), GTK_ADJUSTMENT (adj));

	/* Drag & Drop */

        gtk_drag_source_set (GTK_WIDGET (display), GDK_BUTTON1_MASK,
			     card_display_drag_types, 3,
			     GDK_ACTION_COPY | GDK_ACTION_DEFAULT);
        g_signal_connect (G_OBJECT (display), "drag_data_get",
			  G_CALLBACK (card_display_drag_data_get), NULL);

	/* Background */

	priv->background_box =
		gnome_canvas_item_new (root, GNOME_TYPE_CANVAS_RECT,
				       "x1", 0.0,
				       "y1", 0.0,
				       "x2", CANVAS_WIDTH - 1.0,
				       "y2", CANVAS_HEIGHT - 1.0,
				       "fill_color",    "white",
				       "outline_color", "black",
				       "width_pixels", 1,
				       NULL);

	/* Card name */

	priv->cardname_box = gnome_canvas_item_new (root, GNOME_TYPE_CANVAS_RECT,
						    "x1", 5.0,
						    "y1", 5.0,
						    "x2", CANVAS_WIDTH - 10.0,
						    "y2", 35.0,
						    "fill_color",    COLOR_HEADER_BOX,
						    "outline_color", COLOR_HEADER_BOX,
						    "width_pixels", 0,
						    NULL);

	priv->cardname = gnome_canvas_item_new (root, GNOME_TYPE_CANVAS_TEXT,
						"text", "",
						"x", 6.0,
						"y", 12.0,
						"font", FONT_CARDNAME,
						"anchor", GTK_ANCHOR_NORTH_WEST,
						"fill_color", COLOR_HEADER_TEXT,
						NULL);

	/* Card body */

	priv->body = gnome_canvas_item_new (root, GNOME_TYPE_CANVAS_TEXT,
					    "text", "",
					    "x",  5.0,
					    "y", 45.0,
					    "font", FONT_CANVAS,
					    "anchor", GTK_ANCHOR_NORTH_WEST,
					    "fill_color", COLOR_BODY_TEXT,
					    NULL);

	{
		/* Scrolling region */

		gdouble x1, x2, y1, y2;

		gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (root),
					      &x1, &y1, &x2, &y2);
		gnome_canvas_set_scroll_region (GNOME_CANVAS (display),
						x1, y1, x2, y2);
	}
}

static void
card_display_update (CardDisplay *display)
{
	CardDisplayPriv *priv = display->priv;
	GnomeCanvasGroup *root;
	gchar *text;
	gdouble x1, x2, y1, y2;
	gdouble width, height;

	root = gnome_canvas_root (GNOME_CANVAS (display));

	/* Card body */

	if (priv->card) {
		text = mimedir_vcard_get_as_string (priv->card);
		gnome_canvas_item_set (priv->body, "text", text, NULL);
		g_free (text);
	} else
		gnome_canvas_item_set (priv->body, "text", "", NULL);

	/* Card name */

	gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (priv->body), 
				      NULL, &y1, &x2, &y2);

	{
		PangoLayout *layout;
		PangoFontDescription *font;
		gchar *str;
		int w;

		font = pango_font_description_from_string (FONT_CARDNAME);
		g_assert (font != NULL);

		if (priv->card) {
			g_object_get (G_OBJECT (priv->card), "name", &str, NULL);
			if (!str || *str == '\0') {
				g_free (str);
				str = g_strdup (_("No Card Name"));
			}
		} else
			str = g_strdup ("");

		layout = gtk_widget_create_pango_layout (GTK_WIDGET (display), str);
		pango_layout_set_font_description (layout, font);

		pango_layout_get_pixel_size (layout, &w, NULL);
		width = (gdouble) w;

		pango_font_description_free (font);
		g_object_unref (G_OBJECT (layout));

		width = MAX (CANVAS_WIDTH - 5.0, width);
		width = MAX (x2, width);
		gnome_canvas_item_set (priv->cardname, "text", str, NULL);
		gnome_canvas_item_set (priv->cardname_box, "x2", width, NULL);

		g_free (str);
	}

	width += 4.0;
	height = MAX (CANVAS_HEIGHT - 1, y2 - y1 + 45);
	gnome_canvas_item_set (priv->background_box, 
			       "x2", width, 
			       "y2", height, 
			       NULL);

	/* Scrolling region */

	gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (root), &x1, &y1, &x2, &y2);
	gnome_canvas_set_scroll_region (GNOME_CANVAS (display),
					x1, y1, width, height);
}

/*
 * Public Methods
 */

GtkWidget *
card_display_new (void)
{
	CardDisplay *display;

	display = g_object_new (CARD_TYPE_DISPLAY, NULL);

	return GTK_WIDGET (display);
}

void
card_display_set_card (CardDisplay *display, MIMEDirVCard *card)
{
	g_return_if_fail (display != NULL);
	g_return_if_fail (CARD_IS_DISPLAY (display));
	g_return_if_fail (card == NULL || MIMEDIR_IS_VCARD (card));

	if (display->priv->card) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (display->priv->card),
						      G_CALLBACK (card_display_update),
						      display);
		g_object_unref (G_OBJECT (display->priv->card));
	}

	display->priv->card = card;

	if (card) {
		g_object_ref (G_OBJECT (card));

		g_signal_connect_swapped (G_OBJECT (card), "changed",
					  G_CALLBACK (card_display_update), display);
	}

	card_display_update (display);
}

MIMEDirVCard *
card_display_get_card (CardDisplay *display)
{
	g_return_val_if_fail (display != NULL, NULL);
	g_return_val_if_fail (CARD_IS_DISPLAY (display), NULL);

	return display->priv->card;
}
