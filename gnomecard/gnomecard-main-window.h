#ifndef __GNOMECARD_MAIN_WINDOW_H__
#define __GNOMECARD_MAIN_WINDOW_H__

/* GNOME Card Main Window Object
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <libgnomeui/gnome-app.h>

#define GNCARD_TYPE_MAIN_WINDOW			(gncard_main_window_get_type())
#define GNCARD_MAIN_WINDOW(obj)			(GTK_CHECK_CAST ((obj), GNCARD_TYPE_MAIN_WINDOW, GncardMainWindow))
#define GNCARD_MAIN_WINDOW_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GNCARD_TYPE_MAIN_WINDOW))
#define GNCARD_IS_MAIN_WINDOW(obj)		(GTK_CHECK_TYPE ((obj), GNCARD_TYPE_MAIN_WINDOW))
#define GNCARD_IS_MAIN_WINDOW_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GNCARD_TYPE_MAIN_WINDOW))

typedef struct _GncardMainWindow	GncardMainWindow;
typedef struct _GncardMainWindowClass	GncardMainWindowClass;
typedef struct _GncardMainWindowPriv	GncardMainWindowPriv;

struct _GncardMainWindow
{
	GnomeApp parent;

	GncardMainWindowPriv *priv;
};

struct _GncardMainWindowClass
{
	GnomeAppClass parent_class;

	void (*quit) (void);
};

GType		 gncard_main_window_get_type		(void);
GtkWidget	*gncard_main_window_new			(void);

GtkWidget	*gncard_main_window_get_cardlist	(GncardMainWindow *win);

void		 gncard_main_window_set_changed		(GncardMainWindow *win,
							 gboolean changed);
gboolean	 gncard_main_window_get_changed		(GncardMainWindow *win);
void		 gncard_main_window_open		(GncardMainWindow *win);
gboolean	 gncard_main_window_open_default	(GncardMainWindow *win,
							 GError **error);
gboolean	 gncard_main_window_open_file		(GncardMainWindow *win,
							 const gchar *filename,
							 GError **error);
void		 gncard_main_window_append		(GncardMainWindow *win);
gboolean	 gncard_main_window_append_file		(GncardMainWindow *win,
							 const gchar *filename,
							 GError **error);
gboolean	 gncard_main_window_save		(GncardMainWindow *win,
							 GError **error);
void		 gncard_main_window_save_as		(GncardMainWindow *win);
gboolean	 gncard_main_window_save_file		(GncardMainWindow *win,
							 const gchar *filename,
							 GError **error);

#endif /* __GNOMECARD_MAIN_WINDOW_H__ */
