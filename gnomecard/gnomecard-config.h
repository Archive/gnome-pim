#ifndef __GNOMECARD_CONFIG_H__
#define __GNOMECARD_CONFIG_H__

/* Configuration Path Defines
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#define CONFIG_DIR			"/apps/gnomecard"

#define CONFIG_UI_DIR			"/apps/gnomecard/ui"
#define CONFIG_UI_COLUMN_HEADERS	"/apps/gnomecard/ui/column_headers"

#endif /* __GNOMECARD_CONFIG_H__ */
