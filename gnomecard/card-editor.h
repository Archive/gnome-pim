#ifndef __CARD_EDITOR_H__
#define __CARD_EDITOR_H__

/* GNOME Card Editor Dialog
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <gtk/gtkdialog.h>

#include <mimedir/mimedir-vcard.h>


#define CARD_TYPE_EDITOR		(card_editor_get_type())
#define CARD_EDITOR(obj)		(GTK_CHECK_CAST ((obj), CARD_TYPE_EDITOR, CardEditor))
#define CARD_EDITOR_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CARD_TYPE_EDITOR))
#define CARD_IS_EDITOR(obj)		(GTK_CHECK_TYPE ((obj), CARD_TYPE_EDITOR))
#define CARD_IS_EDITOR_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CARD_TYPE_EDITOR))

typedef struct _CardEditor	CardEditor;
typedef struct _CardEditorClass	CardEditorClass;
typedef struct _CardEditorPriv	CardEditorPriv;

struct _CardEditor
{
	GtkDialog parent;

	CardEditorPriv *priv;
};

struct _CardEditorClass
{
	GtkDialogClass parent_class;
};

GType		 card_editor_get_type		(void);
GtkWidget	*card_editor_new		(MIMEDirVCard *card);

MIMEDirVCard	*card_editor_get_card		(CardEditor *ce);

#endif /* __CARD_EDITOR_H__ */
