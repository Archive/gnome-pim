/* GNOME Card Find Dialog Widget
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* FIXME: save state in session */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <glade/glade.h>

#include "gnomecard-find-dialog.h"


struct _GncardFindDialogPriv {
	GtkWidget *find_entry, *case_toggle, *back_toggle;
};


static GtkDialogClass *parent_class = NULL;


static void gncard_find_dialog_class_init	(GncardFindDialogClass *klass);
static void gncard_find_dialog_init		(GncardFindDialog *win);
static void gncard_find_dialog_dispose		(GObject *object);

static void gncard_find_dialog_setup		(GncardFindDialog *win);

/*
 * Class and Object Management
 */

GType
gncard_find_dialog_get_type (void)
{
	static GType gncard_find_dialog_type = 0;

	if (!gncard_find_dialog_type) {
		static const GTypeInfo gncard_find_dialog_info = {
			sizeof (GncardFindDialogClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) gncard_find_dialog_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (GncardFindDialog),
			0,    /* n_preallocs */
			(GInstanceInitFunc) gncard_find_dialog_init,
		};

		gncard_find_dialog_type = g_type_register_static (GTK_TYPE_DIALOG,
								  "GncardFindDialog",
								  &gncard_find_dialog_info,
								  0);
	}

	return gncard_find_dialog_type;
}

static void
gncard_find_dialog_class_init (GncardFindDialogClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (GNCARD_IS_FIND_DIALOG_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose  = gncard_find_dialog_dispose;

	parent_class = g_type_class_peek_parent (klass);
}

static void
gncard_find_dialog_init (GncardFindDialog *win)
{
	g_return_if_fail (win != NULL);
	g_return_if_fail (GNCARD_IS_FIND_DIALOG (win));

	win->priv = g_new0 (GncardFindDialogPriv, 1);

	gncard_find_dialog_setup (win);
}

static void
gncard_find_dialog_dispose (GObject *object)
{
	GncardFindDialog *win;
	GncardFindDialogPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNCARD_IS_FIND_DIALOG (object));

	win = GNCARD_FIND_DIALOG (object);
	priv = win->priv;

	if (priv) {
		g_free (priv);
		win->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Callbacks
 */

static void
gncard_find_dialog_text_changed_cb (GtkEntry *entry, GncardFindDialog *dialog)
{
	const gchar *s;

	s = gtk_entry_get_text (entry);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog), GTK_RESPONSE_OK, s[0] != '\0');
}

/*
 * Setup
 */

static void
gncard_find_dialog_setup (GncardFindDialog *win)
{
	GncardFindDialogPriv *priv = win->priv;
	GtkWidget *widget;
	GladeXML *xml;

	gtk_window_set_title (GTK_WINDOW (win), _("Find Card"));

	gtk_dialog_add_buttons (GTK_DIALOG (win),
				GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
				GTK_STOCK_FIND,  GTK_RESPONSE_OK,
				GTK_STOCK_HELP,  GTK_RESPONSE_HELP,
				NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (win), GTK_RESPONSE_OK);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (win), GTK_RESPONSE_OK, FALSE);

	/* Content */

	xml = glade_xml_new (GLADEDIR "/gnomecard.glade", "find-box", GETTEXT_PACKAGE);
	if (!xml) {
                g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/gnomecard.glade");
		gtk_widget_destroy (GTK_WIDGET (win));
		return;
	}

	widget = glade_xml_get_widget (xml, "find-box");
	priv->find_entry  = glade_xml_get_widget (xml, "find-entry");
	priv->case_toggle = glade_xml_get_widget (xml, "case-check");
	priv->back_toggle = glade_xml_get_widget (xml, "backwards-check");
	if (!widget ||
	    !priv->find_entry || !priv->case_toggle || !priv->back_toggle) {
		g_printerr (_("File %s is corrupt. Please check your installation."), "/gnomecard.glade");
		gtk_widget_destroy (GTK_WIDGET (win));
		return;
	}

	glade_xml_signal_connect_data (xml, "find-changed",
				       G_CALLBACK (gncard_find_dialog_text_changed_cb), win);

	g_object_unref (G_OBJECT (xml));

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (win)->vbox), widget, FALSE, FALSE, 0);
	gtk_widget_show (widget);
}

/*
 * Public Methods
 */

GtkWidget *
gncard_find_dialog_new (GtkWindow *parent)
{
	GncardFindDialog *win;

	g_return_val_if_fail (parent == NULL || GTK_IS_WINDOW (parent), NULL);

	win = g_object_new (GNCARD_TYPE_FIND_DIALOG, NULL);

	if (parent)
		gtk_window_set_transient_for (GTK_WINDOW (win), parent);

	return GTK_WIDGET (win);
}

gchar *
gncard_find_dialog_get_find_string (GncardFindDialog *dialog)
{
	const gchar *ts;
	gchar *s;

	g_return_val_if_fail (dialog != NULL, NULL);
	g_return_val_if_fail (GNCARD_IS_FIND_DIALOG (dialog), NULL);

	ts = gtk_entry_get_text (GTK_ENTRY (dialog->priv->find_entry));

	s = g_strdup (ts);
	g_strstrip (s);

	return s;
}

gboolean
gncard_find_dialog_get_case_sensitive (GncardFindDialog *dialog)
{
	g_return_val_if_fail (dialog != NULL, FALSE);
	g_return_val_if_fail (GNCARD_IS_FIND_DIALOG (dialog), FALSE);

	return gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->priv->case_toggle));
}

gboolean
gncard_find_dialog_get_backwards (GncardFindDialog *dialog)
{
	g_return_val_if_fail (dialog != NULL, FALSE);
	g_return_val_if_fail (GNCARD_IS_FIND_DIALOG (dialog), FALSE);

	return gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->priv->back_toggle));
}
