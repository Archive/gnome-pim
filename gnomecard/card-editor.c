/* Card Editor Object
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* FIXME: It's possible to have multiple keys */
/* FIXME: when the window should be closed (because of a destroy event or
 *        because Close was clicked), send a new signal "close-request"
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <glade/glade.h>
#include <libegg/egg-datetime.h>

#include <mimedir/mimedir-datetime.h>
#include <mimedir/mimedir-vcard.h>

#include "cardeditor-addresses.h"
#include "cardeditor-email.h"
#include "cardeditor-phone.h"
#include "card-editor.h"
#include "gnomecard-ui.h"


struct _CardEditorPriv {
	MIMEDirVCard *card;

	GtkWidget *fullname, *prefix, *given, *middle, *family, *suffix;
	GtkWidget *organization, *job, *birthdate;
	GtkWidget *url, *categories, *comment;
	GtkWidget *email, *address, *phone;
	GtkWidget *tz_check, *tz_hl, *tz_hours, *tz_ml, *tz_minutes;
	GtkWidget *geo_check, *lat_label, *latitude, *long_label, *longitude;
	GtkWidget *pubkey, *pgp, *x509, *key_other;
};


static GtkDialogClass *parent_class = NULL;


static void card_editor_class_init	(CardEditorClass *klass);
static void card_editor_init		(CardEditor *editor);
static void card_editor_dispose		(GObject *object);

static void card_editor_response	(GtkDialog *dialog, gint response);

static void card_editor_setup		(CardEditor *editor);

/*
 * Class and Object Management
 */

GType
card_editor_get_type (void)
{
	static GType card_editor_type = 0;

	if (!card_editor_type) {
		static const GTypeInfo card_editor_info = {
			sizeof (CardEditorClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) card_editor_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CardEditor),
			0,    /* n_preallocs */
			(GInstanceInitFunc) card_editor_init,
		};

		card_editor_type = g_type_register_static (GTK_TYPE_DIALOG,
							   "CardEditor",
							   &card_editor_info,
							   0);
	}

	return card_editor_type;
}

static void
card_editor_class_init (CardEditorClass *klass)
{
	GObjectClass *gobject_class;
	GtkDialogClass *dialog_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CARD_IS_EDITOR_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);
	dialog_class  = GTK_DIALOG_CLASS (klass);

	gobject_class->dispose = card_editor_dispose;
	dialog_class->response = card_editor_response;

	parent_class = g_type_class_peek_parent (klass);
}

static void
card_editor_init (CardEditor *editor)
{
	g_return_if_fail (editor != NULL);
	g_return_if_fail (CARD_IS_EDITOR (editor));

	editor->priv = g_new0 (CardEditorPriv, 1);

	card_editor_setup (editor);
}

static void
card_editor_dispose (GObject *object)
{
	CardEditor *editor;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CARD_IS_EDITOR (object));

	editor = CARD_EDITOR (object);

	if (editor->priv) {
		if (editor->priv->card)
			g_object_unref (G_OBJECT (editor->priv->card));

		g_free (editor->priv);
		editor->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Overridden Methods
 */

static void
card_editor_response (GtkDialog *dialog, gint response)
{
	switch (response) {
	case GTK_RESPONSE_CANCEL:
		/* FIXME: revert */
		break;
	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;
	case GTK_RESPONSE_DELETE_EVENT:
		break;
	case GTK_RESPONSE_HELP:
		gncard_ui_show_help (GTK_WINDOW (dialog), "edit");
		break;
	default:
		g_return_if_reached ();
	}
}

/*
 * Callbacks
 */

static void
card_editor_string_changed_cb (CardEditor *editor, GtkEntry *entry)
{
	const gchar *property;
	const gchar *s;

	s = gtk_entry_get_text (entry);
	property = (const gchar *) g_object_get_data (G_OBJECT (entry), "property");

	g_object_set (G_OBJECT (editor->priv->card), property, s, NULL);
}

static void
card_editor_birthdate_changed_cb (CardEditor *editor, EggDateTime *edt)
{
	GDate date;

	if (egg_datetime_get_as_gdate (edt, &date)) {
		MIMEDirDateTime *dt;

		dt = mimedir_datetime_new_from_gdate (&date);
		mimedir_vcard_set_birthday (editor->priv->card, dt);
		g_object_unref (G_OBJECT (dt));
	}  else
		mimedir_vcard_set_birthday (editor->priv->card, NULL);
}

static void
card_editor_timezone_changed_cb (CardEditor *editor, GtkSpinButton *button)
{
	gint hour, minute;

	hour   = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (editor->priv->tz_hours));
	minute = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (editor->priv->tz_minutes));

	if (hour < 0)
		minute = -minute;

	mimedir_vcard_set_timezone (editor->priv->card, hour * 60 + minute);
}

static void
card_editor_tz_toggled_cb (CardEditor *editor, GtkToggleButton *button)
{
	CardEditorPriv *priv = editor->priv;
	gboolean active;

	active = gtk_toggle_button_get_active (button);

	gtk_widget_set_sensitive (priv->tz_hl,      active);
	gtk_widget_set_sensitive (priv->tz_hours,   active);
	gtk_widget_set_sensitive (priv->tz_ml,      active);
	gtk_widget_set_sensitive (priv->tz_minutes, active);

	if (active)
		card_editor_timezone_changed_cb (editor, NULL);
	else
		mimedir_vcard_clear_timezone (priv->card);
}

static void
card_editor_latitude_changed_cb (CardEditor *editor, GtkSpinButton *button)
{
	gdouble d;

	d = gtk_spin_button_get_value (button);

	g_object_set (G_OBJECT (editor->priv->card), "latitude", d, NULL);
}

static void
card_editor_longitude_changed_cb (CardEditor *editor, GtkSpinButton *button)
{
	gdouble d;

	d = gtk_spin_button_get_value (button);

	g_object_set (G_OBJECT (editor->priv->card), "longitude", d, NULL);
}

static void
card_editor_geo_toggled_cb (CardEditor *editor, GtkToggleButton *button)
{
	CardEditorPriv *priv = editor->priv;
	gboolean active;

	active = gtk_toggle_button_get_active (button);

	gtk_widget_set_sensitive (priv->lat_label,  active);
	gtk_widget_set_sensitive (priv->latitude,   active);
	gtk_widget_set_sensitive (priv->long_label, active);
	gtk_widget_set_sensitive (priv->longitude,  active);

	if (active) {
		card_editor_latitude_changed_cb  (editor, GTK_SPIN_BUTTON (priv->latitude));
		card_editor_longitude_changed_cb (editor, GTK_SPIN_BUTTON (priv->longitude));
	} else {
		mimedir_vcard_clear_geo_position (priv->card);
	}
}

static void
card_editor_comment_changed_cb (CardEditor *editor, GtkTextBuffer *buffer)
{
	GtkTextIter start, end;
	gchar *s;

	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_get_end_iter   (buffer, &end);

	s = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
	g_object_set (G_OBJECT (editor->priv->card), "note", s, NULL);
	g_free (s);
}

static void
card_editor_key_changed_cb (CardEditor *editor, GtkWidget *widget)
{
	MIMEDirVCardKey key;
	GtkTextBuffer *buffer;
	GtkTextIter start, end;
	GList *list = NULL;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (editor->priv->pubkey));
	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_get_end_iter   (buffer, &end);
	key.key = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (editor->priv->pgp)))
		key.type = MIMEDIR_VCARD_KEY_PGP;
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (editor->priv->x509)))
		key.type = MIMEDIR_VCARD_KEY_X509;
	else
		key.type = MIMEDIR_VCARD_KEY_UNKNOWN;

	if (key.key[0] != '\0')
		list = g_list_append (list, &key);
	g_object_set (G_OBJECT (editor->priv->card), "key-list", list, NULL);
	g_list_free (list);

	g_free (key.key);
}

static void
card_editor_take_from_name_cb (CardEditor *editor, GtkButton *button)
{
	GString *name;
	const gchar *s;
	gchar *n;

	name = g_string_new (NULL);

	s = gtk_entry_get_text (GTK_ENTRY (editor->priv->prefix));
	if (s && *s) {
		g_string_append (name, s);
		g_string_append_c (name, ' ');
	}

	s = gtk_entry_get_text (GTK_ENTRY (editor->priv->given));
	if (s && *s) {
		g_string_append (name, s);
		g_string_append_c (name, ' ');
	}

	s = gtk_entry_get_text (GTK_ENTRY (editor->priv->middle));
	if (s && *s) {
		g_string_append (name, s);
		g_string_append_c (name, ' ');
	}

	s = gtk_entry_get_text (GTK_ENTRY (editor->priv->family));
	if (s && *s) {
		g_string_append (name, s);
		g_string_append_c (name, ' ');
	}

	s = gtk_entry_get_text (GTK_ENTRY (editor->priv->suffix));
	if (s && *s) {
		g_string_append (name, s);
		g_string_append_c (name, ' ');
	}

	n = g_string_free (name, FALSE);
	n = g_strstrip (n);
	gtk_entry_set_text (GTK_ENTRY (editor->priv->fullname), n);
	g_free (n);
}

/*
 * Internal Methods
 */

static void
card_editor_setup (CardEditor *ce)
{
	CardEditorPriv *priv = ce->priv;
	GtkWidget *widget, *button, *label;
	GtkWidget *notebook;
	GtkTextBuffer *text_buffer;
	GladeXML *xml;

	gtk_dialog_set_has_separator (GTK_DIALOG (ce), FALSE);
	gtk_dialog_add_buttons (GTK_DIALOG (ce),
				GTK_STOCK_HELP,            GTK_RESPONSE_HELP,
				GTK_STOCK_REVERT_TO_SAVED, GTK_RESPONSE_CANCEL,
				GTK_STOCK_CLOSE,           GTK_RESPONSE_CLOSE,
				NULL);

	/* Notebook */

	notebook = gtk_notebook_new ();
	gtk_widget_show (notebook);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (ce)->vbox), notebook);

	/* Identity page */

	xml = glade_xml_new (GLADEDIR "/card-editor.glade", "identity_box", GETTEXT_PACKAGE);
	if (!xml) {
                g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/card-editor.glade");
		return;
	}

	widget = glade_xml_get_widget (xml, "identity_box");
	priv->fullname     = glade_xml_get_widget (xml, "name_entry");
	priv->prefix       = glade_xml_get_widget (xml, "prefix_entry");
	priv->family       = glade_xml_get_widget (xml, "family_entry");
	priv->middle       = glade_xml_get_widget (xml, "middle_entry");
	priv->given        = glade_xml_get_widget (xml, "given_entry");
	priv->suffix       = glade_xml_get_widget (xml, "suffix_entry");
	priv->organization = glade_xml_get_widget (xml, "org_entry");
	priv->job          = glade_xml_get_widget (xml, "job_entry");
	priv->birthdate    = glade_xml_get_widget (xml, "birthdate_custom");
	button = glade_xml_get_widget (xml, "tfn_button");

	if (!widget || !button ||
	    !priv->fullname     || !priv->prefix || !priv->family    ||
	    !priv->middle       || !priv->given  || !priv->suffix    ||
	    !priv->organization || !priv->job    || !priv->birthdate) {
                g_printerr (_("File %s is corrupt. Please check your installation."), "/card-editor.glade");
                return;
	}

	g_signal_connect_swapped (G_OBJECT (button), "clicked",
				  G_CALLBACK (card_editor_take_from_name_cb), ce);

	g_object_unref (G_OBJECT (xml));

	egg_datetime_set_lazy (EGG_DATETIME (priv->birthdate), TRUE);
	gtk_widget_show (priv->birthdate);

	label = gtk_label_new (_("Identity"));
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), widget, label);

	/* E-mail page */

	xml = glade_xml_new (GLADEDIR "/card-editor.glade", "network_box", GETTEXT_PACKAGE);
	if (!xml) {
                g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/card-editor.glade");
		return;
	}

	widget = glade_xml_get_widget (xml, "network_box");
	priv->url   = glade_xml_get_widget (xml, "url_entry");
	priv->email = glade_xml_get_widget (xml, "network_custom");

	if (!widget || !priv->url || !priv->email) {
                g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
                return;
	}

	g_object_unref (G_OBJECT (xml));

	gtk_widget_show (priv->email);

	label = gtk_label_new (_("Network"));
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), widget, label);

	/* Address page */

	xml = glade_xml_new (GLADEDIR "/card-editor.glade", "addresses_custom", GETTEXT_PACKAGE);
	if (!xml) {
                g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/card-editor.glade");
		return;
	}

	priv->address = glade_xml_get_widget (xml, "addresses_custom");

	if (!priv->address) {
                g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
                return;
	}

	g_object_unref (G_OBJECT (xml));

	gtk_container_set_border_width (GTK_CONTAINER (priv->address), 12);
	gtk_widget_show (priv->address);

	label = gtk_label_new (_("Addresses"));
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), priv->address, label);

	/* Phone page */

	xml = glade_xml_new (GLADEDIR "/card-editor.glade", "phone_custom", GETTEXT_PACKAGE);
	if (!xml) {
                g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/card-editor.glade");
		return;
	}

	priv->phone = glade_xml_get_widget (xml, "phone_custom");

	if (!priv->phone) {
                g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
                return;
	}

	g_object_unref (G_OBJECT (xml));

	gtk_container_set_border_width (GTK_CONTAINER (priv->phone), 12);
	gtk_widget_show (priv->phone);

	label = gtk_label_new (_("Phone"));
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), priv->phone, label);

	/* Geographical page */

	xml = glade_xml_new (GLADEDIR "/card-editor.glade", "geo_box", GETTEXT_PACKAGE);
	if (!xml) {
                g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/card-editor.glade");
		return;
	}

	widget = glade_xml_get_widget (xml, "geo_box");
	priv->tz_check   = glade_xml_get_widget (xml, "tz_check");
	priv->tz_hl      = glade_xml_get_widget (xml, "hours_label");
	priv->tz_hours   = glade_xml_get_widget (xml, "hours_spin");
	priv->tz_ml      = glade_xml_get_widget (xml, "min_label");
	priv->tz_minutes = glade_xml_get_widget (xml, "min_spin");
	priv->geo_check  = glade_xml_get_widget (xml, "geo_check");
	priv->lat_label  = glade_xml_get_widget (xml, "lat_label");
	priv->latitude   = glade_xml_get_widget (xml, "lat_spin");
	priv->long_label = glade_xml_get_widget (xml, "long_label");
	priv->longitude  = glade_xml_get_widget (xml, "long_spin");

	if (!widget ||
	    !priv->tz_check || !priv->tz_hl || !priv->tz_hours ||
	    !priv->tz_ml || !priv->tz_minutes ||
	    !priv->geo_check || !priv->lat_label || !priv->latitude ||
	    !priv->long_label || !priv->longitude) {
                g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
                return;
	}

	g_object_unref (G_OBJECT (xml));

	gtk_widget_set_sensitive (priv->tz_hl,      FALSE);
	gtk_widget_set_sensitive (priv->tz_hours,   FALSE);
	gtk_widget_set_sensitive (priv->tz_ml,      FALSE);
	gtk_widget_set_sensitive (priv->tz_minutes, FALSE);
	gtk_widget_set_sensitive (priv->lat_label,  FALSE);
	gtk_widget_set_sensitive (priv->latitude,   FALSE);
	gtk_widget_set_sensitive (priv->long_label, FALSE);
	gtk_widget_set_sensitive (priv->longitude,  FALSE);

	label = gtk_label_new (_("Geographical"));
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), widget, label);

	/* Explanatory page */

	xml = glade_xml_new (GLADEDIR "/card-editor.glade", "expl_box", GETTEXT_PACKAGE);
	if (!xml) {
                g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/card-editor.glade");
		return;
	}

	widget = glade_xml_get_widget (xml, "expl_box");
	priv->categories = glade_xml_get_widget (xml, "cat_entry");
	priv->comment    = glade_xml_get_widget (xml, "comments_text");

	if (!widget || !priv->categories || !priv->comment) {
                g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
                return;
	}

	g_object_unref (G_OBJECT (xml));

	label = gtk_label_new (_("Explanatory"));
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), widget, label);

	/* Security page */

	xml = glade_xml_new (GLADEDIR "/card-editor.glade", "sec_box", GETTEXT_PACKAGE);
	if (!xml) {
                g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/card-editor.glade");
		return;
	}

	widget = glade_xml_get_widget (xml, "sec_box");
	priv->pubkey    = glade_xml_get_widget (xml, "key_text");
	priv->pgp       = glade_xml_get_widget (xml, "pgp_radio");
	priv->x509      = glade_xml_get_widget (xml, "x509_radio");
	priv->key_other = glade_xml_get_widget (xml, "keyother_radio");

	if (!widget ||
	    !priv->pubkey || !priv->pgp || !priv->x509 || !priv->key_other) {
                g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
                return;
	}

	g_object_unref (G_OBJECT (xml));

	label = gtk_label_new (_("Security"));
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), widget, label);

	/* Signals */

	g_object_set_data (G_OBJECT (priv->fullname),     "property", "name");
	g_object_set_data (G_OBJECT (priv->family),       "property", "familyname");
	g_object_set_data (G_OBJECT (priv->middle),       "property", "middlename");
	g_object_set_data (G_OBJECT (priv->given),        "property", "givenname");
	g_object_set_data (G_OBJECT (priv->prefix),       "property", "prefix");
	g_object_set_data (G_OBJECT (priv->suffix),       "property", "suffix");
	g_object_set_data (G_OBJECT (priv->organization), "property", "organization");
	g_object_set_data (G_OBJECT (priv->job),          "property", "jobtitle");
	g_object_set_data (G_OBJECT (priv->url),          "property", "url");
	g_object_set_data (G_OBJECT (priv->categories),   "property", "categories");

	g_signal_connect_swapped (G_OBJECT (priv->fullname), "changed",
				  G_CALLBACK (card_editor_string_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->family), "changed",
				  G_CALLBACK (card_editor_string_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->middle), "changed",
				  G_CALLBACK (card_editor_string_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->given), "changed",
				  G_CALLBACK (card_editor_string_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->prefix), "changed",
				  G_CALLBACK (card_editor_string_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->suffix), "changed",
				  G_CALLBACK (card_editor_string_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->organization), "changed",
				  G_CALLBACK (card_editor_string_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->url), "changed",
				  G_CALLBACK (card_editor_string_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->categories), "changed",
				  G_CALLBACK (card_editor_string_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->birthdate), "date-changed",
				  G_CALLBACK (card_editor_birthdate_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->tz_check), "toggled",
				  G_CALLBACK (card_editor_tz_toggled_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->tz_hours), "value-changed",
				  G_CALLBACK (card_editor_timezone_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->tz_minutes), "value-changed",
				  G_CALLBACK (card_editor_timezone_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->geo_check), "toggled",
				  G_CALLBACK (card_editor_geo_toggled_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->latitude), "value-changed",
				  G_CALLBACK (card_editor_latitude_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->longitude), "value-changed",
				  G_CALLBACK (card_editor_longitude_changed_cb), ce);
	text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->comment));
	g_signal_connect_swapped (G_OBJECT (text_buffer), "changed",
				  G_CALLBACK (card_editor_comment_changed_cb), ce);
	text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->pubkey));
	g_signal_connect_swapped (G_OBJECT (text_buffer), "changed",
				  G_CALLBACK (card_editor_key_changed_cb), ce);
	g_signal_connect_swapped (G_OBJECT (priv->pgp), "toggled",
				  G_CALLBACK (card_editor_key_changed_cb), ce);
}

static void
card_editor_update_card (CardEditor *ce)
{
	GtkTextBuffer *text_buffer;
	CardEditorPriv *priv = ce->priv;
	gchar *name, *family, *middle, *given, *prefix, *suffix, *org, *job;
	gchar *url, *cats, *note;

	g_object_get (G_OBJECT (priv->card),
		      "name",         &name,
		      "familyname",   &family,
		      "middlename",   &middle,
		      "givenname",    &given,
		      "prefix",       &prefix,
		      "suffix",       &suffix,
		      "organization", &org,
		      "jobtitle",     &job,
		      "categories",   &cats,
		      "url",          &url,
		      "note",         &note,
		      NULL);

	gtk_entry_set_text (GTK_ENTRY (priv->fullname), name ? name : "");
	gtk_entry_set_text (GTK_ENTRY (priv->family), family ? family : "");
	gtk_entry_set_text (GTK_ENTRY (priv->middle), middle ? middle : "");
	gtk_entry_set_text (GTK_ENTRY (priv->given), given ? given : "");
	gtk_entry_set_text (GTK_ENTRY (priv->prefix), prefix ? prefix : "");
	gtk_entry_set_text (GTK_ENTRY (priv->suffix), suffix ? suffix : "");
	gtk_entry_set_text (GTK_ENTRY (priv->organization), org ? org : "");
	gtk_entry_set_text (GTK_ENTRY (priv->job), job ? job : "");
	gtk_entry_set_text (GTK_ENTRY (priv->url), url ? url : "");
	gtk_entry_set_text (GTK_ENTRY (priv->categories), cats ? cats : "");
	text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->comment));
	gtk_text_buffer_set_text (text_buffer, note ? note : "", -1);

	g_free (name);
	g_free (family);
	g_free (middle);
	g_free (given);
	g_free (prefix);
	g_free (suffix);
	g_free (org);
	g_free (job);
	g_free (url);
	g_free (cats);
	g_free (note);

	card_editor_email_edit_vcard (CARD_EDITOR_EMAIL (priv->email), priv->card);
	card_editor_addresses_edit_vcard (CARD_EDITOR_ADDRESSES (priv->address), priv->card);
	card_editor_phone_edit_vcard (CARD_EDITOR_PHONE (priv->phone), priv->card);

	/* Birthday */

	{
		MIMEDirDateTime *dt;

		g_object_get (G_OBJECT (priv->card), "birthday", &dt, NULL);

		if (dt) {
			GDate date;

			mimedir_datetime_get_gdate (dt, &date);
			egg_datetime_set_from_gdate (EGG_DATETIME (priv->birthdate), &date);

			g_object_unref (G_OBJECT (dt));
		} else
			egg_datetime_set_none (EGG_DATETIME (priv->birthdate));
	}

	/* Time zone and geographical position */

	{
		gint timezone = 0, hours, minutes;
		gdouble latitude = 0.0, longitude = 0.0;
		gboolean tz_active, geo_active;

		tz_active = mimedir_vcard_get_timezone (priv->card, &timezone);
		hours   = timezone / 60;
		minutes = timezone % 60;

		geo_active = mimedir_vcard_get_geo_position (priv->card, &latitude, &longitude);

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->tz_check),  tz_active);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->geo_check), geo_active);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (priv->tz_hours),   (gdouble) hours);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (priv->tz_minutes), (gdouble) minutes);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (priv->latitude),  latitude);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (priv->longitude), longitude);
	}

	/* Public key */

	{
		gchar *key;
		gint keytype;

		g_object_get (G_OBJECT (priv->card),
			      "pubkey",  &key,
			      "keytype", &keytype,
			      NULL);

		text_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->pubkey));
		gtk_text_buffer_set_text (text_buffer, key ? key : "", -1);

		switch (keytype) {
		case MIMEDIR_VCARD_KEY_PGP:
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->pgp), TRUE);
			break;
		case MIMEDIR_VCARD_KEY_X509:
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->x509), TRUE);
			break;
		default:
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->key_other), TRUE);
			break;
		}

		g_free (key);
	}
}

static void
card_editor_set_card (CardEditor *ce, MIMEDirVCard *card)
{
	CardEditorPriv *priv;

	g_return_if_fail (ce != NULL);
	g_return_if_fail (CARD_IS_EDITOR (ce));
	g_return_if_fail (card != NULL);
	g_return_if_fail (MIMEDIR_IS_VCARD (card));

	priv = ce->priv;

	if (priv->card)
		g_object_unref (priv->card);

	priv->card = card;

	card_editor_update_card (ce);
}

/*
 * Public Methods
 */

GtkWidget *
card_editor_new (MIMEDirVCard *card)
{
	CardEditor *editor;

	g_return_val_if_fail (card != NULL, NULL);
	g_return_val_if_fail (MIMEDIR_IS_VCARD (card), NULL);

	editor = g_object_new (CARD_TYPE_EDITOR, NULL);
	card_editor_set_card (editor, card);

	return GTK_WIDGET (editor);
}

MIMEDirVCard *
card_editor_get_card (CardEditor *ce)
{
	g_return_val_if_fail (ce != NULL, NULL);
	g_return_val_if_fail (CARD_IS_EDITOR (ce), NULL);

	return ce->priv->card;
}
