#ifndef __GNOMECARD_UI_H__
#define __GNOMECARD_UI_H__

/* Miscellaneous GNOME Card UI Functions
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib/gerror.h>

#include <gtk/gtkwidget.h>
#include <gtk/gtkwindow.h>


void gncard_ui_init (void);
void gncard_ui_fini (void);
void gncard_ui_run (void);
GtkWidget *gncard_ui_open_main_window (const gchar *path);

gint gncard_ui_save_confirmation (GtkWindow *parent, const gchar *filename);

void gncard_ui_show_help (GtkWindow *parent, const gchar *anchor);
void gncard_ui_show_error (GtkWindow *parent, GError *error);

#endif /* __GNOMECARD_UI_H__ */
