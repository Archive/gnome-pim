#ifndef __GNOMECARD_FIND_DIALOG_H__
#define __GNOMECARD_FIND_DIALOG_H__

/* GNOME Card Find Dialog Widget
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <gtk/gtkdialog.h>

#define GNCARD_TYPE_FIND_DIALOG			(gncard_find_dialog_get_type())
#define GNCARD_FIND_DIALOG(obj)			(GTK_CHECK_CAST ((obj), GNCARD_TYPE_FIND_DIALOG, GncardFindDialog))
#define GNCARD_FIND_DIALOG_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GNCARD_TYPE_FIND_DIALOG))
#define GNCARD_IS_FIND_DIALOG(obj)		(GTK_CHECK_TYPE ((obj), GNCARD_TYPE_FIND_DIALOG))
#define GNCARD_IS_FIND_DIALOG_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GNCARD_TYPE_FIND_DIALOG))

typedef struct _GncardFindDialog	GncardFindDialog;
typedef struct _GncardFindDialogClass	GncardFindDialogClass;
typedef struct _GncardFindDialogPriv	GncardFindDialogPriv;

struct _GncardFindDialog
{
	GtkDialog parent;

	GncardFindDialogPriv *priv;
};

struct _GncardFindDialogClass
{
	GtkDialogClass parent_class;
};

GType		 gncard_find_dialog_get_type		(void);
GtkWidget	*gncard_find_dialog_new			(GtkWindow *parent);

gchar		*gncard_find_dialog_get_find_string	(GncardFindDialog *dialog);
gboolean	 gncard_find_dialog_get_case_sensitive	(GncardFindDialog *dialog);
gboolean	 gncard_find_dialog_get_backwards	(GncardFindDialog *dialog);

#endif /* __GNOMECARD_FIND_DIALOG_H__ */
