/* GNOME Card Main Function
 * Copyright (C) 2003, 2004  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libintl.h>
#include <locale.h>

#include <mimedir/mimedir-init.h>

#include <gnome.h>

#include "gnomecard-main-window.h"
#include "gnomecard-ui.h"

gchar *in_file = NULL;

const struct poptOption gnomecard_popt_options [] = {
	{ "address-book", 'a', POPT_ARG_STRING, &in_file, 0,
	  N_("Address book to load"), N_("path") },
	{ NULL, '\0', 0, NULL, 0 }
};

int
main (int argc, char *argv[])
{
	/* I18n setup */

	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	/* Initializations */

	mimedir_init ();

	gnome_program_init ("gnomecard", VERSION,
			    LIBGNOMEUI_MODULE,
			    argc, argv,
			    GNOME_PARAM_POPT_TABLE, gnomecard_popt_options,
			    GNOME_PROGRAM_STANDARD_PROPERTIES,
			    GNOME_PARAM_NONE);

	gncard_ui_init ();
	gncard_ui_open_main_window (in_file);

	/* Main loop */

	gncard_ui_run ();

	/* Cleanup */

	gncard_ui_fini ();

	return 0;
}
