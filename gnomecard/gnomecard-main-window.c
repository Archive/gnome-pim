/* GNOME Card Main Window Object
 * Copyright (C) 2003-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * Based on the original GnomeCard code, which is:
 *
 * Copyright (C) 1999 The Free Software Foundation
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib.h>

#include <gnome.h>

#include <mimedir/mimedir-vcard.h>

#include "card-display.h"
#include "card-editor.h"
#include "cardlist-headers.h"
#include "cardlist-widget.h"
#include "gnomecard-find-dialog.h"
#include "gnomecard-main-window.h"
#include "gnomecard-prefs.h"
#include "gnomecard-ui.h"


enum {
	SIGNAL_QUIT,
	SIGNAL_LAST
};


struct _GncardMainWindowPriv {
	GtkWidget *cardlist;
	GtkWidget *canvas;

	GtkToolItem *save_button;
	GtkToolItem *edit_button;
	GtkToolItem *delete_button;
	GtkToolItem *find_button;

	GtkWidget *save_menuitem;
	GtkWidget *edit_menuitem;
	GtkWidget *delete_menuitem;
	GtkWidget *find_menuitem;

	GtkWidget *find_dialog;

	gboolean changed; /* Has the card list or one of the cards changed
			   * since the last save?
			   */
};


static GnomeAppClass *parent_class = NULL;

static gint gncard_main_window_signals[SIGNAL_LAST] = { 0 };


static void gncard_main_window_class_init	(GncardMainWindowClass *klass);
static void gncard_main_window_init		(GncardMainWindow *win);
static void gncard_main_window_dispose		(GObject *object);

static void gncard_main_window_setup		(GncardMainWindow *win);
static void gncard_main_window_edit_card	(GncardMainWindow *win,
						 MIMEDirVCard *card);
static void gncard_main_window_new_card		(GncardMainWindow *win);
static void gncard_main_window_edit_current_card
						(GncardMainWindow *win);
static void gncard_main_window_delete_current_card
						(GncardMainWindow *win);
static void gncard_main_window_sensitize_edit_widgets
						(GncardMainWindow *win, gboolean state);

/*
 * Class and Object Management
 */

GType
gncard_main_window_get_type (void)
{
	static GType gncard_main_window_type = 0;

	if (!gncard_main_window_type) {
		static const GTypeInfo gncard_main_window_info = {
			sizeof (GncardMainWindowClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) gncard_main_window_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (GncardMainWindow),
			1,    /* n_preallocs */
			(GInstanceInitFunc) gncard_main_window_init,
		};

		gncard_main_window_type = g_type_register_static (GNOME_TYPE_APP,
								  "GncardMainWindow",
								  &gncard_main_window_info,
								  0);
	}

	return gncard_main_window_type;
}

static void
gncard_main_window_class_init (GncardMainWindowClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (GNCARD_IS_MAIN_WINDOW_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose  = gncard_main_window_dispose;

	parent_class = g_type_class_peek_parent (klass);

	/* Signals */

	gncard_main_window_signals[SIGNAL_QUIT] =
		g_signal_new ("quit",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (GncardMainWindowClass, quit),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
}

static void
gncard_main_window_init (GncardMainWindow *win)
{
	g_return_if_fail (win != NULL);
	g_return_if_fail (GNCARD_IS_MAIN_WINDOW (win));

	win->priv = g_new0 (GncardMainWindowPriv, 1);

	win->priv->changed = FALSE;

	gncard_main_window_setup (win);
}

static void
gncard_main_window_dispose (GObject *object)
{
	GncardMainWindow *win;
	GncardMainWindowPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNCARD_IS_MAIN_WINDOW (object));

	win = GNCARD_MAIN_WINDOW (object);
	priv = win->priv;

	if (priv) {
		g_free (priv);
		win->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Callbacks
 */

static void
gncard_main_window_find_response_cb (GncardMainWindow *win, gint response, GtkDialog *dialog)
{
	switch (response) {
	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		// fall-through
	case GTK_RESPONSE_DELETE_EVENT:
		win->priv->find_dialog = NULL;
		break;
	case GTK_RESPONSE_OK: {
		gchar *s;
		gboolean _case, back;

		s = gncard_find_dialog_get_find_string (GNCARD_FIND_DIALOG (dialog));
		_case = gncard_find_dialog_get_case_sensitive (GNCARD_FIND_DIALOG (dialog));
		back = gncard_find_dialog_get_backwards (GNCARD_FIND_DIALOG (dialog));

		cardlist_widget_find (CARDLIST_WIDGET (win->priv->cardlist), s, _case, back);

		g_free (s);

		break;
	}
	case GTK_RESPONSE_HELP:
		gncard_ui_show_help (GTK_WINDOW (dialog), "find-dialog");
		break;
	default:
		g_return_if_reached ();
	}
}

/* File callbacks */

static void
gncard_main_window_new_cb (GtkWidget *widget, GncardMainWindow *win)
{
	gncard_ui_open_main_window (NULL);
}

static void
gncard_main_window_open_cb (GtkWidget *widget, GncardMainWindow *win)
{
	gncard_main_window_open (win);
}

static void
gncard_main_window_insert_cb (GtkWidget *widget, GncardMainWindow *win)
{
	gncard_main_window_append (win);
}

static void
gncard_main_window_open_default_cb (GtkWidget *widget, GncardMainWindow *win)
{
	GError *error = NULL;

	if (!gncard_main_window_open_default (win, &error)) {
		gncard_ui_show_error (GTK_WINDOW (win), error);
		g_error_free (error);
	}
}

static void
gncard_main_window_save_cb (GtkWidget *widget, GncardMainWindow *win)
{
	const gchar *filename;

	filename = cardlist_widget_get_filename (CARDLIST_WIDGET (win->priv->cardlist));
	if (!filename)
		gncard_main_window_save_as (win);
	else {
		GError *error = NULL;

		if (!gncard_main_window_save (win, &error)) {
			gncard_ui_show_error (GTK_WINDOW (win), error);
			g_error_free (error);
		}
	}
}

static void
gncard_main_window_save_as_cb (GtkWidget *widget, GncardMainWindow *win)
{
	gncard_main_window_save_as (win);
}

static void
gncard_main_window_quit_cb (GtkWidget *widget, GncardMainWindow *win)
{
	g_signal_emit (G_OBJECT (win),
		       gncard_main_window_signals[SIGNAL_QUIT], 0);
}

/* Edit callbacks */

static void
gncard_main_window_new_card_cb (GtkWidget *widget, GncardMainWindow *win)
{
	gncard_main_window_new_card (win);
}

static void
gncard_main_window_edit_card_cb (GtkWidget *widget, GncardMainWindow *win)
{
	gncard_main_window_edit_current_card (win);
}

static void
gncard_main_window_delete_card_cb (GtkWidget *widget, GncardMainWindow *win)
{
	gncard_main_window_delete_current_card (win);
}

static void
gncard_main_window_find_cb (GtkWidget *widget, GncardMainWindow *win)
{
	GncardMainWindowPriv *priv = win->priv;

	if (priv->find_dialog) {
		gtk_window_present (GTK_WINDOW (priv->find_dialog));
		return;
	}

	priv->find_dialog = gncard_find_dialog_new (GTK_WINDOW (win));
	g_signal_connect_swapped (G_OBJECT (priv->find_dialog), "response",
				  G_CALLBACK (gncard_main_window_find_response_cb), win);
	gtk_widget_show (priv->find_dialog);
}

/* Prefs callback */

static void
gncard_main_window_prefs_cb (GtkWidget *widget, GncardMainWindow *win)
{
	GtkWidget *dialog;

	dialog = gncard_prefs_new (GTK_WINDOW (win));
	gtk_widget_show (dialog);
}

/* Miscellaneous callbacks */

static void
gncard_main_window_list_selection_changed_cb (GncardMainWindow *win, GtkTreeSelection *selection)
{
	GtkTreeModel *model;
	GtkTreeIter   iter;
	MIMEDirVCard *card = NULL;

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		/* Widget sensibility */

		gncard_main_window_sensitize_edit_widgets (win, TRUE);

		gtk_tree_model_get (model, &iter, 0, &card, -1);
		g_assert (card != NULL && MIMEDIR_IS_VCARD (card));
	} else {
		/* Widget sensibility */

		gncard_main_window_sensitize_edit_widgets (win, FALSE);
	}

	card_display_set_card (CARD_DISPLAY (win->priv->canvas), card);
}

static void
gncard_main_window_list_row_activated_cb (GncardMainWindow *win, GtkTreePath *path, GtkTreeViewColumn *column, GtkTreeView *tv)
{
	GtkTreeModel     *model;
	GtkTreeIter       iter;
	MIMEDirVCard     *card;

	g_return_if_fail (tv != NULL);
	g_return_if_fail (GTK_IS_TREE_VIEW (tv));

	model = gtk_tree_view_get_model (tv);

	gtk_tree_model_get_iter (model, &iter, path);

	gtk_tree_model_get (model, &iter, 0, &card, -1);

	g_assert (MIMEDIR_IS_VCARD (card));

	gncard_main_window_edit_card (win, card);
}

static void
gncard_main_window_set_changed_cb (GncardMainWindow *win, MIMEDirVCard *card)
{
	gncard_main_window_set_changed (win, TRUE);
}

static gboolean
gncard_main_window_close_editor_cb (GncardMainWindow *win, GdkEvent *ev, GtkWidget *widget)
{
	MIMEDirVCard *card;

	card = card_editor_get_card (CARD_EDITOR (widget));

	g_object_set_data (G_OBJECT (card), "editor", NULL);
	g_object_unref (G_OBJECT (card));

	return FALSE;
}

static void
gncard_main_window_sort_name_cb (GtkMenuItem *item, GncardMainWindow *win)
{
	cardlist_widget_sort (CARDLIST_WIDGET (win->priv->cardlist), CLH_NAME);
}

static void
gncard_main_window_sort_familyname_cb (GtkMenuItem *item, GncardMainWindow *win)
{
	cardlist_widget_sort (CARDLIST_WIDGET (win->priv->cardlist), CLH_FAMILYNAME);
}

static void
gncard_main_window_sort_email_cb (GtkMenuItem *item, GncardMainWindow *win)
{
	cardlist_widget_sort (CARDLIST_WIDGET (win->priv->cardlist), CLH_EMAIL);
}

static void
gncard_main_window_sort_org_cb (GtkMenuItem *item, GncardMainWindow *win)
{
	cardlist_widget_sort (CARDLIST_WIDGET (win->priv->cardlist), CLH_ORGANIZATION);
}

/*
 * Private Methods
 */

static void
show_about (GncardMainWindow *win)
{
	static const gchar *authors[] = {
		"Arturo Espinosa <arturo@nuclecu.unam.mx>", 
		"Michael Fulbright <drmike@redhat.com>",
		"Sebastian Rittau <srittau@jroger.in-berlin.de>",
		NULL
	};

	const gchar *translators = _("translator_credits");
	if (strcmp (translators, "translator_credits") == 0)
		translators = NULL;

	gtk_show_about_dialog (GTK_WINDOW (win),
			       "authors",		authors,
			       "comments",		_("Electronic Business Card Manager"),
			       "copyright",		"(C) 1997-2000 the Free Software Foundation\n"
							"Copyright \xc2\xa9 2002-2005 Sebastian Rittau",
			       "name",			_("GNOME Address Book"),
			       "translator-credits",	translators,
			       "version",		VERSION,
			       NULL);
}

static void
gncard_main_window_edit_card (GncardMainWindow *win, MIMEDirVCard *card)
{
	CardEditor *ce;
	GtkWidget *dialog;

	ce = (CardEditor *) g_object_get_data (G_OBJECT (card), "editor");
	if (ce) {
		gtk_window_present (GTK_WINDOW (ce));
		return;
	}

	dialog = card_editor_new (card);
	g_signal_connect_swapped (G_OBJECT (dialog), "delete-event", /* FIXME: "close-request" */
				  G_CALLBACK (gncard_main_window_close_editor_cb), win);
	gtk_widget_show (dialog);

	g_object_ref (G_OBJECT (card));
	g_object_set_data (G_OBJECT (card), "editor", dialog);
}

static void
gncard_main_window_new_card (GncardMainWindow *win)
{
	MIMEDirVCard *card;

	card = mimedir_vcard_new ();
	cardlist_widget_append_card (CARDLIST_WIDGET (win->priv->cardlist), card);

	gncard_main_window_edit_card (win, card);
}

static void
gncard_main_window_edit_current_card (GncardMainWindow *win)
{
	GtkTreeModel     *model;
	GtkTreeSelection *selection;
	GtkTreeIter       iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (win->priv->cardlist));

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCard *card;

		gtk_tree_model_get (model, &iter, 0, &card, -1);

		g_assert (MIMEDIR_IS_VCARD (card));

		gncard_main_window_edit_card (win, card);
	}
}

static void
gncard_main_window_delete_current_card (GncardMainWindow *win)
{
	MIMEDirVCard *vcard;

	vcard = cardlist_widget_get_current_card (CARDLIST_WIDGET (win->priv->cardlist));
	if (vcard) {
		cardlist_widget_remove_card (CARDLIST_WIDGET (win->priv->cardlist), vcard);
	
		gncard_main_window_set_changed (win, TRUE);
	}
}

static void
gncard_main_window_update_title (GncardMainWindow *win)
{
	const gchar *filename;
	gchar *base, *title;

	filename = cardlist_widget_get_filename (CARDLIST_WIDGET (win->priv->cardlist));
	if (filename)
		base = g_path_get_basename (filename);
	else
		base = g_strdup (_("<unnamed>"));

	title = g_strdup_printf (_("%s - GNOME Address Book"), base);

	gtk_window_set_title (GTK_WINDOW (win), title);

	g_free (base);
	g_free (title);
}

static void
gncard_main_window_sensitize_edit_widgets (GncardMainWindow *win, gboolean state)
{
	GncardMainWindowPriv *priv = win->priv;

	gtk_widget_set_sensitive (GTK_WIDGET (priv->edit_button),     state);
	gtk_widget_set_sensitive (GTK_WIDGET (priv->edit_menuitem),   state);
	gtk_widget_set_sensitive (GTK_WIDGET (priv->delete_button),   state);
	gtk_widget_set_sensitive (GTK_WIDGET (priv->delete_menuitem), state);
	gtk_widget_set_sensitive (GTK_WIDGET (priv->find_button),     state);
	gtk_widget_set_sensitive (GTK_WIDGET (priv->find_menuitem),   state);
}

/*
 * Callbacks
 */

static void
show_about_cb (GtkMenuItem *item, gpointer data)
{
	show_about (GNCARD_MAIN_WINDOW (data));
}

/*
 * Setup
 */

static GnomeUIInfo
gncard_main_window_filemenu[] = {

	GNOMEUIINFO_MENU_NEW_ITEM (N_("New"), N_("Create a new card file"),
				   gncard_main_window_new_cb, NULL),

	GNOMEUIINFO_MENU_OPEN_ITEM (gncard_main_window_open_cb, NULL),

	{
		GNOME_APP_UI_ITEM,
		N_("_Insert..."), N_("Add the contents of another card file"),
		gncard_main_window_insert_cb, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GTK_STOCK_OPEN, 0, 0, NULL
	},

	{
		GNOME_APP_UI_ITEM,
		N_("Open _Default"), N_("Open the default file"),
		gncard_main_window_open_default_cb, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GTK_STOCK_OPEN, 0, 0, NULL
	},

	GNOMEUIINFO_MENU_SAVE_ITEM (gncard_main_window_save_cb, NULL),

	GNOMEUIINFO_MENU_SAVE_AS_ITEM (gncard_main_window_save_as_cb, NULL),

	GNOMEUIINFO_SEPARATOR,

	GNOMEUIINFO_MENU_QUIT_ITEM (gncard_main_window_quit_cb, NULL),

	GNOMEUIINFO_END
};

static GnomeUIInfo
gncard_main_window_sortradios[] = {
	{
		GNOME_APP_UI_ITEM,
		N_("By Card Name"), N_("Sort by card name"),
		gncard_main_window_sort_name_cb, NULL, NULL,
		GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL
	},
	
	{
		GNOME_APP_UI_ITEM,
		N_("By Family Name"), N_("Sort by last name"),
		gncard_main_window_sort_familyname_cb, NULL, NULL,
		GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL
	},
	
	{
		GNOME_APP_UI_ITEM,
		N_("By E-mail"), N_("Sort by e-mail address"),
		gncard_main_window_sort_email_cb, NULL, NULL,
		GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL
	},

	{
		GNOME_APP_UI_ITEM,
		N_("By Organization"), N_("Sort by organization"),
		gncard_main_window_sort_org_cb, NULL, NULL,
		GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL
	},
	
	{GNOME_APP_UI_ENDOFINFO}
};

static GnomeUIInfo
gncard_main_window_sortmenu[] = {
	GNOMEUIINFO_RADIOLIST (gncard_main_window_sortradios),
	GNOMEUIINFO_END
};

static GnomeUIInfo
gncard_main_window_editmenu[] = {
	{
		GNOME_APP_UI_ITEM,
		N_("_New card"), N_("Create new card"),
		gncard_main_window_new_card_cb, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GTK_STOCK_NEW, 0, 0, NULL
	},

	GNOMEUIINFO_MENU_PROPERTIES_ITEM (gncard_main_window_edit_card_cb, NULL),

	{
		GNOME_APP_UI_ITEM,
		N_("_Delete card"), N_("Remove the current card"),
		gncard_main_window_delete_card_cb, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GTK_STOCK_DELETE, 0, 0, NULL
	},
	
	{ GNOME_APP_UI_SEPARATOR },

	GNOMEUIINFO_MENU_FIND_ITEM (gncard_main_window_find_cb, NULL),

	{
		GNOME_APP_UI_SUBTREE,
		N_("Sort"), N_("Set sort criterium"),
		gncard_main_window_sortmenu, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GTK_STOCK_SORT_ASCENDING, 0, 0, NULL
	},
	
	GNOMEUIINFO_END
};

static GnomeUIInfo
gncard_main_window_settingsmenu[] = {
	GNOMEUIINFO_MENU_PREFERENCES_ITEM  (gncard_main_window_prefs_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo
gncard_main_window_helpmenu[] = {
	GNOMEUIINFO_HELP ("gnomecard"),
	GNOMEUIINFO_MENU_ABOUT_ITEM (show_about_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo
gncard_main_window_menu[] = {
        GNOMEUIINFO_MENU_FILE_TREE (gncard_main_window_filemenu),
	GNOMEUIINFO_MENU_EDIT_TREE (gncard_main_window_editmenu),
	GNOMEUIINFO_MENU_SETTINGS_TREE (gncard_main_window_settingsmenu),
	GNOMEUIINFO_MENU_HELP_TREE (gncard_main_window_helpmenu),
	GNOMEUIINFO_END
};


/* FIXME: save pane position in session */
static void
gncard_main_window_setup (GncardMainWindow *win)
{
	GncardMainWindowPriv *priv = win->priv;
	GtkWidget *widget;
	GtkWidget *paned, *scrollwin;
	GtkToolItem *item;
	GtkTreeModel *model;
	GtkTreeSelection *selection;

	/* Construct */

	gnome_app_construct (GNOME_APP (win), "GnomeCard", "");

	/* Status bar */

	/* The status bar must be created early so that other setup functions
	 * can connect their help texts to it.
	 */

	widget = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_USER);
	gnome_app_set_statusbar (GNOME_APP (win), widget);

	/* Menu */

	gnome_app_create_menus_with_data (GNOME_APP (win), gncard_main_window_menu, win);
	gnome_app_install_menu_hints (GNOME_APP (win), gncard_main_window_menu);

	/* FIXME: plain ugly */
	priv->save_menuitem   = gncard_main_window_filemenu[4].widget;
	priv->edit_menuitem   = gncard_main_window_editmenu[1].widget;
	priv->delete_menuitem = gncard_main_window_editmenu[2].widget;
	priv->find_menuitem   = gncard_main_window_editmenu[4].widget;

	/* Tool bar */

	widget = gtk_toolbar_new ();

	item = gtk_tool_button_new_from_stock (GTK_STOCK_OPEN);
	g_signal_connect (G_OBJECT (item), "clicked",
			  G_CALLBACK (gncard_main_window_open_cb), win);
	gtk_toolbar_insert (GTK_TOOLBAR (widget), item, -1);
	gtk_widget_show (GTK_WIDGET (item));

	priv->save_button =
		gtk_tool_button_new_from_stock (GTK_STOCK_SAVE);
	g_signal_connect (G_OBJECT (priv->save_button), "clicked",
			  G_CALLBACK (gncard_main_window_save_cb), win);
	gtk_toolbar_insert (GTK_TOOLBAR (widget), priv->save_button, -1);
	gtk_widget_show (GTK_WIDGET (priv->save_button));

	item = gtk_separator_tool_item_new ();
	gtk_toolbar_insert (GTK_TOOLBAR (widget), item, -1);
	gtk_widget_show (GTK_WIDGET (item));

	item = gtk_tool_button_new_from_stock (GTK_STOCK_NEW);
	g_signal_connect (G_OBJECT (item), "clicked",
			  G_CALLBACK (gncard_main_window_new_card_cb), win);
	gtk_toolbar_insert (GTK_TOOLBAR (widget), item, -1);
	gtk_widget_show (GTK_WIDGET (item));

	priv->edit_button =
		gtk_tool_button_new_from_stock (GTK_STOCK_PROPERTIES);
	g_signal_connect (G_OBJECT (priv->edit_button), "clicked",
			  G_CALLBACK (gncard_main_window_edit_card_cb), win);
	gtk_toolbar_insert (GTK_TOOLBAR (widget), priv->edit_button, -1);
	gtk_widget_show (GTK_WIDGET (priv->edit_button));

	priv->delete_button =
		gtk_tool_button_new_from_stock (GTK_STOCK_DELETE);
	g_signal_connect (G_OBJECT (priv->delete_button), "clicked",
			  G_CALLBACK (gncard_main_window_delete_card_cb), win);
	gtk_toolbar_insert (GTK_TOOLBAR (widget), priv->delete_button, -1);
	gtk_widget_show (GTK_WIDGET (priv->delete_button));

	item = gtk_separator_tool_item_new ();
	gtk_toolbar_insert (GTK_TOOLBAR (widget), item, -1);
	gtk_widget_show (GTK_WIDGET (item));

	priv->find_button =
		gtk_tool_button_new_from_stock (GTK_STOCK_FIND);
	g_signal_connect (G_OBJECT (priv->find_button), "clicked",
			  G_CALLBACK (gncard_main_window_find_cb), win);
	gtk_toolbar_insert (GTK_TOOLBAR (widget), priv->find_button, -1);
	gtk_widget_show (GTK_WIDGET (priv->find_button));

	gnome_app_set_toolbar (GNOME_APP (win), GTK_TOOLBAR (widget));

	/* Pane */

	paned = gtk_hpaned_new ();
	gtk_container_set_border_width (GTK_CONTAINER (paned), 12);
	gtk_widget_show (paned);
	gnome_app_set_contents (GNOME_APP (win), paned);

	/* Card list */

	scrollwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrollwin), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrollwin), GTK_POLICY_ALWAYS, GTK_POLICY_ALWAYS);
	gtk_widget_show (scrollwin);
	gtk_paned_pack1 (GTK_PANED (paned), scrollwin, TRUE, TRUE);

	priv->cardlist = cardlist_widget_new ();
	gtk_widget_show (priv->cardlist);
	gtk_container_add (GTK_CONTAINER (scrollwin), priv->cardlist);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->cardlist));
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (priv->cardlist));
	g_signal_connect_swapped (G_OBJECT (selection), "changed",
				  G_CALLBACK (gncard_main_window_list_selection_changed_cb), win);
	g_signal_connect_swapped (G_OBJECT (priv->cardlist), "row-activated",
				  G_CALLBACK (gncard_main_window_list_row_activated_cb), win);
	g_signal_connect_swapped (G_OBJECT (priv->cardlist), "vcard-changed",
				  G_CALLBACK (gncard_main_window_set_changed_cb), win);
	g_signal_connect_swapped (G_OBJECT (model), "row-changed",
				  G_CALLBACK (gncard_main_window_set_changed_cb), win);

	/* Card view */

	scrollwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrollwin), GTK_POLICY_ALWAYS, GTK_POLICY_ALWAYS);
	gtk_widget_show (scrollwin);
	gtk_paned_pack2 (GTK_PANED (paned), scrollwin, TRUE, TRUE);

	priv->canvas = card_display_new ();
	gtk_container_add (GTK_CONTAINER (scrollwin), priv->canvas);
	gtk_widget_show (priv->canvas);

	/* Finishing touches */

	gncard_main_window_update_title (win);
	gncard_main_window_set_changed (win, FALSE);
	gncard_main_window_sensitize_edit_widgets (win, FALSE);
}

/*
 * Public Methods
 */

GtkWidget *
gncard_main_window_new (void)
{
	GncardMainWindow *win;

	win = g_object_new (GNCARD_TYPE_MAIN_WINDOW, NULL);

	return GTK_WIDGET (win);
}

GtkWidget *
gncard_main_window_get_cardlist (GncardMainWindow *win)
{
	g_return_val_if_fail (win != NULL, NULL);
	g_return_val_if_fail (GNCARD_IS_MAIN_WINDOW (win), NULL);

	return win->priv->cardlist;
}

void
gncard_main_window_set_changed (GncardMainWindow *win, gboolean changed)
{
	g_return_if_fail (win != NULL);
	g_return_if_fail (GNCARD_IS_MAIN_WINDOW (win));

	win->priv->changed = changed;

	gtk_widget_set_sensitive (GTK_WIDGET (win->priv->save_button), changed);
	gtk_widget_set_sensitive (win->priv->save_menuitem, changed);
}

gboolean
gncard_main_window_get_changed (GncardMainWindow *win)
{
	g_return_val_if_fail (win != NULL, TRUE);
	g_return_val_if_fail (GNCARD_IS_MAIN_WINDOW (win), TRUE);

	return win->priv->changed ? TRUE : FALSE;
}

void
gncard_main_window_open (GncardMainWindow *win)
{
	GtkWidget *dialog;
	const gchar *filename;

	g_return_if_fail (win != NULL);
	g_return_if_fail (GNCARD_IS_MAIN_WINDOW (win));

	dialog = gtk_file_chooser_dialog_new (_("Open Address Book"),
					      GTK_WINDOW (win),
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_OPEN,   GTK_RESPONSE_ACCEPT,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      NULL);

	filename = cardlist_widget_get_filename (CARDLIST_WIDGET (win->priv->cardlist));
	if (filename)
		gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (dialog), filename);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		GError *error = NULL;
		gchar *fn;

		fn = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

		if (!gncard_main_window_open_file (win, fn, &error)) {
			gncard_ui_show_error (GTK_WINDOW (win), error);
			g_error_free (error);
		}

		g_free (fn);
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

gboolean
gncard_main_window_open_default (GncardMainWindow *win, GError **error)
{
	GError *err = NULL;
	gchar *filename;
	gboolean success;

	g_return_val_if_fail (win != NULL, FALSE);
	g_return_val_if_fail (GNCARD_IS_MAIN_WINDOW (win), FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	filename = gnome_config_get_real_path ("GnomeCard.gcrd");
	success = gncard_main_window_open_file (win, filename, &err);
	g_free (filename);

	if (!success) {
		if (err->domain == G_FILE_ERROR &&
		    err->code   == G_FILE_ERROR_NOENT) {
			success = TRUE;
			g_error_free (err);

			gncard_main_window_set_changed (win, FALSE);
			gncard_main_window_update_title (win);
		} else
			g_propagate_error (error, err);
	}

	return success;
}

gboolean
gncard_main_window_open_file (GncardMainWindow *win, const gchar *filename, GError **error)
{
	g_return_val_if_fail (win != NULL, FALSE);
	g_return_val_if_fail (GNCARD_IS_MAIN_WINDOW (win), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	if (!cardlist_widget_open_file (CARDLIST_WIDGET (win->priv->cardlist),
					filename, error))
		return FALSE;

	gncard_main_window_set_changed (win, FALSE);
	gncard_main_window_update_title (win);

	return TRUE;
}

void
gncard_main_window_append (GncardMainWindow *win)
{
	GtkWidget *dialog;
	const gchar *filename;

	g_return_if_fail (win != NULL);
	g_return_if_fail (GNCARD_IS_MAIN_WINDOW (win));

	/* FIXME: multi select */
	dialog = gtk_file_chooser_dialog_new (_("Append Address Book"),
					      GTK_WINDOW (win),
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_OPEN,   GTK_RESPONSE_OK,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      NULL);

	filename = cardlist_widget_get_filename (CARDLIST_WIDGET (win->priv->cardlist));
	if (filename)
		gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (dialog), filename);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		GError *error = NULL;
		gchar *fn;

		fn = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

		if (!gncard_main_window_append_file (win, fn, &error)) {
			gncard_ui_show_error (GTK_WINDOW (win), error);
			g_error_free (error);
		}

		g_free (fn);
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

gboolean
gncard_main_window_append_file (GncardMainWindow *win, const gchar *filename, GError **error)
{
	g_return_val_if_fail (win != NULL, FALSE);
	g_return_val_if_fail (GNCARD_IS_MAIN_WINDOW (win), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	if (!cardlist_widget_append_file (CARDLIST_WIDGET (win->priv->cardlist),
					  filename, error))
		return FALSE;

	return TRUE;
}

gboolean
gncard_main_window_save (GncardMainWindow *win, GError **error)
{
	g_return_val_if_fail (win != NULL, FALSE);
	g_return_val_if_fail (GNCARD_IS_MAIN_WINDOW (win), FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	if (!cardlist_widget_save (CARDLIST_WIDGET (win->priv->cardlist), error))
		return FALSE;

	gncard_main_window_set_changed (win, FALSE);

	return TRUE;
}

void
gncard_main_window_save_as (GncardMainWindow *win)
{
	GtkWidget *dialog;
	const gchar *filename;

	g_return_if_fail (win != NULL);
	g_return_if_fail (GNCARD_IS_MAIN_WINDOW (win));

	dialog = gtk_file_chooser_dialog_new (_("Save Address Book"),
					      GTK_WINDOW (win),
					      GTK_FILE_CHOOSER_ACTION_SAVE,
					      GTK_STOCK_SAVE,   GTK_RESPONSE_ACCEPT,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      NULL);

	filename = cardlist_widget_get_filename (CARDLIST_WIDGET (win->priv->cardlist));
	if (filename)
		gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (dialog), filename);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
		GError *error = NULL;
		gchar *fn;

		fn = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

		if (!gncard_main_window_save_file (win, fn, &error)) {
			gncard_ui_show_error (GTK_WINDOW (win), error);
			g_error_free (error);
		}

		g_free (fn);
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

gboolean
gncard_main_window_save_file (GncardMainWindow *win, const gchar *filename, GError **error)
{
	g_return_val_if_fail (win != NULL, FALSE);
	g_return_val_if_fail (GNCARD_IS_MAIN_WINDOW (win), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	if (!cardlist_widget_save_file (CARDLIST_WIDGET (win->priv->cardlist), filename, error))
		return FALSE;

	gncard_main_window_set_changed (win, FALSE);

	return TRUE;
}
