/* Miscellaneous GNOME Card UI Functions
 * Copyright (C) 2002, 2003, 2004  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <gconf/gconf-client.h>

#include "cardlist-widget.h"
#include "gnomecard-main-window.h"
#include "gnomecard-ui.h"


static GMainLoop *main_loop = NULL;
static GList *window_list = NULL;

/*
 * Error Handling
 */

static void
gncard_ui_message_handler (const gchar *message, GtkMessageType type)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new (NULL, 0, type,
					 GTK_BUTTONS_OK,
					 "%s", message);
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (gtk_widget_destroy), NULL);
	gtk_widget_show (dialog);
}

static void
gncard_ui_standard_message_handler (const gchar *message)
{
	gncard_ui_message_handler (message, GTK_MESSAGE_INFO);
}

static void
gncard_ui_error_message_handler (const gchar *message)
{
	gncard_ui_message_handler (message, GTK_MESSAGE_WARNING);
}

/*
 * Callbacks
 */

static gboolean
gncard_ui_window_closed_cb (GtkWidget *widget, GdkEvent *event, GMainLoop *main_loop)
{
	GncardMainWindow *win;
	GError *error = NULL;

	win = GNCARD_MAIN_WINDOW (widget);

	if (gncard_main_window_get_changed (win)) {
		GtkWidget *cardlist;
		const gchar *filename;

		cardlist = gncard_main_window_get_cardlist (win);
		filename = cardlist_widget_get_filename (CARDLIST_WIDGET (cardlist));

		switch (gncard_ui_save_confirmation (GTK_WINDOW (widget), filename)) {
		case GTK_RESPONSE_CANCEL:
			return TRUE;
		case GTK_RESPONSE_OK:
			if (!gncard_main_window_save (win, &error)) {
				gncard_ui_show_error (GTK_WINDOW (win), error);
				g_error_free (error);
				return TRUE;
			}
			break;
		case GTK_RESPONSE_CLOSE:
			break; // dummy
		}
	}

	window_list = g_list_remove (window_list, widget);

	if (window_list == NULL)
		g_main_loop_quit (main_loop);

	return FALSE;
}

static void
gncard_ui_quit_cb (GncardMainWindow *win, GMainLoop *main_loop)
{
	GList *node;
	gboolean changed = FALSE;

	for (node = window_list; node != NULL; node = g_list_next (node)) {
		win = GNCARD_MAIN_WINDOW (node->data);
		if (gncard_main_window_get_changed (win)) {
			changed = TRUE;
			break;
		}
	}

	if (changed) {
		gint response;

		response =
			gncard_ui_save_confirmation (GTK_WINDOW (window_list->data), NULL);

		switch (response) {
		case GTK_RESPONSE_CANCEL: // cancel
			return;

		case GTK_RESPONSE_OK: // save
			for (node = window_list; node != NULL; node = g_list_next (node)) {
				GError *error = NULL;

				win = GNCARD_MAIN_WINDOW (node->data);
				if (!gncard_main_window_save (win, &error)) {
					gncard_ui_show_error (GTK_WINDOW (win), error);
					g_error_free (error);
					return;
				}
			}

			break;

		case GTK_RESPONSE_CLOSE: // quit without saving
			break; // dummy
		}
	}

	g_main_loop_quit (main_loop);
}

/*
 * Main Loop
 */

void
gncard_ui_init (void)
{
	GConfClient *gconf;

	/* Error handling */

	g_set_print_handler    (gncard_ui_standard_message_handler);
	g_set_printerr_handler (gncard_ui_error_message_handler);

	gconf = gconf_client_get_default ();
	gconf_client_set_error_handling (gconf, GCONF_CLIENT_HANDLE_ALL);
	g_object_unref (G_OBJECT (gconf));

	/* Window icons */

	gnome_window_icon_set_default_from_file (GNOME_ICONDIR "/gnome-gnomecard.png");

	/* Main loop */

	main_loop = g_main_loop_new (NULL, FALSE);
}

void
gncard_ui_fini (void)
{
	g_main_loop_unref (main_loop);
	main_loop = NULL;
}

void
gncard_ui_run (void)
{
	g_main_loop_run (main_loop);
}

/*
 * Main Window
 */

GtkWidget *
gncard_ui_open_main_window (const gchar *path)
{
	GError *err = NULL;
	GtkWidget *win;

	win = gncard_main_window_new ();

	g_signal_connect (G_OBJECT (win), "delete_event",
			  G_CALLBACK (gncard_ui_window_closed_cb), main_loop);
	g_signal_connect (G_OBJECT (win), "quit",
			  G_CALLBACK (gncard_ui_quit_cb), main_loop);

	window_list = g_list_append (window_list, win);

	if (path)
		gncard_main_window_open_file (GNCARD_MAIN_WINDOW (win), path, &err);
	else
		gncard_main_window_open_default (GNCARD_MAIN_WINDOW (win), &err);

	gtk_widget_show (win);

	return win;
}

/*
 * Dialogs
 */

gint
gncard_ui_save_confirmation (GtkWindow *parent, const gchar *filename)
{
	GtkWidget *dialog;
	gchar *msg;
	gint response;

	if (!filename)
		msg = g_strdup (_("Save changes to address books before quitting?"));
	else if (filename[0] == '\0')
		msg = g_strdup (_("Save changes to default address book before quitting?"));
	else
		msg = g_strdup_printf (_("Save changes to address book \"%s\" before quitting?"), filename);

	dialog = gtk_message_dialog_new (parent,
					 GTK_DIALOG_MODAL,
					 GTK_MESSAGE_WARNING,
					 GTK_BUTTONS_NONE,
					 msg);

	g_free (msg);

	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				_("Quit without Saving"), GTK_RESPONSE_CLOSE,
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_SAVE, GTK_RESPONSE_OK,
				NULL);

	gtk_widget_show (dialog);
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);

	if (response == GTK_RESPONSE_NONE)
		response = GTK_RESPONSE_CANCEL;

	return response;
}

/*
 * Helper Functions
 */

void
gncard_ui_show_help (GtkWindow *parent, const gchar *anchor)
{
	GError *error = NULL;

	if (!gnome_help_display ("gnomecal", anchor, &error)) {
		GtkWidget *win;

		win = gtk_message_dialog_new (parent,
					      0,
					      GTK_MESSAGE_ERROR,
					      GTK_BUTTONS_OK,
					      _("Could not display help: %s"),
					      error->message);
		g_signal_connect (G_OBJECT (win), "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);
		gtk_widget_show (win);
		g_error_free (error);
	}
}

void
gncard_ui_show_error (GtkWindow *parent, GError *error)
{
	GtkWidget *dialog;

	g_return_if_fail (parent == NULL || GTK_IS_WINDOW (parent));
	g_return_if_fail (error != NULL);

	dialog = gtk_message_dialog_new (parent,
					 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
					 error->message);
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (gtk_widget_destroy), NULL);
	gtk_widget_show (dialog);
}
