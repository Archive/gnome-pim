/* GNOME Card Editor Phone Object
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <glade/glade.h>

#include <mimedir/mimedir-vcard.h>
#include <mimedir/mimedir-vcard-phone.h>

#include "cardeditor-phone.h"


/* FIXME: list reorderable */


struct _CardEditorPhonePriv {
	MIMEDirVCard	*vcard;

	GtkWidget	*treeview;
	GtkListStore	*model;

	GtkWidget	*number_entry;

	GtkWidget	*home_check;
	GtkWidget	*work_check;
	GtkWidget	*voice_check;
	GtkWidget	*fax_check;
	GtkWidget	*bbs_check;
	GtkWidget	*modem_check;
	GtkWidget	*pager_check;
	GtkWidget	*video_check;
	GtkWidget	*cell_check;
	GtkWidget	*car_check;
	GtkWidget	*isdn_check;
	GtkWidget	*pcs_check;
	GtkWidget	*pref_check;
	GtkWidget	*message_check;
};


static GtkHBoxClass *parent_class = NULL;


static void		card_editor_phone_class_init		(CardEditorPhoneClass *klass);
static void		card_editor_phone_init			(CardEditorPhone *phone);
static void		card_editor_phone_dispose		(GObject *object);

static gboolean		card_editor_phone_setup			(CardEditorPhone *phone);
static GtkTreeIter	card_editor_phone_internal_append	(CardEditorPhone *phone, MIMEDirVCardPhone *em);

static void		card_editor_phone_address_changed	(CardEditorPhone *phone, MIMEDirVCardPhone *em);

/*
 * Class and Object Management
 */

GType
card_editor_phone_get_type (void)
{
	static GType card_editor_phone_type = 0;

	if (!card_editor_phone_type) {
		static const GTypeInfo card_editor_phone_info = {
			sizeof (CardEditorPhoneClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) card_editor_phone_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CardEditorPhone),
			1,    /* n_preallocs */
			(GInstanceInitFunc) card_editor_phone_init,
		};

		card_editor_phone_type = g_type_register_static (CARD_EDITOR_TYPE_EDITLIST,
								 "CardEditorPhone",
								 &card_editor_phone_info,
								 0);
	}

	return card_editor_phone_type;
}


static void
card_editor_phone_class_init (CardEditorPhoneClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CARD_EDITOR_IS_PHONE_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = card_editor_phone_dispose;

	parent_class = g_type_class_peek_parent (klass);
}


static void
card_editor_phone_init (CardEditorPhone *widget)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (CARD_EDITOR_IS_PHONE (widget));

	widget->priv = g_new0 (CardEditorPhonePriv, 1);

	card_editor_phone_setup (widget);
}


static void
card_editor_phone_dispose (GObject *object)
{
	CardEditorPhone *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CARD_EDITOR_IS_PHONE (object));

	list = CARD_EDITOR_PHONE (object);

	if (list->priv) {
		if (list->priv->vcard)
			g_object_unref (G_OBJECT (list->priv->vcard));
		g_object_unref (G_OBJECT (list->priv->model));

		g_free (list->priv);
		list->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Callbacks
 */

static void
card_editor_phone_update_editor (CardEditorPhone *phone, CardEditorEditList *list)
{
	CardEditorPhonePriv *priv;

	GtkTreeSelection *selection;
	GtkTreeIter iter;
	GtkTreeModel *model;

	priv = phone->priv;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (phone->priv->treeview));

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardPhone *ph;
		gchar *number;
		gboolean home, work, voice, fax, bbs, modem, pager, video, cell, car, isdn, pcs, pref, message;

		gtk_tree_model_get (model, &iter, 0, &ph, -1);
		g_assert (MIMEDIR_IS_VCARD_PHONE (ph));

		g_object_get (G_OBJECT (ph),
			      "number",    &number,

			      "home",      &home,
			      "work",      &work,
			      "voice",     &voice,
			      "fax",       &fax,
			      "bbs",       &bbs,
			      "modem",     &modem,
			      "pager",     &pager,
			      "video",     &video,
			      "cell",      &cell,
			      "car",       &car,
			      "isdn",      &isdn,
			      "pcs",       &pcs,
			      "preferred", &pref,
			      "message",   &message,
			      NULL);

		gtk_entry_set_text (GTK_ENTRY (priv->number_entry), number ? number : "");
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->home_check),    home);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->work_check),    work);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->voice_check),   voice);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->fax_check),     fax);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->bbs_check),     bbs);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->modem_check),   modem);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->pager_check),   pager);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->video_check),   video);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->cell_check),    cell);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->car_check),     car);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->isdn_check),    isdn);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->pcs_check),     pcs);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->pref_check),    pref);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->message_check), message);

		g_free (number);
	}
}


static void
card_editor_phone_add_item (CardEditorPhone *phone, CardEditorEditList *list)
{
	MIMEDirVCardPhone *em;
	GtkTreeIter iter;
	GtkTreeSelection *selection;

	em = mimedir_vcard_phone_new ();

	mimedir_vcard_append_phone (phone->priv->vcard, em);

	iter = card_editor_phone_internal_append (phone, em);

	g_object_unref (G_OBJECT (em));

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (phone->priv->treeview));
	gtk_tree_selection_select_iter (selection, &iter);

	gtk_widget_grab_focus (phone->priv->number_entry);
}


static void
card_editor_phone_remove_item (CardEditorPhone *phone, CardEditorEditList *list)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (phone->priv->treeview));

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardPhone *ph;

		gtk_tree_model_get (model, &iter, 0, &ph, -1);

		g_assert (ph != NULL && MIMEDIR_IS_VCARD_PHONE (ph));

		g_signal_handlers_disconnect_by_func (G_OBJECT (ph),
						      card_editor_phone_address_changed,
						      phone);

		if (gtk_list_store_remove (GTK_LIST_STORE (model), &iter)) {
			gtk_tree_selection_select_iter (selection, &iter);
		} else {
			/* Select last element */

			gint n;

			n = gtk_tree_model_iter_n_children (model, NULL);
			if (n > 0) {
				gtk_tree_model_iter_nth_child (model, &iter, NULL, n - 1);
				gtk_tree_selection_select_iter (selection, &iter);
			}
		}
		mimedir_vcard_remove_phone (phone->priv->vcard, ph);

		gtk_widget_grab_focus (phone->priv->number_entry);
	}
}


static void
card_editor_phone_render_item_cb (GtkTreeViewColumn *column,
				  GtkCellRenderer *renderer,
				  GtkTreeModel *model,
				  GtkTreeIter *iter,
				  gpointer data)
{
	MIMEDirVCardPhone *phone;
	gchar *number;

	gtk_tree_model_get (model, iter, 0, &phone, -1);
	g_assert (phone != NULL && MIMEDIR_IS_VCARD_PHONE (phone));

	g_object_get (G_OBJECT (phone), "number", &number, NULL);
	g_object_set (G_OBJECT (renderer), "text", number, NULL);
	g_free (number);
}


static void
card_editor_phone_entry_updated (CardEditorPhone *phone, GtkEntry *entry, const gchar *prop)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (phone->priv->treeview));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardPhone *phone;
		const gchar *text;

		text = gtk_entry_get_text (entry);

		gtk_tree_model_get (model, &iter, 0, &phone, -1);
		g_assert (phone != NULL && MIMEDIR_IS_VCARD_PHONE (phone));
		g_object_set (G_OBJECT (phone), prop, text, NULL);
	}
}


static void
card_editor_phone_number_updated (CardEditorPhone *phone, GtkEntry *entry)
{
	card_editor_phone_entry_updated (phone, entry, "number");
}


static void
card_editor_phone_toggle_updated (CardEditorPhone *phone, GtkToggleButton *button)
{
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (phone->priv->treeview));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVCardPhone *ph;
		const gchar *prop;
		gboolean b;

		b = gtk_toggle_button_get_active (button);
		prop = g_object_get_data (G_OBJECT (button), "property");
		g_assert (prop != NULL);

		gtk_tree_model_get (model, &iter, 0, &ph, -1);
		g_assert (ph != NULL && MIMEDIR_IS_VCARD_PHONE (ph));
		g_object_set (G_OBJECT (ph), prop, b, NULL);
	}
}


static gboolean
card_editor_phone_address_changed_fe (GtkTreeModel *model,
				      GtkTreePath  *path,
				      GtkTreeIter  *iter,
				      gpointer      data)
{
	MIMEDirVCardPhone *phone;

	g_return_val_if_fail (MIMEDIR_IS_VCARD_PHONE (data), FALSE);

	gtk_tree_model_get (model, iter, 0, &phone, -1);
	g_assert (MIMEDIR_IS_VCARD_PHONE (phone));

	if (phone == data)
		gtk_tree_model_row_changed (model, path, iter);

	return FALSE;
}


static void
card_editor_phone_address_changed (CardEditorPhone *phone, MIMEDirVCardPhone *em)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL (phone->priv->model), card_editor_phone_address_changed_fe, em);
}

/*
 * Internal Methods
 */

static gboolean
card_editor_phone_setup (CardEditorPhone *phone)
{
	CardEditorPhonePriv *priv;
	GladeXML *xml;
	GtkWidget *w, *tree;
	GtkCellRenderer *renderer;

	priv = phone->priv;

	/* Internal signals */

	g_signal_connect_swapped (G_OBJECT (phone), "selection-changed", G_CALLBACK (card_editor_phone_update_editor), phone);
	g_signal_connect_swapped (G_OBJECT (phone), "add-item", G_CALLBACK (card_editor_phone_add_item), phone);
	g_signal_connect_swapped (G_OBJECT (phone), "remove-item", G_CALLBACK (card_editor_phone_remove_item), phone);

	/* Tree view */

	priv->model = gtk_list_store_new (2, G_TYPE_OBJECT, G_TYPE_ULONG);
	tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (priv->model));
	priv->treeview = tree;
	gtk_tree_view_set_reorderable (GTK_TREE_VIEW (tree), TRUE);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree), FALSE);
	gtk_widget_show (tree);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (tree),
						    0, NULL, renderer,
						    card_editor_phone_render_item_cb, phone, NULL);

	card_editor_editlist_set_list (CARD_EDITOR_EDITLIST (phone), GTK_TREE_VIEW (tree));

	/* Editor */

	xml = glade_xml_new (GLADEDIR "/card-editor.glade", "phone_box", GETTEXT_PACKAGE);
	if (!xml) {
		g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/card-editor.glade");
		return FALSE;
	}

	glade_xml_signal_autoconnect (xml);

	w = glade_xml_get_widget (xml, "phone_box");

	priv->number_entry  = glade_xml_get_widget (xml, "number_entry");

	priv->home_check    = glade_xml_get_widget (xml, "home_check");
	priv->work_check    = glade_xml_get_widget (xml, "work_check");
	priv->voice_check   = glade_xml_get_widget (xml, "voice_check");
	priv->fax_check     = glade_xml_get_widget (xml, "fax_check");
	priv->bbs_check     = glade_xml_get_widget (xml, "bbs_check");
	priv->modem_check   = glade_xml_get_widget (xml, "modem_check");
	priv->pager_check   = glade_xml_get_widget (xml, "pager_check");
	priv->video_check   = glade_xml_get_widget (xml, "video_check");
	priv->cell_check    = glade_xml_get_widget (xml, "cell_check");
	priv->car_check     = glade_xml_get_widget (xml, "car_check");
	priv->isdn_check    = glade_xml_get_widget (xml, "isdn_check");
	priv->pcs_check     = glade_xml_get_widget (xml, "pcs_check");
	priv->pref_check    = glade_xml_get_widget (xml, "pref_check");
	priv->message_check = glade_xml_get_widget (xml, "message_check");

	if (!w ||
	    !priv->number_entry ||
	    !priv->home_check   ||
	    !priv->work_check   ||
	    !priv->voice_check  ||
	    !priv->fax_check    ||
	    !priv->bbs_check    ||
	    !priv->modem_check  ||
	    !priv->pager_check  ||
	    !priv->video_check  ||
	    !priv->cell_check   ||
	    !priv->car_check    ||
	    !priv->isdn_check   ||
	    !priv->pcs_check    ||
	    !priv->pref_check   ||
	    !priv->message_check) {
		g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
		return FALSE;
	}

	g_signal_connect_swapped (priv->number_entry, "changed", G_CALLBACK (card_editor_phone_number_updated),  phone);

	g_object_set_data (G_OBJECT (priv->home_check),    "property", "home");
	g_object_set_data (G_OBJECT (priv->work_check),    "property", "work");
	g_object_set_data (G_OBJECT (priv->voice_check),   "property", "voice");
	g_object_set_data (G_OBJECT (priv->fax_check),     "property", "fax");
	g_object_set_data (G_OBJECT (priv->bbs_check),     "property", "bbs");
	g_object_set_data (G_OBJECT (priv->modem_check),   "property", "modem");
	g_object_set_data (G_OBJECT (priv->pager_check),   "property", "pager");
	g_object_set_data (G_OBJECT (priv->video_check),   "property", "video");
	g_object_set_data (G_OBJECT (priv->cell_check),    "property", "cell");
	g_object_set_data (G_OBJECT (priv->car_check),     "property", "car");
	g_object_set_data (G_OBJECT (priv->isdn_check),    "property", "isdn");
	g_object_set_data (G_OBJECT (priv->pcs_check),     "property", "pcs");
	g_object_set_data (G_OBJECT (priv->pref_check),    "property", "preferred");
	g_object_set_data (G_OBJECT (priv->message_check), "property", "message");

	g_signal_connect_swapped (G_OBJECT (priv->home_check),    "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->work_check),    "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->voice_check),   "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->fax_check),     "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->bbs_check),     "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->modem_check),   "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->pager_check),   "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->video_check),   "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->cell_check),    "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->car_check),     "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->isdn_check),    "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->pcs_check),     "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->pref_check),    "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);
	g_signal_connect_swapped (G_OBJECT (priv->message_check), "toggled", G_CALLBACK (card_editor_phone_toggle_updated), phone);

	card_editor_editlist_set_editor (CARD_EDITOR_EDITLIST (phone), w);
	g_object_unref (G_OBJECT (xml));

	gtk_widget_grab_focus (priv->number_entry);

	return TRUE;
}


static GtkTreeIter
card_editor_phone_internal_append (CardEditorPhone *phone, MIMEDirVCardPhone *em)
{
	GtkTreeIter iter;

	g_signal_connect_swapped (G_OBJECT (em), "changed", G_CALLBACK (card_editor_phone_address_changed), phone);

	gtk_list_store_append (phone->priv->model, &iter);
	gtk_list_store_set (phone->priv->model, &iter, 0, em, -1);

	return iter;
}

/*
 * External Methods
 */

GtkWidget *
card_editor_phone_new (void)
{
	CardEditorPhone *phone;

	phone = g_object_new (CARD_EDITOR_TYPE_PHONE, NULL);

	return GTK_WIDGET (phone);
}

void
card_editor_phone_edit_vcard (CardEditorPhone *phone, MIMEDirVCard *vcard)
{
	CardEditorPhonePriv *priv;
	GSList *list, *l;

	g_return_if_fail (phone != NULL);
	g_return_if_fail (CARD_EDITOR_IS_PHONE (phone));
	g_return_if_fail (vcard != NULL);
	g_return_if_fail (MIMEDIR_IS_VCARD (vcard));

	priv = phone->priv;

	if (priv->vcard)
		g_object_unref (G_OBJECT (priv->vcard));
	g_object_ref (vcard);
	priv->vcard = vcard;

	g_object_get (G_OBJECT (vcard), "phone_list", &list, NULL);

	for (l = list; l != NULL; l = g_slist_next (l)) {
		MIMEDirVCardPhone *em;

		em = MIMEDIR_VCARD_PHONE (l->data);
		card_editor_phone_internal_append (phone, em);
	}

	if (list) {
		GtkTreeSelection *selection;
		GtkTreePath *path;

		selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->treeview));

		path = gtk_tree_path_new_from_string ("0");
		gtk_tree_selection_select_path (selection, path);
		gtk_tree_path_free (path);
	}
}
