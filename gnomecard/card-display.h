#ifndef __CARD_DISPLAY_H__
#define __CARD_DISPLAY_H__

/* GNOME Card Display Object
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <libgnomecanvas/gnome-canvas.h>

#include <mimedir/mimedir-vcard.h>


#define CARD_TYPE_DISPLAY		(card_display_get_type())
#define CARD_DISPLAY(obj)		(GTK_CHECK_CAST ((obj), CARD_TYPE_DISPLAY, CardDisplay))
#define CARD_DISPLAY_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CARD_TYPE_DISPLAY))
#define CARD_IS_DISPLAY(obj)		(GTK_CHECK_TYPE ((obj), CARD_TYPE_DISPLAY))
#define CARD_IS_DISPLAY_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CARD_TYPE_DISPLAY))

typedef struct _CardDisplay		CardDisplay;
typedef struct _CardDisplayClass	CardDisplayClass;
typedef struct _CardDisplayPriv		CardDisplayPriv;

struct _CardDisplay
{
	GnomeCanvas parent;

	CardDisplayPriv *priv;
};

struct _CardDisplayClass
{
	GnomeCanvasClass parent_class;
};

GType		 card_display_get_type		(void);
GtkWidget	*card_display_new		(void);

void		 card_display_set_card		(CardDisplay *display,
						 MIMEDirVCard *card);
MIMEDirVCard	*card_display_get_card		(CardDisplay *display);

#endif /* __CARD_DISPLAY_H__ */
