/* GNOME Card Preferences Dialog Object
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* FIXME: list reorderable */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>

#include <glib.h>

#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include <gnome.h>

#include "cardlist-headers.h"
#include "gnomecard-config.h"
#include "gnomecard-prefs.h"
#include "gnomecard-ui.h"


struct _GncardPrefsPriv {
	GtkListStore *store;

	guint notify;
};

enum {
	COLUMN_CHECK, /* G_TYPE_BOOLEAN */
	COLUMN_TEXT,  /* G_TYPE_STRING  */
	COLUMN_NAME,  /* G_TYPE_STRING  */
	COLUMN_COUNT
};


static GtkDialogClass *parent_class = NULL;


static void gncard_prefs_class_init	(GncardPrefsClass *dialog);
static void gncard_prefs_init		(GncardPrefs *dialog);
static void gncard_prefs_dispose	(GObject *object);
static void gncard_prefs_finalize	(GObject *object);

static void gncard_prefs_setup		(GncardPrefs *dialog);

static void gncard_prefs_set_column_header_list	(GncardPrefs *prefs, GSList *list);
static void gncard_prefs_unset_all_columns	(GncardPrefs *prefs);
static void gncard_prefs_activate_column_header_by_name
						(GncardPrefs *prefs, const gchar *col);

/*
 * Class and Object Management
 */

GType
gncard_prefs_get_type (void)
{
	static GType gncard_prefs_type = 0;

	if (!gncard_prefs_type) {
		static const GTypeInfo gncard_prefs_info = {
			sizeof (GncardPrefsClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) gncard_prefs_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (GncardPrefs),
			1,    /* n_preallocs */
			(GInstanceInitFunc) gncard_prefs_init,
		};

		gncard_prefs_type = g_type_register_static (GTK_TYPE_DIALOG,
							    "GncardPrefs",
							    &gncard_prefs_info,
							    0);
	}

	return gncard_prefs_type;
}

static void
gncard_prefs_class_init (GncardPrefsClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (GNCARD_IS_PREFS_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose  = gncard_prefs_dispose;
	gobject_class->finalize = gncard_prefs_finalize;

	parent_class = g_type_class_peek_parent (klass);
}

static void
gncard_prefs_init (GncardPrefs *dialog)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCARD_IS_PREFS (dialog));

	dialog->priv = g_new0 (GncardPrefsPriv, 1);

	gncard_prefs_setup (dialog);
}

static void
gncard_prefs_dispose (GObject *object)
{
	GncardPrefs *dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNCARD_IS_PREFS (object));

	dialog = GNCARD_PREFS (object);

	if (dialog->priv->store) {
		g_object_unref (G_OBJECT (dialog->priv->store));
		dialog->priv->store = NULL;
	}
	if (dialog->priv->notify) {
		GConfClient *gconf;

		gconf = gconf_client_get_default ();

		gconf_client_notify_remove (gconf, dialog->priv->notify);
		dialog->priv->notify = 0;

		g_object_unref (G_OBJECT (gconf));
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gncard_prefs_finalize (GObject *object)
{
	GncardPrefs *dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNCARD_IS_PREFS (object));

	dialog = GNCARD_PREFS (object);

	g_free (dialog->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*
 * Callbacks
 */

static void
gncard_prefs_response (GtkDialog *dialog, gint response, gpointer data)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GTK_IS_DIALOG (dialog));

	switch (response) {
	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;
	case GTK_RESPONSE_DELETE_EVENT:
		break;
	case GTK_RESPONSE_HELP:
		gncard_ui_show_help (GTK_WINDOW (dialog), "properties");
		break;
	default:
		g_return_if_reached ();
	}
}

static gboolean
gncard_prefs_append_selected_column_name (GtkTreeModel *model,
					  GtkTreePath  *path,
					  GtkTreeIter  *iter,
					  gpointer      data)
{
	gboolean  checked;
	gchar    *name;

	gtk_tree_model_get (model, iter,
			    COLUMN_CHECK, &checked,
			    COLUMN_NAME,  &name,
			    -1);

	if (checked) {
		GSList **list = (GSList **) data;

		*list = g_slist_append (*list, name);
	} else
		g_free (name);

	return FALSE;
}

static GSList *
gncard_prefs_get_selected_column_names (GncardPrefs *prefs)
{
	GncardPrefsPriv *priv;

	GSList *list = NULL;

	priv = prefs->priv;

	gtk_tree_model_foreach (GTK_TREE_MODEL (priv->store),
				gncard_prefs_append_selected_column_name,
				&list);

	return list;
}

static void
gncard_prefs_column_toggled (GtkCellRendererToggle *renderer,
			     gchar *path_str,
			     GncardPrefs *prefs)
{
	GtkListStore *store;
	GtkTreePath  *path;
	GtkTreeIter   iter;
	GConfClient  *gconf;

	GSList *selected_columns, *item;

	gboolean b;

	g_return_if_fail (prefs != NULL);
	g_return_if_fail (GNCARD_IS_PREFS (prefs));

	store = prefs->priv->store;

	path = gtk_tree_path_new_from_string (path_str);
	gtk_tree_model_get_iter (GTK_TREE_MODEL (store), &iter, path);
	gtk_tree_path_free (path);

	gtk_tree_model_get (GTK_TREE_MODEL (store), &iter, COLUMN_CHECK, &b, -1);
	b = b ? FALSE : TRUE;
	gtk_list_store_set (store, &iter, COLUMN_CHECK, b, -1);

	/* Save configuration */

	selected_columns = gncard_prefs_get_selected_column_names (prefs);

	gconf = gconf_client_get_default ();
	gconf_client_set_list (gconf, CONFIG_UI_COLUMN_HEADERS,
			       GCONF_VALUE_STRING, selected_columns, NULL);
	g_object_unref (G_OBJECT (gconf));

	/* Free list */

	for (item = selected_columns; item != NULL; item = g_slist_next (item))
		g_free (item->data);
	g_slist_free (selected_columns);
}

static void
gncard_prefs_column_headers_changed (GConfClient *gconf,
				     guint        cnxn_id,
				     GConfEntry  *entry,
				     gpointer     data)
{
	GncardPrefs *prefs;
	const gchar *key;

	g_return_if_fail (data != NULL);
	g_return_if_fail (GNCARD_IS_PREFS (data));

	prefs = GNCARD_PREFS (data);

	key = gconf_entry_get_key (entry);

	if (!strcmp (key, CONFIG_UI_COLUMN_HEADERS)) {
		GConfValue *value;

		value = gconf_entry_get_value (entry);

		if (value->type == GCONF_VALUE_LIST && gconf_value_get_list_type (value) == GCONF_VALUE_STRING) {
			GSList *list;

			/* Unselect all rows */

			gncard_prefs_unset_all_columns (prefs);

			/* Select rows */

			list = gconf_value_get_list (value);
			for (; list != NULL; list = g_slist_next (list)) {
				const gchar *s;

				s = gconf_value_get_string ((GConfValue *) list->data);

				gncard_prefs_activate_column_header_by_name (prefs, s);
			}
		}
	}
}

/*
 * Internal Methods
 */

static gboolean
gncard_prefs_activate_column_if_name (GtkTreeModel *model,
				      GtkTreePath  *path,
				      GtkTreeIter  *iter,
				      gpointer      data)
{
	gboolean ret = FALSE;
	gchar *name;

	gtk_tree_model_get (model, iter, COLUMN_NAME, &name, -1);

	if (!g_ascii_strcasecmp (name, (const gchar *) data)) {
		gtk_list_store_set (GTK_LIST_STORE (model), iter, COLUMN_CHECK, TRUE, -1);
		ret = TRUE;
	}

	g_free (name);

	return ret;
}

static void
gncard_prefs_activate_column_header_by_name (GncardPrefs *prefs,
					     const gchar *col)
{
	GncardPrefsPriv *priv = prefs->priv;

	gtk_tree_model_foreach (GTK_TREE_MODEL (priv->store),
				gncard_prefs_activate_column_if_name,
				(gpointer) col);
}

static gboolean
gncard_prefs_unset_column (GtkTreeModel *model,
			   GtkTreePath  *path,
			   GtkTreeIter  *iter,
			   gpointer      data)
{
	gtk_list_store_set (GTK_LIST_STORE (model), iter,
			    COLUMN_CHECK, FALSE, -1);

	return FALSE;
}

static void
gncard_prefs_unset_all_columns (GncardPrefs *prefs)
{
	gtk_tree_model_foreach (GTK_TREE_MODEL (prefs->priv->store),
				gncard_prefs_unset_column, NULL);

}

static void
gncard_prefs_set_column_header_list (GncardPrefs *prefs, GSList *list)
{
	GncardPrefsPriv *priv;
	GSList *item;

	priv = prefs->priv;

	/* Unselect all rows */

	gncard_prefs_unset_all_columns (prefs);

	/* Select some rows */

	for (item = list; item != NULL; item = g_slist_next (item))
		gncard_prefs_activate_column_header_by_name (prefs, item->data);
}

static void
gncard_prefs_load (GncardPrefs *prefs)
{
	GncardPrefsPriv *priv;
	GConfClient *gconf;
	GSList *list, *tmp;

	priv = prefs->priv;

	/* Unset columns */

	gtk_tree_model_foreach (GTK_TREE_MODEL (priv->store),
				gncard_prefs_unset_column, NULL);

	/* Set columns */

	gconf = gconf_client_get_default ();

	gconf_client_add_dir (gconf, CONFIG_DIR,
			      GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);

	list = gconf_client_get_list (gconf, CONFIG_UI_COLUMN_HEADERS,
				      GCONF_VALUE_STRING, NULL);

	gncard_prefs_set_column_header_list (prefs, list);

	for (tmp = list; tmp != NULL; tmp = g_slist_next (tmp))
		g_free (tmp->data);
	g_slist_free (list);

	/* Notifications */

	priv->notify = gconf_client_notify_add (gconf,
						CONFIG_UI_COLUMN_HEADERS,
						gncard_prefs_column_headers_changed,
						prefs,
						NULL, NULL);

	g_object_unref (G_OBJECT (gconf));
}

/*
 * Setup
 */

static void
gncard_prefs_setup_column_list (GncardPrefs *prefs)
{
	CardListHeader *header;
	gint i;

	for (i = 0, header = cardlist_header_list; header->name != NULL; i++, header++) {
		GtkTreeIter   iter;

		gtk_list_store_append (prefs->priv->store, &iter);
		gtk_list_store_set (prefs->priv->store, &iter,
				    COLUMN_TEXT,  _(header->title),
				    COLUMN_CHECK, FALSE,
				    COLUMN_NAME,  header->name,
				    -1);
	}
}

static GtkWidget *
gncard_prefs_setup_layout_page (GncardPrefs *dialog)
{
	GncardPrefsPriv *priv;

	GtkWidget *page;
	GtkWidget *list_label;
	GtkWidget *sw;
	GtkWidget *tree;

	GtkCellRenderer *renderer;
	GtkTreeSelection *selection;

	priv = dialog->priv;

	page = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (page);

	list_label = gtk_label_new_with_mnemonic (_("Displayed c_olumns"));
	gtk_widget_show (list_label);
	gtk_box_pack_start (GTK_BOX (page), list_label, FALSE, FALSE, 0);

	/* List */

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_set_size_request (sw, 250, 300);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
					     GTK_SHADOW_IN);
	gtk_widget_show (sw);
	gtk_box_pack_start (GTK_BOX (page), sw, TRUE, TRUE, 0);

	priv->store = gtk_list_store_new (COLUMN_COUNT, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_STRING);
	gncard_prefs_setup_column_list (dialog);

	tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (priv->store));
        gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree), FALSE);

	renderer = gtk_cell_renderer_toggle_new ();
	g_signal_connect (G_OBJECT (renderer), "toggled",
			  G_CALLBACK (gncard_prefs_column_toggled), dialog);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree),
						     -1, NULL,
						     renderer,
						     "active", COLUMN_CHECK,
						     NULL);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree),
						     -1, NULL,
						     renderer,
						     "text", COLUMN_TEXT,
						     NULL);

        selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
        gtk_tree_selection_set_mode (selection, GTK_SELECTION_NONE);

	gtk_widget_show (tree);
	gtk_container_add (GTK_CONTAINER (sw), tree);

	gtk_label_set_mnemonic_widget (GTK_LABEL (list_label), tree);

	return page;
}

static void
gncard_prefs_setup (GncardPrefs *dialog)
{
	GtkWidget *widget;
	GtkWidget *notebook;

	gtk_window_set_title (GTK_WINDOW (dialog), _("GNOME Card Preferences"));

	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				GTK_STOCK_HELP,  GTK_RESPONSE_HELP,
				GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
				NULL);
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (gncard_prefs_response), NULL);

	/* Notebook */

	notebook = gtk_notebook_new ();
	gtk_container_set_border_width (GTK_CONTAINER (notebook), 6);
	gtk_widget_show (notebook);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), notebook);

	widget = gncard_prefs_setup_layout_page (dialog);
	gtk_container_set_border_width (GTK_CONTAINER (widget), 12);
	gtk_widget_show (widget);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), widget,
				  gtk_label_new (_("Layout")));

	gncard_prefs_load (dialog);
}

/*
 * Public Methods
 */

GtkWidget *
gncard_prefs_new (GtkWindow *parent)
{
	GncardPrefs *dialog;

	g_return_val_if_fail (parent == NULL || GTK_IS_WINDOW (parent), NULL);

	dialog = g_object_new (GNCARD_TYPE_PREFS, NULL);

	if (parent)
		gtk_window_set_transient_for (GTK_WINDOW (dialog), parent);

	return GTK_WIDGET (dialog);
}
