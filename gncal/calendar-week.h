#ifndef __CALENDAR_WEEK_H__
#define __CALENDAR_WEEK_H__

/* Calendar Week Object
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtkvbox.h>


#define CALENDAR_TYPE_WEEK		(calendar_week_get_type())
#define CALENDAR_WEEK(obj)		(GTK_CHECK_CAST ((obj), CALENDAR_TYPE_WEEK, CalendarWeek))
#define CALENDAR_WEEK_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_WEEK))
#define CALENDAR_IS_WEEK(obj)		(GTK_CHECK_TYPE ((obj), CALENDAR_TYPE_WEEK))
#define CALENDAR_IS_WEEK_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_WEEK))

typedef struct _CalendarWeek		CalendarWeek;
typedef struct _CalendarWeekClass	CalendarWeekClass;
typedef struct _CalendarWeekPriv	CalendarWeekPriv;

struct _CalendarWeek
{
	GtkVBox parent;

	CalendarWeekPriv *priv;
};

struct _CalendarWeekClass
{
	GtkVBoxClass parent_class;

	void (* day_changed)  (CalendarWeek *cw, guint year, guint month, guint day);
	void (* day_selected) (CalendarWeek *cw, guint year, guint month, guint day);
	void (* day_menu)     (CalendarWeek *cw, guint year, guint month, guint day);
};

GType		 calendar_week_get_type	(void);
GtkWidget	*calendar_week_new		(void);

void		 calendar_week_set_date	(CalendarWeek *cm, const GDate *date);
void		 calendar_week_get_date	(CalendarWeek *cm, GDate *date);

#endif /* __CALENDAR_WEEK_H__ */
