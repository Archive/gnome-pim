#ifndef __CALENDAR_DAY_VIEW_H__
#define __CALENDAR_DAY_VIEW_H__

/* Calendar Day View Object
 * Copyright (C) 2003-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* This class provides a widget that contains a complete day view. */

#include <time.h>

#include <gtk/gtkvbox.h>

#include "calendar-manager.h"


#define CALENDAR_TYPE_DAY_VIEW			(calendar_day_view_get_type())
#define CALENDAR_DAY_VIEW(obj)			(GTK_CHECK_CAST ((obj), CALENDAR_TYPE_DAY_VIEW, CalendarDayView))
#define CALENDAR_DAY_VIEW_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_DAY_VIEW))
#define CALENDAR_IS_DAY_VIEW(obj)		(GTK_CHECK_TYPE ((obj), CALENDAR_TYPE_DAY_VIEW))
#define CALENDAR_IS_DAY_VIEW_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_DAY_VIEW))

typedef struct _CalendarDayView		CalendarDayView;
typedef struct _CalendarDayViewClass	CalendarDayViewClass;
typedef struct _CalendarDayViewPriv	CalendarDayViewPriv;

struct _CalendarDayView
{
	GtkContainer parent;

	CalendarDayViewPriv *priv;
};

struct _CalendarDayViewClass
{
	GtkContainerClass parent_class;
};

GType		 calendar_day_view_get_type	(void);

GtkWidget	*calendar_day_view_new		(void);
void		 calendar_day_view_set_calendar	(CalendarDayView *cdv, CalendarManager *calendar);
void		 calendar_day_view_set_date	(CalendarDayView *cdv, GDate *date);

gint		 calendar_day_view_get_y_pos_for_time	(CalendarDayView *cdv, gint time);

#endif /* __CALENDAR_DAY_VIEW_H__ */
