/* Miscellaneous GNOME Calendar UI Functions
 * Copyright (C) 2002, 2003, 2004  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <libgnomeui/gnome-window-icon.h>

#include "gnomecal-main-window.h"
#include "gnomecal-ui.h"

/*
 * Window Handling
 */

static guint window_count = 0;

static void
gncal_ui_main_window_closed_cb (GncalMainWindow *window, GMainLoop *main_loop)
{
	if (--window_count == 0)
		g_main_loop_quit (main_loop);
}

static void
gncal_ui_quit_cb (GncalMainWindow *window, GMainLoop *main_loop)
{
	g_main_loop_quit (main_loop);
}

static void
gncal_ui_open_new_window_cb (GncalMainWindow *window,
			     const gchar *file,
			     GMainLoop *main_loop)
{
	GtkWidget *win;

	win = gncal_ui_open_main_window (main_loop);

	if (file) {
		GError *error = NULL;

		gncal_main_window_set_calendar (GNCAL_MAIN_WINDOW (win), file, &error);

		if (error) {
			gncal_ui_show_error (GTK_WINDOW (window), error);
			gtk_widget_destroy (win);
			return;
		}
	}

	gtk_widget_show (win);
}

GtkWidget *
gncal_ui_open_main_window (GMainLoop *main_loop)
{
	GtkWidget *window;

	window = gncal_main_window_new ();

	window_count++;

	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (gncal_ui_main_window_closed_cb), main_loop);
	g_signal_connect (G_OBJECT (window), "quit",
			  G_CALLBACK (gncal_ui_quit_cb), main_loop);
	g_signal_connect (G_OBJECT (window), "open-new",
			  G_CALLBACK (gncal_ui_open_new_window_cb), main_loop);

	return window;
}

/*
 * Error Handling
 */

static void
gncal_ui_message_handler (const gchar *message, GtkMessageType type)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new (NULL, 0, type,
					 GTK_BUTTONS_OK,
					 "%s", message);
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (gtk_widget_destroy), NULL);
	gtk_widget_show (dialog);
}

static void
gncal_ui_standard_message_handler (const gchar *message)
{
	gncal_ui_message_handler (message, GTK_MESSAGE_INFO);
}

static void
gncal_ui_error_message_handler (const gchar *message)
{
	gncal_ui_message_handler (message, GTK_MESSAGE_WARNING);
}

/*
 * Setup
 */

void
gncal_ui_init (void)
{
	GConfClient *gconf;

	/* Error handling */

	g_set_print_handler    (gncal_ui_standard_message_handler);
	g_set_printerr_handler (gncal_ui_error_message_handler);

	gconf = gconf_client_get_default ();
	gconf_client_set_error_handling (gconf, GCONF_CLIENT_HANDLE_ALL);
	g_object_unref (G_OBJECT (gconf));

	/* Window icons */

	gnome_window_icon_set_default_from_file (GNOME_ICONDIR "/gnome-calendar.png");
}

/*
 * Error Handling
 */

void
gncal_ui_show_error (GtkWindow *parent, GError *error)
{
	GtkWidget *dialog;

	g_return_if_fail (parent == NULL || GTK_IS_WINDOW (parent));
	g_return_if_fail (error != NULL);

	dialog = gtk_message_dialog_new (parent,
					 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
					 error->message);
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (gtk_widget_destroy), NULL);
	gtk_widget_show (dialog);
}
