#ifndef __CALENDAR_MANAGER_H__
#define __CALENDAR_MANAGER_H__

/* Calendar Manager
 * Copyright (C) 2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* This class handles a calendar, i.e. basically a list of appointments.
 * These appointments are MIME Directory Component objects.
 */

#include <glib.h>
#include <glib-object.h>

#include <mimedir/mimedir-vcomponent.h>
#include <mimedir/mimedir-vevent.h>
#include <mimedir/mimedir-vtodo.h>

#define CALENDAR_TYPE_MANAGER			(calendar_manager_get_type())
#define CALENDAR_MANAGER(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), CALENDAR_TYPE_MANAGER, CalendarManager))
#define CALENDAR_MANAGER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_MANAGER))
#define CALENDAR_IS_MANAGER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), CALENDAR_TYPE_MANAGER))
#define CALENDAR_IS_MANAGER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_MANAGER))

typedef struct _CalendarManager		CalendarManager;
typedef struct _CalendarManagerClass	CalendarManagerClass;
typedef struct _CalendarManagerPriv	CalendarManagerPriv;

struct _CalendarManager
{
	GObject parent;

	CalendarManagerPriv *priv;
};

struct _CalendarManagerClass
{
	GObjectClass parent_class;

	void (* appointment_added)	(CalendarManager *cal, MIMEDirVEvent *appointment);
	void (* appointment_removed)	(CalendarManager *cal, MIMEDirVEvent *appointment);
	void (* todo_added)		(CalendarManager *cal, MIMEDirVTodo *todo);
	void (* todo_removed)		(CalendarManager *cal, MIMEDirVTodo *todo);
};

GType		 calendar_manager_get_type	(void);
CalendarManager	*calendar_manager_new		(void);

void		 calendar_manager_empty		(CalendarManager *cal);
gboolean	 calendar_manager_load_file	(CalendarManager *cal, const gchar *filename, GError **error);
gboolean	 calendar_manager_append_file	(CalendarManager *cal, const gchar *filename, GError **error);
gboolean	 calendar_manager_save_file	(CalendarManager *cal, const gchar *filename, GError **error);

GList		*calendar_manager_get_appointments	(CalendarManager *cal);
GList		*calendar_manager_get_appointments_intersecting	(CalendarManager *cal, GDate *from, GDate *to);
GList		*calendar_manager_get_todo_items	(CalendarManager *cal);
void		 calendar_manager_add_appointment	(CalendarManager *cal, MIMEDirVEvent *appointment);
void		 calendar_manager_add_todo_item		(CalendarManager *cal, MIMEDirVTodo *todo);
void		 calendar_manager_remove_appointment	(CalendarManager *cal, MIMEDirVEvent *appointment);
void		 calendar_manager_remove_todo_item	(CalendarManager *cal, MIMEDirVTodo *todo);

#endif /* __CALENDAR_MANAGER_H__ */
