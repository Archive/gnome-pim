#ifndef __GNOMECAL_ALARM_SINK_H__
#define __GNOMECAL_ALARM_SINK_H__

/* GNOME Calendar Alarm Sink
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* This is the GNOME Calendar alarm sink.
 *
 * You can throw MIME Directory Component objects into it and it will
 * set-up and manage all their alarms automatically. You can take out
 * component objects and all its alarms will be disabled and removed
 * from the alarm list.
 */

#include <glib.h>
#include <glib-object.h>

#include <mimedir/mimedir-vcomponent.h>

#define GNCAL_TYPE_ALARM_SINK			(gncal_alarm_sink_get_type())
#define GNCAL_ALARM_SINK(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNCAL_TYPE_ALARM_SINK, GncalAlarmSink))
#define GNCAL_ALARM_SINK_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GNCAL_TYPE_ALARM_SINK))
#define GNCAL_IS_ALARM_SINK(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNCAL_TYPE_ALARM_SINK))
#define GNCAL_IS_ALARM_SINK_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNCAL_TYPE_ALARM_SINK))

typedef struct _GncalAlarmSink		GncalAlarmSink;
typedef struct _GncalAlarmSinkClass	GncalAlarmSinkClass;
typedef struct _GncalAlarmSinkPriv	GncalAlarmSinkPriv;

struct _GncalAlarmSink
{
	GObject parent;

	GncalAlarmSinkPriv *priv;
};

struct _GncalAlarmSinkClass
{
	GObjectClass parent_class;
};

GType		 gncal_alarm_sink_get_type	(void);
GncalAlarmSink	*gncal_alarm_sink_new		(void);

void		 gncal_alarm_sink_add		(GncalAlarmSink *sink, MIMEDirVComponent *component);
void		 gncal_alarm_sink_remove	(GncalAlarmSink *sink, MIMEDirVComponent *component);
void		 gncal_alarm_sink_clear		(GncalAlarmSink *sink);

#endif /* __GNOMECAL_ALARM_SINK_H__ */
