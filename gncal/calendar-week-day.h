#ifndef __CALENDAR_WEEK_DAY_H__
#define __CALENDAR_WEEK_DAY_H__

/* Calendar Week Day Object
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* A single day of a CalendarWeek widget. */

#include <gtk/gtkwidget.h>


#define CALENDAR_TYPE_WEEK_DAY			(calendar_week_day_get_type())
#define CALENDAR_WEEK_DAY(obj)			(GTK_CHECK_CAST ((obj), CALENDAR_TYPE_WEEK_DAY, CalendarWeekDay))
#define CALENDAR_WEEK_DAY_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_WEEK_DAY))
#define CALENDAR_IS_WEEK_DAY(obj)		(GTK_CHECK_TYPE ((obj), CALENDAR_TYPE_WEEK_DAY))
#define CALENDAR_IS_WEEK_DAY_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_WEEK_DAY))

typedef struct _CalendarWeekDay		CalendarWeekDay;
typedef struct _CalendarWeekDayClass	CalendarWeekDayClass;
typedef struct _CalendarWeekDayPriv	CalendarWeekDayPriv;

struct _CalendarWeekDay
{
	GtkWidget parent;

	CalendarWeekDayPriv *priv;
};

struct _CalendarWeekDayClass
{
	GtkWidgetClass parent_class;

	void (* clicked)  (void);
	void (* selected) (void);
	void (* menu)     (void);
};

GType		 calendar_week_day_get_type	(void);
GtkWidget	*calendar_week_day_new		(void);

void		 calendar_week_day_set_date	(CalendarWeekDay *cwd, const GDate *date);
void		 calendar_week_day_get_date	(CalendarWeekDay *cwd, GDate *date);

void		 calendar_week_day_set_text	(CalendarWeekDay *cwd, const gchar *text);
const gchar	*calendar_week_day_get_text	(CalendarWeekDay *cwd);

void		 calendar_week_day_set_active	(CalendarWeekDay *cwd, gboolean active);
gboolean	 calendar_week_day_get_active	(CalendarWeekDay *cwd);

/* for CalendarWeek */
void		 calendar_week_day_set_parent_focussed
						(CalendarWeekDay *cwd, gboolean focussed);

#endif /* __CALENDAR_WEEK_DAY_H__ */
