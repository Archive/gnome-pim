/* Calendar Week Object
 * Copyright (C) 2003-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* FIXME: ability to select single days, including keyboard nav */

#include <config.h>

#include <string.h>
#include <time.h>

#include <glib.h>
#include <gnome.h>

#include "calendar-marshal.h"
#include "calendar-week.h"
#include "calendar-week-day.h"

static void	calendar_week_class_init	(CalendarWeekClass	*klass);
static void	calendar_week_init		(CalendarWeek		*cw);
static void	calendar_week_finalize		(GObject		*object);
static gboolean	calendar_week_key_event		(GtkWidget		*widget,
						 GdkEventKey		*event);

static void	setup				(CalendarWeek		*cw);
static gint	get_focussed_day		(CalendarWeek		*cw);


#define DAYS_IN_WEEK 7


struct _CalendarWeekPriv {
	GDate date;

	/* Widgets */

	GtkWidget *top_label, *bottom_label;
	GtkWidget *calendar;

	GtkWidget *days[DAYS_IN_WEEK];
};


enum {
	SIGNAL_DAY_CHANGED,
	SIGNAL_DAY_SELECTED,
	SIGNAL_DAY_MENU,
	SIGNAL_LAST
};


static GtkVBoxClass *parent_class = NULL;

static guint signals[SIGNAL_LAST] = { 0 };

/*
 * Class and Instance Setup
 */

GType
calendar_week_get_type (void)
{
	static GType calendar_week_type = 0;

	if (!calendar_week_type) {
		static const GTypeInfo calendar_week_info = {
			sizeof (CalendarWeekClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_week_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarWeek),
			0,    /* n_preallocs */
			(GInstanceInitFunc) calendar_week_init,
		};

		calendar_week_type = g_type_register_static (GTK_TYPE_VBOX,
							     "CalendarWeek",
							     &calendar_week_info,
							     0);
	}

	return calendar_week_type;
}

static void
calendar_week_class_init (CalendarWeekClass *klass)
{
	GObjectClass *gobject_class;
	GtkWidgetClass *widget_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);
	widget_class  = GTK_WIDGET_CLASS (klass);

	gobject_class->finalize = calendar_week_finalize;

	widget_class->key_press_event = calendar_week_key_event;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_DAY_CHANGED] =
		g_signal_new ("day-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarWeekClass, day_changed),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	signals[SIGNAL_DAY_SELECTED] =
		g_signal_new ("day-selected",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarWeekClass, day_selected),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	signals[SIGNAL_DAY_MENU] =
		g_signal_new ("day-menu",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarWeekClass, day_menu),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);
}

static void
calendar_week_init (CalendarWeek *cw)
{
	g_return_if_fail (cw != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK (cw));

	cw->priv = g_new0 (CalendarWeekPriv, 1);

	g_date_clear (&cw->priv->date, 1);
	g_date_set_time (&cw->priv->date, time (NULL));

	setup (cw);
}

static void
calendar_week_finalize (GObject *object)
{
	CalendarWeek *cw;
	CalendarWeekPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK (object));

	cw = CALENDAR_WEEK (object);

	priv = cw->priv;
	if (priv) {
		g_free (priv);
		cw->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gboolean
calendar_week_key_event (GtkWidget *widget, GdkEventKey *event)
{
	CalendarWeek *cw;
	CalendarWeekPriv *priv;
	gint day;

	cw = CALENDAR_WEEK (widget);
	priv = cw->priv;

	if (event->state != 0)
		return FALSE;

	switch (event->keyval) {
	case GDK_Down:
		day = get_focussed_day(cw);
		switch (day) {
		case 0:
		case 1:
		case 2:
		case 5:
		case 6:
			return TRUE;
		case 3:
		case 4:
			gtk_widget_grab_focus (priv->days[day + 2]);
			return TRUE;
		default:
			return FALSE;
		}

	case GDK_Up:
		day = get_focussed_day(cw);
		switch (day) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
			return TRUE;
		case 5:
		case 6:
			gtk_widget_grab_focus (priv->days[day - 2]);
			return TRUE;
		default:
			return FALSE;
		}

	case GDK_Left:
		day = get_focussed_day(cw);
		switch (day) {
		case 0:
			gtk_widget_grab_focus (priv->days[6]);
			return TRUE;
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
			gtk_widget_grab_focus(priv->days[day - 1]);
			return TRUE;
		default:
			return FALSE;
		}

	case GDK_Right:
		day = get_focussed_day(cw);
		switch (day) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			gtk_widget_grab_focus (priv->days[day + 1]);
			return TRUE;
		case 6:
			gtk_widget_grab_focus(priv->days[0]);
			return TRUE;
		default:
			return FALSE;
		}

	default:
		return FALSE;
	}
}

/*
 * Utilities
 */

static gboolean
week_starts_monday (void)
{
	return strcmp (_("calendar:week_start:0"),
		       "calendar:week_start:1") == 0;
}

static void
start_of_week (GDate *date)
{
	GDateWeekday day;

	day = g_date_get_weekday (date);

	if (week_starts_monday ())
		day--;
	else
		day %= 7;

	g_date_subtract_days (date, day);
}

static void
end_of_week (GDate *date)
{
	GDateWeekday day;

	day = g_date_get_weekday (date);

	if (week_starts_monday ())
		day = 7 - day;
	else
		day = 6 - (day % 7);

	g_date_add_days (date, day);
}

/*
 * Callbacks
 */

static void
calendar_changed (CalendarWeek *cw, GtkCalendar *cal)
{
	guint year, month, day;
	GDate date;

	gtk_calendar_get_date (cal, &year, &month, &day);
	g_date_set_dmy (&date, day, month + 1, year);

	calendar_week_set_date (cw, &date);
}

static void
day_clicked (CalendarWeek *cw, CalendarWeekDay *cwd)
{
	GDate date;

	calendar_week_day_get_date (cwd, &date);
	calendar_week_set_date (cw, &date);
}

static void
day_selected (CalendarWeek *cw, CalendarWeekDay *cwd)
{
	GDate date;

	calendar_week_day_get_date (cwd, &date);
	g_signal_emit (G_OBJECT (cw), signals[SIGNAL_DAY_SELECTED], 0,
		       g_date_get_year (&date), g_date_get_month (&date), g_date_get_day (&date));
}

static void
day_menu (CalendarWeek *cw, CalendarWeekDay *cwd)
{
	GDate date;

	day_clicked (cw, cwd);

	calendar_week_day_get_date (cwd, &date);
	g_signal_emit (G_OBJECT (cw), signals[SIGNAL_DAY_MENU], 0,
		       g_date_get_year (&date), g_date_get_month (&date), g_date_get_day (&date));
}

/*
 * Private Methods
 */

static void
update_label (CalendarWeek *cw)
{
	CalendarWeekPriv *priv = cw->priv;
	GDate start, end;
	gchar week[100], ss[100], es[100];
	gchar *label;

	/* Find week start */

	memcpy (&start, &priv->date, sizeof (priv->date));
	memcpy (&end,   &priv->date, sizeof (priv->date));
	start_of_week (&start);
	end_of_week (&end);

	/* Update labels */

	g_date_strftime (week, sizeof (week), _("Week %V/%G"), &start);
	gtk_label_set_text (GTK_LABEL (priv->top_label), week);

	g_date_strftime (ss, sizeof (ss), "%x", &start);
	g_date_strftime (es, sizeof (es), "%x", &end);

	/* Translators: This is "%weekstart to %weekend". You should probably
	 * use FIGURE DASH (0x2012) instead of '-'. */
	label = g_strdup_printf (_("%s - %s"), ss, es);
	gtk_label_set_text (GTK_LABEL (priv->bottom_label), label);
	g_free (label);
}

static void
update_days (CalendarWeek *cw)
{
	CalendarWeekPriv *priv = cw->priv;
	GDate date;
	int i;

	/* Find week start */

	memcpy (&date, &priv->date, sizeof (date));
	start_of_week (&date);

	/* Set days */

	for (i = 0; i < DAYS_IN_WEEK; i++) {
		calendar_week_day_set_date (CALENDAR_WEEK_DAY (priv->days[i]), &date);
		calendar_week_day_set_active (CALENDAR_WEEK_DAY (priv->days[i]),
					      g_date_get_day (&priv->date) == g_date_get_day (&date));
		g_date_add_days (&date, 1);
	}
}

static void
update_calendar (CalendarWeek *cw)
{
	CalendarWeekPriv *priv = cw->priv;

	g_signal_handlers_block_by_func (G_OBJECT (priv->calendar),
					 G_CALLBACK (calendar_changed), cw);

	gtk_calendar_select_month (GTK_CALENDAR (priv->calendar),
				   g_date_get_month (&priv->date) - 1,
				   g_date_get_year (&priv->date));
	gtk_calendar_select_day (GTK_CALENDAR (priv->calendar),
				 g_date_get_day (&priv->date));

	g_signal_handlers_unblock_by_func (G_OBJECT (priv->calendar),
					   G_CALLBACK (calendar_changed), cw);
}

static void
update (CalendarWeek *cw)
{
	update_label (cw);
	update_days (cw);
	update_calendar (cw);
}

static gint
get_focussed_day (CalendarWeek *cw)
{
	CalendarWeekPriv *priv = cw->priv;
	gint i;

	for (i = 0; i < G_N_ELEMENTS (priv->days); i++) {
		if (gtk_widget_is_focus(priv->days[i]))
			return i;
	}

	return -1;
}

/*
 * Setup
 */

static void
setup (CalendarWeek *cw)
{
	CalendarWeekPriv *priv = cw->priv;
	PangoAttrList *attrs;
	PangoAttribute *attr;
	GtkWidget *box, *table;
	gint i;

	gtk_box_set_spacing (GTK_BOX (cw), 12);

	/* Header labels */

	box = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (box);
	gtk_box_pack_start (GTK_BOX (cw), box, FALSE, FALSE, 0);

	attrs = pango_attr_list_new ();
	attr = pango_attr_scale_new (PANGO_SCALE_X_LARGE);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);
	attr = pango_attr_weight_new (PANGO_WEIGHT_BOLD);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);

	priv->top_label = gtk_label_new ("");
	gtk_label_set_justify (GTK_LABEL (priv->top_label), GTK_JUSTIFY_CENTER);
	gtk_label_set_attributes (GTK_LABEL (priv->top_label), attrs);
	gtk_widget_show (priv->top_label);
	gtk_box_pack_start (GTK_BOX (box), priv->top_label, FALSE, FALSE, 0);

	pango_attr_list_unref (attrs);

	priv->bottom_label = gtk_label_new ("");
	gtk_label_set_justify (GTK_LABEL (priv->bottom_label), GTK_JUSTIFY_CENTER);
	gtk_widget_show (priv->bottom_label);
	gtk_box_pack_start (GTK_BOX (box), priv->bottom_label, FALSE, FALSE, 0);

	/* Table */

	table = gtk_table_new (0, 0, TRUE);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (cw), table, TRUE, TRUE, 0);

	/* Days */

	for (i = 0; i < DAYS_IN_WEEK; i++) {
		priv->days[i] = calendar_week_day_new ();
		g_signal_connect_swapped (G_OBJECT (priv->days[i]),
					  "clicked",
					  G_CALLBACK (day_clicked), cw);
		g_signal_connect_swapped (G_OBJECT (priv->days[i]),
					  "selected",
					  G_CALLBACK (day_selected), cw);
		g_signal_connect_swapped (G_OBJECT (priv->days[i]),
					  "menu",
					  G_CALLBACK (day_menu), cw);
		gtk_widget_show (priv->days[i]);

		if (i < 5)
			gtk_table_attach (GTK_TABLE (table), priv->days[i],
					  i, i + 1,
					  0, 1,
					  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
					  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
					  0, 0);
		else
			gtk_table_attach (GTK_TABLE (table), priv->days[i],
					  i - 2, i - 1,
					  1, 2,
					  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
					  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
					  0, 0);
	}

	/* Calendar */

	priv->calendar = gtk_calendar_new ();

	{
		GtkCalendarDisplayOptions week_start = 0;

		if (week_starts_monday ())
			week_start = GTK_CALENDAR_WEEK_START_MONDAY;

		gtk_calendar_set_display_options (GTK_CALENDAR (priv->calendar),
						  GTK_CALENDAR_SHOW_HEADING      |
						  GTK_CALENDAR_SHOW_DAY_NAMES    |
						  GTK_CALENDAR_SHOW_WEEK_NUMBERS |
						  week_start);
	}

	g_signal_connect_swapped (G_OBJECT (priv->calendar),
				  "day-selected",
				  G_CALLBACK (calendar_changed), cw);
	g_signal_connect_swapped (G_OBJECT (priv->calendar),
				  "month-changed",
				  G_CALLBACK (calendar_changed), cw);

	gtk_widget_show (priv->calendar);

	gtk_table_attach (GTK_TABLE (table), priv->calendar,
			  0, 3, 1, 2,
			  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			  0, 0);

	update (cw);
}

/*
 * Public Methods
 */

GtkWidget *
calendar_week_new (void)
{
	CalendarWeek *cw;

	cw = g_object_new (CALENDAR_TYPE_WEEK, NULL);

	return GTK_WIDGET (cw);
}

void
calendar_week_set_date (CalendarWeek *cw, const GDate *date)
{
	CalendarWeekPriv *priv;

	g_return_if_fail (cw != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK (cw));
	g_return_if_fail (date != NULL);
	g_return_if_fail (g_date_valid (date));

	priv = cw->priv;

	memcpy (&priv->date, date, sizeof (priv->date));

	update (cw);

	/* Emit signal */

	g_signal_emit (G_OBJECT (cw), signals[SIGNAL_DAY_CHANGED], 0,
		       date->year, date->month, date->day);
}

void
calendar_week_get_date (CalendarWeek *cw, GDate *date)
{
	CalendarWeekPriv *priv;

	g_return_if_fail (cw != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK (cw));
	g_return_if_fail (date != NULL);

	priv = cw->priv;

	memcpy (date, &priv->date, sizeof (priv->date));
}








#if 0 /* FIXME */
static gchar *
get_event_summary ()
{
	GString *buf;
	CalendarObject *co = list->data;
	MIMEDirVEvent *event = co->event;
	gchar *s, *summary;
	struct tm tm_start, tm_end;

	buf = g_string_new (NULL);

	tm_start = *localtime (&co->ev_start);
	tm_end   = *localtime (&co->ev_end);

	s = time_get_short_time_string (tm_start.tm_hour, tm_start.tm_min);
	g_string_append (buf, s);
	g_free (s);
			
	g_string_append_c (buf, '-');
	s = time_get_short_time_string (tm_end.tm_hour, tm_end.tm_min);
	g_string_append (buf, s);
	g_free (s);

	g_object_get (G_OBJECT (event), "summary", &summary, NULL);
	if (mimedir_vcomponent_get_allday (MIMEDIR_VCOMPONENT (event))) {
		g_string_assign (buf, summary);
	} else {
		g_string_append (buf, ": ");
		g_string_append (buf, summary);
	}
	g_free (summary);

	return g_string_free (buf, FALSE);
}
#endif
