/* To Do Editor Object
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * Based on the original to-do dialog by Federico Mena.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* FIXME: window resize is broken... */
/* FIXME: priority button should be an option menu */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>

#include <gnome.h>

#include <libegg/egg-datetime.h>

#include <mimedir/mimedir-vtodo.h>

#include "gnomecal-config.h"
#include "todo-categories.h"
#include "todo-dialog.h"
#include "utils-ui.h"


static void todo_dialog_init		(TodoDialog *dialog);
static void todo_dialog_dispose		(GObject *dialog);
static void todo_dialog_finalize	(GObject *dialog);
static void todo_dialog_class_init	(TodoDialogClass *klass);

static void set_title			(TodoDialog *dialog);
static void setup			(TodoDialog *dialog);


struct _TodoDialogPriv {
	MIMEDirVTodo	*item;

	GtkWidget	*summary;
	GtkWidget	*due_date;
	GtkWidget	*categories;
	GtkWidget	*cat_button;
	GtkAdjustment	*priority;
	GtkTextBuffer	*comment;

	GtkWidget	*cat_dialog;
};


static GtkDialogClass *parent_class = NULL;


GType
todo_dialog_get_type (void)
{
	static GType todo_dialog_type = 0;

	if (!todo_dialog_type) {
		static const GTypeInfo todo_dialog_info = {
			sizeof (TodoDialogClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) todo_dialog_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (TodoDialog),
			4,    /* n_preallocs */
			(GInstanceInitFunc) todo_dialog_init,
		};

		todo_dialog_type = g_type_register_static (GTK_TYPE_DIALOG,
							   "TodoDialog",
							   &todo_dialog_info,
							   0);
	}

	return todo_dialog_type;
}

static void
todo_dialog_class_init (TodoDialogClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (TODO_IS_DIALOG_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose  = todo_dialog_dispose;
	gobject_class->finalize = todo_dialog_finalize;

	parent_class = g_type_class_peek_parent (klass);
}

static void
todo_dialog_init (TodoDialog *dialog)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (TODO_IS_DIALOG (dialog));

	dialog->priv = g_new0 (TodoDialogPriv, 1);

	setup (dialog);
}

static void
todo_dialog_dispose (GObject *object)
{
	TodoDialogPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (TODO_IS_DIALOG (object));

	priv = TODO_DIALOG (object)->priv;

	if (priv->item) {
		g_object_unref (G_OBJECT (priv->item));
		priv->item = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
todo_dialog_finalize (GObject *object)
{
	TodoDialog *dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (TODO_IS_DIALOG (object));

	dialog = TODO_DIALOG (object);

	g_free (dialog->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*
 * Callbacks
 */

static void
response_cb (GtkDialog *dialog, gint response, gpointer data)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GTK_IS_WINDOW (dialog));

	switch (response) {
	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;
	case GTK_RESPONSE_DELETE_EVENT:
		break;
	case GTK_RESPONSE_HELP:
		ui_show_help (GTK_WINDOW (dialog), "todo-dialog");
		break;
	default:
		g_return_if_reached ();
	}
}

static void
summary_changed_cb (GtkWidget *widget, TodoDialog *dialog)
{
	const gchar *content;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_ENTRY (widget));
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (TODO_IS_DIALOG (dialog));

	content = gtk_entry_get_text (GTK_ENTRY (widget));

	if (dialog->priv->item) {
		g_object_set (G_OBJECT (dialog->priv->item),
			      "summary", content,
			      NULL);

		set_title (dialog);
	}
}

static void
date_changed_cb (EggDateTime *datetime, TodoDialog *dialog)
{
	g_return_if_fail (datetime != NULL);
	g_return_if_fail (EGG_IS_DATETIME (datetime));
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (TODO_IS_DIALOG (dialog));

	if (dialog->priv->item) {
		MIMEDirDateTime *dt;
		GDate date;

		egg_datetime_get_as_gdate (datetime, &date);

		dt = mimedir_datetime_new_from_gdate (&date);
		g_object_set (G_OBJECT (dialog->priv->item),
			      "due", dt,
			      NULL);
		g_object_unref (G_OBJECT (dt));
	}
}

static void
priority_changed_cb (GtkAdjustment *adjustment, TodoDialog *dialog)
{
	g_return_if_fail (adjustment != NULL);
	g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (TODO_IS_DIALOG (dialog));

	if (dialog->priv->item) {
		gdouble value;

		value = gtk_adjustment_get_value (adjustment);

		g_object_set (G_OBJECT (dialog->priv->item),
			      "priority", (guint) value,
			      NULL);
	}
}

static void
comments_changed_cb (GtkTextBuffer *buffer, TodoDialog *dialog)
{
	g_return_if_fail (buffer != NULL);
	g_return_if_fail (GTK_IS_TEXT_BUFFER (buffer));
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (TODO_IS_DIALOG (dialog));

	if (dialog->priv->item) {
		GtkTextIter start, end;
		gchar *text;

		gtk_text_buffer_get_start_iter (buffer, &start);
		gtk_text_buffer_get_end_iter   (buffer, &end);

		text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
		g_object_set (G_OBJECT (dialog->priv->item),
			      "description", text,
			      NULL);
		g_free (text);
	}
}

static void
categories_changed_cb (TodoCategories	*categories_dialog,
		       TodoDialog	*todo_dialog)
{
	GList *list;
	gchar *string;

	g_return_if_fail (categories_dialog != NULL);
	g_return_if_fail (TODO_IS_CATEGORIES (categories_dialog));
	g_return_if_fail (todo_dialog != NULL);
	g_return_if_fail (TODO_IS_DIALOG (todo_dialog));

	list = todo_categories_get_list (categories_dialog);
	g_object_set (G_OBJECT (todo_dialog->priv->item),
		      "category_list", list,
		      NULL);
	todo_categories_free_list (categories_dialog, list);

	string = mimedir_vcomponent_get_categories_as_string (MIMEDIR_VCOMPONENT (todo_dialog->priv->item));
	gtk_entry_set_text (GTK_ENTRY (todo_dialog->priv->categories), string);
	g_free (string);
}

static void
categories_closed_cb (TodoCategories	*categories_dialog,
		      TodoDialog	*todo_dialog)
{
	g_return_if_fail (todo_dialog != NULL);
	g_return_if_fail (TODO_IS_DIALOG (todo_dialog));

	gtk_widget_set_sensitive (todo_dialog->priv->cat_button, TRUE);

	todo_dialog->priv->cat_dialog = NULL;
}

static void
modify_clicked_cb (GtkWidget *button, TodoDialog *dialog)
{
	GList     *list;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (TODO_IS_DIALOG (dialog));
	g_return_if_fail (dialog->priv->cat_dialog == NULL);

	gtk_widget_set_sensitive (dialog->priv->cat_button, FALSE);

	dialog->priv->cat_dialog = todo_categories_new (GTK_WINDOW (dialog));
	gtk_window_set_destroy_with_parent (GTK_WINDOW (dialog->priv->cat_dialog),
					    TRUE);
	set_title (dialog); /* for the categories dialog only */

	g_object_get (G_OBJECT (dialog->priv->item),
		      "category_list", &list,
		      NULL);

	todo_categories_set_list (TODO_CATEGORIES (dialog->priv->cat_dialog),
				  list);

	g_signal_connect (G_OBJECT (dialog->priv->cat_dialog), "changed",
			  G_CALLBACK (categories_changed_cb), dialog);
	g_signal_connect (G_OBJECT (dialog->priv->cat_dialog), "destroy",
			  G_CALLBACK (categories_closed_cb), dialog);

	gtk_widget_show (dialog->priv->cat_dialog);
}

/*
 * Private Methods
 */

static void
update (TodoDialog *dialog, MIMEDirVTodo *item)
{
	TodoDialogPriv	*priv;
	gchar		*summary;
	gchar		*description;
	MIMEDirDateTime	*due;
	guint		 priority;
	gchar		*categories;
	GDate		 date;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (TODO_IS_DIALOG (dialog));
	g_return_if_fail (item != NULL);
	g_return_if_fail (MIMEDIR_IS_VTODO (item));

	priv = dialog->priv;

	/* The window title is set via a callback that's connected to
	 * priv->summary.
	 */

	g_object_get (G_OBJECT (item),
		      "summary",	&summary,
		      "description",	&description,
		      "due",		&due,
		      "priority",	&priority,
		      NULL);

	if (!summary)
		summary = g_strdup ("");

	if (!description)
		description = g_strdup ("");

	g_date_clear (&date, 1);
	if (due)
		mimedir_datetime_get_gdate (due, &date);

	gtk_entry_set_text (GTK_ENTRY (priv->summary), summary);
	egg_datetime_set_from_gdate (EGG_DATETIME (priv->due_date), &date);
	gtk_adjustment_set_value (priv->priority, priority);
	gtk_text_buffer_set_text (priv->comment, description, -1);

	g_free (summary);
	g_free (description);

	categories = mimedir_vcomponent_get_categories_as_string (MIMEDIR_VCOMPONENT (item));
	gtk_entry_set_text (GTK_ENTRY (priv->categories), categories);
	g_free (categories);
}

static void
set_title (TodoDialog *dialog)
{
	gchar *summary = NULL, *title;

	if (dialog->priv->item) {
		gchar *text;

		g_object_get (G_OBJECT (dialog->priv->item),
			      "summary", &text,
			      NULL);

		if (text != NULL && text[0] != '\0')
			summary = text;
	}

	if (!summary)
		summary = g_strdup (_("<Unnamed>")); /* default */

	title = g_strdup_printf (_("%s properties"), summary);
	gtk_window_set_title (GTK_WINDOW (dialog), title);
	g_free (title);

	/* Set the title of the categories dialog also */

	if (dialog->priv->cat_dialog) {
		title = g_strdup_printf (_("%s categories"), summary);
		gtk_window_set_title (GTK_WINDOW (dialog->priv->cat_dialog),
				      title);
		g_free (title);
	}

	g_free (summary);
}

static void
setup (TodoDialog *dialog)
{
	TodoDialogPriv	*priv;
	GtkWidget	*widget;
	GtkWidget	*table;
	GtkWidget	*label;
	GtkWidget	*sw;

	priv = dialog->priv;

	/* Widget Setup */

	set_title (dialog);

	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				GTK_STOCK_HELP,  GTK_RESPONSE_HELP,
				GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
				NULL);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CLOSE);

	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (response_cb), NULL);

	table = gtk_table_new (5, 6, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 6);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_table_set_col_spacings (GTK_TABLE (table), 12);
	gtk_widget_show (table);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), table);

	label = gtk_label_new_with_mnemonic (_("_Summary:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL, 0, 0, 0);

	priv->summary = gtk_entry_new ();
	g_signal_connect (G_OBJECT (priv->summary), "changed",
			  G_CALLBACK (summary_changed_cb), dialog);
	gtk_widget_show (priv->summary);
	gtk_table_attach (GTK_TABLE (table), priv->summary, 1, 6, 0, 1, GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0, 0);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->summary);

	label = gtk_label_new_with_mnemonic (_("_Due Date:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, GTK_FILL, 0, 0, 0);

	priv->due_date = egg_datetime_new ();
	egg_datetime_set_lazy (EGG_DATETIME (priv->due_date), TRUE);
	egg_datetime_set_none (EGG_DATETIME (priv->due_date));
	g_signal_connect (G_OBJECT (priv->due_date), "date-changed",
			  G_CALLBACK (date_changed_cb), dialog);
	gtk_widget_show (priv->due_date);
	gtk_table_attach (GTK_TABLE (table), priv->due_date, 1, 2, 1, 2, 0, 0, 0, 0);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->due_date);

	label = gtk_label_new_with_mnemonic (_("_Priority:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label, 2, 3, 1, 2, GTK_FILL, 0, 0, 0);

	widget = gtk_spin_button_new_with_range (0.0, 9.0, 1.0);
	gtk_spin_button_set_numeric       (GTK_SPIN_BUTTON (widget), TRUE);
	gtk_spin_button_set_wrap          (GTK_SPIN_BUTTON (widget), FALSE);
	gtk_spin_button_set_snap_to_ticks (GTK_SPIN_BUTTON (widget), TRUE);
	gtk_widget_show (widget);
	gtk_table_attach (GTK_TABLE (table), widget, 3, 4, 1, 2, 0, 0, 0, 0);
	priv->priority = gtk_spin_button_get_adjustment (GTK_SPIN_BUTTON (widget));
       	g_signal_connect (G_OBJECT (priv->priority), "value-changed",
			  G_CALLBACK (priority_changed_cb), dialog);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), widget);

	label = gtk_label_new_with_mnemonic (_("C_ategories:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, GTK_FILL, 0, 0, 0);

	priv->categories = gtk_entry_new ();
	gtk_editable_set_editable (GTK_EDITABLE (priv->categories), FALSE);
	gtk_widget_show (priv->categories);
	gtk_table_attach (GTK_TABLE (table), priv->categories, 1, 5, 2, 3, GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0, 0);

	priv->cat_button = gtk_button_new_with_label (_("Modify..."));
	gtk_widget_show (priv->cat_button);
	gtk_table_attach (GTK_TABLE (table), priv->cat_button, 5, 6, 2, 3, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (priv->cat_button), "clicked",
			  G_CALLBACK (modify_clicked_cb), dialog);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->cat_button);

	label = gtk_label_new_with_mnemonic (_("Item C_omments:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label, 0, 6, 3, 4, GTK_FILL, 0, 0, 0);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_set_size_request (sw, 200, 100);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
					     GTK_SHADOW_IN);
	gtk_widget_show (sw);
	gtk_table_attach (GTK_TABLE (table), sw, 0, 6, 4, 5, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	widget = gtk_text_view_new ();
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (widget), GTK_WRAP_WORD);
	gtk_widget_show (widget);
	gtk_container_add (GTK_CONTAINER (sw), widget);
	priv->comment = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
	g_signal_connect (G_OBJECT (priv->comment), "changed",
			  G_CALLBACK (comments_changed_cb), dialog);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), widget);

	gtk_widget_grab_focus (priv->summary);
}

/*
 * Public Methods
 */

GtkWidget *
todo_dialog_new (GtkWindow *parent, MIMEDirVTodo *item)
{
	TodoDialog *dialog;

	g_return_val_if_fail (parent == NULL || GTK_IS_WINDOW (parent), NULL);
	g_return_val_if_fail (item != NULL, NULL);
	g_return_val_if_fail (MIMEDIR_IS_VTODO (item), NULL);

	dialog = g_object_new (TODO_TYPE_DIALOG, NULL);

	if (parent)
		gtk_window_set_transient_for (GTK_WINDOW (dialog), parent);

	todo_dialog_set_item (dialog, item);

	return GTK_WIDGET (dialog);
}

void
todo_dialog_set_item (TodoDialog *dialog, MIMEDirVTodo *item)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (TODO_IS_DIALOG (dialog));
	g_return_if_fail (item != NULL);
	g_return_if_fail (MIMEDIR_IS_VTODO (item));

	/* Remove the old object first, to prevent if from being updated
	 * with values from the new one.
	 */
	if (dialog->priv->item) {
		g_object_unref (G_OBJECT (dialog->priv->item));
		dialog->priv->item = NULL;
	}

	g_object_ref (item);

	update (dialog, item);

	/* Set after update() to prevent widget callbacks from re-setting
	 * values in item.
	 */
	dialog->priv->item = item;
}

MIMEDirVTodo *
todo_dialog_get_item (TodoDialog *dialog)
{
	g_return_val_if_fail (dialog != NULL, NULL);
	g_return_val_if_fail (TODO_IS_DIALOG (dialog), NULL);

	return dialog->priv->item;
}
