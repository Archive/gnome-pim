/* Calendar Day View Object
 * Copyright (C) 2003-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * Based on gncal-full-day.c:
 *
 * Copyright (C) 1998 The Free Software Foundation
 *
 * Authors: Federico Mena <quartic@gimp.org>
 *          Miguel de Icaza <miguel@kernel.org>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <time.h>

#include <glib.h>
#include <gnome.h>

#include "calendar-day-view.h"
#include "calendar-manager.h"
#include "gnomecal-config.h"
#include "utils-time.h"


static void	calendar_day_view_class_init	(CalendarDayViewClass	*klass);
static void	calendar_day_view_init		(CalendarDayView	*cdv);
static void	calendar_day_view_dispose	(GObject		*object);
static void	calendar_day_view_realize	(GtkWidget		*widget);
static void	calendar_day_view_unrealize	(GtkWidget		*widget);
static gboolean	calendar_day_view_expose	(GtkWidget		*widget,
						 GdkEventExpose		*event);
static void	calendar_day_view_size_request	(GtkWidget		*widget,
						 GtkRequisition		*requisition);
static void	calendar_day_view_size_allocate	(GtkWidget		*widget,
						 GtkAllocation		*allocation);
static gboolean	calendar_day_view_button_press	(GtkWidget		*widget,
						 GdkEventButton		*event);

static void	child_realize			(CalendarDayView	*cdv,
						 void			*child);
static void	child_unrealize			(CalendarDayView	*cdv,
						 void			*child);

static void	used_rows			(CalendarDayView	*cdv,
						 guint			*time_lower,
						 guint			*time_upper,
						 guint			*first_row,
						 guint			*row_count);
static gint	time_column_width		(CalendarDayView	*cdv);
static void	draw				(CalendarDayView	*cdv,
						 GdkRectangle		*area);


struct _CalendarDayViewPriv {
	GList		*children;

	CalendarManager	*calendar;
	GList		*appointments;

	GDate		 date;

	guint		 lower_bounds, upper_bounds;
	guint		 interval; /* interval between rows in minutes */
};


static GtkContainerClass *parent_class = NULL;


#define TEXT_BORDER 6
#define MIN_TEXT_WIDTH 200

/*
 * Class and Instance Setup
 */

GType
calendar_day_view_get_type (void)
{
	static GType calendar_day_view_type = 0;

	if (!calendar_day_view_type) {
		static const GTypeInfo calendar_day_view_info = {
			sizeof (CalendarDayViewClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_day_view_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarDayView),
			0,    /* n_preallocs */
			(GInstanceInitFunc) calendar_day_view_init,
		};

		calendar_day_view_type = g_type_register_static (GTK_TYPE_CONTAINER,
								 "CalendarDayView",
								 &calendar_day_view_info,
								 0);
	}

	return calendar_day_view_type;
}

static void
calendar_day_view_class_init (CalendarDayViewClass *klass)
{
	GObjectClass *gobject_class;
	GtkWidgetClass *widget_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_DAY_VIEW_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);
	widget_class = GTK_WIDGET_CLASS (klass);

	gobject_class->dispose = calendar_day_view_dispose;

	widget_class->realize            = calendar_day_view_realize;
	widget_class->unrealize          = calendar_day_view_unrealize;
	widget_class->expose_event       = calendar_day_view_expose;
	widget_class->size_request       = calendar_day_view_size_request;
	widget_class->size_allocate      = calendar_day_view_size_allocate;
	widget_class->button_press_event = calendar_day_view_button_press;

	parent_class = g_type_class_peek_parent (klass);
}

static void
calendar_day_view_init (CalendarDayView *cdv)
{
	g_return_if_fail (cdv != NULL);
	g_return_if_fail (CALENDAR_IS_DAY_VIEW (cdv));

	cdv->priv = g_new0 (CalendarDayViewPriv, 1);

	g_date_clear (&cdv->priv->date, 1);
	g_date_set_time (&cdv->priv->date, time (NULL));

	cdv->priv->children = NULL;
	cdv->priv->lower_bounds = DEFAULT_CAL_DAY_START * 60; /* FIXME: gconf */
	cdv->priv->upper_bounds = DEFAULT_CAL_DAY_END   * 60; /* FIXME: gconf */
	cdv->priv->interval = 30; /* FIXME: gconf */

	GTK_WIDGET_UNSET_FLAGS (GTK_WIDGET (cdv), GTK_NO_WINDOW);
	GTK_WIDGET_SET_FLAGS (GTK_WIDGET (cdv), GTK_CAN_FOCUS);
}

static void
calendar_day_view_dispose (GObject *object)
{
	CalendarDayView *cdv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_DAY_VIEW (object));

	cdv = CALENDAR_DAY_VIEW (object);

	if (cdv->priv) {
		GList *item;

		g_object_unref (G_OBJECT (cdv->priv->calendar));

		for (item = cdv->priv->appointments; item != NULL; item = g_list_next (item))
			g_object_unref (G_OBJECT (item->data));
		g_list_free (cdv->priv->appointments);

		g_free (cdv->priv);
		cdv->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
calendar_day_view_realize (GtkWidget *widget)
{
	CalendarDayView *cdv;
	CalendarDayViewPriv *priv;
	GList *child;
	GdkWindowAttr attributes;
	gint attributes_mask;

	cdv = CALENDAR_DAY_VIEW (widget);
	priv = cdv->priv;

	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x           = widget->allocation.x;
	attributes.y           = widget->allocation.y;
	attributes.width       = widget->allocation.width;
	attributes.height      = widget->allocation.height;
	attributes.wclass      = GDK_INPUT_OUTPUT;
	attributes.visual      = gtk_widget_get_visual (widget);
	attributes.colormap    = gtk_widget_get_colormap (widget);
	attributes.event_mask  =
		gtk_widget_get_events (widget)	|
		GDK_EXPOSURE_MASK		|
		GDK_BUTTON_PRESS_MASK
#if 0 /* FIXME */
		GDK_BUTTON_RELEASE_MASK        |
		GDK_BUTTON_MOTION_MASK         |
		GDK_POINTER_MOTION_HINT_MASK
#endif
		;

	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

	widget->window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
	gdk_window_set_user_data (widget->window, widget);

	widget->style = gtk_style_attach (widget->style, widget->window);
	gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);

#if 0 /* FIXME */
	fullday->up_down_cursor = gdk_cursor_new (GDK_DOUBLE_ARROW);
	fullday->beam_cursor    = gdk_cursor_new (GDK_XTERM);
#endif

	for (child = priv->children; child != NULL; child = g_list_next (child))
		child_realize (cdv, child->data);
}

static void
calendar_day_view_unrealize (GtkWidget *widget)
{
	CalendarDayView *cdv;
	CalendarDayViewPriv *priv;
	GList *child;

	cdv = CALENDAR_DAY_VIEW (widget);
	priv = cdv->priv;

	for (child = priv->children; child != NULL; child = g_list_next (child))
		child_unrealize (cdv, child->data);

#if 0 /* FIXME */
	gdk_cursor_unref (fullday->up_down_cursor);
	fullday->up_down_cursor = NULL;

	gdk_cursor_unref (fullday->beam_cursor);
	fullday->beam_cursor = NULL;

	if (fullday->allday_gc)
		g_object_unref (fullday->allday_gc);

	if (fullday->bell_gc)
		g_object_unref (fullday->bell_gc);

	if (fullday->recur_gc)
		g_object_unref (fullday->recur_gc);

	if (pixmap_allday){
		g_object_unref (pixmap_allday);
		pixmap_allday = NULL;
	}

	if (pixmap_bell){
		g_object_unref (pixmap_bell);
		pixmap_bell = NULL;
	}
	
	if (pixmap_recur){
		g_object_unref (pixmap_recur);
		pixmap_recur = NULL;
	}
#endif
		
	if (GTK_WIDGET_CLASS (parent_class)->unrealize)
		(* GTK_WIDGET_CLASS (parent_class)->unrealize) (widget);
}

static gboolean
calendar_day_view_expose (GtkWidget *widget, GdkEventExpose *event)
{
	CalendarDayView *cdv;

	if (!GTK_WIDGET_DRAWABLE (widget))
		return TRUE;

	cdv = CALENDAR_DAY_VIEW (widget);

	if (event->window == widget->window) {
		draw (cdv, &event->area);
	} else {
#if 0 /* FIXME */
		Child *child;
		int on_text;

		child = find_child_by_window (fullday, event->window, &on_text);

		if (child && !on_text)
			child_draw (fullday, child, &event->area, event->window);
#endif
	}

	return TRUE;
}

static void
calendar_day_view_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
	CalendarDayView *cdv = CALENDAR_DAY_VIEW (widget);
	GtkStyle *style = gtk_widget_get_style (widget);
	gint label_width;
	guint rows;
	gint fontsize;

	fontsize = pango_font_description_get_size (style->font_desc) / PANGO_SCALE + 1;

	/* Border and min width */

	label_width = time_column_width (cdv);

	requisition->width  = 4 * TEXT_BORDER + label_width + MIN_TEXT_WIDTH;

	/* Rows */

	{
		guint begin = 0, end = 1440;
		used_rows (cdv, &begin, &end, NULL, &rows);
	}

	requisition->height = rows * (2 * TEXT_BORDER + fontsize);
	requisition->height += rows - 1; /* division lines */
}

static void
calendar_day_view_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
	CalendarDayView *cdv;

	widget->allocation = *allocation;

	cdv = CALENDAR_DAY_VIEW (widget);

	if (GTK_WIDGET_REALIZED (widget))
		gdk_window_move_resize (widget->window,
					allocation->x, allocation->y,
					allocation->width, allocation->height);

#if 0 /* FIXME */
	layout_children (cdv);
#endif
}

static gboolean
calendar_day_view_button_press (GtkWidget *widget, GdkEventButton *event)
{
	gtk_widget_grab_focus (widget);

	return TRUE;
}

/*
 * Child Handling
 */

static void
child_realize (CalendarDayView *cdv, void *child)
{
	/* FIXME */
}

static void
child_unrealize (CalendarDayView *cdv, void *child)
{
	/* FIXME */
}

/*
 * Private Methods
 */

static void
update_children (CalendarDayView *cdv)
{
	/* FIXME */
}

static void
add_appointment (CalendarDayView *cdv, MIMEDirVEvent *appointment)
{
	CalendarDayViewPriv *priv = cdv->priv;

	g_log (NULL, G_LOG_LEVEL_DEBUG, "adding appointment %p to day", appointment);

	g_object_ref (G_OBJECT (appointment));
	priv->appointments = g_list_prepend (priv->appointments, appointment);

	update_children (cdv);
}

static void
remove_appointment (CalendarDayView *cdv, MIMEDirVEvent *appointment)
{
	CalendarDayViewPriv *priv = cdv->priv;

	g_log (NULL, G_LOG_LEVEL_DEBUG, "removing appointment %p from day", appointment);

	priv->appointments = g_list_remove (priv->appointments, appointment);
	g_object_unref (G_OBJECT (appointment));

	update_children (cdv);
}

static void
remove_all_appointments (CalendarDayView *cdv)
{
	CalendarDayViewPriv *priv = cdv->priv;

	while (priv->appointments)
		remove_appointment (cdv, MIMEDIR_VEVENT (priv->appointments->data));
}

static void
add_appointments_of_today (CalendarDayView *cdv)
{
	CalendarDayViewPriv *priv = cdv->priv;
	GList *list, *item;

	g_assert (priv->calendar != NULL);

	/* Load appointments */

	list = calendar_manager_get_appointments_intersecting (priv->calendar, &priv->date, &priv->date);

	for (item = list; item != NULL; item = g_list_next (item))
		add_appointment (cdv, MIMEDIR_VEVENT (item->data));

	g_list_free (list);
}

/*
 * Drawing
 */

/*
 * This function computes, which rows are used for a time interval given
 * by "time_lower" and "time_upper" (in minutes since 0:00). The first
 * row is 0:00 of each day. The first row used is returned in "first_row"
 * (if not NULL) and the number of rows used is returned in "row_count"
 * (if not NULL).
 *
 * The variables "time_lower" and "time_upper" will be changed to contain
 * the start and end time of the selected rows, respectively.
 */
static void
used_rows (CalendarDayView *cdv,
	   guint *time_lower, guint *time_upper,
	   guint *first_row, guint *row_count)
{
	CalendarDayViewPriv *priv = cdv->priv;
	guint lower, upper;

	g_assert (cdv->priv->interval > 0);

	lower = time_lower ? *time_lower : priv->lower_bounds;
	upper = time_upper ? *time_upper : priv->upper_bounds;

	g_assert (lower <= upper);

	lower = lower / cdv->priv->interval;
	upper = (upper - 1) / cdv->priv->interval;

	g_assert (upper - lower >= 0);

	if (time_lower)
		*time_lower = lower * cdv->priv->interval;
	if (time_upper)
		*time_upper = (upper + 1) * cdv->priv->interval;
	if (first_row)
		*first_row = lower;
	if (row_count)
		*row_count = upper - lower + 1;
}

static gint
row_height (CalendarDayView *cdv)
{
	GtkWidget *widget = GTK_WIDGET (cdv);
	guint row_count;
	guint start = 0, end = 1440;

	g_assert (GTK_WIDGET_REALIZED (GTK_WIDGET (cdv)));

	used_rows (cdv, &start, &end, NULL, &row_count);

	return widget->allocation.height / row_count;
}

/* FIXME: this should be cached, really... */
static gint
time_column_width (CalendarDayView *cdv)
{
	CalendarDayViewPriv *priv = cdv->priv;
	guint max_width;
	gint time;

	max_width = 0;

	for (time = 0; time < 1400; time += priv->interval) {
		PangoLayout *layout;
		gchar *s;
		gint width;

		s = time_get_short_time_string (time / 60, time % 60);
		layout = gtk_widget_create_pango_layout (GTK_WIDGET (cdv), s);
		pango_layout_get_pixel_size (layout, &width, NULL);
		g_object_unref (G_OBJECT (layout));
		g_free (s);

		if (width > max_width)
			max_width = width;
	}

	return max_width;
}

static void
draw_focus (CalendarDayView *cdv)
{
	GtkWidget *widget = GTK_WIDGET (cdv);

	if (GTK_WIDGET_HAS_FOCUS (widget)) {
		gtk_paint_focus (widget->style, widget->window,
				 GTK_WIDGET_STATE (widget),
				 &widget->allocation,
				 widget,
				 NULL,
				 1, 1,
				 widget->allocation.width - 2,
				 widget->allocation.height - 2);
	}
}

static void
draw_row (CalendarDayView *cdv, GdkRectangle *area, gint row)
{
	CalendarDayViewPriv *priv = cdv->priv;
	GtkWidget *widget = GTK_WIDGET (cdv);
	GtkAllocation *allocation = &widget->allocation;
	GtkStyle *style;
	GdkRectangle rect, dest;
	GdkGC *left_gc, *right_gc, *text_gc;

	gint r_height = row_height (cdv);
	gint label_width = time_column_width (cdv);
	gint begin_row, end_row;

	style = gtk_widget_get_style (widget);

	begin_row = priv->lower_bounds / priv->interval;
	end_row   = priv->upper_bounds / priv->interval;

	/* Choose GCs to use */

#if 0 /* FIXME */
	if ((p->di->sel_rows_used != 0)
	    && (row >= p->di->sel_start_row)
	    && (row < (p->di->sel_start_row + p->di->sel_rows_used))) {
		left_gc  = style->bg_gc[GTK_STATE_SELECTED];
		right_gc = left_gc;
		text_gc  = style->fg_gc[GTK_STATE_SELECTED];
	} else
#endif

	if (row < begin_row || row >= end_row) {
		left_gc  = style->bg_gc[GTK_STATE_NORMAL];
		right_gc = style->bg_gc[GTK_STATE_ACTIVE];
		text_gc  = style->fg_gc[GTK_STATE_NORMAL];
	} else {
		left_gc  = style->bg_gc[GTK_STATE_NORMAL];
		right_gc = style->bg_gc[GTK_STATE_PRELIGHT];
		text_gc  = style->fg_gc[GTK_STATE_NORMAL];
	}

	/* Left background and text */

	rect.x      = 0;
	rect.y      = row * (r_height + 1);
	rect.width  = 2 * TEXT_BORDER + label_width;
	rect.height = r_height;

	if (gdk_rectangle_intersect (&rect, area, &dest)) {
		PangoLayout *layout;
		guint time = row * priv->interval;
		gchar *s;

		gdk_draw_rectangle (widget->window,
				    left_gc,
				    TRUE,
				    dest.x, dest.y,
				    dest.width, dest.height);

		s = time_get_short_time_string (time / 60, time % 60);
		layout = gtk_widget_create_pango_layout (widget, s);
		gdk_draw_layout (widget->window, text_gc,
				 rect.x + TEXT_BORDER,
				 rect.y + TEXT_BORDER,
				 layout);
		g_object_unref (G_OBJECT (layout));
		g_free (s);
	}

	/* Right background */

	rect.x = 2 * TEXT_BORDER + label_width;
	rect.width = allocation->width -  2 * TEXT_BORDER - label_width;

	if (gdk_rectangle_intersect (&rect, area, &dest)) {
		gdk_draw_rectangle (widget->window,
				    right_gc,
				    TRUE,
				    dest.x, dest.y,
				    dest.width, dest.height);
	}
}

static void
draw (CalendarDayView *cdv, GdkRectangle *area)
{
	GtkWidget *widget = GTK_WIDGET (cdv);
	GtkAllocation *allocation = &widget->allocation;
	GtkStyle *style;
	GdkRectangle local_area;

	gint i;
	gint r_height = row_height (cdv);
	gint start_row, end_row;
	guint row_count;
	guint start, end;

	style = gtk_widget_get_style (widget);

	/* Prepare area */

	if (!area) {
		area = &local_area;

		area->x      = 0;
		area->y      = 0;
		area->width  = allocation->width;
		area->height = allocation->height;
	}

	/* Rows */

	start = 0;
	end = 1440;
	used_rows (cdv, &start, &end, NULL, &row_count);

	start_row = area->y / (r_height + 1);
	end_row   = (area->y + area->height) / (r_height + 1);

	if (end_row >= row_count)
		end_row = row_count - 1;

	for (i = start_row; i <= end_row; i++) {
		draw_row (cdv, area, i);

		/* Horizontal division at bottom of row */

		if (i != end_row) {
			GdkRectangle rect, dest;

			rect.x      = 0;
			rect.y      = i * (r_height + 1) + r_height;
			rect.width  = allocation->width;
			rect.height = 1;

			if (gdk_rectangle_intersect (&rect, area, &dest)) {
				gdk_draw_line (widget->window,
					       widget->style->black_gc,
					       rect.x, rect.y,
					       rect.x + rect.width, rect.y);
			}
		}
	}

	/* Focus */

	draw_focus (cdv);
}

/*
 * Public Methods
 */

GtkWidget *
calendar_day_view_new (void)
{
	CalendarDayView *cdv;

	cdv = g_object_new (CALENDAR_TYPE_DAY_VIEW, NULL);

	return GTK_WIDGET (cdv);
}

void
calendar_day_view_set_calendar (CalendarDayView *cdv, CalendarManager *calendar)
{
	g_return_if_fail (cdv != NULL);
	g_return_if_fail (CALENDAR_IS_DAY_VIEW (cdv));
	g_return_if_fail (calendar != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (calendar));

	g_return_if_fail (cdv->priv->calendar == NULL);

	cdv->priv->calendar = calendar;
	g_object_ref (G_OBJECT (calendar));

	add_appointments_of_today (cdv);

#if 0 /* FIXME */
	priv->todo_added_sig = g_signal_connect_swapped (G_OBJECT (cal), "todo-added",
		G_CALLBACK (todo_added_cb), todo);
	priv->todo_removed_sig = g_signal_connect_swapped (G_OBJECT (cal), "todo-removed",
		G_CALLBACK (todo_removed_cb), todo);
	priv->todo_edited_sig = g_signal_connect_swapped (G_OBJECT (cal), "todo-edited",
		G_CALLBACK (todo_edited_cb), todo);
#endif
}

void
calendar_day_view_set_date (CalendarDayView *cdv, GDate *date)
{
	CalendarDayViewPriv *priv;

	g_return_if_fail (cdv != NULL);
	g_return_if_fail (CALENDAR_IS_DAY_VIEW (cdv));
	g_return_if_fail (date != NULL);
	g_return_if_fail (g_date_valid (date));

	priv = cdv->priv;

	if (g_date_compare (&priv->date, date) == 0)
		return;

	priv->date = *date;

	remove_all_appointments (cdv);
	add_appointments_of_today (cdv);
}

/* time is in minutes since the start of the day. */
gint
calendar_day_view_get_y_pos_for_time (CalendarDayView *cdv, gint time)
{
	gint row, height;

	g_return_val_if_fail (cdv != NULL, 0);
	g_return_val_if_fail (CALENDAR_IS_DAY_VIEW (cdv), 0);
	g_return_val_if_fail (time >= 0 && time < 1440, 0);
	g_return_val_if_fail (GTK_WIDGET_REALIZED (GTK_WIDGET (cdv)), 0);

	row = time / cdv->priv->interval;

	height = row_height (cdv);

	return row * height;
}
