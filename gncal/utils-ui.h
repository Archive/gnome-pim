#ifndef __UTILS_UI_H__
#define __UTILS_UI_H__

/* Miscellaneous UI Utility Functions
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <time.h>

#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include <gtk/gtkwindow.h>

/* Open the help file at a specific anchor. */
void ui_show_help (GtkWindow *parent, const gchar *anchor);

/* Create a geometry string from a Gtk+ window. */
gchar *ui_gtk_window_get_geometry (GtkWindow *win);

#endif /* __UTILS_UI_H__ */
