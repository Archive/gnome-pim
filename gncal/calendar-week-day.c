/* Calendar Week Day Object
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * FIXME:
 *  o buffer pango layout
 *  o week days bold
 */

#include <string.h>
#include <time.h>

#include <glib.h>
#include <gnome.h>

#include "calendar-week-day.h"

static void	calendar_week_day_class_init	(CalendarWeekDayClass	*klass);
static void	calendar_week_day_init		(CalendarWeekDay	*cw);
static void	calendar_week_day_finalize	(GObject		*object);
static void	calendar_week_day_size_request	(GtkWidget		*widget,
						 GtkRequisition		*requisition);
static void	calendar_week_day_realize	(GtkWidget		*widget);
static gboolean	calendar_week_day_expose	(GtkWidget		*widget,
						 GdkEventExpose		*event);
static gboolean	calendar_week_day_button_press	(GtkWidget		*widget,
						 GdkEventButton		*event);
static gboolean	calendar_week_day_key_event	(GtkWidget		*widget,
						 GdkEventKey		*event);
static gboolean calendar_week_day_enter		(GtkWidget		*widget,
						 GdkEventCrossing	*event);
static gboolean calendar_week_day_leave		(GtkWidget		*widget,
						 GdkEventCrossing	*event);
static gboolean	calendar_week_day_focus_event	(GtkWidget		*widget,
						 GdkEventFocus		*event);

static void	update				(CalendarWeekDay	*cwd);
static void	draw_text			(CalendarWeekDay	*cwd,
						 GdkRectangle		*rect);


struct _CalendarWeekDayPriv {
	GDate date;

	gchar *title;
	gchar *text;

	/* State */

	gboolean prelight;
	gboolean active;
	gboolean parent_focussed;
};


enum {
	SIGNAL_CLICKED,
	SIGNAL_SELECTED,
	SIGNAL_MENU,
	SIGNAL_LAST
};


static GtkWidgetClass *parent_class = NULL;

static guint signals[SIGNAL_LAST] = { 0 };

/*
 * Class and Instance Setup
 */

GType
calendar_week_day_get_type (void)
{
	static GType calendar_week_day_type = 0;

	if (!calendar_week_day_type) {
		static const GTypeInfo calendar_week_day_info = {
			sizeof (CalendarWeekDayClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_week_day_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarWeekDay),
			0,    /* n_preallocs */
			(GInstanceInitFunc) calendar_week_day_init,
		};

		calendar_week_day_type = g_type_register_static (GTK_TYPE_WIDGET,
								 "CalendarWeekDay",
								 &calendar_week_day_info,
								 0);
	}

	return calendar_week_day_type;
}

static void
calendar_week_day_class_init (CalendarWeekDayClass *klass)
{
	GObjectClass *gobject_class;
	GtkWidgetClass *widget_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_DAY_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);
	widget_class  = GTK_WIDGET_CLASS (klass);

	gobject_class->finalize          = calendar_week_day_finalize;

	widget_class->realize            = calendar_week_day_realize;
	widget_class->size_request       = calendar_week_day_size_request;
	widget_class->expose_event       = calendar_week_day_expose;
	widget_class->button_press_event = calendar_week_day_button_press;
	widget_class->key_press_event    = calendar_week_day_key_event;
	widget_class->enter_notify_event = calendar_week_day_enter;
	widget_class->leave_notify_event = calendar_week_day_leave;
	widget_class->focus_in_event     = calendar_week_day_focus_event;
	widget_class->focus_out_event    = calendar_week_day_focus_event;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_CLICKED] =
		g_signal_new ("clicked",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarWeekDayClass, clicked),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	signals[SIGNAL_SELECTED] =
		g_signal_new ("selected",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarWeekDayClass, selected),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	signals[SIGNAL_MENU] =
		g_signal_new ("menu",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarWeekDayClass, menu),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
}

static void
calendar_week_day_init (CalendarWeekDay *cwd)
{
	g_return_if_fail (cwd != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_DAY (cwd));

	GTK_WIDGET_SET_FLAGS (GTK_WIDGET (cwd), GTK_CAN_FOCUS);
	GTK_WIDGET_UNSET_FLAGS (GTK_WIDGET (cwd), GTK_NO_WINDOW);

	cwd->priv = g_new0 (CalendarWeekDayPriv, 1);

	cwd->priv->title = NULL;
	cwd->priv->text  = NULL;
	g_date_set_time (&cwd->priv->date, time (NULL));
	cwd->priv->prelight = FALSE;
	cwd->priv->active = FALSE;
	cwd->priv->parent_focussed = FALSE;

	update (cwd);
}

static void
calendar_week_day_finalize (GObject *object)
{
	CalendarWeekDay *cwd;
	CalendarWeekDayPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_DAY (object));

	cwd = CALENDAR_WEEK_DAY (object);

	priv = cwd->priv;
	if (priv) {
		g_free (priv->title);
		g_free (priv->text);

		g_free (priv);
		cwd->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

#define MIN_TITLE_WIDTH 50
#define TEXT_BORDER 6

static void
calendar_week_day_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
	CalendarWeekDay *cwd;
	GtkStyle *style;
	PangoLayout *layout;
	int str_height, str_width, width;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_DAY (widget));
	g_return_if_fail (requisition != NULL);

	cwd = CALENDAR_WEEK_DAY (widget);

	/* border and min width */

	style = gtk_widget_get_style (widget);

	layout = gtk_widget_create_pango_layout (widget, cwd->priv->title);
	pango_layout_get_pixel_size (layout, &str_width, &str_height);

	width = MAX (MIN_TITLE_WIDTH, str_width);

	requisition->width  = 2 * (style->xthickness + TEXT_BORDER) + width;
	requisition->height = 2 * (style->ythickness + TEXT_BORDER);

	/* division line */

	requisition->height += 2 * TEXT_BORDER + style->ythickness;

	/* title and at least one line of text */

	requisition->height += 2 * str_height;
}

static void
calendar_week_day_realize (GtkWidget *widget)
{
	GdkWindow *parent;
	GdkWindowAttr attributes;
	gint attributes_mask;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_DAY (widget));

	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x = widget->allocation.x;
	attributes.y = widget->allocation.y;
	attributes.width = widget->allocation.width;
	attributes.height = widget->allocation.height;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gtk_widget_get_visual (widget);
	attributes.colormap = gtk_widget_get_colormap (widget);
	attributes.event_mask = (gtk_widget_get_events (widget)	|
				 GDK_EXPOSURE_MASK		|
				 GDK_ENTER_NOTIFY_MASK		|
				 GDK_LEAVE_NOTIFY_MASK		|
				 GDK_BUTTON_PRESS_MASK		|
				 GDK_KEY_PRESS_MASK);

	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

	parent = gtk_widget_get_parent_window (widget);
	widget->window = gdk_window_new (parent, &attributes, attributes_mask);
	gdk_window_set_user_data (widget->window, widget);

	widget->style = gtk_style_attach (widget->style, widget->window);

	gdk_window_set_background (widget->window, &widget->style->bg[GTK_STATE_NORMAL]);
}

static gboolean
calendar_week_day_expose (GtkWidget *widget, GdkEventExpose *event)
{
	CalendarWeekDay *cwd;
	CalendarWeekDayPriv *priv;
	GtkStyle *style;
	PangoLayout *layout;
	GdkRectangle rect, dest;
	int width, height;
	int str_width, str_height;
	gint head_state, body_state;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (CALENDAR_IS_WEEK_DAY (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	if (!GTK_WIDGET_DRAWABLE (widget))
		return TRUE;

	cwd = CALENDAR_WEEK_DAY (widget);
	priv = cwd->priv;

	style = gtk_widget_get_style (widget);
	body_state = cwd->priv->prelight ? GTK_STATE_PRELIGHT : GTK_STATE_NORMAL;
	head_state = cwd->priv->active && (GTK_WIDGET_HAS_FOCUS (widget) || priv->parent_focussed) ? GTK_STATE_SELECTED : body_state;

	width  = widget->allocation.width;
	height = widget->allocation.height;

	/* Clear and paint title */

	layout = gtk_widget_create_pango_layout (widget, cwd->priv->title);
	pango_layout_get_pixel_size (layout, &str_width, &str_height);

	rect.x = 0;
	rect.y = 0;
	rect.width = width;
	rect.height = 2 * TEXT_BORDER + str_height;

	if (gdk_rectangle_intersect (&rect, &event->area, &dest)) {
		GdkGC *gc;

		gc = cwd->priv->active ? style->dark_gc[head_state] : style->mid_gc[head_state];

		gdk_draw_rectangle (widget->window, gc, TRUE,
				    dest.x, dest.y, dest.width, dest.height);

		dest = rect;

		dest.x += TEXT_BORDER;
		dest.y += TEXT_BORDER;
		dest.width  -= 2 * TEXT_BORDER;
		dest.height -= 2 * TEXT_BORDER;

		gc = style->fg_gc[head_state];
		gdk_gc_set_clip_rectangle (gc, &dest);

		pango_layout_get_pixel_size (layout, &str_width, NULL);

		gdk_draw_layout (widget->window, gc,
				 dest.x + (dest.width - str_width) / 2,
				 dest.y,
				 layout);

		gdk_gc_set_clip_rectangle (gc, NULL);
	}

	/* Text */

	rect.x = 0;
	rect.y = 2 * TEXT_BORDER + str_height;
	rect.width = width;
	rect.height = height - rect.y;

	if (gdk_rectangle_intersect (&rect, &event->area, &dest)) {
		gdk_draw_rectangle (widget->window,
				    widget->style->mid_gc[body_state],
				    TRUE,
				    dest.x, dest.y,
				    dest.width, dest.height);
	}

	if (cwd->priv->text) {
		rect.x += TEXT_BORDER;
		rect.y += TEXT_BORDER;
		rect.width  -= 2 * TEXT_BORDER;
		rect.height -= 2 * TEXT_BORDER;

		if (gdk_rectangle_intersect (&rect, &event->area, &dest))
			draw_text (cwd, &rect);
	}

	/* Focus */

	if (GTK_WIDGET_HAS_FOCUS (widget)) {
		rect.x = 0;
		rect.y = 0;
		rect.width  = widget->allocation.width;
		rect.height = widget->allocation.height;

		gtk_paint_focus (widget->style, widget->window,
				 GTK_WIDGET_STATE (widget),
				 &rect,
				 widget,
				 NULL,
				 1, 2 * TEXT_BORDER + str_height + 1,
				 widget->allocation.width - 2,
				 widget->allocation.height - 2 * TEXT_BORDER - str_height - 2);
	}

	return TRUE;
}

static gboolean
calendar_week_day_button_press (GtkWidget *widget, GdkEventButton *event)
{
	CalendarWeekDay *cwd;

	cwd = CALENDAR_WEEK_DAY (widget);

	gtk_widget_grab_focus (widget);

	if (event->button == 1) {
		if (event->type == GDK_BUTTON_PRESS)
			g_signal_emit (G_OBJECT (cwd), signals[SIGNAL_CLICKED], 0);
		else if (event->type == GDK_2BUTTON_PRESS)
			g_signal_emit (G_OBJECT (cwd), signals[SIGNAL_SELECTED], 0);
	} else if (event->button == 3) {
		g_signal_emit (G_OBJECT (cwd), signals[SIGNAL_MENU], 0);
	}

	return TRUE;
}

static gboolean
calendar_week_day_key_event (GtkWidget *widget, GdkEventKey *event)
{
	CalendarWeekDay *cwd;

	cwd = CALENDAR_WEEK_DAY (widget);

	if (event->state != 0)
		return FALSE;

	switch (event->keyval) {
	case GDK_Return:
	case GDK_KP_Enter:
		gtk_widget_grab_focus (widget);
		g_signal_emit (G_OBJECT (cwd), signals[SIGNAL_SELECTED], 0);
		return TRUE;

	case GDK_space:
		gtk_widget_grab_focus (widget);
		g_signal_emit (G_OBJECT (cwd), signals[SIGNAL_CLICKED], 0);
		return TRUE;

	default:
		return FALSE;
	}
}

static gboolean
calendar_week_day_enter (GtkWidget *widget, GdkEventCrossing *event)
{
	CALENDAR_WEEK_DAY (widget)->priv->prelight = TRUE;

	gtk_widget_queue_draw (widget);

	return TRUE;
}

static gboolean
calendar_week_day_leave (GtkWidget *widget, GdkEventCrossing *event)
{
	CALENDAR_WEEK_DAY (widget)->priv->prelight = FALSE;

	gtk_widget_queue_draw (widget);

	return TRUE;
}

static gboolean
calendar_week_day_focus_event (GtkWidget *widget, GdkEventFocus *event)
{
	gtk_widget_queue_draw (widget);

	return FALSE;
}

/*
 * Internal Methods
 */

static void
update (CalendarWeekDay *cwd)
{
	CalendarWeekDayPriv *priv = cwd->priv;
	gchar buffer[100];

	g_date_strftime (buffer, sizeof (buffer), _("%A %d"), &priv->date);

	g_free (priv->title);
	priv->title = g_strdup (buffer);

	gtk_widget_queue_draw (GTK_WIDGET (cwd));
}

typedef struct {
	PangoLayout *layout;
	int lines;
	int assigned_lines;
} line_info_t;

static void
draw_text (CalendarWeekDay *cwd, GdkRectangle *rect)
{
	CalendarWeekDayPriv *priv = cwd->priv;
	gchar *cpy, *tmp;
	line_info_t *lines;
	gint para_count, fontsize;
	gint lines_per_para, extra_lines;
	gboolean lines_missing;
	gint i;

	if (!priv->text)
		return;

	/* Calculate number of lines */

	cpy = g_strdup (priv->text);

	para_count = 0;
	for (tmp = cpy; *tmp != '\0'; tmp++) {
		if (*tmp == '\n') {
			para_count++;
			*tmp = '\0';
		}
	}

	/* Measure size requirements */

	lines = g_new0 (line_info_t, para_count);

	for (i = 0, tmp = cpy; i < para_count; i++, tmp += strlen (tmp) + 1) {
		lines[i].layout =
			gtk_widget_create_pango_layout (GTK_WIDGET (cwd), tmp);
		pango_layout_set_width  (lines[i].layout, rect->width * PANGO_SCALE);
		pango_layout_set_wrap   (lines[i].layout, PANGO_WRAP_WORD);
		pango_layout_set_indent (lines[i].layout, -4 * PANGO_SCALE);
		lines[i].lines = pango_layout_get_line_count (lines[i].layout);
	}

	/* Compute lines per paragraph */

	{
		GtkStyle *style;
		gint maxy, available_lines;

		style = gtk_widget_get_style (GTK_WIDGET (cwd));
		fontsize = pango_font_description_get_size (style->font_desc) / PANGO_SCALE + 1;

		maxy = rect->y + rect->height - fontsize;
		maxy *= 2;

		available_lines = 1 + maxy / fontsize;
		lines_per_para  = available_lines / para_count;
		extra_lines     = available_lines % para_count;
	}

	lines_missing = FALSE;

	for (i = 0; i < para_count; i++){
		if (lines[i].lines <= lines_per_para){
			extra_lines += lines_per_para - lines [i].lines;
			lines[i].assigned_lines = lines[i].lines;
		} else {
			lines_missing = TRUE;
			lines[i].assigned_lines = lines_per_para;
		}
	}

	/* Give extra space to clipped paragraphs */

	while (lines_missing && extra_lines > 0) {
		lines_missing = FALSE;
		
		for (i = 0; i < para_count; i++){
			if (lines[i].lines > lines[i].assigned_lines){
				lines[i].assigned_lines++;
				extra_lines--;
			}

			if (extra_lines == 0)
				break;
			
			if (lines[i].lines > lines[i].assigned_lines)
				lines_missing = TRUE;
		}
	}

	/* Render */

	{
		GdkGC *gc;
		GtkStyle *style;
		gint y;

		style = gtk_widget_get_style (GTK_WIDGET (cwd));
		gc = style->fg_gc[GTK_STATE_NORMAL];
		gdk_gc_set_clip_rectangle (gc, rect);

		y = rect->y + fontsize;

		for (i = 0; i < para_count; i++) {
			gint line;
			GSList *line_list;

			line_list = pango_layout_get_lines (lines[i].layout);

			for (line = 0; line < lines[i].assigned_lines; line++) {
				gdk_draw_layout_line (GTK_WIDGET (cwd)->window, gc,
						      rect->x, y,
						      line_list->data);

				y += fontsize;
				line_list = line_list->next;
			}
		}

		gdk_gc_set_clip_rectangle (gc, NULL);
	}

	/* Cleanup */

	g_free (lines);	
}

/*
 * Public Methods
 */

GtkWidget *
calendar_week_day_new (void)
{
	CalendarWeekDay *cwd;

	cwd = g_object_new (CALENDAR_TYPE_WEEK_DAY, NULL);

	return GTK_WIDGET (cwd);
}

void
calendar_week_day_set_date (CalendarWeekDay *cwd, const GDate *date)
{
	CalendarWeekDayPriv *priv;

	g_return_if_fail (cwd != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_DAY (cwd));
	g_return_if_fail (date != NULL);
	g_return_if_fail (g_date_valid (date));

	priv = cwd->priv;

	memcpy (&priv->date, date, sizeof (priv->date));

	update (cwd);
}

void
calendar_week_day_get_date (CalendarWeekDay *cwd, GDate *date)
{
	CalendarWeekDayPriv *priv;

	g_return_if_fail (cwd != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_DAY (cwd));
	g_return_if_fail (date != NULL);

	priv = cwd->priv;

	memcpy (date, &priv->date, sizeof (priv->date));
}

void
calendar_week_day_set_text (CalendarWeekDay *cwd, const gchar *text)
{
	CalendarWeekDayPriv *priv;

	g_return_if_fail (cwd != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_DAY (cwd));

	priv = cwd->priv;

	g_free (priv->text);
	priv->text = g_strdup (text);

	gtk_widget_queue_draw (GTK_WIDGET (cwd));
}

const gchar *
calendar_week_day_get_text (CalendarWeekDay *cwd)
{
	g_return_val_if_fail (cwd != NULL, NULL);
	g_return_val_if_fail (CALENDAR_IS_WEEK_DAY (cwd), NULL);

	return cwd->priv->text;
}

void
calendar_week_day_set_active (CalendarWeekDay *cwd, gboolean active)
{
	g_return_if_fail (cwd != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_DAY (cwd));

	cwd->priv->active = active ? TRUE : FALSE;

	gtk_widget_queue_draw (GTK_WIDGET (cwd));
}

gboolean
calendar_week_day_get_active (CalendarWeekDay *cwd)
{
	g_return_val_if_fail (cwd != NULL, FALSE);
	g_return_val_if_fail (CALENDAR_IS_WEEK_DAY (cwd), FALSE);

	return cwd->priv->active;
}

void
calendar_week_day_set_parent_focussed (CalendarWeekDay *cwd, gboolean focussed)
{
	CalendarWeekDayPriv *priv;

	g_return_if_fail (cwd != NULL);
	g_return_if_fail (CALENDAR_IS_WEEK_DAY (cwd));

	priv = cwd->priv;

	if ((priv->parent_focussed && !focussed) ||
	    (!priv->parent_focussed && focussed)) {
		priv->parent_focussed = focussed ? TRUE : FALSE;
		gtk_widget_queue_draw (GTK_WIDGET (cwd));
	}
}
