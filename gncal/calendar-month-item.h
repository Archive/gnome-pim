#ifndef __CALENDAR_MONTH_ITEM_H__
#define __CALENDAR_MONTH_ITEM_H__

/* Calendar Month Item Object
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib-object.h>

#include <libgnomecanvas/gnome-canvas.h>

#include "calendar-manager.h"

#define CALENDAR_TYPE_MONTH_ITEM		(calendar_month_item_get_type())
#define CALENDAR_MONTH_ITEM(obj)		(GTK_CHECK_CAST ((obj), CALENDAR_TYPE_MONTH_ITEM, CalendarMonthItem))
#define CALENDAR_MONTH_ITEM_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_MONTH_ITEM))
#define CALENDAR_IS_MONTH_ITEM(obj)		(GTK_CHECK_TYPE ((obj), CALENDAR_TYPE_MONTH_ITEM))
#define CALENDAR_IS_MONTH_ITEM_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_MONTH_ITEM))

typedef struct _CalendarMonthItem	CalendarMonthItem;
typedef struct _CalendarMonthItemClass	CalendarMonthItemClass;
typedef struct _CalendarMonthItemPriv	CalendarMonthItemPriv;

struct _CalendarMonthItem
{
	GnomeCanvasGroup parent;

	CalendarMonthItemPriv *priv;
};

struct _CalendarMonthItemClass
{
	GnomeCanvasGroupClass parent_class;

	void (* day_changed)  (CalendarMonthItem *mitem, guint year, guint month, guint day);
	void (* day_selected) (CalendarMonthItem *mitem, guint year, guint month, guint day);
	void (* day_menu)     (CalendarMonthItem *mitem, guint year, guint month, guint day);
};

GType		 calendar_month_item_get_type	(void);

GnomeCanvasItem *
		calendar_month_item_new		(GnomeCanvasGroup *parent,
						 const gchar *arg1, ...);
void		calendar_month_item_construct	(CalendarMonthItem *mitem);

void		calendar_month_item_set_calendar	(CalendarMonthItem *mitem, CalendarManager *calendar);

void		calendar_month_item_set_date	(CalendarMonthItem *mitem, const GDate *date);
void		calendar_month_item_get_date	(CalendarMonthItem *mitem, GDate *date);

void		calendar_month_item_mark_day	(CalendarMonthItem *mitem, GDateDay day);
void		calendar_month_item_unmark_day	(CalendarMonthItem *mitem, GDateDay day);
void		calendar_month_item_clear_marks	(CalendarMonthItem *mitem);

#endif /* __CALENDAR_MONTH_ITEM_H__ */
