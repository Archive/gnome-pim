/* Main Calendar Widget
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <glib.h>

#include <gnome.h>

#include <mimedir/mimedir-vcal.h>
#include <mimedir/mimedir-vcomponent.h>
#include <mimedir/mimedir-vtodo.h>

#include "calendar-day.h"
#include "calendar-manager.h"
#include "calendar-marshal.h"
#include "calendar-month.h"
#include "calendar-week.h"
#include "calendar-widget.h"
#include "calendar-year.h"


static void calendar_widget_init	(CalendarWidget		*widget);
static void calendar_widget_dispose	(GObject		*object);
static void calendar_widget_class_init	(CalendarWidgetClass	*klass);

static void setup			(CalendarWidget		*widget);


struct _CalendarWidgetPriv {
	CalendarManager	*calendar;

	GtkWidget	*notebook;
	GtkWidget	*day_view;
	GtkWidget	*week_view;
	GtkWidget	*month_view;
	GtkWidget	*year_view;

	gchar		*filename; /* in system encoding */
	time_t		 file_read_time;
	GList		*appointments;

	GDate		 displayed_date;
};


enum {
	SIGNAL_NEW_APPOINTMENT,
	SIGNAL_LAST
};


static GtkVBoxClass *parent_class = NULL;

static guint signals[SIGNAL_LAST] = { 0 };

/*
 * Class and Instance Setup
 */

GType
calendar_widget_get_type (void)
{
	static GType calendar_widget_type = 0;

	if (!calendar_widget_type) {
		static const GTypeInfo calendar_widget_info = {
			sizeof (CalendarWidgetClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_widget_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarWidget),
			0,    /* n_preallocs */
			(GInstanceInitFunc) calendar_widget_init,
		};

		calendar_widget_type = g_type_register_static (GTK_TYPE_VBOX,
							       "CalendarWidget",
							       &calendar_widget_info,
							       0);
	}

	return calendar_widget_type;
}

static void
calendar_widget_class_init (CalendarWidgetClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_WIDGET_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = calendar_widget_dispose;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_NEW_APPOINTMENT] =
		g_signal_new ("new-appointment",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarWidgetClass, new_appointment),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);
}

static void
calendar_widget_init (CalendarWidget *widget)
{
	time_t t;
	struct tm tm;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CALENDAR_IS_WIDGET (widget));

	widget->priv = g_new0 (CalendarWidgetPriv, 1);

	t = time (NULL);
	localtime_r (&t, &tm);
	g_date_set_dmy (&widget->priv->displayed_date, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);

	widget->priv->calendar = calendar_manager_new ();

	setup (widget);
}

static void
calendar_widget_dispose (GObject *object)
{
	CalendarWidget *widget;
	CalendarWidgetPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_WIDGET (object));

	widget = CALENDAR_WIDGET (object);
	priv = widget->priv;

	if (priv) {
		g_object_unref (priv->calendar);
		g_free (priv->filename);
		g_free (priv);
		widget->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Utility Functions
 */

static gchar *
get_filename_for_saving (GtkWindow *parent)
{
	GtkWidget *dialog;
	gchar *filename = NULL;
	gint resp;

	dialog = gtk_file_chooser_dialog_new (_("Save file"),
					      parent,
					      GTK_FILE_CHOOSER_ACTION_SAVE,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
					      NULL);

	resp = gtk_dialog_run (GTK_DIALOG (dialog));
	if (resp == GTK_RESPONSE_ACCEPT)
		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

	gtk_widget_destroy (dialog);

	return filename;
}

/*
 * Callbacks
 */

static void
day_changed (CalendarWidget *widget, guint year, guint month, guint day, GtkWidget *w)
{
	CalendarWidgetPriv *priv = widget->priv;
	GDate date;

	g_date_set_dmy (&date, day, month, year);

	g_signal_handlers_block_by_func (G_OBJECT (priv->day_view),   G_CALLBACK (day_changed), widget);
	g_signal_handlers_block_by_func (G_OBJECT (priv->week_view),  G_CALLBACK (day_changed), widget);
	g_signal_handlers_block_by_func (G_OBJECT (priv->month_view), G_CALLBACK (day_changed), widget);
	g_signal_handlers_block_by_func (G_OBJECT (priv->year_view),  G_CALLBACK (day_changed), widget);

	calendar_widget_set_date (widget, &date);

	g_signal_handlers_unblock_by_func (G_OBJECT (priv->day_view),   G_CALLBACK (day_changed), widget);
	g_signal_handlers_unblock_by_func (G_OBJECT (priv->week_view),  G_CALLBACK (day_changed), widget);
	g_signal_handlers_unblock_by_func (G_OBJECT (priv->month_view), G_CALLBACK (day_changed), widget);
	g_signal_handlers_unblock_by_func (G_OBJECT (priv->year_view),  G_CALLBACK (day_changed), widget);
}

static void
new_appointment (CalendarWidget *widget, GtkMenuItem *item)
{
	CalendarWidgetPriv *priv = widget->priv;

	g_signal_emit (G_OBJECT (widget), signals[SIGNAL_NEW_APPOINTMENT], 0,
		       g_date_get_year  (&priv->displayed_date),
		       g_date_get_month (&priv->displayed_date),
		       g_date_get_day   (&priv->displayed_date));
}

static void
context_menu (CalendarWidget *widget, guint year, guint month, guint day, GnomeCanvasItem *item)
{
	GtkWidget *menu;
	GtkWidget *mitem;

	menu = gtk_menu_new (); /* FIXME: this baby is never freed */

	mitem = gtk_menu_item_new_with_label (_("New appointment..."));
	g_signal_connect_swapped (G_OBJECT (mitem), "activate",
				  G_CALLBACK (new_appointment), widget);
	gtk_widget_show (mitem);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), mitem);

	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, 0, gtk_get_current_event_time ());
}

#if 0 /* FIXME */
void
gncal_day_view_update (GncalDayView *dview, MIMEDirVComponent *comp, int flags)
{
	struct tm tm;
	gchar buf[256];

	g_return_if_fail (dview != NULL);
	g_return_if_fail (GNCAL_IS_DAY_VIEW (dview));

	if (dview->day_str)
		g_free (dview->day_str);

	tm = *localtime (&dview->lower);
	strftime (buf, sizeof (buf), _("%A %d"), &tm);
	dview->day_str = g_locale_to_utf8 (buf, -1, NULL, NULL, NULL);

	if (dview->events)
		calendar_destroy_event_list (dview->events);

	dview->events = calendar_get_events_in_range (dview->calendar->cal,
						      dview->lower,
						      dview->upper);

	gtk_widget_queue_draw (GTK_WIDGET (dview));
}
#endif

/*
 * Private Methods
 */

static gboolean
save_calendar (CalendarWidget *widget, const gchar *filename, GError **error)
{
	GError *err = NULL;
	CalendarWidgetPriv *priv;
	gchar *backup_name;
	struct stat s;

	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	priv = widget->priv;

	/* Create backup file */

	backup_name = g_strconcat (filename, "~", NULL);

	if (g_file_test (filename, G_FILE_TEST_EXISTS)) {
		if (rename (filename, backup_name) == -1) {
			gchar *fn_utf8;

			fn_utf8 = g_filename_to_utf8 (backup_name, -1, NULL, NULL, NULL);
			g_set_error (error, G_FILE_ERROR, g_file_error_from_errno (errno), _("Error while creating backup file %s: %s"), fn_utf8, strerror (errno));
			g_free (fn_utf8);
			g_free (backup_name);
			return FALSE;
		}
	}

	/* Write file */

	calendar_manager_save_file (priv->calendar, filename, &err);

	/* Error handling: try to restore backup file */

	if (err) {
		rename (backup_name, filename);
		g_free (backup_name);

		g_propagate_error (error, err);

		return FALSE;
	}

	g_free (backup_name);

	/* Update calendar */

	if (priv->filename != filename) {
		g_free (priv->filename);
		priv->filename = g_strdup (filename);
	}
	if (stat (filename, &s) == -1)
		priv->file_read_time = time (NULL);
	else
		priv->file_read_time = s.st_mtime;

	return TRUE;
}

/*
 * Setup
 */

static void
setup (CalendarWidget *widget)
{
	CalendarWidgetPriv *priv;
	GtkWidget *label;

	priv = widget->priv;

	/* Notebook */

	priv->notebook = gtk_notebook_new ();
	gtk_container_add (GTK_CONTAINER (widget), priv->notebook);
	gtk_widget_show (priv->notebook);

	/* Day view */

	priv->day_view = calendar_day_new ();
	calendar_day_set_calendar (CALENDAR_DAY (priv->day_view), priv->calendar);
	gtk_container_set_border_width (GTK_CONTAINER (priv->day_view), 12);
	g_signal_connect_swapped (G_OBJECT (priv->day_view), "day-changed",
				  G_CALLBACK (day_changed), widget);
	label = gtk_label_new (_("Day View"));
	gtk_notebook_append_page (GTK_NOTEBOOK (priv->notebook), priv->day_view, label);
	gtk_widget_show (priv->day_view);

	/* Week view */

	priv->week_view = calendar_week_new ();
	/* FIXME: calendar_week_set_calendar (CALENDAR_WEEK (priv->week_view), priv->calendar); */
	gtk_container_set_border_width (GTK_CONTAINER (priv->week_view), 12);
	g_signal_connect_swapped (G_OBJECT (priv->week_view), "day-changed",
				  G_CALLBACK (day_changed), widget);
	g_signal_connect_swapped (G_OBJECT (priv->week_view), "day-menu",
				  G_CALLBACK (context_menu), widget);
	label = gtk_label_new (_("Week View"));
	gtk_notebook_append_page (GTK_NOTEBOOK (priv->notebook), priv->week_view, label);
	gtk_widget_show (priv->week_view);

	/* Month view */

	priv->month_view = calendar_month_new ();
	calendar_month_set_calendar (CALENDAR_MONTH (priv->month_view), priv->calendar);
	gtk_container_set_border_width (GTK_CONTAINER (priv->month_view), 12);
	calendar_month_set_show_label (CALENDAR_MONTH (priv->month_view), TRUE);
	g_signal_connect_swapped (G_OBJECT (priv->month_view), "day-changed",
				  G_CALLBACK (day_changed), widget);
	g_signal_connect_swapped (G_OBJECT (priv->month_view), "day-menu",
				  G_CALLBACK (context_menu), widget);
	label = gtk_label_new (_("Month View"));
	gtk_notebook_append_page (GTK_NOTEBOOK (priv->notebook), priv->month_view, label);
	gtk_widget_show (priv->month_view);

	/* Year view */

	priv->year_view = calendar_year_new ();
	/* FIXME: calendar_year_set_calendar (CALENDAR_YEAR (priv->year_view), priv->calendar); */
	gtk_container_set_border_width (GTK_CONTAINER (priv->year_view), 12);
	calendar_year_set_show_label (CALENDAR_YEAR (priv->year_view), TRUE);
	g_signal_connect_swapped (G_OBJECT (priv->year_view), "day-changed",
				  G_CALLBACK (day_changed), widget);
	g_signal_connect_swapped (G_OBJECT (priv->year_view), "day-menu",
				  G_CALLBACK (context_menu), widget);
	label = gtk_label_new (_("Year View"));
	gtk_notebook_append_page (GTK_NOTEBOOK (priv->notebook), priv->year_view, label);
	gtk_widget_show (priv->year_view);
}

/*
 * Public Methods
 */

GtkWidget *
calendar_widget_new (void)
{
	CalendarWidget *widget;

	widget = g_object_new (CALENDAR_TYPE_WIDGET, NULL);

	return GTK_WIDGET (widget);
}

void
calendar_widget_calendar_new (CalendarWidget *widget)
{
	CalendarWidgetPriv *priv;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CALENDAR_IS_WIDGET (widget));

	priv = widget->priv;

	/* FIXME: implement */

	g_free (priv->filename);
	priv->filename = NULL;
	priv->file_read_time = -1;
}

void
calendar_widget_set_display (CalendarWidget *widget, CalendarWidgetDisplay display)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (CALENDAR_IS_WIDGET (widget));
	g_return_if_fail (display >= CALENDAR_WIDGET_DISPLAY_DAY && display <= CALENDAR_WIDGET_DISPLAY_YEAR);

	gtk_notebook_set_current_page (GTK_NOTEBOOK (widget->priv->notebook), display);
}

CalendarWidgetDisplay
calendar_widget_get_display (CalendarWidget *widget)
{
	g_return_val_if_fail (widget != NULL, -1);
	g_return_val_if_fail (CALENDAR_IS_WIDGET (widget), -1);

	return gtk_notebook_get_current_page (GTK_NOTEBOOK (widget->priv->notebook));
}

/**
 * calendar_widget_calendar_load:
 * @widget: #CalendarWidget object
 * @file: filename to load calendar from in system encoding
 * @error: error storage or %NULL
 *
 * Loads a calendar file. If the was an error during the load, %FALSE
 * will be returned and @error will be set accordingly.
 *
 * Return value: success indicator
 */
gboolean
calendar_widget_calendar_load (CalendarWidget *widget,
			       const gchar *filename,
			       GError **error)
{
	CalendarWidgetPriv *priv;
	gboolean ret;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (CALENDAR_IS_WIDGET (widget), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	priv = widget->priv;

	/* Filename setup */

	g_free (priv->filename);
	priv->filename = g_strdup (filename);

	/* Load */

	priv->file_read_time = time (NULL);

	ret = calendar_manager_load_file (priv->calendar, filename, error);

	return ret;
}

gboolean
calendar_widget_calendar_load_default (CalendarWidget *widget)
{
	CalendarWidgetPriv *priv = widget->priv;
	gchar *filename = NULL;
	gboolean ret;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (CALENDAR_IS_WIDGET (widget), FALSE);

	filename = g_build_filename (g_get_home_dir (),
				     ".gnome/user-cal.vcf", /* FIXME: .gnome2/user-cal.ics */
				     NULL);

	ret = calendar_widget_calendar_load (widget, filename, NULL);

	g_free (priv->filename);
	priv->filename = filename;

	return ret;
}

/**
 * calendar_widget_calendar_save:
 * @widget: #CalendarWidget object
 * @file: filename to save calendar to in system encoding or %NULL
 * @error: error storage or %NULL
 *
 * Saves the current to calendar. If the supplied filename is %NULL, the old
 * filename will be used. If there is no old filename, the user will be
 * prompted for a filename. If the was an error during the save, %FALSE
 * will be returned and @error will be set accordingly. If the user aborted the
 * saving, %FALSE will be returned and error will be set to %NULL.
 *
 * Return value: success indicator
 */
gboolean
calendar_widget_calendar_save (CalendarWidget *widget, const gchar *file, GError **error)
{
	CalendarWidgetPriv *priv;
	const gchar *filename = NULL;
        struct stat s;
        gboolean ret;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (CALENDAR_IS_WIDGET (widget), FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	priv = widget->priv;

	/* Retrieve the file name */

	if (file) {
		filename = g_strdup (file);
	} else if (priv->filename) {
		filename = g_strdup (priv->filename);
	} else {
		GtkWidget *toplevel;
		
		toplevel = gtk_widget_get_toplevel (GTK_WIDGET (widget));
		if (!GTK_IS_WINDOW (toplevel))
			toplevel = NULL;
		filename = get_filename_for_saving (toplevel ? GTK_WINDOW (toplevel) : NULL);
	}

	if (!filename)
		return FALSE;

	/* Check whether the file has changed since loading */

	if (stat (filename, &s) == -1) {
		if (errno != ENOENT) {
			gchar *fn_utf8;

			fn_utf8 = g_filename_to_utf8 (filename, -1, NULL, NULL, NULL);
			g_set_error (error, G_FILE_ERROR, g_file_error_from_errno (errno), _("Could not stat %s: %s"), fn_utf8, strerror (errno));
			g_free (fn_utf8);
			g_free (filename);
			return FALSE;
		}
	} else if (s.st_mtime > priv->file_read_time){
		GtkWidget *box;
		gchar *fn_utf8;

		fn_utf8 = g_filename_to_utf8 (filename, -1, NULL, NULL, NULL);

		box = gtk_message_dialog_new (NULL, 0,
					      GTK_MESSAGE_INFO,
					      GTK_BUTTONS_NONE,
					      _("File %s has changed since it was loaded. Save anyways and overwrite any changes?"),
					      fn_utf8);
		g_free (fn_utf8);
		gtk_dialog_add_buttons (GTK_DIALOG (box),
					GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					GTK_STOCK_SAVE,   GTK_RESPONSE_OK,
					NULL);
		gtk_dialog_set_default_response (GTK_DIALOG (box), GTK_RESPONSE_OK);

		switch (gtk_dialog_run (GTK_DIALOG (box))) {
		case GTK_RESPONSE_OK:
			break;
		default:
			g_free (filename);
			return FALSE;
		}

		gtk_widget_destroy (box);
	}

	ret = save_calendar (widget, filename, error);

	g_free (priv->filename);
	priv->filename = filename;

	return ret;
}

void
calendar_widget_today (CalendarWidget *widget)
{
	time_t t;
	struct tm tm;
	GDate date;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CALENDAR_IS_WIDGET (widget));

	t = time (NULL);
	localtime_r (&t, &tm);
	g_date_set_dmy (&date, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);

	calendar_widget_set_date (widget, &date);
}

void
calendar_widget_set_date (CalendarWidget *widget, const GDate *date)
{
	CalendarWidgetPriv *priv;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (CALENDAR_IS_WIDGET (widget));
	g_return_if_fail (date != NULL);
	g_return_if_fail (g_date_valid (date));

	priv = widget->priv;

	priv->displayed_date = *date;

	calendar_day_set_date   (CALENDAR_DAY   (priv->day_view),   date);
	calendar_week_set_date  (CALENDAR_WEEK  (priv->week_view),  date);
	calendar_month_set_date (CALENDAR_MONTH (priv->month_view), date);
	calendar_year_set_date  (CALENDAR_YEAR  (priv->year_view),  date);
}

const GDate *
calendar_widget_get_date (CalendarWidget *widget)
{
	g_return_val_if_fail (widget != NULL, NULL);
	g_return_val_if_fail (CALENDAR_IS_WIDGET (widget), NULL);

	return &widget->priv->displayed_date;
}
