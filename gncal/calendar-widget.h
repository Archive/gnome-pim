#ifndef __CALENDAR_WIDGET_H__
#define __CALENDAR_WIDGET_H__

/* Main Calendar Widget
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* This class provides a widget that contains a complete month view.
 * It's derived from GnomeCanvas and makes use of CalendarMonthItem.
 */

#include <time.h>

#include <glib.h>

#include <gtk/gtkbox.h>

#include <mimedir/mimedir-vcal.h>


#define CALENDAR_TYPE_WIDGET		(calendar_widget_get_type())
#define CALENDAR_WIDGET(obj)		(GTK_CHECK_CAST ((obj), CALENDAR_TYPE_WIDGET, CalendarWidget))
#define CALENDAR_WIDGET_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_WIDGET))
#define CALENDAR_IS_WIDGET(obj)		(GTK_CHECK_TYPE ((obj), CALENDAR_TYPE_WIDGET))
#define CALENDAR_IS_WIDGET_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_WIDGET))

typedef struct _CalendarWidget		CalendarWidget;
typedef struct _CalendarWidgetClass	CalendarWidgetClass;
typedef struct _CalendarWidgetPriv	CalendarWidgetPriv;

struct _CalendarWidget
{
	GtkVBox parent;

	CalendarWidgetPriv *priv;
};

struct _CalendarWidgetClass
{
	GtkVBoxClass parent_class;

	void (* new_appointment)	(CalendarWidget *widget, guint year, guint month, guint day);
};

typedef enum _CalendarWidgetDisplay {
	CALENDAR_WIDGET_DISPLAY_DAY,
	CALENDAR_WIDGET_DISPLAY_WEEK,
	CALENDAR_WIDGET_DISPLAY_MONTH,
	CALENDAR_WIDGET_DISPLAY_YEAR
} CalendarWidgetDisplay;

GType		 calendar_widget_get_type		(void);
GtkWidget	*calendar_widget_new			(void);

void		 calendar_widget_set_display		(CalendarWidget *widget, CalendarWidgetDisplay display);
CalendarWidgetDisplay
		 calendar_widget_get_display		(CalendarWidget *widget);

void		 calendar_widget_calendar_new		(CalendarWidget *widget);
gboolean	 calendar_widget_calendar_load		(CalendarWidget *widget, const gchar *filename, GError **error);
gboolean	 calendar_widget_calendar_load_default	(CalendarWidget *widget);
gboolean	 calendar_widget_calendar_save		(CalendarWidget *widget, const gchar *filename, GError **error);

void		 calendar_widget_today			(CalendarWidget *widget);
void		 calendar_widget_set_date		(CalendarWidget *widget, const GDate *date);
const GDate	*calendar_widget_get_date		(CalendarWidget *widget);

#endif /* __CALENDAR_WIDGET_H__ */
