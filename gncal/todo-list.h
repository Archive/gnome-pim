#ifndef __TODO_LIST_H__
#define __TODO_LIST_H__

/* To Do List Object
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <gtk/gtk.h>

#include "calendar-manager.h"


#define TODO_TYPE_LIST			(todo_list_get_type())
#define TODO_LIST(obj)			(GTK_CHECK_CAST ((obj), TODO_TYPE_LIST, TodoList))
#define TODO_LIST_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), TODO_TYPE_LIST))
#define TODO_IS_LIST(obj)		(GTK_CHECK_TYPE ((obj), TODO_TYPE_LIST))
#define TODO_IS_LIST_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), TODO_TYPE_LIST))

typedef struct _TodoList	TodoList;
typedef struct _TodoListClass	TodoListClass;
typedef struct _TodoListPriv	TodoListPriv;

struct _TodoList
{
	GtkVBox       parent;

	TodoListPriv *priv;
};

struct _TodoListClass
{
	GtkVBoxClass parent_class;
};

GType		 todo_list_get_type	(void);
GtkWidget	*todo_list_new		(void);

void		 todo_list_set_calendar	(TodoList *todo, CalendarManager *cal);

#endif /* __TODO_LIST_H__ */
