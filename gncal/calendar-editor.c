/* Calendar Event Editor
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>

#include <gnome.h>
#include <glade/glade.h>

#include <mimedir/mimedir-vevent.h>
#include <libegg/egg-datetime.h>

#include "calendar-editor.h"
#include "utils-ui.h"


static void	 calendar_editor_class_init	(CalendarEditorClass	*klass);
static void	 calendar_editor_init		(CalendarEditor		*editor);
static void	 calendar_editor_dispose	(GObject		*object);
static gboolean	 setup				(CalendarEditor		*editor);


struct _CalendarEditorPriv {
	MIMEDirVEvent	*event;
	gulong		 event_signal;

	GladeXML	*xml;
};


static GtkDialogClass *parent_class = NULL;

/*
 * Class and Instance Setup
 */

GType
calendar_editor_get_type (void)
{
	static GType calendar_editor_type = 0;

	if (!calendar_editor_type) {
		static const GTypeInfo calendar_editor_info = {
			sizeof (CalendarEditorClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_editor_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarEditor),
			1,    /* n_preallocs */
			(GInstanceInitFunc) calendar_editor_init,
		};

		calendar_editor_type = g_type_register_static (GTK_TYPE_DIALOG,
							       "CalendarEditor",
							       &calendar_editor_info,
							       0);
	}

	return calendar_editor_type;
}

static void
calendar_editor_class_init (CalendarEditorClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_EDITOR_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = calendar_editor_dispose;

	parent_class = g_type_class_peek_parent (klass);
}

static void
calendar_editor_init (CalendarEditor *editor)
{
	g_return_if_fail (editor != NULL);
	g_return_if_fail (CALENDAR_IS_EDITOR (editor));

	editor->priv = g_new0 (CalendarEditorPriv, 1);

	setup (editor);
}

static void
calendar_editor_dispose (GObject *object)
{
	CalendarEditor *editor;
	CalendarEditorPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_EDITOR (object));

	editor = CALENDAR_EDITOR (object);
	priv = editor->priv;

	if (priv) {
		if (priv->event) {
			g_signal_handler_disconnect (G_OBJECT (priv->event), priv->event_signal);
			g_object_unref (G_OBJECT (priv->event));
		}

		g_free (editor->priv);
		editor->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Internal Methods
 */

static void
calendar_editor_event_updated (CalendarEditor *ce)
{
	GtkWidget *w;
	GDate date;

	gchar *summary;
	MIMEDirDateTime *dtstart, *dtend;

	g_object_get (G_OBJECT (ce->priv->event),
		      "summary", &summary,
		      "dtstart", &dtstart,
		      "dtend",   &dtend,
		      NULL);

	w = glade_xml_get_widget (ce->priv->xml, "summary_entry");
	if (!w) {
		g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
		return;
	}
	gtk_entry_set_text (GTK_ENTRY (w), summary);

	w = glade_xml_get_widget (ce->priv->xml, "begin_date");
	if (!w) {
		g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
		return;
	}
	mimedir_datetime_get_gdate (dtstart, &date);
	egg_datetime_set_from_gdate (EGG_DATETIME (w), &date);

	w = glade_xml_get_widget (ce->priv->xml, "end_date");
	if (!w) {
		g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
		return;
	}
	mimedir_datetime_get_gdate (dtend, &date);
	egg_datetime_set_from_gdate (EGG_DATETIME (w), &date);

	/* FIXME */

	g_free (summary);
}

/*
 * Callbacks
 */

static void
calendar_editor_response  (GtkDialog *dialog, gint response, gpointer data)
{
	switch (response) {
	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;
	case GTK_RESPONSE_DELETE_EVENT:
		break;
	case GTK_RESPONSE_CANCEL:
		/* FIXME */
		break;
	case GTK_RESPONSE_HELP:
		ui_show_help (GTK_WINDOW (dialog), "calendar-editor");
		break;
	default:
		g_return_if_reached ();
	}
}


static void
calendar_editor_event_cb (CalendarEditor *ce, MIMEDirVEvent *event)
{
	g_assert (event == ce->priv->event);

	calendar_editor_event_updated (ce);
}


static void
calendar_editor_recur_cb (CalendarEditor *editor, GtkComboBox *cb)
{
	GtkWidget *w;

	w = glade_xml_get_widget (editor->priv->xml, "recur_box");
	if (!w) {
		g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
		return;
	}

	switch (gtk_combo_box_get_active (cb)) {
	case 0: /* never */
		gtk_widget_set_sensitive (w, FALSE);
		break;
	case 1: /* daily */
	case 2: /* weekly */
	case 3: /* monthly */
	case 4: /* yearly */
		gtk_widget_set_sensitive (w, TRUE);
		break;
	default:
		g_return_if_reached ();
	}
}

/*
 * Setup
 */

static gboolean
setup (CalendarEditor *editor)
{
	CalendarEditorPriv *priv;
	GtkWidget *w;

	priv = editor->priv;

	gtk_window_set_title (GTK_WINDOW (editor), _("Appointment properties"));

	gtk_dialog_set_has_separator (GTK_DIALOG (editor), FALSE);

	gtk_dialog_add_buttons (GTK_DIALOG (editor),
				GTK_STOCK_HELP,            GTK_RESPONSE_HELP,
				GTK_STOCK_REVERT_TO_SAVED, GTK_RESPONSE_CANCEL,
				GTK_STOCK_CLOSE,           GTK_RESPONSE_CLOSE,
				NULL);

        gtk_dialog_set_default_response (GTK_DIALOG (editor), GTK_RESPONSE_CLOSE);

        g_signal_connect (G_OBJECT (editor), "response",
                          G_CALLBACK (calendar_editor_response), NULL);

	priv->xml = glade_xml_new (GLADEDIR "/calendar-editor.glade", "editor_notebook", GETTEXT_PACKAGE);
	if (!priv->xml) {
		g_printerr (_("File %s could not be found. Please check your installation."), GLADEDIR "/calendar-editor.glade");
		return FALSE;
	}

	w = glade_xml_get_widget (priv->xml, "editor_notebook");
	if (!w) {
		g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
		return FALSE;
	}

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (editor)->vbox),
			    w, TRUE, TRUE, 0);

	w = glade_xml_get_widget (priv->xml, "recur_cb");
	if (!w) {
		g_printerr (_("File %s is corrupt. Please check your installation."), "card-editor.glade");
		return FALSE;
	}
	gtk_combo_box_set_active (GTK_COMBO_BOX (w), 0);

	g_signal_connect_swapped (G_OBJECT (w), "changed", G_CALLBACK (calendar_editor_recur_cb), editor);

	return TRUE;
}

/*
 * Public Methods
 */

/* Required by Glade */
GtkWidget *
datetime_new (void)
{
	GtkWidget *datetime;

	datetime = egg_datetime_new ();
	gtk_widget_show (datetime);

	return datetime;
}

/* Required by Glade */
GtkWidget *
datetime_new_with_time (void)
{
	GtkWidget *datetime;

	datetime = egg_datetime_new ();
	egg_datetime_set_display_mode (EGG_DATETIME (datetime), EGG_DATETIME_DISPLAY_DATE | EGG_DATETIME_DISPLAY_TIME);
	gtk_widget_show (datetime);

	return datetime;
}

GtkWidget *
calendar_editor_new (MIMEDirVEvent *event)
{
	CalendarEditor *editor;

	g_return_val_if_fail (event != NULL, NULL);
	g_return_val_if_fail (MIMEDIR_IS_VEVENT (event), NULL);

	editor = g_object_new (CALENDAR_TYPE_EDITOR, NULL);

	calendar_editor_set_event (editor, event);

	return GTK_WIDGET (editor);
}


void
calendar_editor_set_event (CalendarEditor *editor, MIMEDirVEvent *event)
{
	CalendarEditorPriv *priv;

	g_return_if_fail (editor != NULL);
	g_return_if_fail (CALENDAR_IS_EDITOR (editor));
	g_return_if_fail (event != NULL);
	g_return_if_fail (MIMEDIR_IS_VEVENT (event));

	priv = editor->priv;

	if (priv->event) {
		g_signal_handler_disconnect (G_OBJECT (priv->event), priv->event_signal);
		g_object_unref (G_OBJECT (priv->event));
	}

	g_object_ref (G_OBJECT (event));
	priv->event_signal = g_signal_connect_swapped (G_OBJECT (event), "changed", G_CALLBACK (calendar_editor_event_cb), editor);
	priv->event = event;

	calendar_editor_event_updated (editor);
}
