#ifndef __CALENDAR_EDITOR_H__
#define __CALENDAR_EDITOR_H__

/* Calendar Event Editor
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* This class provides a editor that contains a complete month view.
 * It's derived from GnomeCanvas and makes use of CalendarMonthItem.
 */

#include <time.h>

#include <glib.h>

#include <gtk/gtkdialog.h>

#include <mimedir/mimedir-vevent.h>


#define CALENDAR_TYPE_EDITOR		(calendar_editor_get_type())
#define CALENDAR_EDITOR(obj)		(GTK_CHECK_CAST ((obj), CALENDAR_TYPE_EDITOR, CalendarEditor))
#define CALENDAR_EDITOR_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_EDITOR))
#define CALENDAR_IS_EDITOR(obj)		(GTK_CHECK_TYPE ((obj), CALENDAR_TYPE_EDITOR))
#define CALENDAR_IS_EDITOR_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_EDITOR))

typedef struct _CalendarEditor		CalendarEditor;
typedef struct _CalendarEditorClass	CalendarEditorClass;
typedef struct _CalendarEditorPriv	CalendarEditorPriv;

struct _CalendarEditor
{
	GtkDialog parent;

	CalendarEditorPriv *priv;
};

struct _CalendarEditorClass
{
	GtkDialogClass parent_class;
};

GType		 calendar_editor_get_type	(void);
GtkWidget	*calendar_editor_new		(MIMEDirVEvent *event);
void		 calendar_editor_set_event	(CalendarEditor *editor, MIMEDirVEvent *event);

#endif /* __CALENDAR_EDITOR_H__ */
