#ifndef __UTILS_TIME_H__
#define __UTILS_TIME_H__

/* Miscellaneous Time Utility Functions
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <time.h>

/* Convert a time to a (localized) time string with hour and minute and
 * (locale-dependent) AM/PM indicator.
 */
gchar *time_get_short_time_string (guint8 hour, guint8 minute);

/* Convert a time to a (localized) time and date string. */
gchar *time_get_date_time_string (struct tm *tm);

/* Convert a time to a (localized) date string. */
gchar *time_get_date_string (struct tm *tm);

#endif /* __UTILS_TIME_H__ */
