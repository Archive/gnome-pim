/* Calendar Manager
 * Copyright (C) 2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib-object.h>

#include <gconf/gconf.h>

#include <mimedir/mimedir-datetime.h>
#include <mimedir/mimedir-vcal.h>
#include <mimedir/mimedir-vcomponent.h>
#include <mimedir/mimedir-vtodo.h>

#include "calendar-manager.h"
#include "calendar-marshal.h"


static void	calendar_manager_class_init	(CalendarManagerClass	*klass);
static void	calendar_manager_init		(CalendarManager	*calendar);
static void	calendar_manager_dispose	(GObject		*object);


enum {
	SIGNAL_APPOINTMENT_ADDED,
	SIGNAL_APPOINTMENT_REMOVED,
	SIGNAL_TODO_ADDED,
	SIGNAL_TODO_REMOVED,
	SIGNAL_LAST
};


static guint signals[SIGNAL_LAST] = { 0 };


struct _CalendarManagerPriv {
	GList *appointments;
	GList *todo;
	GList *components;
};


static GObjectClass *parent_class = NULL;

/*
 * Utility Functions
 */

static void
free_object_list (GList *list)
{
	GList *item;

	for (item = list; item != NULL; item = g_list_next (item))
		g_object_unref (G_OBJECT (item->data));

	g_list_free (list);
}

/*
 * Class and Object Management
 */

GType
calendar_manager_get_type (void)
{
	static GType calendar_manager_type = 0;

	if (!calendar_manager_type) {
		static const GTypeInfo calendar_manager_info = {
			sizeof (CalendarManagerClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_manager_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarManager),
			1,    /* n_preallocs */
			(GInstanceInitFunc) calendar_manager_init,
		};

		calendar_manager_type = g_type_register_static (G_TYPE_OBJECT,
								"CalendarManager",
								&calendar_manager_info,
								0);
	}

	return calendar_manager_type;
}


static void
calendar_manager_class_init (CalendarManagerClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = calendar_manager_dispose;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_APPOINTMENT_ADDED] =
		g_signal_new ("appointment-added",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarManagerClass, appointment_added),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, G_TYPE_OBJECT);
	signals[SIGNAL_APPOINTMENT_REMOVED] =
		g_signal_new ("appointment-removed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarManagerClass, appointment_removed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, G_TYPE_OBJECT);
	signals[SIGNAL_TODO_ADDED] =
		g_signal_new ("todo-added",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarManagerClass, todo_added),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, G_TYPE_OBJECT);
	signals[SIGNAL_TODO_REMOVED] =
		g_signal_new ("todo-removed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarManagerClass, todo_removed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1, G_TYPE_OBJECT);
}

static void
calendar_manager_init (CalendarManager *sink)
{
	g_return_if_fail (sink != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (sink));

	sink->priv = g_new0 (CalendarManagerPriv, 1);
}

static void
calendar_manager_dispose (GObject *object)
{
	CalendarManager *calendar;
	CalendarManagerPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (object));

	calendar = CALENDAR_MANAGER (object);
	priv = calendar->priv;

	if (priv) {
/*		free_object_list (priv->appointments);*/
		free_object_list (priv->todo);
		free_object_list (priv->components);
		g_free (priv);
		calendar->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Date Utility Functions
 */

static gboolean
appointment_intersects_with_time_range (MIMEDirVEvent *app, GDate *start, GDate *end)
{
	MIMEDirDateTime *start_dt, *end_dt;
	GDate start_gd, end_gd;

	g_object_get (G_OBJECT (app),
		      "dtstart", &start_dt,
		      "dtend",   &end_dt,
		      NULL);

	mimedir_datetime_get_gdate (start_dt, &start_gd);
	mimedir_datetime_get_gdate (end_dt, &end_gd);

	if (g_date_compare (&start_gd, &end_gd) > 0)
		return FALSE;

	/* FIXME: handle recurrence */

	if (g_date_compare (&start_gd, end) > 0 || g_date_compare (&end_gd, start) < 0)
		return FALSE;

	return TRUE;
}

/*
 * Internal Methods
 */

static void
add_appointment (CalendarManager *cal, MIMEDirVEvent *appointment)
{
	CalendarManagerPriv *priv = cal->priv;

	g_object_ref (G_OBJECT (appointment));

	priv->appointments = g_list_prepend (priv->appointments, appointment);

	g_signal_emit (G_OBJECT (cal), signals[SIGNAL_APPOINTMENT_ADDED], 0, appointment);
}

static void
remove_appointment (CalendarManager *cal, MIMEDirVEvent *appointment)
{
	CalendarManagerPriv *priv = cal->priv;
	GList *list;
	gboolean found = FALSE;

	for (list = priv->appointments; list != NULL; list = g_list_next (list)) {
		if (list->data == appointment) {
			priv->appointments = g_list_delete_link (priv->appointments, list);
			found = TRUE;
			break;
		}
	}

	g_return_if_fail (found != FALSE);

	g_signal_emit (G_OBJECT (cal), signals[SIGNAL_APPOINTMENT_REMOVED], 0, appointment);

	g_object_unref (G_OBJECT (appointment));
}

static void
add_todo (CalendarManager *cal, MIMEDirVTodo *todo)
{
	CalendarManagerPriv *priv = cal->priv;

	g_object_ref (G_OBJECT (todo));

	priv->todo = g_list_prepend (priv->todo, todo);

	g_signal_emit (G_OBJECT (cal), signals[SIGNAL_TODO_ADDED], 0, todo);
}

static void
remove_todo (CalendarManager *cal, MIMEDirVTodo *todo)
{
	CalendarManagerPriv *priv = cal->priv;
	GList *list;
	gboolean found = FALSE;

	for (list = priv->todo; list != NULL; list = g_list_next (list)) {
		if (list->data == todo) {
			priv->todo = g_list_delete_link (priv->todo, list);
			found = TRUE;
			break;
		}
	}

	g_return_if_fail (found != FALSE);

	g_signal_emit (G_OBJECT (cal), signals[SIGNAL_TODO_REMOVED], 0, todo);

	g_object_unref (G_OBJECT (todo));
}

static void
add_component (CalendarManager *cal, MIMEDirVComponent *component)
{
	CalendarManagerPriv *priv = cal->priv;

	g_object_ref (G_OBJECT (component));

	priv->components = g_list_prepend (priv->components, component);
}

static void
load_from_vcal_list (CalendarManager *cal, GList *list)
{
	GList *item;

	for (item = list; item != NULL; item = g_list_next (item)) {
		MIMEDirVCal *vcal;
		GSList *list2, *item2;

		g_assert (MIMEDIR_IS_VCAL (item->data));
		vcal = MIMEDIR_VCAL (item->data);

		list2 = mimedir_vcal_get_component_list (vcal);

		for (item2 = list2; item2 != NULL; item2 = g_slist_next (item2)) {
			gpointer data = item2->data;

			g_assert (data != NULL && MIMEDIR_IS_VCOMPONENT (data));

			if (MIMEDIR_IS_VTODO (data))
				add_todo (cal, MIMEDIR_VTODO (data));
			else if (MIMEDIR_IS_VEVENT (data)) 
				add_appointment (cal, MIMEDIR_VEVENT (data));
			else
				add_component (cal, MIMEDIR_VCOMPONENT (data));
		}

		mimedir_vcal_free_component_list (list2);
	}
}

static MIMEDirVCal *
save_to_vcal (CalendarManager *cal)
{
	CalendarManagerPriv *priv = cal->priv;
	MIMEDirVCal *vcal;

	vcal = mimedir_vcal_new ();
	
	mimedir_vcal_add_component_list (vcal, priv->appointments);
	mimedir_vcal_add_component_list (vcal, priv->todo);
	mimedir_vcal_add_component_list (vcal, priv->components);

	return vcal;
}

/*
 * Public Methods
 */

CalendarManager *
calendar_manager_new (void)
{
	CalendarManager *cal;

	cal = g_object_new (CALENDAR_TYPE_MANAGER, NULL);

	return cal;
}

void
calendar_manager_empty (CalendarManager *cal)
{
	CalendarManagerPriv *priv = cal->priv;

	g_return_if_fail (cal != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (cal));

	while (priv->appointments != NULL)
		remove_appointment (cal, MIMEDIR_VEVENT (priv->appointments->data));
	while (priv->todo != NULL)
		remove_todo (cal, MIMEDIR_VTODO (priv->todo->data));
	free_object_list (priv->components);
}

/**
 * calendar_manager_load_file:
 * @cal: a #CalendarManager object
 * @filename: name of file to load in system encoding
 * @error: error storage location or %NULL
 *
 * Loads a calendar from a file, replacing the current calendar.
 *
 * Return value: success indicator
 */
gboolean
calendar_manager_load_file (CalendarManager *cal, const gchar *filename, GError **error)
{
	g_return_val_if_fail (cal != NULL, FALSE);
	g_return_val_if_fail (CALENDAR_IS_MANAGER (cal), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	calendar_manager_empty (cal);
	return calendar_manager_append_file (cal, filename, error);
}

/**
 * calendar_manager_append_file:
 * @cal: a #CalendarManager object
 * @filename: file to load in system encoding
 * @error: error storage location or %NULL
 *
 * Loads a calendar from a file and append the entries to the current calendar.
 *
 * Return value: success indicator
 */
gboolean
calendar_manager_append_file (CalendarManager *cal, const gchar *filename, GError **error)
{
	GList *list;
	GError *err = NULL;

	g_return_val_if_fail (cal != NULL, FALSE);
	g_return_val_if_fail (CALENDAR_IS_MANAGER (cal), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	list = mimedir_vcal_read_file (filename, &err);
	if (err) {
		g_propagate_error (error, err);
		return FALSE;
	}

	load_from_vcal_list (cal, list);

	mimedir_vcal_free_list (list);

	return TRUE;
}

/**
 * calendar_manager_save_file:
 * @cal: a #CalendarManager object
 * @filename: name of file to save to in system encoding
 * @error: error storage location or %NULL
 *
 * Saves the current calendar to a file.
 *
 * Return value: success indicator
 */
gboolean
calendar_manager_save_file (CalendarManager *cal, const gchar *filename, GError **error)
{
	MIMEDirVCal *vcal;
	gboolean ret;

	g_return_val_if_fail (cal != NULL, FALSE);
	g_return_val_if_fail (CALENDAR_IS_MANAGER (cal), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	vcal = save_to_vcal (cal);

	ret = mimedir_vcal_write_to_file (vcal, filename, error);

	g_object_unref (G_OBJECT (vcal));

	return ret;
}

/**
 * calendar_manager_get_appointments:
 * @cal: a #CalendarManager
 *
 * Returns a list of all appointments. This list should be freed with
 * g_list_free().
 *
 * Return value: list of all appointments
 */
GList *
calendar_manager_get_appointments (CalendarManager *cal)
{
	g_return_val_if_fail (cal != NULL, NULL);
	g_return_val_if_fail (CALENDAR_IS_MANAGER (cal), NULL);

	return g_list_copy (cal->priv->appointments);
}

/**
 * calendar_manager_get_appointments_intersecting:
 * @calendar: a #CalendarManager
 * @from: starting date
 * @to: ending date
 *
 * Returns a list of all appointments that intersect with the given time
 * interval. This list should be freed with g_list_free().
 *
 * Return value: a list of appointment during the time interval
 */
GList *
calendar_manager_get_appointments_intersecting (CalendarManager *cal, GDate *from, GDate *to)
{
	GList *list, *item;

	g_return_val_if_fail (cal != NULL, NULL);
	g_return_val_if_fail (CALENDAR_IS_MANAGER (cal), NULL);
	g_return_val_if_fail (from != NULL, NULL);
	g_return_val_if_fail (g_date_valid (from), NULL);
	g_return_val_if_fail (to != NULL, NULL);
	g_return_val_if_fail (g_date_valid (to), NULL);
	g_return_val_if_fail (g_date_compare (from, to) <= 0, NULL);

	list = NULL;

	for (item = cal->priv->appointments; item != NULL; item = g_list_next (item)) {
		MIMEDirVEvent *appointment;

		appointment = MIMEDIR_VEVENT (item->data);
		if (appointment_intersects_with_time_range (appointment, from, to))
			list = g_list_prepend (list, appointment);
	}

	return list;
}

/**
 * calendar_manager_get_todo_items:
 * @cal: a #CalendarManager
 *
 * Returns a list of all todo items. This list should be freed with
 * g_list_free().
 *
 * Return value: list of all todo items
 */
GList *calendar_manager_get_todo_items (CalendarManager *cal)
{
	g_return_val_if_fail (cal != NULL, NULL);
	g_return_val_if_fail (CALENDAR_IS_MANAGER (cal), NULL);

	return g_list_copy (cal->priv->todo);
}

void
calendar_manager_add_appointment (CalendarManager *cal, MIMEDirVEvent *appointment)
{
	g_return_if_fail (cal != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (cal));
	g_return_if_fail (appointment != NULL);
	g_return_if_fail (MIMEDIR_IS_VEVENT (appointment));

	add_appointment (cal, appointment);
}

void
calendar_manager_add_todo_item (CalendarManager *cal, MIMEDirVTodo *todo)
{
	g_return_if_fail (cal != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (cal));
	g_return_if_fail (todo != NULL);
	g_return_if_fail (MIMEDIR_IS_VTODO (todo));

	add_todo (cal, todo);
}

void
calendar_manager_remove_appointment (CalendarManager *cal, MIMEDirVEvent *appointment)
{
	g_return_if_fail (cal != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (cal));
	g_return_if_fail (appointment != NULL);
	g_return_if_fail (MIMEDIR_IS_VEVENT (appointment));

	remove_appointment (cal, appointment);
}

void
calendar_manager_remove_todo_item (CalendarManager *cal, MIMEDirVTodo *todo)
{
	g_return_if_fail (cal != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (cal));
	g_return_if_fail (todo != NULL);
	g_return_if_fail (MIMEDIR_IS_VTODO (todo));

	remove_todo (cal, todo);
}
