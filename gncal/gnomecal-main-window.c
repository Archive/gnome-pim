/* Gnome Calendar Main Window Object
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <time.h>

#include <glib.h>

#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include <gnome.h>

#include <mimedir/mimedir-vevent.h>

#include "calendar-editor.h"
#include "calendar-marshal.h"
#include "calendar-widget.h"
#include "gnomecal-config.h"
#include "gnomecal-goto.h"
#include "gnomecal-main-window.h"
#include "gnomecal-prefs.h"
#include "gnomecal-ui.h"


enum {
	SIGNAL_QUIT,
	SIGNAL_OPEN_NEW,
	SIGNAL_LAST
};


static guint signals[SIGNAL_LAST] = { 0 };


static void gncal_main_window_init		(GncalMainWindow	*window);
static void gncal_main_window_finalize		(GObject		*window);
static void gncal_main_window_class_init	(GncalMainWindowClass	*klass);

static void setup				(GncalMainWindow	*window);


struct _GncalMainWindowPriv {
	GtkWidget *calendar;
};


static GnomeAppClass *parent_class = NULL;

/*
 * Class and Object Management
 */

GType
gncal_main_window_get_type (void)
{
	static GType gncal_main_window_type = 0;

	if (!gncal_main_window_type) {
		static const GTypeInfo gncal_main_window_info = {
			sizeof (GncalMainWindowClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) gncal_main_window_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (GncalMainWindow),
			1,    /* n_preallocs */
			(GInstanceInitFunc) gncal_main_window_init,
		};

		gncal_main_window_type = g_type_register_static (GNOME_TYPE_APP,
								 "GncalMainWindow",
								 &gncal_main_window_info,
								 0);
	}

	return gncal_main_window_type;
}

static void
gncal_main_window_class_init (GncalMainWindowClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (GNCAL_IS_MAIN_WINDOW_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->finalize = gncal_main_window_finalize;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_QUIT] =
		g_signal_new ("quit",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (GncalMainWindowClass, quit),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	signals[SIGNAL_OPEN_NEW] =
		g_signal_new ("open-new",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (GncalMainWindowClass, open_new),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__STRING,
			      G_TYPE_NONE, 1, G_TYPE_STRING);
}

static void
gncal_main_window_init (GncalMainWindow *window)
{
	g_return_if_fail (window != NULL);
	g_return_if_fail (GNCAL_IS_MAIN_WINDOW (window));

	window->priv = g_new0 (GncalMainWindowPriv, 1);

	setup (window);
}

static void
gncal_main_window_finalize (GObject *object)
{
	GncalMainWindow *window;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNCAL_IS_MAIN_WINDOW (object));

	window = GNCAL_MAIN_WINDOW (object);

	g_free (window->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*
 * Internal Methods
 */

static void
show_about (GncalMainWindow *window)
{
	static const gchar *authors[] = {
		"Miguel de Icaza <miguel@kernel.org>",
		"Federico Mena <federico@gimp.org>",
		"Arturo Espinosa <arturo@nuclecu.unam.mx>",
		"Russell Steinthal <rms39@columbia.edu>",
		"Sebastian Rittau <srittau@jroger.in-berlin.de>",
		NULL
	};
		
	const gchar *translators = _("translator_credits");
	if (strcmp (translators, "translator_credits") == 0)
		translators = NULL;

	gtk_show_about_dialog (GTK_WINDOW (window),
			       "authors",		authors,
			       "comments",		_("The GNOME personal calendar and schedule manager"),
			       "copyright",		"(C) 1998 Free Software Foundation\n"
							"(C) 1998 Red Hat Software, Inc.\n"
							"Copyright \xc2\xa9 2002-2005 Sebastian Rittau",
			       "name",			_("GNOME Calendar"),
			       "translator-credits",	translators,
			       "version",		VERSION,
			       NULL);
}

static void
gncal_main_window_set_title (GncalMainWindow *window)
{
	gtk_window_set_title (GTK_WINDOW (window), _("GNOME Calendar")); /* FIXME: add calendar name */
}

static void
new_appointment (GncalMainWindow *window, guint year, guint month, guint day)
{
	GncalMainWindowPriv *priv = window->priv;
	GConfClient *gconf;
	GtkWidget *ce;
	MIMEDirVEvent *event;
	MIMEDirDateTime *dtstart_dt, *dtend_dt;
	const GDate *date;
	gint day_begin;

	/* Load prefs */

	gconf = gconf_client_get_default ();
	day_begin = gconf_client_get_int (gconf, CONFIG_CAL_DAY_START, NULL);
	g_object_unref (G_OBJECT (gconf));

	event = mimedir_vevent_new ();

	/* Default to the day the user is looking at */

	date = calendar_widget_get_date (CALENDAR_WIDGET (priv->calendar));

	dtstart_dt = mimedir_datetime_new_from_gdate (date);
	dtstart_dt->hour = day_begin;
	dtstart_dt->minute = 0;

	dtend_dt         = mimedir_datetime_new_from_gdate (date);
	dtend_dt->hour   = day_begin;
	dtend_dt->minute = 30;

	g_object_set (G_OBJECT (event),
		      "dtstart", dtstart_dt,
		      "dtend",   dtend_dt,
		      NULL);

	g_object_unref (G_OBJECT (dtstart_dt));
	g_object_unref (G_OBJECT (dtend_dt));

	ce = calendar_editor_new (event);
	gtk_widget_show (ce);

	/* FIXME: add event to event list */

	g_object_unref (G_OBJECT (event));
}

/*
 * Menu and Toolbar Callbacks
 */

static void
gncal_main_window_new_calendar_cb (GtkMenuItem *item, gpointer data)
{
	g_signal_emit (G_OBJECT (data), signals[SIGNAL_OPEN_NEW], 0, NULL);
}

static void
open_calendar_cb (GtkMenuItem *item, GncalMainWindow *window)
{
	GtkWidget *dialog;

	dialog = gtk_file_chooser_dialog_new (_("Load calendar"),
					      GTK_WINDOW (window),
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_OPEN,   GTK_RESPONSE_ACCEPT,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      NULL);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		GError *error = NULL;
		gchar *filename;

		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

		if (!calendar_widget_calendar_load (CALENDAR_WIDGET (window->priv->calendar), filename, &error)) {
			gncal_ui_show_error (GTK_WINDOW (window), error);
			g_error_free (error);
		}

		g_free (filename);
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
save_calendar_cb (GtkMenuItem *item, gpointer data)
{
	GError *error = NULL;
	GncalMainWindowPriv *priv;

	priv = GNCAL_MAIN_WINDOW (data)->priv;

	calendar_widget_calendar_save (CALENDAR_WIDGET (priv->calendar), NULL, &error);

	if (error) {
		gncal_ui_show_error (NULL, error);
		g_error_free (error);
	}
}

static void
save_as_calendar_cb (GtkMenuItem *item, GncalMainWindow *window)
{
	GtkWidget *dialog;

	dialog = gtk_file_chooser_dialog_new (_("Save calendar"),
					      GTK_WINDOW (window),
					      GTK_FILE_CHOOSER_ACTION_SAVE,
					      GTK_STOCK_SAVE,   GTK_RESPONSE_ACCEPT,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      NULL);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		GError *error = NULL;
		gchar *filename;

		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

		if (!calendar_widget_calendar_save (CALENDAR_WIDGET (window->priv->calendar), filename, &error)) {
			gncal_ui_show_error (GTK_WINDOW (window), error);
			g_error_free (error);
		}

		g_free (filename);
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
gncal_main_window_close_cb (GtkMenuItem *item, gpointer data)
{
	gtk_widget_destroy (GTK_WIDGET (data));
}

static void
gncal_main_window_quit_cb (GtkMenuItem *item, gpointer data)
{
	g_signal_emit (G_OBJECT (data), signals[SIGNAL_QUIT], 0);
}

static void
calendar_new_appointment_cb (GncalMainWindow *window, guint year, guint month, guint day, CalendarWidget *calendar)
{
	new_appointment (window, year, month, day);
}

static void
toolbar_new_appointment_cb (GtkMenuItem *item, GncalMainWindow *window)
{
	GncalMainWindowPriv *priv = window->priv;
	const GDate *date;

	date = calendar_widget_get_date (CALENDAR_WIDGET (priv->calendar));

	new_appointment (window,
			 g_date_get_year  (date),
			 g_date_get_month (date),
			 g_date_get_day   (date));
}

static void
toolbar_new_appointment_today_cb (GtkMenuItem *item, gpointer data)
{
	MIMEDirVEvent *event;
	GtkWidget *ce;

	event = mimedir_vevent_new ();

	ce = calendar_editor_new (event);
	gtk_widget_show (ce);

	/* FIXME: add event to event list */

	g_object_unref (G_OBJECT (event));
}

static void
gncal_main_window_prefs_cb (GtkMenuItem *item, gpointer data)
{
	static GtkWidget *dialog = NULL;

	if (!dialog) {
		dialog = gncal_prefs_new (GTK_WINDOW (data));
		g_signal_connect (G_OBJECT (dialog), "destroy",
				  G_CALLBACK (gtk_widget_destroyed), &dialog);
	}

	gtk_window_present (GTK_WINDOW (dialog));
}

static void
show_about_cb (GtkMenuItem *item, gpointer data)
{
	show_about (GNCAL_MAIN_WINDOW (data));
}

static void
gncal_main_window_prev_cb (GtkMenuItem *item, gpointer data)
{
	GncalMainWindowPriv *priv;
	const GDate *date;
	GDate mydate;

	priv = GNCAL_MAIN_WINDOW (data)->priv;

	date = calendar_widget_get_date (CALENDAR_WIDGET (priv->calendar));
	mydate = *date;

	switch (calendar_widget_get_display (CALENDAR_WIDGET (priv->calendar))) {
	case CALENDAR_WIDGET_DISPLAY_DAY:
		g_date_subtract_days (&mydate, 1);
		break;
	case CALENDAR_WIDGET_DISPLAY_WEEK:
		g_date_subtract_days (&mydate, 7);
		break;
	case CALENDAR_WIDGET_DISPLAY_MONTH:
		g_date_subtract_months (&mydate, 1);
		break;
	case CALENDAR_WIDGET_DISPLAY_YEAR:
		g_date_subtract_years (&mydate, 1);
		break;
	default:
		g_return_if_reached ();
	}

	calendar_widget_set_date (CALENDAR_WIDGET (priv->calendar), &mydate);
}

static void
gncal_main_window_today_cb (GtkMenuItem *item, gpointer data)
{
	GncalMainWindowPriv *priv;

	priv = GNCAL_MAIN_WINDOW (data)->priv;

	calendar_widget_today (CALENDAR_WIDGET (priv->calendar));
}
static void
gncal_main_window_next_cb (GtkMenuItem *item, gpointer data)
{
	GncalMainWindowPriv *priv;
	const GDate *date;
	GDate mydate;

	priv = GNCAL_MAIN_WINDOW (data)->priv;

	date = calendar_widget_get_date (CALENDAR_WIDGET (priv->calendar));
	mydate = *date;
	g_assert (g_date_valid (&mydate));

	switch (calendar_widget_get_display (CALENDAR_WIDGET (priv->calendar))) {
	case CALENDAR_WIDGET_DISPLAY_DAY:
		g_date_add_days (&mydate, 1);
		break;
	case CALENDAR_WIDGET_DISPLAY_WEEK:
		g_date_add_days (&mydate, 7);
		break;
	case CALENDAR_WIDGET_DISPLAY_MONTH:
		g_date_add_months (&mydate, 1);
		break;
	case CALENDAR_WIDGET_DISPLAY_YEAR:
		g_date_add_years (&mydate, 1);
		break;
	default:
		g_return_if_reached ();
	}

	calendar_widget_set_date (CALENDAR_WIDGET (priv->calendar), &mydate);
}

static void
gncal_main_window_goto_changed_cb (GncalMainWindow *window, GncalGoto *dialog)
{
	GDate date;

	gncal_goto_get_date (dialog, &date);

	calendar_widget_set_date (CALENDAR_WIDGET (window->priv->calendar), &date);
}

static void
gncal_main_window_goto_cb (GtkMenuItem *item, GncalMainWindow *window)
{
	static GtkWidget *dialog = NULL;

	if (!dialog) {
		const GDate *date;

		date = calendar_widget_get_date (CALENDAR_WIDGET (window->priv->calendar));

		dialog = gncal_goto_new (GTK_WINDOW (window));
		gncal_goto_set_date (GNCAL_GOTO (dialog), date);

		g_signal_connect_swapped (G_OBJECT (dialog), "changed",
					  G_CALLBACK (gncal_main_window_goto_changed_cb), window);
		g_signal_connect (G_OBJECT (dialog), "destroy",
				  G_CALLBACK (gtk_widget_destroyed), &dialog);
	}

	gtk_window_present (GTK_WINDOW (dialog));
}

static void
destroyed_cb (GncalMainWindow *window, gpointer data)
{
	GError *error = NULL;

	gncal_main_window_save_calendar (window, &error);

	if (error)
		gncal_ui_show_error (GTK_WINDOW (window), error);
}

/*
 * Setup
 */

static GnomeUIInfo
gncal_main_window_file_menu [] = {
	GNOMEUIINFO_MENU_NEW_ITEM	(N_("_New calendar"), N_("Create a new calendar"), gncal_main_window_new_calendar_cb, NULL),
	GNOMEUIINFO_MENU_OPEN_ITEM	(open_calendar_cb,	NULL),
	GNOMEUIINFO_MENU_SAVE_ITEM	(save_calendar_cb,	NULL),
	GNOMEUIINFO_MENU_SAVE_AS_ITEM	(save_as_calendar_cb,	NULL),

	GNOMEUIINFO_SEPARATOR,

	GNOMEUIINFO_MENU_CLOSE_ITEM	(gncal_main_window_close_cb,	NULL),
	GNOMEUIINFO_MENU_QUIT_ITEM	(gncal_main_window_quit_cb,	NULL),

	GNOMEUIINFO_END
};

static GnomeUIInfo
gncal_main_window_edit_menu [] = {
	{ GNOME_APP_UI_ITEM, N_("_New appointment..."), N_("Create a new appointment"), toolbar_new_appointment_cb, NULL, NULL, GNOME_APP_PIXMAP_STOCK, GTK_STOCK_NEW, 0, 0, NULL },
	{ GNOME_APP_UI_ITEM, N_("New appointment for _today..."), N_("Create a new appointment for today"), toolbar_new_appointment_today_cb, NULL, NULL, GNOME_APP_PIXMAP_STOCK, GTK_STOCK_NEW, 0, 0, NULL },

	GNOMEUIINFO_END
};

static GnomeUIInfo
gncal_main_window_settings_menu [] = {
	GNOMEUIINFO_MENU_PREFERENCES_ITEM (gncal_main_window_prefs_cb, NULL),

	GNOMEUIINFO_END
};

static GnomeUIInfo
gncal_main_window_help_menu [] = {
	GNOMEUIINFO_HELP ("gnomecal"),

	GNOMEUIINFO_MENU_ABOUT_ITEM (show_about_cb, NULL),

	GNOMEUIINFO_END
};

static GnomeUIInfo
gncal_main_window_menu [] = {
	GNOMEUIINFO_MENU_FILE_TREE	(gncal_main_window_file_menu),
	GNOMEUIINFO_MENU_EDIT_TREE	(gncal_main_window_edit_menu),
	GNOMEUIINFO_MENU_SETTINGS_TREE	(gncal_main_window_settings_menu),
	GNOMEUIINFO_MENU_HELP_TREE	(gncal_main_window_help_menu),

	GNOMEUIINFO_END
};

static GnomeUIInfo
gncal_main_window_toolbar [] = {
	GNOMEUIINFO_ITEM_STOCK (N_("New"), N_("Create a new appointment"), toolbar_new_appointment_cb, GTK_STOCK_NEW),

	GNOMEUIINFO_SEPARATOR,

	GNOMEUIINFO_ITEM_STOCK (N_("Prev"), N_("Go back in time"), gncal_main_window_prev_cb, GTK_STOCK_GO_BACK),
	GNOMEUIINFO_ITEM_STOCK (N_("Today"), N_("Go to present time"), gncal_main_window_today_cb, GTK_STOCK_HOME),
	GNOMEUIINFO_ITEM_STOCK (N_("Next"), N_("Go forward in time"), gncal_main_window_next_cb, GTK_STOCK_GO_FORWARD),

	GNOMEUIINFO_SEPARATOR,

	GNOMEUIINFO_ITEM_STOCK (N_("Go to"), N_("Go to a specific date"), gncal_main_window_goto_cb, GTK_STOCK_JUMP_TO),

	GNOMEUIINFO_END
};

static void
setup (GncalMainWindow *window)
{
	GncalMainWindowPriv *priv;

	GtkWidget *appbar;

	priv = window->priv;

	g_signal_connect (G_OBJECT (window), "destroy",
			  G_CALLBACK (destroyed_cb), NULL);

	gncal_main_window_set_title (window);

	/* Menus */

	gnome_app_create_menus_with_data (GNOME_APP (window), gncal_main_window_menu, window);

	/* Toolbar */

	gnome_app_create_toolbar_with_data (GNOME_APP (window), gncal_main_window_toolbar, window);

	/* Status bar */

        appbar = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_USER);
        gnome_app_set_statusbar (GNOME_APP (window), appbar);
	gnome_app_install_menu_hints (GNOME_APP (window), gncal_main_window_menu);

	/* Calendar */

	priv->calendar = calendar_widget_new ();
	gtk_container_set_border_width (GTK_CONTAINER (priv->calendar), 12);
	g_signal_connect_swapped (G_OBJECT (priv->calendar), "new-appointment",
				  G_CALLBACK (calendar_new_appointment_cb), window);
	gnome_app_set_contents (GNOME_APP (window), priv->calendar);
	gtk_widget_show (priv->calendar);
}

/*
 * External Methods
 */

GtkWidget *
gncal_main_window_new (void)
{
	GncalMainWindow *window;

	window = g_object_new (GNCAL_TYPE_MAIN_WINDOW, NULL);

	return GTK_WIDGET (window);
}

gboolean
gncal_main_window_set_calendar (GncalMainWindow *win,
				const gchar *filename,
				GError **error)
{
	g_return_val_if_fail (win != NULL, FALSE);
	g_return_val_if_fail (GNCAL_IS_MAIN_WINDOW (win), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	return calendar_widget_calendar_load (CALENDAR_WIDGET (win->priv->calendar), filename, error);
}

gboolean
gncal_main_window_set_calendar_default (GncalMainWindow *win)
{
	g_return_val_if_fail (win != NULL, FALSE);
	g_return_val_if_fail (GNCAL_IS_MAIN_WINDOW (win), FALSE);

	return calendar_widget_calendar_load_default (CALENDAR_WIDGET (win->priv->calendar));
}

void
gncal_main_window_set_calendar_new (GncalMainWindow *win)
{
	g_return_if_fail (win != NULL);
	g_return_if_fail (GNCAL_IS_MAIN_WINDOW (win));

	calendar_widget_calendar_new (CALENDAR_WIDGET (win->priv->calendar));
}

gboolean
gncal_main_window_save_calendar (GncalMainWindow *win, GError **error)
{
	g_return_val_if_fail (win != NULL, FALSE);
	g_return_val_if_fail (GNCAL_IS_MAIN_WINDOW (win), FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	return calendar_widget_calendar_save (CALENDAR_WIDGET (win->priv->calendar), NULL, error);
}
