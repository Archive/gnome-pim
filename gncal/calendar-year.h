#ifndef __CALENDAR_YEAR_H__
#define __CALENDAR_YEAR_H__

/* Calendar Year Object
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* This class provides a widget that contains a complete month view.
 * It's derived from GnomeCanvas and makes use of CalendarMonthItem.
 */

#include <time.h>

#include <gtk/gtkvbox.h>


#define CALENDAR_TYPE_YEAR		(calendar_year_get_type())
#define CALENDAR_YEAR(obj)		(GTK_CHECK_CAST ((obj), CALENDAR_TYPE_YEAR, CalendarYear))
#define CALENDAR_YEAR_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_YEAR))
#define CALENDAR_IS_YEAR(obj)		(GTK_CHECK_TYPE ((obj), CALENDAR_TYPE_YEAR))
#define CALENDAR_IS_YEAR_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_YEAR))

typedef struct _CalendarYear		CalendarYear;
typedef struct _CalendarYearClass	CalendarYearClass;
typedef struct _CalendarYearPriv	CalendarYearPriv;

struct _CalendarYear
{
	GtkVBox parent;

	CalendarYearPriv *priv;
};

struct _CalendarYearClass
{
	GtkVBoxClass parent_class;

	void (* day_changed)  (CalendarYear *yitem, guint year, guint month, guint day);
	void (* day_selected) (CalendarYear *yitem, guint year, guint month, guint day);
	void (* day_menu)     (CalendarYear *yitem, guint year, guint month, guint day);
};

GType		 calendar_year_get_type		(void);
GtkWidget	*calendar_year_new		(void);

void		 calendar_year_set_date		(CalendarYear *cy, const GDate *date);
void		 calendar_year_get_date		(CalendarYear *cy, GDate *date);

void		 calendar_year_set_show_label	(CalendarYear *cy, gboolean show);
gboolean	 calendar_year_get_show_label	(CalendarYear *cy);

#endif /* __CALENDAR_YEAR_H__ */
