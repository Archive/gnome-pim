/* Calendar Year Object
 * Copyright (C) 2003-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <time.h>

#include <glib.h>
#include <gnome.h>
#include <libgnomecanvas/gnome-canvas.h>

#include "calendar-marshal.h"
#include "calendar-year.h"
#include "calendar-year-item.h"

static void calendar_year_class_init	(CalendarYearClass	*klass);
static void calendar_year_init		(CalendarYear		*cy);
static void calendar_year_finalize	(GObject		*object);

static void setup			(CalendarYear		*cy);


struct _CalendarYearPriv {
	GtkWidget *label;
	GtkWidget *canvas;
	GnomeCanvasItem *year_item;

	gboolean show_label;
};

enum {
	SIGNAL_DAY_CHANGED,
	SIGNAL_DAY_SELECTED,
	SIGNAL_DAY_MENU,
	SIGNAL_LAST
};


static GtkVBoxClass *parent_class = NULL;

static guint signals[SIGNAL_LAST] = { 0 };

/*
 * Class and Instance Setup
 */

GType
calendar_year_get_type (void)
{
	static GType calendar_year_type = 0;

	if (!calendar_year_type) {
		static const GTypeInfo calendar_year_info = {
			sizeof (CalendarYearClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_year_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarYear),
			0,    /* n_preallocs */
			(GInstanceInitFunc) calendar_year_init,
		};

		calendar_year_type = g_type_register_static (GTK_TYPE_VBOX,
							      "CalendarYear",
							      &calendar_year_info,
							      0);
	}

	return calendar_year_type;
}

static void
calendar_year_class_init (CalendarYearClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->finalize = calendar_year_finalize;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_DAY_CHANGED] =
		g_signal_new ("day-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarYearClass, day_changed),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	signals[SIGNAL_DAY_SELECTED] =
		g_signal_new ("day-selected",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarYearClass, day_selected),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	signals[SIGNAL_DAY_MENU] =
		g_signal_new ("day-menu",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarYearClass, day_menu),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);
}

static void
calendar_year_init (CalendarYear *cy)
{
	g_return_if_fail (cy != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR (cy));

	cy->priv = g_new0 (CalendarYearPriv, 1);
	cy->priv->show_label = FALSE;

	setup (cy);
}

static void
calendar_year_finalize (GObject *object)
{
	CalendarYear *cy;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR (object));

	cy = CALENDAR_YEAR (object);

	g_free (cy->priv);
	cy->priv = NULL;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*
 * Callbacks
 */

static void
day_changed_cb (CalendarYear *cy, guint year, guint month, guint day, CalendarYearItem *yitem)
{
	g_signal_emit (G_OBJECT (cy), signals[SIGNAL_DAY_CHANGED], 0, year, month, day);
}

static void
day_selected_cb (CalendarYear *cy, guint year, guint month, guint day, CalendarYearItem *yitem)
{
	g_signal_emit (G_OBJECT (cy), signals[SIGNAL_DAY_SELECTED], 0, year, month, day);
}

static void
day_menu_cb (CalendarYear *cy, guint year, guint month, guint day, CalendarYearItem *yitem)
{
	g_signal_emit (G_OBJECT (cy), signals[SIGNAL_DAY_MENU], 0, year, month, day);
}

/*
 * Private Methods
 */

static void
update_label (CalendarYear *cy)
{
	CalendarYearPriv *priv = cy->priv;
	GDate date;
	guint year, month, day;
	gchar buffer[100];

	g_object_get (G_OBJECT (priv->year_item),
		      "year",  &year,
		      "month", &month,
		      "day",   &day,
		      NULL);
	g_date_set_dmy (&date, day, month, year);

	/* Translators: This is YEAR */
	g_date_strftime (buffer, sizeof (buffer), "%Y", &date);
	gtk_label_set_text (GTK_LABEL (priv->label), buffer);
}

/*
 * Setup
 */

/* Sets the scrolling region of the canvas to the allocation size */
static void
set_scroll_region (CalendarYear *cy, GtkAllocation *allocation, GtkWidget *canvas)
{
	CalendarYearPriv *priv = cy->priv;

	if (GTK_WIDGET_CLASS (parent_class)->size_allocate)
		(* GTK_WIDGET_CLASS (parent_class)->size_allocate) (GTK_WIDGET (canvas), allocation);

        gnome_canvas_item_set (GNOME_CANVAS_ITEM (priv->year_item),
                               "width",  (double) (allocation->width - 1),
                               "height", (double) (allocation->height - 1),
                               NULL);

        gnome_canvas_set_scroll_region (GNOME_CANVAS (priv->canvas),
                                        0, 0,
                                        allocation->width, allocation->height);
}

static void
setup (CalendarYear *cy)
{
	CalendarYearPriv *priv;
	PangoAttrList *attrs;
	PangoAttribute *attr;

	priv = cy->priv;

	gtk_widget_set_size_request (GTK_WIDGET (cy), 300, 180);
	gtk_box_set_spacing (GTK_BOX (cy), 12);

	/* Label */

	attrs = pango_attr_list_new ();
	attr = pango_attr_scale_new (PANGO_SCALE_X_LARGE);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);
	attr = pango_attr_weight_new (PANGO_WEIGHT_BOLD);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);

	priv->label = gtk_label_new ("");
	gtk_label_set_attributes (GTK_LABEL (priv->label), attrs);
	gtk_box_pack_start (GTK_BOX (cy), priv->label, FALSE, FALSE, 0);

	pango_attr_list_unref (attrs);

	/* Canvas */

	priv->canvas = gnome_canvas_new ();
	gtk_box_pack_start (GTK_BOX (cy), priv->canvas, TRUE, TRUE, 0);
	gtk_widget_show (priv->canvas);

	priv->year_item = calendar_year_item_new (gnome_canvas_root (GNOME_CANVAS (priv->canvas)), NULL);
	g_signal_connect_swapped (G_OBJECT (priv->year_item), "day-changed",
				  G_CALLBACK (day_changed_cb), cy);
	g_signal_connect_swapped (G_OBJECT (priv->year_item), "day-selected",
				  G_CALLBACK (day_selected_cb), cy);
	g_signal_connect_swapped (G_OBJECT (priv->year_item), "day-menu",
				  G_CALLBACK (day_menu_cb), cy);

	/* Connect to size_allocate so that we can change the size of the
	 * year item and the scrolling region appropriately.
	 */

	/* FIXME: size request */
	g_signal_connect_swapped (G_OBJECT (priv->canvas), "size_allocate",
				  G_CALLBACK (set_scroll_region), cy);

	update_label (cy);
}

/*
 * Public Methods
 */

GtkWidget *
calendar_year_new (void)
{
	CalendarYear *cy;

	cy = g_object_new (CALENDAR_TYPE_YEAR, NULL);

	return GTK_WIDGET (cy);
}

void
calendar_year_set_date (CalendarYear *cy, const GDate *date)
{
	CalendarYearPriv *priv;

	g_return_if_fail (cy != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR (cy));
	g_return_if_fail (date != NULL);
	g_return_if_fail (g_date_valid (date));

	priv = cy->priv;

	calendar_year_item_set_date (CALENDAR_YEAR_ITEM (priv->year_item), date);
	update_label (cy);

	g_signal_emit (G_OBJECT (cy), signals[SIGNAL_DAY_SELECTED], 0,
		       (guint) date->year, (guint) date->month, (guint) date->day);
}

void
calendar_year_get_date (CalendarYear *cy, GDate *date)
{
	CalendarYearPriv *priv;

	g_return_if_fail (cy != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR (cy));
	g_return_if_fail (date != NULL);

	priv = cy->priv;

	calendar_year_item_get_date (CALENDAR_YEAR_ITEM (priv->year_item), date);
}

void
calendar_year_set_show_label (CalendarYear *cy, gboolean show)
{
	CalendarYearPriv *priv;

	g_return_if_fail (cy != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR (cy));

	priv = cy->priv;

	priv->show_label = show ? TRUE : FALSE;

	if (show)
		gtk_widget_show (priv->label);
	else
		gtk_widget_hide (priv->label);
}

gboolean
calendar_year_get_show_label (CalendarYear *cy)
{
	CalendarYearPriv *priv;

	g_return_val_if_fail (cy != NULL, FALSE);
	g_return_val_if_fail (CALENDAR_IS_YEAR (cy), FALSE);

	priv = cy->priv;

	return priv->show_label;
}
