#ifndef __GNOMECAL_CONFIG_H__
#define __GNOMECAL_CONFIG_H__

/* GNOME Calendar Configuration Functions and Defines
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <gconf/gconf-client.h>


#define CONFIG_DIR			"/apps/gnomecal"
#define CONFIG_GENERAL_DIR		"/apps/gnomecal/general"

#define CONFIG_CALENDAR_DIR		"/apps/gnomecal/calendar"
#define CONFIG_CAL_DAY_START		"/apps/gnomecal/calendar/day_start"
#define CONFIG_CAL_DAY_END		"/apps/gnomecal/calendar/day_end"
#define CONFIG_CAL_ROLLOVER		"/apps/gnomecal/calendar/rollover_at_midnight"

#define CONFIG_TODO_DIR			"/apps/gnomecal/todo"
#define CONFIG_TODO_SHOW_DUEDATE	"/apps/gnomecal/todo/show_duedate"
#define CONFIG_TODO_SHOW_PRIORITY	"/apps/gnomecal/todo/show_priority"
#define CONFIG_TODO_SHOW_TIMELEFT	"/apps/gnomecal/todo/show_timeleft"
#define CONFIG_TODO_SHOW_CATEGORIES	"/apps/gnomecal/todo/show_categories"

#define CONFIG_COLOR_DIR		"/apps/gnomecal/colors"
#define CONFIG_COLOR_OUTLINE		"/apps/gnomecal/colors/outline"
#define CONFIG_COLOR_HEADINGS		"/apps/gnomecal/colors/headings"
#define CONFIG_COLOR_EMPTY_DAYS		"/apps/gnomecal/colors/empty_days"
#define CONFIG_COLOR_APPOINTMENTS	"/apps/gnomecal/colors/appointments"
#define CONFIG_COLOR_DAY_HIGHLIGHT	"/apps/gnomecal/colors/day_highlight"
#define CONFIG_COLOR_DAY_NUMBERS	"/apps/gnomecal/colors/day_numbers"
#define CONFIG_COLOR_CURRENT_DAY	"/apps/gnomecal/colors/current_day"
#define CONFIG_COLOR_EVENT_DAYS		"/apps/gnomecal/colors/event_days"
#define CONFIG_COLOR_NOT_DUE		"/apps/gnomecal/colors/todo_not_due"
#define CONFIG_COLOR_DUE_TODAY		"/apps/gnomecal/colors/todo_due_today"
#define CONFIG_COLOR_OVERDUE		"/apps/gnomecal/colors/todo_overdue"

#define CONFIG_ALARM_DIR		"/apps/gnomecal/alarm"
#define CONFIG_ALARM_BEEP		"/apps/gnomecal/alarm/beep"
#define CONFIG_ALARM_TIMEOUT		"/apps/gnomecal/alarm/timeout"
#define CONFIG_ALARM_SNOOZE		"/apps/gnomecal/alarm/snooze"
#define CONFIG_ALARM_DISPLAY_ENABLED	"/apps/gnomecal/alarm/display_enabled"
#define CONFIG_ALARM_DISPLAY_TIME	"/apps/gnomecal/alarm/display_time"
#define CONFIG_ALARM_AUDIO_ENABLED	"/apps/gnomecal/alarm/audio_enabled"
#define CONFIG_ALARM_AUDIO_TIME		"/apps/gnomecal/alarm/audio_time"
#define CONFIG_ALARM_PROGRAM_ENABLED	"/apps/gnomecal/alarm/program_enabled"
#define CONFIG_ALARM_PROGRAM_TIME	"/apps/gnomecal/alarm/program_time"
#define CONFIG_ALARM_PROGRAM_COMMAND	"/apps/gnomecal/alarm/program_command"
#define CONFIG_ALARM_MAIL_ENABLED	"/apps/gnomecal/alarm/mail_enabled"
#define CONFIG_ALARM_MAIL_TIME		"/apps/gnomecal/alarm/mail_time"
#define CONFIG_ALARM_MAIL_RECEIVER	"/apps/gnomecal/alarm/mail_receiver"

/* Defaults */

#define DEFAULT_CAL_DAY_START		(8)
#define DEFAULT_CAL_DAY_END		(17)

#define DEFAULT_COLOR_OUTLINE		"DarkSlateBlue"
#define DEFAULT_COLOR_HEADINGS		"White"
#define DEFAULT_COLOR_EMPTY_DAYS	"Wheat"
#define DEFAULT_COLOR_APPOINTMENTS	"Yellow"
#define DEFAULT_COLOR_DAY_HIGHLIGHT	"#FFEFD5"
#define DEFAULT_COLOR_DAY_NUMBERS	"Black"
#define DEFAULT_COLOR_CURRENT_DAY	"Blue"
#define DEFAULT_COLOR_EVENT_DAYS	"Red"
#define DEFAULT_COLOR_NOT_DUE		"Black"
#define DEFAULT_COLOR_DUE_TODAY		"Orange"
#define DEFAULT_COLOR_OVERDUE		"Red"

#define DEFAULT_ALARM_BEEP		(FALSE)
#define DEFAULT_ALARM_TIMEOUT		(0)
#define DEFAULT_ALARM_TIMEOUT_VALUE	(30)
#define DEFAULT_ALARM_TIMEOUT_MIN	(1)
#define DEFAULT_ALARM_TIMEOUT_MAX	(3600)
#define DEFAULT_ALARM_SNOOZE		(0)
#define DEFAULT_ALARM_SNOOZE_VALUE	(30)
#define DEFAULT_ALARM_SNOOZE_MIN	(1)
#define DEFAULT_ALARM_SNOOZE_MAX	(3600)
#define DEFAULT_ALARM_DISPLAY_ENABLED	(FALSE)
#define DEFAULT_ALARM_DISPLAY_TIME	(15)
#define DEFAULT_ALARM_AUDIO_ENABLED	(FALSE)
#define DEFAULT_ALARM_AUDIO_TIME	(15)
#define DEFAULT_ALARM_PROGRAM_ENABLED	(FALSE)
#define DEFAULT_ALARM_PROGRAM_TIME	(0)
#define DEFAULT_ALARM_PROGRAM_COMMAND	("")
#define DEFAULT_ALARM_MAIL_ENABLED	(FALSE)
#define DEFAULT_ALARM_MAIL_TIME		(2)
#define DEFAULT_ALARM_MAIL_RECEIVER	(g_get_user_name())
#define DEFAULT_ALARM_TIME_MIN		(0)
#define DEFAULT_ALARM_TIME_MAX		(144000)

/* Functions */

gchar *gncal_config_get_color (GConfClient *gconf, const gchar *key, const gchar *def);

#endif /* __GNOMECAL_CONFIG_H__ */
