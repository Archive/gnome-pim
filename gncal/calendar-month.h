#ifndef __CALENDAR_MONTH_H__
#define __CALENDAR_MONTH_H__

/* Calendar Month Object
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* This class provides a widget that contains a complete month view.
 * It's derived from GnomeCanvas and makes use of CalendarMonthItem.
 */

#include <time.h>

#include <gtk/gtkvbox.h>
#include <libgnomecanvas/gnome-canvas.h>

#include "calendar-manager.h"


#define CALENDAR_TYPE_MONTH		(calendar_month_get_type())
#define CALENDAR_MONTH(obj)		(GTK_CHECK_CAST ((obj), CALENDAR_TYPE_MONTH, CalendarMonth))
#define CALENDAR_MONTH_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_MONTH))
#define CALENDAR_IS_MONTH(obj)		(GTK_CHECK_TYPE ((obj), CALENDAR_TYPE_MONTH))
#define CALENDAR_IS_MONTH_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_MONTH))

typedef struct _CalendarMonth		CalendarMonth;
typedef struct _CalendarMonthClass	CalendarMonthClass;
typedef struct _CalendarMonthPriv	CalendarMonthPriv;

struct _CalendarMonth
{
	GtkVBox parent;

	CalendarMonthPriv *priv;
};

struct _CalendarMonthClass
{
	GtkVBoxClass parent_class;

	void (* day_changed)  (CalendarMonth *cm, guint year, guint month, guint day);
	void (* day_selected) (CalendarMonth *cm, guint year, guint month, guint day);
	void (* day_menu)     (CalendarMonth *cm, guint year, guint month, guint day);
};

GType		 calendar_month_get_type	(void);
GtkWidget	*calendar_month_new		(void);

void		 calendar_month_set_calendar	(CalendarMonth *cm, CalendarManager *calendar);

GnomeCanvasItem	*calendar_month_get_month_item	(CalendarMonth *cm);

void		 calendar_month_set_date	(CalendarMonth *cm, const GDate *date);
void		 calendar_month_get_date	(CalendarMonth *cm, GDate *date);

void		 calendar_month_set_show_label	(CalendarMonth *cm, gboolean show);
gboolean	 calendar_month_get_show_label	(CalendarMonth *cm);

#endif /* __CALENDAR_MONTH_H__ */
