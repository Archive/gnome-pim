/* To Do List Object
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* FIXME: row size changeable, rows reorderable, sort during instant apply */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <time.h>

#include <glib.h>

#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include <gnome.h>

#include <mimedir/mimedir-attribute.h>
#include <mimedir/mimedir-vtodo.h>

#include "calendar-manager.h"
#include "gnomecal-alarm-sink.h"
#include "gnomecal-config.h"
#include "todo-dialog.h"
#include "todo-list.h"

static void	 todo_list_class_init	(TodoListClass *klass);
static void	 todo_list_init		(TodoList *dialog);
static void	 todo_list_dispose	(GObject *dialog);

static void	 setup			(TodoList *box);

static void	 remove_all_items	(TodoList *box);

static void	 item_changed_cb	(MIMEDirVTodo *item, TodoList *box);

/*
 * Class and Instance Setup
 */

struct _TodoListPriv {
	CalendarManager *calendar;

	/* Widgets */

	GtkWidget *tree;
	GtkWidget *edit;
	GtkWidget *delete;

	/* Alarm Sink */

	GncalAlarmSink *alarm_sink;

	/* Cache for gconf */

	gchar *color_not_due;
	gchar *color_due_today;
	gchar *color_overdue;

	/* Signal IDs */

	guint notify1, notify2, notify3, notify4;
	gulong todo_add_sig, todo_remove_sig;
};

enum {
	MODEL_OBJECT,
	MODEL_SIGNAL,
	MODEL_COUNT
};

enum {
	COLUMN_SUMMARY,
	COLUMN_DUEDATE,
	COLUMN_PRIORITY,
	COLUMN_TIMELEFT,
	COLUMN_CATEGORIES,
	COLUMN_COUNT
};

typedef enum {
	DUE_NEVER,  /* if no due date is set */
	DUE_FUTURE,
	DUE_TODAY,
	DUE_PAST
} DueState;


static GtkVBoxClass *parent_class = NULL;


GType
todo_list_get_type (void)
{
	static GType todo_list_type = 0;

	if (!todo_list_type) {
		static const GTypeInfo todo_list_info = {
			sizeof (TodoListClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) todo_list_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (TodoList),
			4,    /* n_preallocs */
			(GInstanceInitFunc) todo_list_init,
		};

		todo_list_type = g_type_register_static (GTK_TYPE_VBOX,
							 "TodoList",
							 &todo_list_info,
							 0);
	}

	return todo_list_type;
}

static void
todo_list_class_init (TodoListClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (TODO_IS_LIST_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose  = todo_list_dispose;

	parent_class = g_type_class_peek_parent (klass);
}

static void
todo_list_init (TodoList *list)
{
	g_return_if_fail (list != NULL);
	g_return_if_fail (TODO_IS_LIST (list));

	list->priv = g_new0 (TodoListPriv, 1);

	list->priv->color_not_due   = NULL;
	list->priv->color_due_today = NULL;
	list->priv->color_overdue   = NULL;

	list->priv->alarm_sink = gncal_alarm_sink_new ();

	setup (list);
}

static void
todo_list_dispose (GObject *object)
{
	TodoList *todo;
	TodoListPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (TODO_IS_LIST (object));

	todo = TODO_LIST (object);
	priv = todo->priv;

	if (todo->priv) {
		GConfClient *gconf;

		remove_all_items (todo);

		g_free (priv->color_not_due);
		g_free (priv->color_due_today);
		g_free (priv->color_overdue);

		g_object_unref (G_OBJECT (priv->alarm_sink));

		g_signal_handler_disconnect (priv->calendar, priv->todo_add_sig);
		g_signal_handler_disconnect (priv->calendar, priv->todo_remove_sig);
		g_object_unref (G_OBJECT (priv->calendar));

		gconf = gconf_client_get_default ();

		if (priv->notify1) {
			gconf_client_notify_remove (gconf, priv->notify1);
			priv->notify1 = 0;
		}
		if (priv->notify2) {
			gconf_client_notify_remove (gconf, priv->notify2);
			priv->notify2 = 0;
		}
		if (priv->notify3) {
			gconf_client_notify_remove (gconf, priv->notify3);
			priv->notify3 = 0;
		}
		if (priv->notify4) {
			gconf_client_notify_remove (gconf, priv->notify4);
			priv->notify4 = 0;
		}

		g_object_unref (G_OBJECT (gconf));
	}

	g_free (todo->priv);
	todo->priv = NULL;

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Tree Sort Functions
 */

static gint
todo_list_sort_summary (GtkTreeModel	*model,
			GtkTreeIter	*a,
			GtkTreeIter	*b,
			gpointer	 data)
{
	MIMEDirVTodo *item_a, *item_b;
	gchar *str_a, *str_b;
	gint ret;

	g_return_val_if_fail (model != NULL, 0);
	g_return_val_if_fail (GTK_IS_TREE_MODEL (model), 0);
	g_return_val_if_fail (a != NULL && b != NULL, 0);

	gtk_tree_model_get (model, a, MODEL_OBJECT, &item_a, -1);
	gtk_tree_model_get (model, b, MODEL_OBJECT, &item_b, -1);

	g_object_get (item_a, "summary", &str_a, NULL);
	g_object_get (item_b, "summary", &str_b, NULL);

	if (str_a == NULL && str_b == NULL)
		ret = 0;
	else if (str_a == NULL)
		ret = 1;
	else if (str_b == NULL)
		ret = -1;
	else
		ret = g_utf8_collate (str_a, str_b);

	g_free (str_a);
	g_free (str_b);

	return ret;
}

static gint
todo_list_sort_due_date (GtkTreeModel	*model,
			 GtkTreeIter	*a,
			 GtkTreeIter	*b,
			 gpointer	 data)
{
	MIMEDirVTodo *item_a, *item_b;
	MIMEDirDateTime *dt_a, *dt_b;
	time_t t_a, t_b;

	g_return_val_if_fail (model != NULL, 0);
	g_return_val_if_fail (GTK_IS_TREE_MODEL (model), 0);
	g_return_val_if_fail (a != NULL && b != NULL, 0);

	gtk_tree_model_get (model, a, MODEL_OBJECT, &item_a, -1);
	gtk_tree_model_get (model, b, MODEL_OBJECT, &item_b, -1);

	g_assert (item_a != NULL && item_b != NULL);
	g_assert (MIMEDIR_IS_VTODO (item_a) && MIMEDIR_IS_VTODO (item_b));

	g_object_get (item_a, "due", &dt_a, NULL);
	g_object_get (item_b, "due", &dt_b, NULL);

	if (!dt_a && !dt_b)
		return 0;
	else if (!dt_a)
		return -1;
	else if (!dt_b)
		return 1;

	t_a = mimedir_datetime_get_time_t (dt_a);
	t_b = mimedir_datetime_get_time_t (dt_b);

	g_object_unref (G_OBJECT (dt_a));
	g_object_unref (G_OBJECT (dt_b));

	if (t_a == t_b)
		return 0;
	else if (t_a > t_b)
		return 1;
	else
		return -1;
}

static gint
todo_list_sort_priority (GtkTreeModel	*model,
			 GtkTreeIter	*a,
			 GtkTreeIter	*b,
			 gpointer	 data)
{
	MIMEDirVTodo *item_a, *item_b;
	guint pri_a, pri_b;

	g_return_val_if_fail (model != NULL, 0);
	g_return_val_if_fail (GTK_IS_TREE_MODEL (model), 0);
	g_return_val_if_fail (a != NULL && b != NULL, 0);

	gtk_tree_model_get (model, a, MODEL_OBJECT, &item_a, -1);
	gtk_tree_model_get (model, b, MODEL_OBJECT, &item_b, -1);

	g_assert (item_a != NULL && item_b != NULL);
	g_assert (MIMEDIR_IS_VTODO (item_a) && MIMEDIR_IS_VTODO (item_b));

	g_object_get (item_a, "priority", &pri_a, NULL);
	g_object_get (item_b, "priority", &pri_b, NULL);

	if (pri_a < pri_b)
		return 1;
	else if (pri_a > pri_b)
		return -1;
	else
		return 0;
}

/*
 * Private Methods
 */

static void
todo_list_properties_deleted (GtkWidget *widget, MIMEDirVTodo *item)
{
	g_return_if_fail (item != NULL);
	g_return_if_fail (MIMEDIR_IS_VTODO (item));

	g_object_set_data (G_OBJECT (item), "editor", NULL);
}

static void
todo_list_open_properties (TodoList *box, MIMEDirVTodo *item)
{
	GtkWidget *dialog;

	dialog = g_object_get_data (G_OBJECT (item), "editor");

	if (!dialog) {
		GtkWidget *toplevel;

		toplevel = gtk_widget_get_toplevel (GTK_WIDGET (box));

		dialog = todo_dialog_new (GTK_WINDOW (toplevel), item);

		g_object_set_data (G_OBJECT (item), "editor", dialog);

		g_signal_connect (G_OBJECT (dialog), "destroy",
				  G_CALLBACK (todo_list_properties_deleted),
				  item);
	}

	gtk_window_present (GTK_WINDOW (dialog));
}

#if 0
static gboolean
get_all_items_cb (GtkTreeModel *model,
		  GtkTreePath  *path,
		  GtkTreeIter  *iter,
		  gpointer      data)
{
	GList **list;
	MIMEDirVTodo *item;

	list = (GList **) data;

	gtk_tree_model_get (model, iter, MODEL_OBJECT, &item, -1);

	*list = g_list_append (*list, item);

	return FALSE;
}

static GList *
get_all_items (TodoList *todo)
{
	GtkTreeModel *model;
	GList *list = NULL;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (todo->priv->tree));

	gtk_tree_model_foreach (model, get_all_items_cb, &list);

	return list;
}
#endif

static void
append_item (TodoList *box, MIMEDirVTodo *item)
{
	GtkTreeView		*tree;
	GtkTreeModel		*model;
	GtkTreeSelection	*selection;
	GtkTreeIter		 iter;
	gulong			 signal;

	g_object_ref (G_OBJECT (item));

	signal = g_signal_connect (G_OBJECT (item), "changed",
				   G_CALLBACK (item_changed_cb), box);

	tree = GTK_TREE_VIEW (box->priv->tree);

	model     = gtk_tree_view_get_model (tree);
	selection = gtk_tree_view_get_selection (tree);

	gtk_list_store_append (GTK_LIST_STORE (model), &iter);
	gtk_list_store_set (GTK_LIST_STORE (model), &iter,
			    MODEL_OBJECT, item,
			    MODEL_SIGNAL, signal,
			    -1);

	gtk_tree_selection_select_iter (selection, &iter);

	gncal_alarm_sink_add (box->priv->alarm_sink, MIMEDIR_VCOMPONENT (item));
}

static void
delete_item (TodoList *box, MIMEDirVTodo *item, gulong signal)
{
	GtkWidget *dialog;

	dialog = g_object_get_data (G_OBJECT (item), "editor");
	if (dialog)
		gtk_widget_destroy (dialog);

	gncal_alarm_sink_remove (box->priv->alarm_sink, MIMEDIR_VCOMPONENT (item));

	g_signal_handler_disconnect (G_OBJECT (item), signal);

	g_object_unref (G_OBJECT (item));
}

static void
remove_item_iter (TodoList *box, GtkTreeIter *iter)
{
	GtkTreeModel *model;
	MIMEDirVTodo *item;
	gulong signal;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (box->priv->tree));

	g_assert (GTK_IS_LIST_STORE (model));

	gtk_tree_model_get (model, iter,
			    MODEL_OBJECT, &item,
			    MODEL_SIGNAL, &signal,
			    -1);

	gtk_list_store_remove (GTK_LIST_STORE (model), iter);

	g_assert (item != NULL);
	g_assert (MIMEDIR_IS_VTODO (item));

	delete_item (box, item, signal);
}

static void
remove_item (TodoList *box, MIMEDirVTodo *item)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gboolean found = FALSE;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (box->priv->tree));

	g_assert (GTK_IS_LIST_STORE (model));

	if (gtk_tree_model_get_iter_first(model, &iter)) {
		do {
			MIMEDirVTodo *item2;

			gtk_tree_model_get (model, &iter,
					    MODEL_OBJECT, &item2,
					    -1);

			if (item2 == item) {
				found = TRUE;
				break;
			}
		} while (gtk_tree_model_iter_next (model, &iter));
	}

	g_return_if_fail (found != FALSE);

	remove_item_iter (box, &iter);
}

static gboolean
remove_all_items_foreach (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	TodoList *box;
	MIMEDirVTodo *item;
	gulong signal;

	box = TODO_LIST (data);

	gtk_tree_model_get (model, iter,
			    MODEL_OBJECT, &item,
			    MODEL_SIGNAL, &signal,
			    -1);

	g_assert (MIMEDIR_IS_VTODO (item));

	delete_item (box, item, signal);

	return FALSE;
}

static void
remove_all_items (TodoList *box)
{
	GtkTreeModel *model;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (box->priv->tree));

	g_assert (GTK_IS_LIST_STORE (model));

	gtk_tree_model_foreach (model, remove_all_items_foreach, box);

	gtk_list_store_clear (GTK_LIST_STORE (model));
}

static void
todo_list_enable_tree_columns (TodoList *box, gboolean *which)
{
	gint i;

	/* Ignore the first (summary) cell, since it's always shown. */

	for (i = 1; i < COLUMN_COUNT; i++) {
		GtkTreeViewColumn *column;

		column = gtk_tree_view_get_column (GTK_TREE_VIEW (box->priv->tree), i);

		gtk_tree_view_column_set_visible (column, which[i]);
	}
}

#define SECS_IN_DAY	(86400)
#define SECS_IN_WEEK	(SECS_IN_DAY * 7)

DueState
todo_list_item_due_state (MIMEDirVTodo *item, gint *remaining_days)
{
	MIMEDirDateTime	*t;
	struct tm	 today_tm;
	time_t		 now;
	GDate		 due, today;
	gint		 remaining;

	/* Ensure that remaining_days is always a valid pointer,
	 * since we use that variable for calculations.
	 */
	if (!remaining_days)
		remaining_days = &remaining;

	g_object_get (G_OBJECT (item), "due", &t, NULL);

	if (!t || !mimedir_datetime_is_valid (t)) {
		*remaining_days = 0;
		return DUE_NEVER;
	}

	mimedir_datetime_get_gdate (t, &due);
	g_object_unref (G_OBJECT (t));

	now = time (NULL);
	today_tm = *localtime (&now);
	g_date_set_dmy (&today, today_tm.tm_mday, today_tm.tm_mon + 1, today_tm.tm_year + 1900);

	*remaining_days = g_date_days_between (&today, &due);

	if (*remaining_days < 0)
		return DUE_PAST;
	else if (*remaining_days == 0)
		return DUE_TODAY;
	else
		return DUE_FUTURE;
}

#ifndef DAYS_IN_WEEK
#   define DAYS_IN_WEEK (7)
#endif /* DAYS_IS_WEEK */

gchar *
todo_list_get_remaining_time_string (MIMEDirVTodo *item)
{
	gint remaining_days;

	switch (todo_list_item_due_state (item, &remaining_days)) {
	case DUE_NEVER:
		return g_strdup ("");

	case DUE_PAST:
		return g_strdup (_("Overdue!"));

	case DUE_TODAY:
		return g_strdup (_("Due today!"));

	case DUE_FUTURE:
		if (remaining_days / DAYS_IN_WEEK > 0)
			return g_strdup_printf (_("%d week(s), %d day(s)"),
						remaining_days / DAYS_IN_WEEK,
						(remaining_days % DAYS_IN_WEEK));
		else
			return g_strdup_printf (_("%d day(s)"),
						remaining_days);
	}

	g_return_val_if_reached (NULL);
}

static void
todo_list_set_due_style (TodoList *list,
			 GtkCellRenderer *renderer,
			 MIMEDirVTodo *item)
{
	DueState due_state;
	const gchar *color;

	due_state = todo_list_item_due_state (item, NULL);

	switch (due_state) {
	case DUE_NEVER:
	case DUE_FUTURE:
		color = list->priv->color_not_due;
		break;

	case DUE_TODAY:
		color = list->priv->color_due_today;
		break;

	case DUE_PAST:
		color = list->priv->color_overdue;
		break;

	default:
		g_return_if_reached ();
	}

	if (!pango_color_parse (NULL, color))
		color = "#000";

	g_object_set (G_OBJECT (renderer), "foreground", color, NULL);
}

static void
todo_list_cell_data (GtkTreeViewColumn	*column,
		     GtkCellRenderer	*renderer,
		     GtkTreeModel	*model,
		     GtkTreeIter	*iter,
		     gpointer		 data)
{
	TodoList	*list;
	MIMEDirVTodo	*item;
	gint		 which;

	g_return_if_fail (column != NULL);
	g_return_if_fail (GTK_IS_TREE_VIEW_COLUMN (column));
	g_return_if_fail (renderer != NULL);
	g_return_if_fail (GTK_IS_CELL_RENDERER (renderer));
	g_return_if_fail (model != NULL);
	g_return_if_fail (GTK_IS_TREE_MODEL (model));
	g_return_if_fail (iter != NULL);
	g_return_if_fail (data != NULL);
	g_return_if_fail (TODO_IS_LIST (data));

	list = TODO_LIST (data);

	gtk_tree_model_get (model, iter, MODEL_OBJECT, &item, -1);

	g_return_if_fail (item != NULL);
	g_return_if_fail (MIMEDIR_IS_VTODO (item));

	/* Determine and set style for this line. This depends on the
	 * due state of the todo item (i.e. overdue, due today, other).
	 */
	todo_list_set_due_style (list, renderer, item);

	which = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (column), "index"));

	switch (which) {
	case COLUMN_SUMMARY: {
		gchar *text;

		g_object_get (G_OBJECT (item), "summary", &text, NULL);

		if (!text || text[0] == '\0') {
			g_free (text);
			text = g_strdup (_("<Unnamed>"));
		}

		g_object_set (G_OBJECT (renderer), "text", text, NULL);

		g_free (text);

		break;
	}

	case COLUMN_DUEDATE: {
		MIMEDirDateTime *due;

		g_object_get (G_OBJECT (item), "due", &due, NULL);

		if (due && mimedir_datetime_is_valid (due)) {
			gchar *s;

			/* Translators: This is YYYY-MM-DD */
			s = g_strdup_printf (_("%04d-%02d-%02d"), due->year, due->month, due->day);
			g_object_set (G_OBJECT (renderer), "text", s, NULL);
			g_free (s);

			g_object_unref (G_OBJECT (due));
		} else
			g_object_set (G_OBJECT (renderer), "text", "", NULL);

		break;
	}

	case COLUMN_PRIORITY: {
		guint  priority;
		gchar *text;

		g_object_get (G_OBJECT (item), "priority", &priority, NULL);

		if (priority >= 1 && priority <= 4)
			text = g_strdup_printf (_("High (%d)"), priority);
		else if (priority == 5)
			text = g_strdup_printf (_("Medium (%d)"), priority);
		else if (priority >= 6 && priority <= 9)
			text = g_strdup_printf (_("Low (%d)"), priority);
		else
			text = g_strdup("");

		g_object_set (G_OBJECT (renderer), "text", text, NULL);

		g_free (text);

		break;
	}

	/* FIXME: must be updated at midnight (by using a timer function) */
	case COLUMN_TIMELEFT: {
		gchar	*text;

		text = todo_list_get_remaining_time_string (item);
		g_object_set (G_OBJECT (renderer), "text", text, NULL);
		g_free (text);

		break;
	}

	case COLUMN_CATEGORIES: {
		gchar *text;

		text = mimedir_vcomponent_get_categories_as_string (MIMEDIR_VCOMPONENT (item));
		g_object_set (G_OBJECT (renderer), "text", text, NULL);
		g_free (text);

		break;
	}

	default:
		g_return_if_reached ();
	}
}

static void
todo_list_selection_changed (GtkTreeSelection *selection, TodoList *box)
{
	g_return_if_fail (selection != NULL);
	g_return_if_fail (GTK_IS_TREE_SELECTION (selection));
	g_return_if_fail (box != NULL);
	g_return_if_fail (TODO_IS_LIST (box));

	if (gtk_tree_selection_get_selected (selection, NULL, NULL)) {
		gtk_widget_set_sensitive (box->priv->edit,   TRUE);
		gtk_widget_set_sensitive (box->priv->delete, TRUE);
	} else {
		gtk_widget_set_sensitive (box->priv->edit,   FALSE);
		gtk_widget_set_sensitive (box->priv->delete, FALSE);
	}
}

static void
todo_list_color_config_changed (GConfClient *client,
				guint        cnxn_id,
				GConfEntry  *entry,
				gpointer     data)
{
	TodoList *list;
	const gchar *key, *value;

	g_return_if_fail (entry != NULL);
	g_return_if_fail (data != NULL);
	g_return_if_fail (TODO_IS_LIST (data));

	list = TODO_LIST (data);

	key = gconf_entry_get_key (entry);
	value = gconf_value_get_string (gconf_entry_get_value (entry));

	if (strcmp (key, CONFIG_COLOR_NOT_DUE) == 0) {
		g_free (list->priv->color_not_due);
		list->priv->color_not_due = g_strdup (value);
	} else if (strcmp (key, CONFIG_COLOR_DUE_TODAY) == 0) {
		g_free (list->priv->color_due_today);
		list->priv->color_due_today = g_strdup (value);
	} else if (strcmp (key, CONFIG_COLOR_OVERDUE) == 0) {
		g_free (list->priv->color_overdue);
		list->priv->color_overdue = g_strdup (value);
	}

	gtk_widget_queue_draw (list->priv->tree);
}

static void
todo_list_column_config_changed (GConfClient *client,
				 guint        cnxn_id,
				 GConfEntry  *entry,
				 gpointer     data)
{
	TodoList		*list;
	GtkTreeView		*tree;
	GtkTreeViewColumn	*column;
	gint			 column_id;
	const gchar		*key;
	gboolean		 value;

	g_return_if_fail (entry != NULL);
	g_return_if_fail (data != NULL);
	g_return_if_fail (TODO_IS_LIST (data));

	list = TODO_LIST (data);

	key = gconf_entry_get_key (entry);
	value = gconf_value_get_bool (gconf_entry_get_value (entry));

	if (strcmp (key, CONFIG_TODO_SHOW_DUEDATE) == 0)
		column_id = COLUMN_DUEDATE;
	else if (strcmp (key, CONFIG_TODO_SHOW_PRIORITY) == 0)
		column_id = COLUMN_PRIORITY;
	else if (strcmp (key, CONFIG_TODO_SHOW_TIMELEFT) == 0)
		column_id = COLUMN_TIMELEFT;
	else if (strcmp (key, CONFIG_TODO_SHOW_CATEGORIES) == 0)
		column_id = COLUMN_CATEGORIES;
	else
		g_return_if_reached ();

	tree = GTK_TREE_VIEW (list->priv->tree);
	column = gtk_tree_view_get_column (tree, column_id);	

	gtk_tree_view_column_set_visible (column, value);
}

/*
 * Callbacks
 */

static void
todo_list_row_activated (GtkTreeView       *tree,
			 GtkTreePath       *path,
			 GtkTreeViewColumn *column,
			 gpointer           data)
{
	GtkTreeModel *model;
	GtkTreeIter   iter;
	MIMEDirVTodo *item;

	g_return_if_fail (tree != NULL);
	g_return_if_fail (GTK_IS_TREE_VIEW (tree));
	g_return_if_fail (data != NULL);
	g_return_if_fail (TODO_IS_LIST (data));

	model = gtk_tree_view_get_model (tree);

	gtk_tree_model_get_iter (model, &iter, path);

	gtk_tree_model_get (model, &iter, MODEL_OBJECT, &item, -1);

	g_return_if_fail (item != NULL);
	g_return_if_fail (MIMEDIR_IS_VTODO (item));

	todo_list_open_properties (TODO_LIST (data), item);
}

static gboolean
item_row_changed_cb (GtkTreeModel *model,
		     GtkTreePath  *path,
		     GtkTreeIter  *iter,
		     gpointer      data)
{
	MIMEDirVTodo *item;
	MIMEDirVTodo *curitem;

	g_return_val_if_fail (model != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TREE_MODEL (model), FALSE);
	g_return_val_if_fail (iter != NULL, FALSE);
	g_return_val_if_fail (data != NULL, FALSE);
	g_return_val_if_fail (MIMEDIR_IS_VTODO (data), FALSE);

	item = MIMEDIR_VTODO (data);

	gtk_tree_model_get (model, iter, MODEL_OBJECT, &curitem, -1);

	g_return_val_if_fail (curitem != NULL, FALSE);
	g_return_val_if_fail (MIMEDIR_IS_VTODO (curitem), FALSE);

	if (item == curitem)
		gtk_tree_model_row_changed (model, path, iter);

	return FALSE;
}

static void
add_clicked_cb (GtkWidget *button, TodoList *box)
{
	MIMEDirVTodo *item;

	g_return_if_fail (box != NULL);
	g_return_if_fail (TODO_IS_LIST (box));
	g_return_if_fail (box->priv->calendar != NULL);

	item = mimedir_vtodo_new ();

	calendar_manager_add_todo_item (box->priv->calendar, item);

	todo_list_open_properties (box, item);

	g_object_unref (item);
}

static void
prop_clicked_cb (GtkWidget *button, TodoList *box)
{
	GtkTreeView		*tree;
	GtkTreeModel		*model;
	GtkTreeSelection	*selection;
	GtkTreeIter		 iter;

	g_return_if_fail (box != NULL);
	g_return_if_fail (TODO_IS_LIST (box));

	tree = GTK_TREE_VIEW (box->priv->tree);

	selection = gtk_tree_view_get_selection (tree);

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVTodo *item;

		gtk_tree_model_get (model, &iter, MODEL_OBJECT, &item, -1);

		g_assert (item != NULL);
		g_assert (MIMEDIR_IS_VTODO (item));

		todo_list_open_properties (box, item);
	}
}

static void
delete_clicked_cb (GtkWidget *button, TodoList *box)
{
	GtkTreeView		*tree;
	GtkTreeModel		*model;
	GtkTreeSelection	*selection;
	GtkTreeIter		 iter;

	g_return_if_fail (box != NULL);
	g_return_if_fail (TODO_IS_LIST (box));
	g_return_if_fail (box->priv->calendar != NULL);

	tree = GTK_TREE_VIEW (box->priv->tree);

	selection = gtk_tree_view_get_selection (tree);

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		MIMEDirVTodo *item;

		gtk_tree_model_get (model, &iter, MODEL_OBJECT, &item, -1);

		g_assert (item != NULL);
		g_assert (MIMEDIR_IS_VTODO (item));

		calendar_manager_remove_todo_item (box->priv->calendar, item);
	}
}

static void
item_changed_cb (MIMEDirVTodo *item, TodoList *box)
{
	GtkTreeView	*tree;
	GtkTreeModel	*model;

	g_return_if_fail (item != NULL);
	g_return_if_fail (MIMEDIR_IS_VTODO (item));
	g_return_if_fail (box != NULL);
	g_return_if_fail (TODO_IS_LIST (box));

	tree = GTK_TREE_VIEW (box->priv->tree);
	model = gtk_tree_view_get_model (tree);

	gtk_tree_model_foreach (model, item_row_changed_cb, item);
}

static void
todo_added_cb (TodoList *box, MIMEDirVTodo *item, gpointer data)
{
	append_item (box, item);
}

static void
todo_removed_cb (TodoList *box, MIMEDirVTodo *item, gpointer data)
{
	remove_item (box, item);
}

/*
 * Setup
 */

static void
setup_tree_columns (TodoList *box)
{
	GtkTreeViewColumn *column;
	GtkCellRenderer   *renderer;

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (box->priv->tree),
						    COLUMN_SUMMARY,
						    _("Summary"),
						    renderer,
						    todo_list_cell_data, box,
						    NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (box->priv->tree), COLUMN_SUMMARY);
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_SUMMARY);
	g_object_set_data (G_OBJECT (column), "index", GINT_TO_POINTER (COLUMN_SUMMARY));

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (box->priv->tree),
						    COLUMN_DUEDATE,
						    _("Due Date"),
						    renderer,
						    todo_list_cell_data, box,
						    NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (box->priv->tree), COLUMN_DUEDATE);
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_DUEDATE);
	g_object_set_data (G_OBJECT (column), "index", GINT_TO_POINTER (COLUMN_DUEDATE));

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (box->priv->tree),
						    COLUMN_PRIORITY,
						    _("Priority"),
						    renderer,
						    todo_list_cell_data, box,
						    NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (box->priv->tree), COLUMN_PRIORITY);
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_PRIORITY);
	g_object_set_data (G_OBJECT (column), "index", GINT_TO_POINTER (COLUMN_PRIORITY));

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (box->priv->tree),
						    COLUMN_TIMELEFT,
						    _("Time Left"),
						    renderer,
						    todo_list_cell_data, box,
						    NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (box->priv->tree), COLUMN_TIMELEFT);
	gtk_tree_view_column_set_sort_column_id (column, COLUMN_DUEDATE);
	g_object_set_data (G_OBJECT (column), "index", GINT_TO_POINTER (COLUMN_TIMELEFT));

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (box->priv->tree),
						    COLUMN_CATEGORIES,
						    _("Categories"),
						    renderer,
						    todo_list_cell_data, box,
						    NULL);
	column = gtk_tree_view_get_column (GTK_TREE_VIEW (box->priv->tree), COLUMN_CATEGORIES);
	g_object_set_data (G_OBJECT (column), "index", GINT_TO_POINTER (COLUMN_CATEGORIES));
}

static GtkTreeModel *
setup_tree_model (TodoList *box)
{
	GtkListStore *store;

	store = gtk_list_store_new (2, G_TYPE_OBJECT, G_TYPE_ULONG);
	gtk_tree_sortable_set_default_sort_func (GTK_TREE_SORTABLE (store),
						 todo_list_sort_summary, NULL,
						 NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 COLUMN_SUMMARY,
					 todo_list_sort_summary, NULL,
					 NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 COLUMN_DUEDATE,
					 todo_list_sort_due_date, NULL,
					 NULL);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (store),
					 COLUMN_PRIORITY,
					 todo_list_sort_priority, NULL,
					 NULL);

	return GTK_TREE_MODEL (store);
}

static void
setup (TodoList *box)
{
	TodoListPriv		*priv;
	GConfClient		*gconf;
	GtkWidget		*widget;
	GtkWidget		*buttonbox;
	GtkTreeModel		*model;
	GtkTreeSelection	*selection;

	gboolean enabled_columns[COLUMN_COUNT] = {
		TRUE, FALSE, FALSE, FALSE, FALSE
	};

	priv = box->priv;

	/* Setup configuration */

	gconf = gconf_client_get_default ();

	gconf_client_add_dir (gconf, CONFIG_COLOR_DIR,
			      GCONF_CLIENT_PRELOAD_NONE, NULL);
	gconf_client_add_dir (gconf, CONFIG_TODO_DIR,
			      GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);

	/* We cache the colors so we don't need to request them every time
	 * the cells are exposed.
	 */

	priv->color_not_due =
		gncal_config_get_color (gconf, CONFIG_COLOR_NOT_DUE,   DEFAULT_COLOR_NOT_DUE);
	priv->color_due_today =
		gncal_config_get_color (gconf, CONFIG_COLOR_DUE_TODAY, DEFAULT_COLOR_DUE_TODAY);
	priv->color_overdue =
		gncal_config_get_color (gconf, CONFIG_COLOR_OVERDUE,   DEFAULT_COLOR_OVERDUE);

	/* Which columns should be shown? */

	enabled_columns[COLUMN_DUEDATE] =
		gconf_client_get_bool (gconf, CONFIG_TODO_SHOW_DUEDATE,    NULL);
	enabled_columns[COLUMN_PRIORITY] =
		gconf_client_get_bool (gconf, CONFIG_TODO_SHOW_PRIORITY,   NULL);
	enabled_columns[COLUMN_TIMELEFT] =
		gconf_client_get_bool (gconf, CONFIG_TODO_SHOW_TIMELEFT,   NULL);
	enabled_columns[COLUMN_CATEGORIES] =
		gconf_client_get_bool (gconf, CONFIG_TODO_SHOW_CATEGORIES, NULL);

	/* Configuration notifications */

	priv->notify1 = gconf_client_notify_add (gconf, CONFIG_COLOR_NOT_DUE,
						 todo_list_color_config_changed, box,
						 NULL, NULL);
	priv->notify2 = gconf_client_notify_add (gconf, CONFIG_COLOR_DUE_TODAY,
						 todo_list_color_config_changed, box,
						 NULL, NULL);
	priv->notify3 = gconf_client_notify_add (gconf, CONFIG_COLOR_OVERDUE,
						 todo_list_color_config_changed, box,
						 NULL, NULL);

	priv->notify4 = gconf_client_notify_add (gconf, CONFIG_TODO_DIR,
						 todo_list_column_config_changed, box,
						 NULL, NULL);

	g_object_unref (G_OBJECT (gconf));

	/* Widget stuff */

	widget = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (widget),
					     GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (widget),
					GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_widget_show (widget);
	gtk_box_pack_start (GTK_BOX (box), widget, TRUE, TRUE, 0);

	model = setup_tree_model (box);
	box->priv->tree = gtk_tree_view_new_with_model (model);
	g_object_unref (G_OBJECT (model));

	g_signal_connect (G_OBJECT (box->priv->tree), "row-activated",
			  G_CALLBACK (todo_list_row_activated), box);
	gtk_widget_show (box->priv->tree);
	gtk_container_add (GTK_CONTAINER (widget), box->priv->tree);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (box->priv->tree));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (selection), "changed",
			  G_CALLBACK (todo_list_selection_changed), box);

	setup_tree_columns (box);
	todo_list_enable_tree_columns (box, enabled_columns);

	buttonbox = gtk_hbox_new (TRUE, 0);
	gtk_widget_show (buttonbox);
	gtk_box_pack_start (GTK_BOX (box), buttonbox, FALSE, FALSE, 0);

	widget = gtk_button_new_from_stock (GTK_STOCK_NEW);
	g_signal_connect (G_OBJECT (widget), "clicked",
			  G_CALLBACK (add_clicked_cb), box);
	gtk_widget_show (widget);
	gtk_box_pack_start (GTK_BOX (buttonbox), widget, FALSE, TRUE, 0);

	box->priv->edit = gtk_button_new_from_stock (GTK_STOCK_PROPERTIES);
	g_signal_connect (G_OBJECT (box->priv->edit), "clicked",
			  G_CALLBACK (prop_clicked_cb), box);
	gtk_widget_set_sensitive (box->priv->edit, FALSE);
	gtk_widget_show (box->priv->edit);
	gtk_box_pack_start (GTK_BOX (buttonbox), box->priv->edit,
			    FALSE, TRUE, 0);

	box->priv->delete = gtk_button_new_from_stock (GTK_STOCK_DELETE);
	g_signal_connect (G_OBJECT (box->priv->delete), "clicked",
			  G_CALLBACK (delete_clicked_cb), box);
	gtk_widget_set_sensitive (box->priv->delete, FALSE);
	gtk_widget_show (box->priv->delete);
	gtk_box_pack_start (GTK_BOX (buttonbox), box->priv->delete,
			    FALSE, TRUE, 0);
}

/*
 * Public Methods
 */

GtkWidget *
todo_list_new (void)
{
	TodoList *list;

	list = g_object_new (TODO_TYPE_LIST, NULL);

	return GTK_WIDGET (list);
}

void
todo_list_set_calendar (TodoList *todo, CalendarManager *cal)
{
	TodoListPriv *priv;
	GList *list, *item;

	g_return_if_fail (todo != NULL);
	g_return_if_fail (TODO_IS_LIST (todo));
	g_return_if_fail (cal != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (cal));

	priv = todo->priv;

	g_return_if_fail (priv->calendar == NULL);

	priv->calendar = cal;
	g_object_ref (G_OBJECT (cal));

	/* Load todo items */

	list = calendar_manager_get_todo_items (priv->calendar);
	
	for (item = list; item != NULL; item = g_list_next (item))
		append_item (todo, MIMEDIR_VTODO (item->data));

	g_list_free (list);

	/* Setup signals */

	priv->todo_add_sig = g_signal_connect_swapped (G_OBJECT (cal), "todo-added",
		G_CALLBACK (todo_added_cb), todo);
	priv->todo_remove_sig = g_signal_connect_swapped (G_OBJECT (cal), "todo-removed",
		G_CALLBACK (todo_removed_cb), todo);

}
