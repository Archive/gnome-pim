/* GNOME Calendar Preferences Dialog Object
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* FIXME: warn if a config key is set to a stupid value; create helper
 * functions that manage all this setting/checking stuff.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib.h>

#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include <gnome.h>

#include <libgnomevfs/gnome-vfs.h>

#include "calendar-month-item.h"
#include "gnomecal-config.h"
#include "gnomecal-prefs.h"
#include "utils-time.h"
#include "utils-ui.h"


static void gncal_prefs_class_init	(GncalPrefsClass *dialog);
static void gncal_prefs_init		(GncalPrefs *dialog);
static void gncal_prefs_dispose		(GObject *object);
static void gncal_prefs_finalize	(GObject *object);

static void gncal_prefs_setup		(GncalPrefs *dialog);


struct color_property {
	gchar *color;
        gchar *label;    /* Label for properties dialog */
        gchar *key;      /* Key for gconf */
};

static const struct color_property
color_properties[] = {
	{ DEFAULT_COLOR_OUTLINE,	N_("Outline:"),				CONFIG_COLOR_OUTLINE		},
	{ DEFAULT_COLOR_HEADINGS,	N_("Headings:"),			CONFIG_COLOR_HEADINGS		},
	{ DEFAULT_COLOR_EMPTY_DAYS,	N_("Empty days:"),			CONFIG_COLOR_EMPTY_DAYS		},
	{ DEFAULT_COLOR_APPOINTMENTS,	N_("Appointments:"),			CONFIG_COLOR_APPOINTMENTS	},
	{ DEFAULT_COLOR_DAY_HIGHLIGHT,	N_("Highlighted day:"),			CONFIG_COLOR_DAY_HIGHLIGHT	},
	{ DEFAULT_COLOR_DAY_NUMBERS,	N_("Day numbers:"),			CONFIG_COLOR_DAY_NUMBERS	},
	{ DEFAULT_COLOR_CURRENT_DAY,	N_("Current day's number:"),		CONFIG_COLOR_CURRENT_DAY	},
	{ DEFAULT_COLOR_EVENT_DAYS,	N_("Event day's number:"),		CONFIG_COLOR_EVENT_DAYS		},
	{ DEFAULT_COLOR_NOT_DUE,	N_("To-Do item that is not yet due:"),	CONFIG_COLOR_NOT_DUE		},
	{ DEFAULT_COLOR_DUE_TODAY,	N_("To-Do item that is due today:"),	CONFIG_COLOR_DUE_TODAY		},
	{ DEFAULT_COLOR_OVERDUE,	N_("To-Do item that is overdue:"),	CONFIG_COLOR_OVERDUE		}
};


struct _GncalPrefsPriv {
	GtkWidget *midnight; /* roll-over at midnight? */
	GtkWidget *day_start, *day_end;

	GtkWidget *duedate;
	GtkWidget *timeleft;
	GtkWidget *priority;
	GtkWidget *categories;

	GtkWidget *colors[G_N_ELEMENTS (color_properties)];

	GtkWidget *beep;
	GtkWidget *timeout_check;
	GtkWidget *timeout_spin;
	GtkWidget *snooze_check;
	GtkWidget *snooze_spin;

	GtkWidget *display_check;
	GtkWidget *display_spin;
	GtkWidget *display_cb;
	GtkWidget *audio_check;
	GtkWidget *audio_spin;
	GtkWidget *audio_cb;
	GtkWidget *program_check;
	GtkWidget *program_spin;
	GtkWidget *program_cb;
	GtkWidget *program_entry;
	GtkWidget *program_button;
	GtkWidget *mail_check;
	GtkWidget *mail_spin;
	GtkWidget *mail_cb;
	GtkWidget *mail_entry;

	GnomeCanvasItem *month_item;

	/* Signal IDs */

	gulong display_signal_spin;
	gulong display_signal_cb;
	gulong audio_signal_spin;
	gulong audio_signal_cb;
	gulong program_signal_spin;
	gulong program_signal_cb;
	gulong mail_signal_spin;
	gulong mail_signal_cb;

	guint notify2;
	guint notify3;
	guint notify4;
	guint notify5;
};


static GtkDialogClass *parent_class = NULL;

/*
 * Class and object management
 */

GType
gncal_prefs_get_type (void)
{
	static GType gncal_prefs_type = 0;

	if (!gncal_prefs_type) {
		static const GTypeInfo gncal_prefs_info = {
			sizeof (GncalPrefsClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) gncal_prefs_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (GncalPrefs),
			1,    /* n_preallocs */
			(GInstanceInitFunc) gncal_prefs_init,
		};

		gncal_prefs_type = g_type_register_static (GTK_TYPE_DIALOG,
							   "GncalPrefs",
							   &gncal_prefs_info,
							   0);
	}

	return gncal_prefs_type;
}

static void
gncal_prefs_class_init (GncalPrefsClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (GNCAL_IS_PREFS_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose  = gncal_prefs_dispose;
	gobject_class->finalize = gncal_prefs_finalize;

	parent_class = g_type_class_peek_parent (klass);
}

static void
gncal_prefs_init (GncalPrefs *dialog)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (dialog));

	dialog->priv = g_new0 (GncalPrefsPriv, 1);

	gncal_prefs_setup (dialog);
}

static void
gncal_prefs_dispose (GObject *object)
{
	GncalPrefsPriv *priv;
	GConfClient *gconf;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (object));

	priv = GNCAL_PREFS (object)->priv;

	gconf = gconf_client_get_default ();

	if (priv->notify2) {
		gconf_client_notify_remove (gconf, priv->notify2);
		priv->notify2 = 0;
	}
	if (priv->notify3) {
		gconf_client_notify_remove (gconf, priv->notify3);
		priv->notify3 = 0;
	}
	if (priv->notify4) {
		gconf_client_notify_remove (gconf, priv->notify4);
		priv->notify4 = 0;
	}
	if (priv->notify5) {
		gconf_client_notify_remove (gconf, priv->notify5);
		priv->notify5 = 0;
	}

	g_object_unref (G_OBJECT (gconf));

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gncal_prefs_finalize (GObject *object)
{
	GncalPrefs *dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (object));

	dialog = GNCAL_PREFS (object);

	g_free (dialog->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*
 * Utility Methods
 */

/* FIXME: improve */
static gboolean
can_user_execute (GnomeVFSFilePermissions permissions)
{
	return permissions & GNOME_VFS_PERM_USER_EXEC;
}

static gboolean
is_file_executable_cb (const GtkFileFilterInfo *info, gpointer data)
{
	GnomeVFSFileInfo vfs_info;
	GnomeVFSResult res;

	g_return_val_if_fail (info->contains | GTK_FILE_FILTER_URI, TRUE);

	res = gnome_vfs_get_file_info (info->uri,
				       &vfs_info,
				       GNOME_VFS_FILE_INFO_FOLLOW_LINKS);
	g_return_val_if_fail (res == GNOME_VFS_OK, FALSE);

	return can_user_execute (vfs_info.permissions);
}

static void
set_file_chooser_filter_executables (GtkFileChooser *chooser)
{
	GtkFileFilter *filter;

	filter = gtk_file_filter_new ();
	gtk_file_filter_add_custom (filter,
				    GTK_FILE_FILTER_URI,
				    is_file_executable_cb,
				    NULL, NULL);
	gtk_file_filter_set_name (filter, _("Executable files"));
	gtk_file_chooser_set_filter (chooser, filter);
	/* FIXME: why does it crash?   g_object_unref (G_OBJECT (filter)); */
}

static gchar *
gncal_prefs_get_color_picker_string (GdkColor *color)
{
	return g_strdup_printf ("#%04x%04x%04x", color->red, color->green, color->blue);
}

static void
gncal_prefs_warn_invalid_time (const gchar *key, gint time)
{
	g_printerr (_("Configuration error: "
		      "The configuration key %s has the invalid value %d! "
		      "Values must be in the range 0 to 144000. "
		      "The key is reset to a sane value."),
		    key, time);
}

static void
gncal_prefs_warn_invalid_hour (const gchar *key, gint hour)
{
	g_printerr (_("Configuration error: "
		      "The configuration key %s has the invalid value %d! "
		      "Values must be day hours, i.e. in the range 0 to 23. "
		      "The key is reset to a sane default value."),
		    key, hour);
}

static void
gncal_prefs_set_time_widgets (GtkWidget *spin,
			      GtkWidget *cb,
			      gulong spin_signal,
			      gulong cb_signal,
			      gint value)
{
	gint v, m;

	if (value == 0) {
		v = 0;
		m = 0;
	} else if (value % 1440 == 0) {
		v = value / 1440;
		m = 2;
	} else if (value % 60 == 0) {
		v = value / 60;
		m = 1;
	} else {
		v = value;
		m = 0;
	}

	g_signal_handler_block (G_OBJECT (spin), spin_signal);
	g_signal_handler_block (G_OBJECT (cb),   cb_signal);

	gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), (gdouble) v);
	gtk_combo_box_set_active (GTK_COMBO_BOX (cb), m);

	g_signal_handler_unblock (G_OBJECT (spin), spin_signal);
	g_signal_handler_unblock (G_OBJECT (cb),   cb_signal);
}

static void
gncal_prefs_set_time_from_config (const gchar *key,
				  GtkWidget *spin,
				  GtkWidget *cb,
				  gulong spin_signal,
				  gulong cb_signal,
				  gint value)
{
	if (value < DEFAULT_ALARM_TIME_MIN || value > DEFAULT_ALARM_TIME_MAX) {
		GConfClient *gconf;

		gncal_prefs_warn_invalid_time (key, value);

		if (value > DEFAULT_ALARM_TIME_MAX)
			value = DEFAULT_ALARM_TIME_MAX;
		else
			value = DEFAULT_ALARM_TIME_MIN;

		gconf = gconf_client_get_default ();
		gconf_client_set_int (gconf, key, value, NULL);
		g_object_unref (G_OBJECT (gconf));
	} else
		gncal_prefs_set_time_widgets (spin, cb, spin_signal, cb_signal, value);
}

static void
gncal_prefs_gconf_set_toggle_button (GtkToggleButton *button,
				     GConfValue *value)
{
	gboolean b;

	if (value->type != GCONF_VALUE_BOOL)
		return;

	b = gconf_value_get_bool (value);

	if (gtk_toggle_button_get_active (button) == b)
		return;

	gtk_toggle_button_set_active (button, b);
}

static void
gncal_prefs_gconf_set_entry (GtkEntry *entry, GConfValue *value)
{
	const gchar *string;
	const gchar *old_string;

	if (value->type != GCONF_VALUE_STRING)
		return;

	string = gconf_value_get_string (value);
	old_string = gtk_entry_get_text (entry);

	if (!strcmp (old_string, string))
		return;

	gtk_entry_set_text (entry, string);
}

static void
gncal_prefs_gconf_set_color_picker (GtkColorButton *color_picker,
				    GConfValue *value)
{
	const gchar *string;
	gchar *old_string;
	GdkColor color;

	if (value->type != GCONF_VALUE_STRING)
		return;

	string = gconf_value_get_string (value);

	gtk_color_button_get_color (color_picker, &color);
	old_string = gncal_prefs_get_color_picker_string (&color);

	if (strcmp (string, old_string) != 0) {
		PangoColor pango_color;
		if (pango_color_parse (&pango_color, string)) {
			color.red   = pango_color.red;
			color.green = pango_color.green;
			color.blue  = pango_color.blue;
			gtk_color_button_set_color (color_picker, &color);
		} else {
			color.red = color.green = color.blue = 0;
			gtk_color_button_set_color (color_picker, &color);
		}
	}

	g_free (old_string);
}

/*
 * Callbacks
 */

static void
gncal_prefs_response (GtkDialog *dialog, gint response, gpointer data)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (dialog));

	switch (response) {
	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;
	case GTK_RESPONSE_DELETE_EVENT:
		break;
	case GTK_RESPONSE_HELP:
		ui_show_help (GTK_WINDOW (dialog), "prefs");
		break;
	default:
		g_return_if_reached ();
	}
}

static void
gncal_prefs_color_set (GtkColorButton *picker, gpointer data)
{
	GConfClient *gconf;
	GdkColor color;
	const gchar *key;
	gchar *value;

	g_return_if_fail (data != NULL);

	key = (const gchar *) data;

	gtk_color_button_get_color (picker, &color);
	value = gncal_prefs_get_color_picker_string (&color);

	gconf = gconf_client_get_default ();
	gconf_client_set_string (gconf, key, value, NULL);
	g_object_unref (G_OBJECT (gconf));

	g_free (value);
}

static void
gncal_prefs_check_button_toggled (GtkWidget *widget, const gchar *key)
{
	GConfClient *gconf;
	gboolean b;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_TOGGLE_BUTTON (widget));
	g_return_if_fail (key != NULL);

	gconf = gconf_client_get_default ();

	b = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget));

	gconf_client_set_bool (gconf, key, b, NULL);

	g_object_unref (G_OBJECT (gconf));
}

static void
gncal_prefs_entry_changed (GtkWidget *widget, const gchar *key)
{
	GConfClient *gconf;
	const gchar *text;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_ENTRY (widget));
	g_return_if_fail (key != NULL);

	text = gtk_entry_get_text (GTK_ENTRY (widget));

	gconf = gconf_client_get_default ();
	gconf_client_set_string (gconf, key, text, NULL);
	g_object_unref (G_OBJECT (gconf));
}

static void
gncal_prefs_timeout_changed (GtkWidget *widget, GncalPrefs *dialog)
{
	GncalPrefsPriv *priv;
	GConfClient *gconf;
	gboolean b;
	gdouble  d;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (dialog));

	priv = dialog->priv;

	b = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (priv->timeout_check));
	d = gtk_spin_button_get_value (GTK_SPIN_BUTTON (priv->timeout_spin));

	if (!b)
		d = 0.0;

	gconf = gconf_client_get_default ();

	gconf_client_set_int (gconf, CONFIG_ALARM_TIMEOUT, (gint) d, NULL);

	g_object_unref (G_OBJECT (gconf));

	gtk_widget_set_sensitive (GTK_WIDGET (priv->timeout_spin), b);
}

static void
gncal_prefs_snooze_changed (GtkWidget *widget, GncalPrefs *dialog)
{
	GncalPrefsPriv *priv;
	GConfClient *gconf;
	gboolean b;
	gdouble  d;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (dialog));

	priv = dialog->priv;

	b = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (priv->snooze_check));
	d = gtk_spin_button_get_value (GTK_SPIN_BUTTON (priv->snooze_spin));

	if (!b)
		d = 0.0;

	gconf = gconf_client_get_default ();

	gconf_client_set_int (gconf, CONFIG_ALARM_SNOOZE, (gint) d, NULL);

	g_object_unref (G_OBJECT (gconf));

	gtk_widget_set_sensitive (GTK_WIDGET (priv->snooze_spin), b);
}

static void
gncal_prefs_hour_widget_changed (GtkWidget *widget, const gchar *key)
{
	GConfClient *gconf;
	gint v;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_COMBO_BOX (widget));
	g_return_if_fail (key != NULL);

	gconf = gconf_client_get_default ();

	v = gtk_combo_box_get_active (GTK_COMBO_BOX (widget));

	gconf_client_set_int (gconf, key, v, NULL);

	g_object_unref (G_OBJECT (gconf));
}

static void
gncal_prefs_time_changed (GtkWidget *spin,
			  GtkWidget *cb,
			  gulong spin_signal,
			  gulong cb_signal,
			  const gchar *key)
{
	GConfClient *gconf;

	gint v;

	v = (gint) gtk_spin_button_get_value (GTK_SPIN_BUTTON (spin));

	switch (gtk_combo_box_get_active (GTK_COMBO_BOX (cb))) {
	case 0:
		break;
	case 1:
		v *= 60;
		break;
	case 2:
		v *= 1440;
		break;
	default:
		g_return_if_reached ();
	}

	if (v > DEFAULT_ALARM_TIME_MAX) {
		v = DEFAULT_ALARM_TIME_MAX;
		gncal_prefs_set_time_widgets (spin, cb, spin_signal, cb_signal, v);
	}

	gconf = gconf_client_get_default ();
	gconf_client_set_int (gconf, key, v, NULL);
	g_object_unref (G_OBJECT (gconf));
}

static void
gncal_prefs_display_time_changed (GtkWidget *widget, GncalPrefs *dialog)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (dialog));

	gncal_prefs_time_changed (dialog->priv->display_spin,
				  dialog->priv->display_cb,
				  dialog->priv->display_signal_spin,
				  dialog->priv->display_signal_cb,
				  CONFIG_ALARM_DISPLAY_TIME);
}

static void
gncal_prefs_audio_time_changed (GtkWidget *widget, GncalPrefs *dialog)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (dialog));

	gncal_prefs_time_changed (dialog->priv->audio_spin,
				  dialog->priv->audio_cb,
				  dialog->priv->audio_signal_spin,
				  dialog->priv->audio_signal_cb,
				  CONFIG_ALARM_AUDIO_TIME);
}

static void
gncal_prefs_program_time_changed (GtkWidget *widget, GncalPrefs *dialog)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (dialog));

	gncal_prefs_time_changed (dialog->priv->program_spin,
				  dialog->priv->program_cb,
				  dialog->priv->program_signal_spin,
				  dialog->priv->program_signal_cb,
				  CONFIG_ALARM_PROGRAM_TIME);
}

static void
gncal_prefs_mail_time_changed (GtkWidget *widget, GncalPrefs *dialog)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (dialog));

	gncal_prefs_time_changed (dialog->priv->mail_spin,
				  dialog->priv->mail_cb,
				  dialog->priv->mail_signal_spin,
				  dialog->priv->mail_signal_cb,
				  CONFIG_ALARM_MAIL_TIME);
}

static void
gncal_prefs_command_chosen (GtkDialog *selector,
			    gint response,
			    GncalPrefs *dialog)
{
	g_return_if_fail (selector != NULL);
	g_return_if_fail (GTK_IS_FILE_CHOOSER_DIALOG (selector));
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (dialog));

	if (response == GTK_RESPONSE_ACCEPT) {
		const gchar *filename;
		gchar *file_utf8;

		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (selector));

		file_utf8 = g_filename_to_utf8 (filename, -1, NULL, NULL, NULL);
		gtk_entry_set_text (GTK_ENTRY (dialog->priv->program_entry), file_utf8);
		g_free (file_utf8);
		g_free (filename);
	}

	gtk_widget_destroy (GTK_WIDGET (selector));
	gtk_widget_set_sensitive (dialog->priv->program_entry, TRUE);
	gtk_widget_set_sensitive (dialog->priv->program_button, TRUE);
}

static void
gncal_prefs_browse_for_command (GtkWidget *widget, GncalPrefs *dialog)
{
	GtkWidget *selector;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (dialog));

	gtk_widget_set_sensitive (dialog->priv->program_entry, FALSE);
	gtk_widget_set_sensitive (dialog->priv->program_button, FALSE);

	selector = gtk_file_chooser_dialog_new (_("Choose command"),
						GTK_WINDOW (dialog),
						GTK_FILE_CHOOSER_ACTION_OPEN,
						GTK_STOCK_OPEN,   GTK_RESPONSE_ACCEPT,
						GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
						NULL);
	gtk_window_set_destroy_with_parent (GTK_WINDOW (selector), TRUE);
	gtk_file_chooser_add_shortcut_folder (GTK_FILE_CHOOSER (selector), "/bin", NULL);
	gtk_file_chooser_add_shortcut_folder (GTK_FILE_CHOOSER (selector), "/usr/bin", NULL);

	set_file_chooser_filter_executables (GTK_FILE_CHOOSER (selector));

	g_signal_connect (G_OBJECT (selector), "response",
			  G_CALLBACK (gncal_prefs_command_chosen), dialog);

	gtk_widget_show (selector);
}

/* Example calendar canvas size allocation. */
static void
gncal_prefs_canvas_size_alloc (GtkWidget *widget,
			       GtkAllocation *allocation,
			       gpointer data)
{
	GncalPrefs *prefs;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GNOME_IS_CANVAS (widget));
	g_return_if_fail (data != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (data));

	prefs = GNCAL_PREFS (data);

	gnome_canvas_item_set (prefs->priv->month_item,
			       "width",  (double) (allocation->width  - 1),
			       "height", (double) (allocation->height - 1),
			       NULL);

	gnome_canvas_set_scroll_region (GNOME_CANVAS (widget),
					0, 0,
					allocation->width, allocation->height);
}

/* This method compares the history of both widgets and ensures that
 * widget1's history is always <= widget2's history. If reset1 is set,
 * widget1 will be changed, otherwise widget2.
 */
static void
gncal_prefs_ensure_hour_order (GtkWidget *widget1, GtkWidget *widget2,
			       gboolean reset1)
{
	gint history1, history2;

	g_return_if_fail (widget1 != NULL);
	g_return_if_fail (widget2 != NULL);
	g_return_if_fail (GTK_IS_COMBO_BOX (widget1));
	g_return_if_fail (GTK_IS_COMBO_BOX (widget2));

	history1 = gtk_combo_box_get_active (GTK_COMBO_BOX (widget1));
	history2 = gtk_combo_box_get_active (GTK_COMBO_BOX (widget2));

	if (history1 > history2) {
		if (reset1)
			gtk_combo_box_set_active (GTK_COMBO_BOX (widget1),
						  history2);
		else
			gtk_combo_box_set_active (GTK_COMBO_BOX (widget2),
						  history1);
	}
}

static void
gncal_prefs_ensure_hour_order1 (GtkWidget *widget1, GtkWidget *widget2)
{
	gncal_prefs_ensure_hour_order (widget1, widget2, FALSE);
}

static void
gncal_prefs_ensure_hour_order2 (GtkWidget *widget1, GtkWidget *widget2)
{
	gncal_prefs_ensure_hour_order (widget1, widget2, TRUE);
}

static void
gncal_prefs_calendar_config_changed (GConfClient *client,
				     guint cnxn_id,
				     GConfEntry *entry,
				     gpointer data)
{
	GncalPrefsPriv *priv;
	GConfValue *value;
	const gchar *key;

	g_return_if_fail (entry != NULL);
	g_return_if_fail (data != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (data));

	priv = GNCAL_PREFS (data)->priv;

	key   = gconf_entry_get_key (entry);
	value = gconf_entry_get_value (entry);

	if (!strcmp (key, CONFIG_CAL_DAY_START)) {
		if (value->type == GCONF_VALUE_INT) {
			gint begin, old;

			begin = gconf_value_get_int (value);
			old = gtk_combo_box_get_active (GTK_COMBO_BOX (priv->day_start));

			if (begin != old) {
				gint end;
				if (begin < 0)
					begin = 0;
				else if (begin > 23)
					begin = 23;
				gtk_combo_box_set_active (GTK_COMBO_BOX (priv->day_start), begin);
				end = gtk_combo_box_get_active (GTK_COMBO_BOX (priv->day_end));
				if (begin > end)
					gtk_combo_box_set_active (GTK_COMBO_BOX (priv->day_end), begin);
			}
		}
	} else if (!strcmp (key, CONFIG_CAL_DAY_END)) {
		if (value->type == GCONF_VALUE_INT) {
			gint end, old;

			end = gconf_value_get_int (value);
			old = gtk_combo_box_get_active (GTK_COMBO_BOX (priv->day_end));

			if (end != old) {
				gint begin;
				if (end < 0)
					end = 0;
				else if (end > 23)
					end = 23;
				gtk_combo_box_set_active (GTK_COMBO_BOX (priv->day_end), end);
				begin = gtk_combo_box_get_active (GTK_COMBO_BOX (priv->day_start));
				if (begin > end)
					gtk_combo_box_set_active (GTK_COMBO_BOX (priv->day_start), end);
			}
		}
	} else if (!strcmp (key, CONFIG_CAL_ROLLOVER))
		gncal_prefs_gconf_set_toggle_button (GTK_TOGGLE_BUTTON (priv->midnight), value);
}

static void
gncal_prefs_todo_config_changed (GConfClient *client,
				 guint cnxn_id,
				 GConfEntry *entry,
				 gpointer data)
{
	GncalPrefsPriv *priv;
	GConfValue *value;
	const gchar *key;

	g_return_if_fail (entry != NULL);
	g_return_if_fail (data != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (data));

	priv = GNCAL_PREFS (data)->priv;

	key   = gconf_entry_get_key (entry);
	value = gconf_entry_get_value (entry);

	if (!strcmp (key, CONFIG_TODO_SHOW_DUEDATE))
		gncal_prefs_gconf_set_toggle_button (GTK_TOGGLE_BUTTON (priv->duedate), value);
	else if (!strcmp (key, CONFIG_TODO_SHOW_PRIORITY))
		gncal_prefs_gconf_set_toggle_button (GTK_TOGGLE_BUTTON (priv->priority), value);
	else if (!strcmp (key, CONFIG_TODO_SHOW_TIMELEFT))
		gncal_prefs_gconf_set_toggle_button (GTK_TOGGLE_BUTTON (priv->timeleft), value);
	else if (!strcmp (key, CONFIG_TODO_SHOW_CATEGORIES))
		gncal_prefs_gconf_set_toggle_button (GTK_TOGGLE_BUTTON (priv->categories), value);
}

static void
gncal_prefs_colors_config_changed (GConfClient *client,
				   guint cnxn_id,
				   GConfEntry *entry,
				   gpointer data)
{
	GncalPrefsPriv *priv;
	GConfValue *value;
	const gchar *key;

	g_return_if_fail (entry != NULL);
	g_return_if_fail (data != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (data));

	priv = GNCAL_PREFS (data)->priv;

	key   = gconf_entry_get_key (entry);
	value = gconf_entry_get_value (entry);

	if (!strcmp (key, CONFIG_COLOR_OUTLINE))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[0]), value);
	else if (!strcmp (key, CONFIG_COLOR_HEADINGS))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[1]), value);
	else if (!strcmp (key, CONFIG_COLOR_EMPTY_DAYS))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[2]), value);
	else if (!strcmp (key, CONFIG_COLOR_APPOINTMENTS))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[3]), value);
	else if (!strcmp (key, CONFIG_COLOR_DAY_HIGHLIGHT))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[4]), value);
	else if (!strcmp (key, CONFIG_COLOR_DAY_NUMBERS))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[5]), value);
	else if (!strcmp (key, CONFIG_COLOR_CURRENT_DAY))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[6]), value);
	else if (!strcmp (key, CONFIG_COLOR_EVENT_DAYS))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[7]), value);
	else if (!strcmp (key, CONFIG_COLOR_NOT_DUE))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[8]), value);
	else if (!strcmp (key, CONFIG_COLOR_DUE_TODAY))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[9]), value);
	else if (!strcmp (key, CONFIG_COLOR_OVERDUE))
		gncal_prefs_gconf_set_color_picker (GTK_COLOR_BUTTON (priv->colors[10]), value);
}

static void
gncal_prefs_alarm_config_changed (GConfClient *client,
				  guint cnxn_id,
				  GConfEntry *entry,
				  gpointer data)
{
	GncalPrefsPriv *priv;
	GConfValue *value;
	const gchar *key;

	g_return_if_fail (entry != NULL);
	g_return_if_fail (data != NULL);
	g_return_if_fail (GNCAL_IS_PREFS (data));

	priv = GNCAL_PREFS (data)->priv;

	key   = gconf_entry_get_key (entry);
	value = gconf_entry_get_value (entry);

	if (!strcmp (key, CONFIG_ALARM_BEEP))
		gncal_prefs_gconf_set_toggle_button (GTK_TOGGLE_BUTTON (priv->beep), value);
	else if (!strcmp (key, CONFIG_ALARM_TIMEOUT)) {
		if (value->type == GCONF_VALUE_INT) {
			gint i = gconf_value_get_int (value);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->timeout_check), i);
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (priv->timeout_spin), i != 0 ? (double) i : DEFAULT_ALARM_TIMEOUT_VALUE);
		}
	} else if (!strcmp (key, CONFIG_ALARM_SNOOZE)) {
		if (value->type == GCONF_VALUE_INT) {
			gint i = gconf_value_get_int (value);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->snooze_check), i);
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (priv->snooze_spin), i != 0 ? (double) i : DEFAULT_ALARM_SNOOZE_VALUE);
		}
	} else if (!strcmp (key, CONFIG_ALARM_DISPLAY_ENABLED))
		gncal_prefs_gconf_set_toggle_button (GTK_TOGGLE_BUTTON (priv->display_check), value);
	else if (!strcmp (key, CONFIG_ALARM_DISPLAY_TIME)) {
		if (value->type == GCONF_VALUE_INT) {
			gint i = gconf_value_get_int (value);
			gncal_prefs_set_time_from_config (CONFIG_ALARM_DISPLAY_TIME,
							  priv->display_spin,
							  priv->display_cb,
							  priv->display_signal_spin,
							  priv->display_signal_cb,
							  i);
		}
	} else if (!strcmp (key, CONFIG_ALARM_AUDIO_ENABLED))
		gncal_prefs_gconf_set_toggle_button (GTK_TOGGLE_BUTTON (priv->audio_check), value);
	else if (!strcmp (key, CONFIG_ALARM_AUDIO_TIME)) {
		if (value->type == GCONF_VALUE_INT) {
			gint i = gconf_value_get_int (value);
			gncal_prefs_set_time_from_config (CONFIG_ALARM_AUDIO_TIME,
							  priv->audio_spin,
							  priv->audio_cb,
							  priv->audio_signal_spin,
							  priv->audio_signal_cb,
							  i);
		}
	} else if (!strcmp (key, CONFIG_ALARM_PROGRAM_ENABLED))
		gncal_prefs_gconf_set_toggle_button (GTK_TOGGLE_BUTTON (priv->program_check), value);
	else if (!strcmp (key, CONFIG_ALARM_PROGRAM_TIME)) {
		if (value->type == GCONF_VALUE_INT) {
			gint i = gconf_value_get_int (value);
			gncal_prefs_set_time_from_config (CONFIG_ALARM_PROGRAM_TIME,
							  priv->program_spin,
							  priv->program_cb,
							  priv->program_signal_spin,
							  priv->program_signal_cb,
							  i);
		}
	} else if (!strcmp (key, CONFIG_ALARM_PROGRAM_COMMAND))
		gncal_prefs_gconf_set_entry (GTK_ENTRY (priv->program_entry), value);
	else if (!strcmp (key, CONFIG_ALARM_MAIL_ENABLED))
		gncal_prefs_gconf_set_toggle_button (GTK_TOGGLE_BUTTON (priv->mail_check), value);
	else if (!strcmp (key, CONFIG_ALARM_MAIL_TIME)) {
		if (value->type == GCONF_VALUE_INT) {
			gint i = gconf_value_get_int (value);
			gncal_prefs_set_time_from_config (CONFIG_ALARM_MAIL_TIME,
							  priv->mail_spin,
							  priv->mail_cb,
							  priv->mail_signal_spin,
							  priv->mail_signal_cb,
							  i);
		}
	} else if (!strcmp (key, CONFIG_ALARM_MAIL_RECEIVER))
		gncal_prefs_gconf_set_entry (GTK_ENTRY (priv->mail_entry), value);
}

/*
 * Helpers
 */

static GtkWidget *
gncal_prefs_create_hour_widget (guint minute)
{
	GtkWidget *widget;

	int i;

	widget = gtk_combo_box_new_text ();

	for (i = 0; i < 24; i++) {
		gchar *s;

		s = time_get_short_time_string (i, minute);
		gtk_combo_box_append_text(GTK_COMBO_BOX (widget), s);
		g_free (s);
	}

	return widget;
}

/*
 * Private Methods
 */

static GtkWidget *
gncal_prefs_setup_time_page (GncalPrefs *dialog)
{
	GncalPrefsPriv *priv;
        PangoAttrList *attrs;
        PangoAttribute *attr;
	GtkWidget *page;
	GtkWidget *label;
	GtkWidget *hbox, *groupbox, *indentbox;
	GtkWidget *widget;

	priv = dialog->priv;

	page = gtk_vbox_new (FALSE, 12);

	attrs = pango_attr_list_new ();
	attr = pango_attr_weight_new (PANGO_WEIGHT_BOLD);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);

	/* Day range */

	widget = gtk_label_new (_("Day Range"));
	gtk_label_set_attributes (GTK_LABEL (widget), attrs);
	gtk_misc_set_alignment (GTK_MISC (widget), 0.0, 0.5);
	gtk_widget_show (widget);
	gtk_box_pack_start (GTK_BOX (page), widget, FALSE, FALSE, 0);

	indentbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (indentbox);
	gtk_box_pack_start (GTK_BOX (page), indentbox, FALSE, FALSE, 0);

	widget = gtk_label_new (NULL);
	gtk_widget_set_size_request (widget, 24, -1);
	gtk_box_pack_start (GTK_BOX (indentbox), widget, FALSE, FALSE, 0);
	gtk_widget_show (widget);

	groupbox = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (groupbox);
	gtk_box_pack_start (GTK_BOX (indentbox), groupbox, FALSE, FALSE, 0);

        label = gtk_label_new (_("Please select the start and end hours you want "
				 "to be displayed in the day view and week view. "
				 "Times outside this range will not be displayed "
				 "by default."));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_FILL);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (groupbox), label, FALSE, FALSE, 0);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (groupbox), hbox, FALSE, FALSE, 0);

	label = gtk_label_new_with_mnemonic (_("Day _start:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	priv->day_start = gncal_prefs_create_hour_widget (00);
	g_signal_connect (G_OBJECT (priv->day_start), "changed",
			  G_CALLBACK (gncal_prefs_hour_widget_changed),
			  CONFIG_CAL_DAY_START);
	gtk_widget_show (priv->day_start);
	gtk_box_pack_start (GTK_BOX (hbox), priv->day_start, FALSE, FALSE, 0);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->day_start);

	label = gtk_label_new_with_mnemonic (_("Day _end:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	priv->day_end = gncal_prefs_create_hour_widget (59);
	g_signal_connect (G_OBJECT (priv->day_end), "changed",
			  G_CALLBACK (gncal_prefs_hour_widget_changed),
			  CONFIG_CAL_DAY_END);
	gtk_widget_show (priv->day_end);
	gtk_box_pack_start (GTK_BOX (hbox), priv->day_end, FALSE, FALSE, 0);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->day_end);

	/* This ensures that day_start will always be <= day_end. */
	g_signal_connect (G_OBJECT (priv->day_start), "changed",
			  G_CALLBACK (gncal_prefs_ensure_hour_order1),
			  priv->day_end);
	g_signal_connect_swapped (G_OBJECT (priv->day_end), "changed",
				  G_CALLBACK (gncal_prefs_ensure_hour_order2),
				  priv->day_start);

	/* Options */

	widget = gtk_label_new (_("Options"));
	gtk_label_set_attributes (GTK_LABEL (widget), attrs);
	gtk_misc_set_alignment (GTK_MISC (widget), 0.0, 0.5);
	gtk_widget_show (widget);
	gtk_box_pack_start (GTK_BOX (page), widget, FALSE, FALSE, 0);

	indentbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (indentbox);
	gtk_box_pack_start (GTK_BOX (page), indentbox, FALSE, FALSE, 0);

	widget = gtk_label_new (NULL);
	gtk_widget_set_size_request (widget, 24, -1);
	gtk_box_pack_start (GTK_BOX (indentbox), widget, FALSE, FALSE, 0);
	gtk_widget_show (widget);

	groupbox = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (groupbox);
	gtk_box_pack_start (GTK_BOX (indentbox), groupbox, FALSE, FALSE, 0);

	priv->midnight = gtk_check_button_new_with_mnemonic (_("_Roll calendar to next day on midnight"));
	g_signal_connect (G_OBJECT (priv->midnight), "toggled",
			  G_CALLBACK (gncal_prefs_check_button_toggled),
			  CONFIG_CAL_ROLLOVER);
	gtk_widget_show (priv->midnight);
	gtk_box_pack_start (GTK_BOX (groupbox), priv->midnight, FALSE, FALSE, 0);

	/* Cleanup */

	pango_attr_list_unref (attrs);

	return page;
}

static void
gncal_prefs_setup_color (GncalPrefs	*dialog,
			 GtkTable	*table,
			 gint		 ypos)
{
	GtkWidget *label;
	GtkWidget *color;
	const gchar *text;

	text = _(color_properties[ypos].label);

	label = gtk_label_new_with_mnemonic (text);
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (table, label, 0, 1, ypos, ypos + 1, GTK_FILL | GTK_EXPAND, 0, 0, 0);

	color = gtk_color_button_new ();
	gtk_color_button_set_title (GTK_COLOR_BUTTON (color), text);
	g_signal_connect (G_OBJECT (color), "color-set",
			  G_CALLBACK (gncal_prefs_color_set),
			  color_properties[ypos].key);
	gtk_widget_show (color);
	gtk_table_attach (table, color, 1, 2, ypos, ypos + 1, 0, 0, 0, 0);
	dialog->priv->colors[ypos] = color;

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), color);
}

static GtkWidget *
gncal_prefs_setup_colors_page (GncalPrefs *dialog)
{
	GncalPrefsPriv *priv = dialog->priv;
        PangoAttrList *attrs;
        PangoAttribute *attr;
	GtkWidget *page;
	GtkWidget *box;
	GtkWidget *table;
	GtkWidget *widget;
	GtkWidget *canvas;
	gint i;

	page = gtk_hbox_new (FALSE, 18);

	attrs = pango_attr_list_new ();
	attr = pango_attr_underline_new (PANGO_UNDERLINE_SINGLE);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);

	/* Color choosers */

	table = gtk_table_new (G_N_ELEMENTS (color_properties), 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 12);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (page), table, FALSE, FALSE, 0);

	for (i = 0; i < G_N_ELEMENTS (color_properties); i++)
		gncal_prefs_setup_color (dialog, GTK_TABLE (table), i);

	/* Example */

	box = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (box);
	gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, 0);

	widget = gtk_label_new (_("Example"));
	gtk_label_set_attributes (GTK_LABEL (widget), attrs);
	gtk_misc_set_alignment (GTK_MISC (widget), 0.0, 0.5);
	gtk_widget_show (widget);
	gtk_box_pack_start (GTK_BOX (box), widget, FALSE, FALSE, 0);

	canvas = gnome_canvas_new ();
	gtk_widget_set_size_request (canvas, 300, 150);
	gtk_widget_show (canvas);
	g_signal_connect (G_OBJECT (canvas), "size_allocate",
			  G_CALLBACK (gncal_prefs_canvas_size_alloc), dialog);
	gtk_box_pack_start (GTK_BOX (box), canvas, TRUE, TRUE, 0);

	priv->month_item =
		calendar_month_item_new (gnome_canvas_root (GNOME_CANVAS (canvas)), NULL);
	calendar_month_item_mark_day (CALENDAR_MONTH_ITEM (priv->month_item), 5);
	calendar_month_item_mark_day (CALENDAR_MONTH_ITEM (priv->month_item), 23);

	/* Cleanup */

	pango_attr_list_unref (attrs);

	return page;
}

static GtkWidget *
gncal_prefs_setup_todo_page (GncalPrefs *dialog)
{
	GncalPrefsPriv *priv;
        PangoAttrList *attrs;
        PangoAttribute *attr;
	GtkWidget *page;
	GtkWidget *indentbox, *groupbox;
	GtkWidget *widget;

	priv = dialog->priv;

	page = gtk_vbox_new (FALSE, 12);

	attrs = pango_attr_list_new ();
	attr = pango_attr_weight_new (PANGO_WEIGHT_BOLD);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);

	/* Title */

	widget = gtk_label_new (_("Displayed Attributes"));
	gtk_label_set_attributes (GTK_LABEL (widget), attrs);
	gtk_misc_set_alignment (GTK_MISC (widget), 0.0, 0.5);
	gtk_widget_show (widget);
	gtk_box_pack_start (GTK_BOX (page), widget, FALSE, FALSE, 0);

	indentbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (indentbox);
	gtk_box_pack_start (GTK_BOX (page), indentbox, FALSE, TRUE, 0);

	widget = gtk_label_new (NULL);
	gtk_widget_set_size_request (widget, 24, -1);
	gtk_box_pack_start (GTK_BOX (indentbox), widget, FALSE, FALSE, 0);
	gtk_widget_show (widget);

	groupbox = gtk_vbox_new (FALSE, 6);
	gtk_widget_show (groupbox);
	gtk_box_pack_start (GTK_BOX (indentbox), groupbox, FALSE, TRUE, 0);

	/* Explanation */

	widget = gtk_label_new (_("The following attributes are displayed in the to do list:"));
	gtk_misc_set_alignment (GTK_MISC (widget), 0.0, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (widget), TRUE);
	gtk_widget_show (widget);
	gtk_box_pack_start (GTK_BOX (groupbox), widget, FALSE, FALSE, 0);

	/* Check boxes */

	widget = gtk_check_button_new_with_label (_("Summary"));
	gtk_widget_set_sensitive (widget, FALSE); /* always visible */
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
	gtk_widget_show (widget);
	gtk_box_pack_start (GTK_BOX (groupbox), widget, FALSE, FALSE, 0);

	priv->duedate = gtk_check_button_new_with_mnemonic (_("_Due date"));
	g_signal_connect (G_OBJECT (priv->duedate), "toggled",
			  G_CALLBACK (gncal_prefs_check_button_toggled),
			  CONFIG_TODO_SHOW_DUEDATE);
	gtk_widget_show (priv->duedate);
	gtk_box_pack_start (GTK_BOX (groupbox), priv->duedate, FALSE, FALSE, 0);

	priv->priority = gtk_check_button_new_with_mnemonic (_("_Priority"));
	g_signal_connect (G_OBJECT (priv->priority), "toggled",
			  G_CALLBACK (gncal_prefs_check_button_toggled),
			  CONFIG_TODO_SHOW_PRIORITY);
	gtk_widget_show (priv->priority);
	gtk_box_pack_start (GTK_BOX (groupbox), priv->priority, FALSE, FALSE, 0);

	priv->timeleft = gtk_check_button_new_with_mnemonic (_("Time _until due"));
	g_signal_connect (G_OBJECT (priv->timeleft), "toggled",
			  G_CALLBACK (gncal_prefs_check_button_toggled),
			  CONFIG_TODO_SHOW_TIMELEFT);
	gtk_widget_show (priv->timeleft);
	gtk_box_pack_start (GTK_BOX (groupbox), priv->timeleft, FALSE, FALSE, 0);

	priv->categories = gtk_check_button_new_with_mnemonic (_("Cate_gories"));
	g_signal_connect (G_OBJECT (priv->categories), "toggled",
			  G_CALLBACK (gncal_prefs_check_button_toggled),
			  CONFIG_TODO_SHOW_CATEGORIES);
	gtk_widget_show (priv->categories);
	gtk_box_pack_start (GTK_BOX (groupbox), priv->categories, FALSE, FALSE, 0);

	/* Cleanup */

	pango_attr_list_unref (attrs);

	return page;
}

static GtkWidget *
gncal_prefs_create_timeunit_cb (GncalPrefs *dialog)
{
	GtkWidget *cb;

	cb = gtk_combo_box_new_text ();

	gtk_combo_box_append_text (GTK_COMBO_BOX (cb), _("Minutes"));
	gtk_combo_box_append_text (GTK_COMBO_BOX (cb), _("Hours"));
	gtk_combo_box_append_text (GTK_COMBO_BOX (cb), _("Days"));

	return cb;
}

static GtkWidget *
gncal_prefs_setup_alarm_page (GncalPrefs *dialog)
{
	GncalPrefsPriv	*priv;
        PangoAttrList	*bold, *underline;
        PangoAttribute	*attr;
	GtkWidget	*widget;
	GtkWidget	*page;
	GtkWidget	*indentbox, *groupbox;
	GtkWidget	*table;
	GtkWidget	*hbox;
	GtkWidget	*label;

	priv = dialog->priv;

	page = gtk_vbox_new (FALSE, 12);

	bold = pango_attr_list_new ();
	attr = pango_attr_weight_new (PANGO_WEIGHT_BOLD);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (bold, attr);

	underline = pango_attr_list_new ();
	attr = pango_attr_underline_new (PANGO_UNDERLINE_SINGLE);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (underline, attr);

	/* Alarm properties header */

	label = gtk_label_new (_("Alarm Properties"));
	gtk_label_set_attributes (GTK_LABEL (label), bold);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (page), label, FALSE, FALSE, 0);

	indentbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (indentbox);
	gtk_box_pack_start (GTK_BOX (page), indentbox, FALSE, TRUE, 0);

	widget = gtk_label_new (NULL);
	gtk_widget_set_size_request (widget, 24, 0);
	gtk_widget_show (widget);
	gtk_box_pack_start (GTK_BOX (indentbox), widget, FALSE, FALSE, 0);

	groupbox = gtk_vbox_new (TRUE, 6);
	gtk_widget_show (groupbox);
	gtk_box_pack_start (GTK_BOX (indentbox), groupbox, FALSE, FALSE, 0);

	/* Beep on display alarms */

	priv->beep = gtk_check_button_new_with_mnemonic (_("_Beep on display alarms"));
	g_signal_connect (G_OBJECT (priv->beep), "toggled",
			  G_CALLBACK (gncal_prefs_check_button_toggled),
			  CONFIG_ALARM_BEEP);
	gtk_widget_show (priv->beep);
	gtk_box_pack_start (GTK_BOX (groupbox), priv->beep, FALSE, FALSE, 0);

	/* Audio alarm timeout */

	hbox = gtk_hbox_new (FALSE, 6);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (groupbox), hbox, FALSE, FALSE, 0);

	priv->timeout_check = gtk_check_button_new_with_mnemonic (_("Audio alarms t_imeout after"));
	g_signal_connect (G_OBJECT (priv->timeout_check), "toggled",
			  G_CALLBACK (gncal_prefs_timeout_changed), dialog);
	gtk_widget_show (priv->timeout_check);
	gtk_box_pack_start (GTK_BOX (hbox), priv->timeout_check, FALSE, FALSE, 0);

	priv->timeout_spin = gtk_spin_button_new_with_range (5, 3600, 1);
	gtk_widget_set_sensitive (priv->timeout_spin, FALSE);
	g_signal_connect (G_OBJECT (priv->timeout_spin), "value-changed",
			  G_CALLBACK (gncal_prefs_timeout_changed), dialog);
	gtk_widget_show (priv->timeout_spin);
	gtk_box_pack_start (GTK_BOX (hbox), priv->timeout_spin, FALSE, FALSE, 0);

	label = gtk_label_new (_("seconds"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	/* Snoozing */

	hbox = gtk_hbox_new (FALSE, 6);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (groupbox), hbox, FALSE, FALSE, 0);

	priv->snooze_check = gtk_check_button_new_with_mnemonic (_("Enable _snoozing for"));
	g_signal_connect (G_OBJECT (priv->snooze_check), "toggled",
			  G_CALLBACK (gncal_prefs_snooze_changed), dialog);
	gtk_widget_show (priv->snooze_check);
	gtk_box_pack_start (GTK_BOX (hbox), priv->snooze_check, FALSE, FALSE, 0);

	priv->snooze_spin = gtk_spin_button_new_with_range (5, 3600, 1);
	gtk_widget_set_sensitive (priv->snooze_spin, FALSE);
	g_signal_connect (G_OBJECT (priv->snooze_spin), "value-changed",
			  G_CALLBACK (gncal_prefs_snooze_changed), dialog);
	gtk_widget_show (priv->snooze_spin);
	gtk_box_pack_start (GTK_BOX (hbox), priv->snooze_spin, FALSE, FALSE, 0);

	label = gtk_label_new (_("seconds"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	/* Default alarm types header */

	label = gtk_label_new (_("Default Alarm Types"));
	gtk_label_set_attributes (GTK_LABEL (label), bold);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (page), label, FALSE, FALSE, 0);

	table = gtk_table_new (5, 7, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 0);
	gtk_table_set_col_spacings (GTK_TABLE (table), 12);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (page), table, FALSE, FALSE, 0);

	widget = gtk_label_new (NULL);
	gtk_widget_set_size_request (widget, 12, 0);
	gtk_widget_show (widget);
	gtk_table_attach (GTK_TABLE (table), widget,
			  0, 1, 0, 5, 0, GTK_FILL, 0, 0);

	label = gtk_label_new (_("Alarm type"));
	gtk_label_set_attributes (GTK_LABEL (label), underline);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label,
			  1, 2, 0, 1, GTK_FILL, 0, 0, 0);

	label = gtk_label_new (_("Advance warning time"));
	gtk_label_set_attributes (GTK_LABEL (label), underline);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label,
			  2, 4, 0, 1, GTK_FILL, 0, 0, 0);

	label = gtk_label_new (_("Options"));
	gtk_label_set_attributes (GTK_LABEL (label), underline);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label,
			  4, 7, 0, 1, GTK_FILL, 0, 0, 0);

	priv->display_check = gtk_check_button_new_with_mnemonic (_("_Display"));
	g_signal_connect (priv->display_check, "toggled",
			  G_CALLBACK (gncal_prefs_check_button_toggled),
			  CONFIG_ALARM_DISPLAY_ENABLED);
	gtk_widget_show (priv->display_check);
	gtk_table_attach (GTK_TABLE (table), priv->display_check,
			  1, 2, 1, 2, GTK_FILL, 0, 0, 0);
	priv->audio_check = gtk_check_button_new_with_mnemonic (_("A_udio"));
	g_signal_connect (priv->audio_check, "toggled",
			  G_CALLBACK (gncal_prefs_check_button_toggled),
			  CONFIG_ALARM_AUDIO_ENABLED);
	gtk_widget_show (priv->audio_check);
	gtk_table_attach (GTK_TABLE (table), priv->audio_check,
			  1, 2, 2, 3, GTK_FILL, 0, 0, 0);
	priv->program_check = gtk_check_button_new_with_mnemonic (_("_Program"));
	g_signal_connect (priv->program_check, "toggled",
			  G_CALLBACK (gncal_prefs_check_button_toggled),
			  CONFIG_ALARM_PROGRAM_ENABLED);
	gtk_widget_show (priv->program_check);
	gtk_table_attach (GTK_TABLE (table), priv->program_check,
			  1, 2, 3, 4, GTK_FILL, 0, 0, 0);
	priv->mail_check = gtk_check_button_new_with_mnemonic (_("_Mail"));
	g_signal_connect (priv->mail_check, "toggled",
			  G_CALLBACK (gncal_prefs_check_button_toggled),
			  CONFIG_ALARM_MAIL_ENABLED);
	gtk_widget_show (priv->mail_check);
	gtk_table_attach (GTK_TABLE (table), priv->mail_check,
			  1, 2, 4, 5, GTK_FILL, 0, 0, 0);

	priv->display_spin = gtk_spin_button_new_with_range ((gdouble) DEFAULT_ALARM_TIME_MIN, (gdouble) DEFAULT_ALARM_TIME_MAX, 1.0);
	priv->display_signal_spin =
		g_signal_connect (G_OBJECT (priv->display_spin), "value-changed",
				  G_CALLBACK (gncal_prefs_display_time_changed),
				  dialog);
	gtk_widget_show (priv->display_spin);
	gtk_table_attach (GTK_TABLE (table), priv->display_spin,
			  2, 3, 1, 2, 0, 0, 0, 0);
	priv->audio_spin = gtk_spin_button_new_with_range ((gdouble) DEFAULT_ALARM_TIME_MIN, (gdouble) DEFAULT_ALARM_TIME_MAX, 1.0);
	priv->audio_signal_spin =
		g_signal_connect (G_OBJECT (priv->audio_spin), "value-changed",
				  G_CALLBACK (gncal_prefs_audio_time_changed),
				  dialog);
	gtk_widget_show (priv->audio_spin);
	gtk_table_attach (GTK_TABLE (table), priv->audio_spin,
			  2, 3, 2, 3, 0, 0, 0, 0);
	priv->program_spin = gtk_spin_button_new_with_range ((gdouble) DEFAULT_ALARM_TIME_MIN, (gdouble) DEFAULT_ALARM_TIME_MAX, 1.0);
	priv->program_signal_spin =
		g_signal_connect (G_OBJECT (priv->program_spin), "value-changed",
				  G_CALLBACK (gncal_prefs_program_time_changed),
				  dialog);
	gtk_widget_show (priv->program_spin);
	gtk_table_attach (GTK_TABLE (table), priv->program_spin,
			  2, 3, 3, 4, 0, 0, 0, 0);
	priv->mail_spin = gtk_spin_button_new_with_range ((gdouble) DEFAULT_ALARM_TIME_MIN, (gdouble) DEFAULT_ALARM_TIME_MAX, 1.0);
	priv->mail_signal_spin =
		g_signal_connect (G_OBJECT (priv->mail_spin), "value-changed",
				  G_CALLBACK (gncal_prefs_mail_time_changed),
				  dialog);
	gtk_widget_show (priv->mail_spin);
	gtk_table_attach (GTK_TABLE (table), priv->mail_spin,
			  2, 3, 4, 5, 0, 0, 0, 0);

	priv->display_cb = gncal_prefs_create_timeunit_cb (dialog);
	priv->display_signal_cb =
		g_signal_connect (G_OBJECT (priv->display_cb), "changed",
				  G_CALLBACK (gncal_prefs_display_time_changed),
				  dialog);
	gtk_widget_show (priv->display_cb);
	gtk_table_attach (GTK_TABLE (table), priv->display_cb,
			  3, 4, 1, 2, 0, 0, 0, 0);
	priv->audio_cb = gncal_prefs_create_timeunit_cb (dialog);
	priv->audio_signal_cb =
		g_signal_connect (G_OBJECT (priv->audio_cb), "changed",
				  G_CALLBACK (gncal_prefs_audio_time_changed),
				  dialog);
	gtk_widget_show (priv->audio_cb);
	gtk_table_attach (GTK_TABLE (table), priv->audio_cb,
			  3, 4, 2, 3, 0, 0, 0, 0);
	priv->program_cb = gncal_prefs_create_timeunit_cb (dialog);
	priv->program_signal_cb =
		g_signal_connect (G_OBJECT (priv->program_cb), "changed",
				  G_CALLBACK (gncal_prefs_program_time_changed),
				  dialog);
	gtk_widget_show (priv->program_cb);
	gtk_table_attach (GTK_TABLE (table), priv->program_cb,
			  3, 4, 3, 4, 0, 0, 0, 0);
	priv->mail_cb = gncal_prefs_create_timeunit_cb (dialog);
	priv->mail_signal_cb =
		g_signal_connect (G_OBJECT (priv->mail_cb), "changed",
				  G_CALLBACK (gncal_prefs_mail_time_changed),
				  dialog);
	gtk_widget_show (priv->mail_cb);
	gtk_table_attach (GTK_TABLE (table), priv->mail_cb,
			  3, 4, 4, 5, 0, 0, 0, 0);

	label = gtk_label_new_with_mnemonic (_("_Run program:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label,
			  4, 5, 3, 4, GTK_FILL, 0, 0, 0);
	priv->program_entry = gtk_entry_new ();
	g_signal_connect (G_OBJECT (priv->program_entry), "changed",
			  G_CALLBACK (gncal_prefs_entry_changed),
			  CONFIG_ALARM_PROGRAM_COMMAND);
	gtk_widget_show (priv->program_entry);
	gtk_table_attach (GTK_TABLE (table), priv->program_entry,
			  5, 6, 3, 4, GTK_EXPAND | GTK_FILL, 0, 0, 0);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->program_entry);
	priv->program_button = gtk_button_new_with_mnemonic (_("Browse..."));
	g_signal_connect (G_OBJECT (priv->program_button), "clicked",
			  G_CALLBACK (gncal_prefs_browse_for_command), dialog);
	gtk_widget_show (priv->program_button);
	gtk_table_attach (GTK_TABLE (table), priv->program_button,
			  6, 7, 3, 4, 0, 0, 0, 0);

	/* translator note: the person who receives mail */
	label = gtk_label_new_with_mnemonic (_("Mail recei_ver:"));
	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label,
			  4, 5, 4, 5, GTK_FILL, 0, 0, 0);
	priv->mail_entry = gtk_entry_new ();
	g_signal_connect (G_OBJECT (priv->mail_entry), "changed",
			  G_CALLBACK (gncal_prefs_entry_changed),
			  CONFIG_ALARM_MAIL_RECEIVER);
	gtk_widget_show (priv->mail_entry);
	gtk_table_attach (GTK_TABLE (table), priv->mail_entry,
			  5, 7, 4, 5, GTK_EXPAND | GTK_FILL, 0, 0, 0);
	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->mail_entry);

	/* Cleanup */

	pango_attr_list_unref (bold);
	pango_attr_list_unref (underline);

	return page;
}

static void
gncal_prefs_setup_gconf (GncalPrefs *dialog)
{
	GncalPrefsPriv *priv;
	GConfClient *gconf;
	gboolean bool;
	gint begin, end;
	gint i;
	gchar *s;

	priv = dialog->priv;

	gconf = gconf_client_get_default ();

	gconf_client_add_dir (gconf, CONFIG_DIR, GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);

	/* Day range/Midnight rollover */

	begin = gconf_client_get_int (gconf, CONFIG_CAL_DAY_START, NULL);
	end   = gconf_client_get_int (gconf, CONFIG_CAL_DAY_END, NULL);

	if (begin < 0 || begin > 23) {
		gncal_prefs_warn_invalid_hour (CONFIG_CAL_DAY_START, begin);
		begin = DEFAULT_CAL_DAY_START;
	}
	if (end < 0 || end > 23) {
		gncal_prefs_warn_invalid_hour (CONFIG_CAL_DAY_END, end);
		end = DEFAULT_CAL_DAY_END;
	}
	if (begin > end) {
		g_printerr (_("Configuration error: "
			      "Day end time (%d) is before day start time (%d)! "
			      "The times are reset to sane default values."),
			    end, begin);
		begin = DEFAULT_CAL_DAY_START;
		end   = DEFAULT_CAL_DAY_END;
	}

	gtk_combo_box_set_active (GTK_COMBO_BOX (priv->day_start), begin);
	gtk_combo_box_set_active (GTK_COMBO_BOX (priv->day_end),   end);

	bool = gconf_client_get_bool (gconf, CONFIG_CAL_ROLLOVER, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->midnight), bool);

	/* Colors */

	for (i = 0; i < G_N_ELEMENTS (color_properties); i++) {
		PangoColor pango_color;
		GdkColor color;
		gchar *s;

		s = gconf_client_get_string (gconf, color_properties[i].key, NULL);
		if (!s)
			s = g_strdup(color_properties[i].color);
		if (pango_color_parse (&pango_color, s)) {
			color.red   = pango_color.red;
			color.green = pango_color.green;
			color.blue  = pango_color.blue;
			gtk_color_button_set_color (GTK_COLOR_BUTTON (priv->colors[i]), &color);
		} else {
			color.red = color.green = color.blue = 0;
			gtk_color_button_set_color (GTK_COLOR_BUTTON (priv->colors[i]), &color);
		}
		g_free (s);
	}

	/* To do list columns */

	bool = gconf_client_get_bool (gconf, CONFIG_TODO_SHOW_DUEDATE, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->duedate),
				      bool);
	bool = gconf_client_get_bool (gconf, CONFIG_TODO_SHOW_PRIORITY, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->priority),
				      bool);
	bool = gconf_client_get_bool (gconf, CONFIG_TODO_SHOW_TIMELEFT, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->timeleft),
				      bool);
	bool = gconf_client_get_bool (gconf, CONFIG_TODO_SHOW_CATEGORIES, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->categories),
				      bool);

	/* Alarms */

	bool = gconf_client_get_bool (gconf, CONFIG_ALARM_BEEP, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->beep), bool);
	i = gconf_client_get_int (gconf, CONFIG_ALARM_TIMEOUT, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->timeout_check), i);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (priv->timeout_spin),
				   i != 0 ? (double) i : DEFAULT_ALARM_TIMEOUT_VALUE);
	i = gconf_client_get_int (gconf, CONFIG_ALARM_SNOOZE, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->snooze_check), i);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (priv->snooze_spin),
				   i != 0 ? (double) i : DEFAULT_ALARM_SNOOZE_VALUE);

	bool = gconf_client_get_bool (gconf, CONFIG_ALARM_DISPLAY_ENABLED, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->display_check), bool);
	bool = gconf_client_get_bool (gconf, CONFIG_ALARM_AUDIO_ENABLED, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->audio_check), bool);
	bool = gconf_client_get_bool (gconf, CONFIG_ALARM_PROGRAM_ENABLED, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->program_check), bool);
	bool = gconf_client_get_bool (gconf, CONFIG_ALARM_MAIL_ENABLED, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (priv->mail_check), bool);

	i = gconf_client_get_int (gconf, CONFIG_ALARM_DISPLAY_TIME, NULL);
	gncal_prefs_set_time_from_config (CONFIG_ALARM_DISPLAY_TIME,
					  priv->display_spin,
					  priv->display_cb,
					  priv->display_signal_spin,
					  priv->display_signal_cb,
					  i);
	i = gconf_client_get_int (gconf, CONFIG_ALARM_AUDIO_TIME, NULL);
	gncal_prefs_set_time_from_config (CONFIG_ALARM_AUDIO_TIME,
					  priv->audio_spin,
					  priv->audio_cb,
					  priv->audio_signal_spin,
					  priv->audio_signal_cb,
					  i);
	i = gconf_client_get_int (gconf, CONFIG_ALARM_PROGRAM_TIME, NULL);
	gncal_prefs_set_time_from_config (CONFIG_ALARM_PROGRAM_TIME,
					  priv->program_spin,
					  priv->program_cb,
					  priv->program_signal_spin,
					  priv->program_signal_cb,
					  i);
	i = gconf_client_get_int (gconf, CONFIG_ALARM_MAIL_TIME, NULL);
	gncal_prefs_set_time_from_config (CONFIG_ALARM_MAIL_TIME,
					  priv->mail_spin,
					  priv->mail_cb,
					  priv->mail_signal_spin,
					  priv->mail_signal_cb,
					  i);

	s = gconf_client_get_string (gconf, CONFIG_ALARM_PROGRAM_COMMAND, NULL);
	if (s) {
		gtk_entry_set_text (GTK_ENTRY (priv->program_entry), s);
		g_free (s);
	} else
		gtk_entry_set_text (GTK_ENTRY (priv->program_entry), "");

	s = gconf_client_get_string (gconf, CONFIG_ALARM_MAIL_RECEIVER, NULL);
	if (s) {
		gtk_entry_set_text (GTK_ENTRY (priv->mail_entry), s);
		g_free (s);
	} else
		gtk_entry_set_text (GTK_ENTRY (priv->mail_entry), "");

	/* Notifications */

	priv->notify2 = gconf_client_notify_add (gconf, CONFIG_CALENDAR_DIR,
						 gncal_prefs_calendar_config_changed,
						 dialog, NULL, NULL);
	priv->notify3 = gconf_client_notify_add (gconf, CONFIG_TODO_DIR,
						 gncal_prefs_todo_config_changed,
						 dialog, NULL, NULL);
	priv->notify4 = gconf_client_notify_add (gconf, CONFIG_COLOR_DIR,
						 gncal_prefs_colors_config_changed,
						 dialog, NULL, NULL);
	priv->notify5 = gconf_client_notify_add (gconf, CONFIG_ALARM_DIR,
						 gncal_prefs_alarm_config_changed,
						 dialog, NULL, NULL);

	g_object_unref (G_OBJECT (gconf));
}

static void
gncal_prefs_setup (GncalPrefs *dialog)
{
	GtkWidget *notebook;
	GtkWidget *page;

	gtk_window_set_title (GTK_WINDOW (dialog), _("GNOME Calendar Preferences"));

        gtk_dialog_add_buttons (GTK_DIALOG (dialog),
                                GTK_STOCK_HELP,  GTK_RESPONSE_HELP,
                                GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                                NULL);

        gtk_dialog_set_default_response (GTK_DIALOG (dialog), 0);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);

        g_signal_connect (G_OBJECT (dialog), "response",
                          G_CALLBACK (gncal_prefs_response), NULL);

	notebook = gtk_notebook_new ();
	gtk_container_set_border_width (GTK_CONTAINER (notebook), 6);
	gtk_widget_show (notebook);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), notebook);

	page = gncal_prefs_setup_time_page (dialog);
	gtk_container_set_border_width (GTK_CONTAINER (page), 12);
	gtk_widget_show (page);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page,
				  gtk_label_new (_("Time Display")));

	page = gncal_prefs_setup_colors_page (dialog);
	gtk_container_set_border_width (GTK_CONTAINER (page), 12);
	gtk_widget_show (page);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page,
				  gtk_label_new (_("Colors")));

	page = gncal_prefs_setup_todo_page (dialog);
	gtk_container_set_border_width (GTK_CONTAINER (page), 12);
	gtk_widget_show (page);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page,
				  gtk_label_new (_("To Do List")));

	page = gncal_prefs_setup_alarm_page (dialog);
	gtk_container_set_border_width (GTK_CONTAINER (page), 12);
	gtk_widget_show (page);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page,
				  gtk_label_new (_("Alarms")));

	gncal_prefs_setup_gconf (dialog);
}

/*
 * Public methods
 */

GtkWidget *
gncal_prefs_new (GtkWindow *parent)
{
	GncalPrefs *dialog;

	g_return_val_if_fail (parent == NULL || GTK_IS_WINDOW (parent), NULL);

	dialog = g_object_new (GNCAL_TYPE_PREFS, NULL);

	if (parent)
		gtk_window_set_transient_for (GTK_WINDOW (dialog), parent);

	return GTK_WIDGET (dialog);
}
