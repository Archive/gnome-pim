/* Calendar Day Object
 * Copyright (C) 2003-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <string.h>
#include <time.h>

#include <glib.h>
#include <gnome.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "calendar-day-view.h"
#include "calendar-marshal.h"
#include "calendar-day.h"
#include "gnomecal-config.h"
#include "todo-list.h"


static void calendar_day_class_init	(CalendarDayClass	*klass);
static void calendar_day_init		(CalendarDay		*cd);
static void calendar_day_finalize	(GObject		*object);

static void setup			(CalendarDay		*cd);
static void update_label		(CalendarDay		*cd);
static void update_calendar		(CalendarDay		*cd);


enum {
	SIGNAL_DAY_CHANGED,
	SIGNAL_LAST
};


struct _CalendarDayPriv {
	GDate		 date;

	GtkWidget	*label;
	GtkWidget	*dayview;
	GtkWidget	*dayview_sw;
	GtkWidget	*calendar;
	GtkWidget	*todo;
};


static GtkVBoxClass *parent_class = NULL;

static guint signals[SIGNAL_LAST] = { 0 };

/*
 * Class and Instance Setup
 */

GType
calendar_day_get_type (void)
{
	static GType calendar_day_type = 0;

	if (!calendar_day_type) {
		static const GTypeInfo calendar_day_info = {
			sizeof (CalendarDayClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_day_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarDay),
			0,    /* n_preallocs */
			(GInstanceInitFunc) calendar_day_init,
		};

		calendar_day_type = g_type_register_static (GTK_TYPE_VBOX,
							    "CalendarDay",
							    &calendar_day_info,
							    0);
	}

	return calendar_day_type;
}

static void
calendar_day_class_init (CalendarDayClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_DAY_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->finalize = calendar_day_finalize;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_DAY_CHANGED] =
		g_signal_new ("day-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarDayClass, day_changed),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);
}

static void
calendar_day_init (CalendarDay *cd)
{
	time_t t;
	struct tm tm;

	g_return_if_fail (cd != NULL);
	g_return_if_fail (CALENDAR_IS_DAY (cd));

	t = time (NULL);
	localtime_r (&t, &tm);

	cd->priv = g_new0 (CalendarDayPriv, 1);

	g_date_set_dmy (&cd->priv->date, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);

	setup (cd);
}

static void
calendar_day_finalize (GObject *object)
{
	CalendarDay *cd;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_DAY (object));

	cd = CALENDAR_DAY (object);

	g_free (cd->priv);
	cd->priv = NULL;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*
 * Callbacks
 */

static void
mapped_cb (GtkWidget *widget, gpointer data)
{
	update_label (CALENDAR_DAY (widget));
	update_calendar (CALENDAR_DAY (widget));
}

static void
calendar_changed (CalendarDay *cd, GtkCalendar *calendar)
{
	GDate date;
	guint year, month, day;

	gtk_calendar_get_date (calendar, &year, &month, &day);

	g_date_set_dmy (&date, day, month + 1, year);

	calendar_day_set_date (cd, &date);
}

/*
 * Private Methods
 */

static void
update_label (CalendarDay *cd)
{
	CalendarDayPriv *priv = cd->priv;
	gchar buffer[100];

	/* Translators: This is "WEEKDAY MONTH DAY YEAR". */
	g_date_strftime (buffer, sizeof (buffer), _("%A %B %d %Y"), &priv->date);
	gtk_label_set_text (GTK_LABEL (priv->label), buffer);
}

static void
update_calendar (CalendarDay *cd)
{
	CalendarDayPriv *priv = cd->priv;
	GConfClient *gconf;
	GtkAdjustment *adj;
	gint begin, ypos;

	g_signal_handlers_block_by_func (G_OBJECT (priv->calendar),
					 G_CALLBACK (calendar_changed), cd);

	gtk_calendar_select_month (GTK_CALENDAR (priv->calendar),
				   g_date_get_month (&priv->date) - 1,
				   g_date_get_year (&priv->date));
	gtk_calendar_select_day (GTK_CALENDAR (priv->calendar),
				 g_date_get_day (&priv->date));

	g_signal_handlers_unblock_by_func (G_OBJECT (priv->calendar),
					   G_CALLBACK (calendar_changed), cd);

	calendar_day_view_set_date (CALENDAR_DAY_VIEW (priv->dayview), &priv->date);

	/* Move day view to start of day */

	gconf = gconf_client_get_default ();
	gconf_client_add_dir (gconf, CONFIG_DIR, GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);
	begin = gconf_client_get_int (gconf, CONFIG_CAL_DAY_START, NULL);
	g_object_unref (G_OBJECT (gconf));

	if (begin < 0 || begin > 23)
		begin = DEFAULT_CAL_DAY_START;

	ypos = calendar_day_view_get_y_pos_for_time (CALENDAR_DAY_VIEW (priv->dayview), begin * 60);
	adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (priv->dayview_sw));
	gtk_adjustment_set_value (adj, (gdouble) ypos);
}

/*
 * Setup
 */

static void
setup (CalendarDay *cd)
{
	CalendarDayPriv *priv;
	PangoAttrList *attrs;
	PangoAttribute *attr;
	GtkWidget *paned, *right_box;

	priv = cd->priv;

	gtk_widget_set_size_request (GTK_WIDGET (cd), 150, 120);
	gtk_box_set_spacing (GTK_BOX (cd), 12);

	/* Label */

	attrs = pango_attr_list_new ();
	attr = pango_attr_scale_new (PANGO_SCALE_X_LARGE);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);
	attr = pango_attr_weight_new (PANGO_WEIGHT_BOLD);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);

	priv->label = gtk_label_new ("");
	gtk_label_set_attributes (GTK_LABEL (priv->label), attrs);
	gtk_box_pack_start (GTK_BOX (cd), priv->label, FALSE, FALSE, 0);

	pango_attr_list_unref (attrs);

	gtk_widget_show (priv->label);

	/* Drag handle */

	paned = gtk_hpaned_new ();
	gtk_paned_set_position (GTK_PANED (paned), 300);
	gtk_box_pack_start (GTK_BOX (cd), paned, TRUE, TRUE, 0);
	gtk_widget_show (paned);

	right_box = gtk_vbox_new (FALSE, 12);
	gtk_paned_pack2 (GTK_PANED (paned), right_box, TRUE, TRUE);
	gtk_widget_show (right_box);

	/* Day view */

	priv->dayview_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->dayview_sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_widget_show (priv->dayview_sw);
	gtk_paned_pack1 (GTK_PANED (paned), priv->dayview_sw, FALSE, TRUE);

	priv->dayview = calendar_day_view_new ();
#if 0 /* FIXME */
	g_signal_connect (G_OBJECT (priv->dayview), "range-activated",
			  G_CALLBACK (day_view_range_activated), dpanel);
#endif
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (priv->dayview_sw), priv->dayview);
	gtk_widget_show (priv->dayview);

	/* Calendar */

	priv->calendar = gtk_calendar_new ();
	gtk_calendar_set_display_options (GTK_CALENDAR (priv->calendar),
					  GTK_CALENDAR_SHOW_HEADING	|
					  GTK_CALENDAR_SHOW_DAY_NAMES	|
					  GTK_CALENDAR_SHOW_WEEK_NUMBERS);
	g_signal_connect_swapped (G_OBJECT (priv->calendar),
				  "day-selected",
				  G_CALLBACK (calendar_changed), cd);
	gtk_box_pack_start (GTK_BOX (right_box), priv->calendar, FALSE, TRUE, 0);
	gtk_widget_show (priv->calendar);

	/* Todo list */

	priv->todo = todo_list_new ();
	gtk_box_pack_start (GTK_BOX (right_box), priv->todo, TRUE, TRUE, 0);
	gtk_widget_show (priv->todo);

	g_signal_connect_after (G_OBJECT (cd), "map", G_CALLBACK (mapped_cb), NULL);
}

/*
 * Public Methods
 */

GtkWidget *
calendar_day_new (void)
{
	CalendarDay *cd;

	cd = g_object_new (CALENDAR_TYPE_DAY, NULL);

	return GTK_WIDGET (cd);
}

void
calendar_day_set_calendar (CalendarDay *cd, CalendarManager *cal)
{
	g_return_if_fail (cd != NULL);
	g_return_if_fail (CALENDAR_IS_DAY (cd));
	g_return_if_fail (cal != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (cal));

	todo_list_set_calendar (TODO_LIST (cd->priv->todo), cal);
	calendar_day_view_set_calendar (CALENDAR_DAY_VIEW (cd->priv->dayview), cal);
}

void
calendar_day_set_date (CalendarDay *cd, const GDate *date)
{
	CalendarDayPriv *priv;

	g_return_if_fail (cd != NULL);
	g_return_if_fail (CALENDAR_IS_DAY (cd));
	g_return_if_fail (date != NULL);
	g_return_if_fail (g_date_valid (date));

	priv = cd->priv;

	priv->date = *date;

	update_label (cd);
	update_calendar (cd);

	/* Emit signal */

	g_signal_emit (G_OBJECT (cd), signals[SIGNAL_DAY_CHANGED], 0,
		       g_date_get_year (date), g_date_get_month (date), g_date_get_day (date));
}

void
calendar_day_get_date (CalendarDay *cd, GDate *date)
{
	CalendarDayPriv *priv;

	g_return_if_fail (cd != NULL);
	g_return_if_fail (CALENDAR_IS_DAY (cd));
	g_return_if_fail (date != NULL);

	priv = cd->priv;

	*date = priv->date;
}
