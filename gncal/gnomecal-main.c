/* Gnome Calendar Main Functions
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* FIXME: honor start view */
/* FIXME: honor arg_hidden */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <locale.h>

#include <gnome.h>

#include "gnomecal-main-window.h"
#include "gnomecal-session.h"
#include "gnomecal-ui.h"

/* Command-line arguments */

typedef enum {
	VIEW_DAY,
	VIEW_WEEK,
	VIEW_MONTH,
	VIEW_YEAR
} View;

static GList *arg_files = NULL;

static View arg_view = VIEW_DAY;

static gboolean arg_hidden = FALSE;

/*
 * Argument Parsing
 */

static void
gncal_main_parse_arg (poptContext ctx,
		      enum poptCallbackReason reason,
		      const struct poptOption *option,
		      char *argument,
		      void *data)
{
	switch (option->val) {
	case 'F':
		arg_files = g_list_append (arg_files, argument);
		break;
	case 'v':
		if (!strcmp (argument, "dayview"))
			arg_view = VIEW_DAY;
		else if (!strcmp (argument, "weekview"))
			arg_view = VIEW_WEEK;
		else if (!strcmp (argument, "monthview"))
			arg_view = VIEW_MONTH;
		else if (!strcmp (argument, "yearview"))
			arg_view = VIEW_YEAR;
		else {
			fprintf (stderr, _("%s: Invalid argument for --view option: %s!\n"),
				 g_get_prgname (), argument);
			exit (1);
		}
		break;
	case 'H':
		arg_hidden = TRUE;
		break;
	}
}

static const struct poptOption
command_line_options[] = {
	{ NULL,		'\0',	POPT_ARG_CALLBACK,	gncal_main_parse_arg, 0, NULL,					NULL },
	{ "file",	'F',	POPT_ARG_STRING,	NULL,	'F',	N_("calendar file to load"),			N_("FILE") },
	{ "view",	'v',	POPT_ARG_STRING,	NULL,	'v',	N_("startup view mode (dayview, weekview, monthview, yearview)"), N_("VIEW") },
	{ "hidden",	'H',	POPT_ARG_NONE,		NULL,	'H',	N_("start in iconic mode"),			NULL },
	{ NULL,		'\0',	0,			NULL,	0,	NULL,						NULL }
};

/*
 * Setup
 */

static void
gncal_main_open_windows (GMainLoop *main_loop)
{
	if (arg_files) {
		GList *l;

		for (l = arg_files; l != NULL; l = g_list_next (l)) {
			GError *error = NULL;
			GtkWidget *window;
			const gchar *file;

			file = (const gchar *) l->data;

			window = gncal_ui_open_main_window (main_loop);

			gncal_main_window_set_calendar (GNCAL_MAIN_WINDOW (window), file, &error);

			if (!error) {
				gtk_widget_show (window);
			} else {
				gtk_widget_destroy (window);
				gncal_ui_show_error (NULL, error);
			}
		}
	} else {
		GtkWidget *window;

		window = gncal_ui_open_main_window (main_loop);

		gncal_main_window_set_calendar_default (GNCAL_MAIN_WINDOW (window));

		gtk_widget_show (window);
	}
}

static void
gncal_main_init (int argc, char *argv[], GMainLoop *main_loop)
{
	gnome_program_init("gnomecal", VERSION,
			   LIBGNOMEUI_MODULE,
			   argc, argv,
			   GNOME_PARAM_POPT_TABLE, command_line_options,
			   GNOME_PROGRAM_STANDARD_PROPERTIES,
			   GNOME_PARAM_NONE);

	gncal_ui_init ();
	gncal_session_init (main_loop);

	gncal_main_open_windows (main_loop);
}

/*
 * Main Function
 */

int
main (int argc, char *argv[])
{
	int ret = 0;

	GMainLoop *main_loop;

	/* I18n setup */

	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
        bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
        textdomain (GETTEXT_PACKAGE);

	main_loop = g_main_loop_new (NULL, FALSE);

	gncal_main_init (argc, argv, main_loop);

	/* Main Loop */

	g_main_loop_run (main_loop);

	/* Cleanup */

	g_main_loop_unref (main_loop);
	g_list_free (arg_files);

	return ret;
}
