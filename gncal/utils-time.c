/* Miscellaneous Time Utility Functions
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <time.h>

#include <glib.h>

#include <gnome.h> /* for _() */

#include "utils-time.h"


/* Convert a time to (localized) time string with hour and minute and
 * (locale-dependent) AM/PM indicator.
 */
gchar *
time_get_short_time_string (guint8 hour, guint8 minute)
{
	gchar buffer[40];
	struct tm tm;

	tm.tm_hour = hour;
	tm.tm_min  = minute;
	tm.tm_sec  = 0;

	/* Translators: This is a time string, containing the hour and
	   minute and (optionally) an AM/PM indicator. Example representations
	   are %I:%M %p for America, or %H:%M for European languages. The
	   default is the ISO format.
	 */
	strftime (buffer, sizeof (buffer), _("%H:%M"), &tm);

	return g_locale_to_utf8 (buffer, -1, NULL, NULL, NULL);
}

/* Convert a time to a (localized) time and date string. */
gchar *
time_get_date_time_string (struct tm *tm)
{
	gchar buffer[100];

	/* Translators: This is a verbose date and time string. Default
	   is the ISO format. */
	strftime (buffer, sizeof (buffer), _("%Y-%m-%d %H:%M"), tm);

	return g_locale_to_utf8 (buffer, -1, NULL, NULL, NULL);
}

/* Create a localized UTF-8 date string. It must be freed by the caller. */
gchar *
time_get_date_string (struct tm *tm)
{
    char buf[100];

    strftime (buf, sizeof (buf), _("%A %B %d %Y"), tm);

    return g_locale_to_utf8 (buf, -1, NULL, NULL, NULL);
}
