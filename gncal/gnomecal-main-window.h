#ifndef __GNOMECAL_MAIN_WINDOW_H__
#define __GNOMECAL_MAIN_WINDOW_H__

/* Gnome Calendar Main Window Object
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gnome.h>


#define GNCAL_TYPE_MAIN_WINDOW			(gncal_main_window_get_type())
#define GNCAL_MAIN_WINDOW(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNCAL_TYPE_MAIN_WINDOW, GncalMainWindow))
#define GNCAL_MAIN_WINDOW_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GNCAL_TYPE_MAIN_WINDOW))
#define GNCAL_IS_MAIN_WINDOW(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNCAL_TYPE_MAIN_WINDOW))
#define GNCAL_IS_MAIN_WINDOW_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNCAL_TYPE_MAIN_WINDOW))

typedef struct _GncalMainWindow		GncalMainWindow;
typedef struct _GncalMainWindowClass	GncalMainWindowClass;
typedef struct _GncalMainWindowPriv	GncalMainWindowPriv;

struct _GncalMainWindow
{
	GnomeApp parent;

	GncalMainWindowPriv *priv;
};

struct _GncalMainWindowClass
{
	GnomeAppClass parent_class;

	void (* quit)		(GncalMainWindow *window);
	void (* open_new)	(GncalMainWindow *window, const gchar *file);
};

GType		 gncal_main_window_get_type	(void);
GtkWidget	*gncal_main_window_new		(void);

gboolean	 gncal_main_window_set_calendar		(GncalMainWindow *win, const gchar *filename, GError **error);
gboolean	 gncal_main_window_set_calendar_default	(GncalMainWindow *win);
void		 gncal_main_window_set_calendar_new	(GncalMainWindow *win);
gboolean	 gncal_main_window_save_calendar	(GncalMainWindow *win, GError **error);

#endif /* __GNOMECAL_MAIN_WINDOW_H__ */
