#ifndef __GNOMECAL_PREFS_H__
#define __GNOMECAL_PREFS_H__

/* GNOME Calendar Preferences Dialog Object
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#define GNCAL_TYPE_PREFS		(gncal_prefs_get_type())
#define GNCAL_PREFS(obj)		(GTK_CHECK_CAST ((obj), GNCAL_TYPE_PREFS, GncalPrefs))
#define GNCAL_PREFS_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNCAL_TYPE_PREFS))
#define GNCAL_IS_PREFS(obj)		(GTK_CHECK_TYPE ((obj), GNCAL_TYPE_PREFS))
#define GNCAL_IS_PREFS_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GNCAL_TYPE_PREFS))

typedef struct _GncalPrefs	GncalPrefs;
typedef struct _GncalPrefsClass	GncalPrefsClass;
typedef struct _GncalPrefsPriv	GncalPrefsPriv;

struct _GncalPrefs
{
	GtkDialog parent;

	GncalPrefsPriv *priv;
};

struct _GncalPrefsClass
{
	GtkDialogClass parent_class;
};

GType		 gncal_prefs_get_type	(void);
GtkWidget	*gncal_prefs_new	(GtkWindow *parent);

#endif /* __GNOMECAL_PREFS_H__ */
