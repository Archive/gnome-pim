/* Gnome Calendar Go To Dialog Object
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * Based on the original go-to dialog by Federico Mena.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <time.h>

#include <glib.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <gnome.h>

#include "calendar-month.h"
#include "gnomecal-goto.h"
#include "utils-ui.h"


static void gncal_goto_init		(GncalGoto *dialog);
static void gncal_goto_finalize		(GObject *dialog);
static void gncal_goto_class_init	(GncalGotoClass *klass);

static void setup			(GncalGoto *dialog);


struct _GncalGotoPriv {
	/* Widgets */

	GtkWidget *year_chooser;
	GtkWidget *month_chooser;
	GtkWidget *day_chooser;

	/* Signal IDs */

	gulong year_signal;
	gulong month_signal;
	gulong day_signal;
};

enum {
	SIGNAL_CHANGED,
	SIGNAL_YEAR_CHANGED,
	SIGNAL_MONTH_CHANGED,
	SIGNAL_DAY_CHANGED,
	SIGNAL_LAST
};


static GtkDialogClass *parent_class = NULL;

static guint signals[SIGNAL_LAST] = { 0 };


/*
 * Class Handling
 */

GType
gncal_goto_get_type (void)
{
	static GType gncal_goto_type = 0;

	if (!gncal_goto_type) {
		static const GTypeInfo gncal_goto_info = {
			sizeof (GncalGotoClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) gncal_goto_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (GncalGoto),
			1,    /* n_preallocs */
			(GInstanceInitFunc) gncal_goto_init,
		};

		gncal_goto_type = g_type_register_static (GTK_TYPE_DIALOG,
							  "GncalGoto",
							  &gncal_goto_info,
							  0);
	}

	return gncal_goto_type;
}

static void
gncal_goto_class_init (GncalGotoClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (GNCAL_IS_GOTO_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->finalize = gncal_goto_finalize;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_CHANGED] =
		g_signal_new ("changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (GncalGotoClass, changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	signals[SIGNAL_YEAR_CHANGED] =
		g_signal_new ("year-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (GncalGotoClass, year_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	signals[SIGNAL_MONTH_CHANGED] =
		g_signal_new ("month-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (GncalGotoClass, month_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
	signals[SIGNAL_DAY_CHANGED] =
		g_signal_new ("day-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (GncalGotoClass, day_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
}

static void
gncal_goto_init (GncalGoto *dialog)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_GOTO (dialog));

	dialog->priv = g_new0 (GncalGotoPriv, 1);

	setup (dialog);
}

static void
gncal_goto_finalize (GObject *object)
{
	GncalGoto *dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNCAL_IS_GOTO (object));

	dialog = GNCAL_GOTO (object);

	g_free (dialog->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*
 * Callbacks
 */

static void
response (GtkDialog *dialog, gint response, gpointer data)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GTK_IS_WINDOW (dialog));

	switch (response) {
	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;
	case GTK_RESPONSE_DELETE_EVENT:
		break;
	case GTK_RESPONSE_HELP:
		ui_show_help (GTK_WINDOW (dialog), "goto-dialog");
		break;
	default:
		g_return_if_reached ();
	}
}

static void
year_changed (GtkWidget *widget, GncalGoto *dialog)
{
	GncalGotoPriv *priv;
	GDate date;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_GOTO (dialog));

	priv = dialog->priv;

	g_signal_handler_block (priv->day_chooser, priv->day_signal);

	calendar_month_get_date (CALENDAR_MONTH (priv->day_chooser), &date);
	date.year = (gint) gtk_spin_button_get_value (GTK_SPIN_BUTTON (priv->year_chooser));
	calendar_month_set_date (CALENDAR_MONTH (priv->day_chooser), &date);

	g_signal_handler_unblock (priv->day_chooser, priv->day_signal);

	g_signal_emit (dialog, signals[SIGNAL_YEAR_CHANGED], 0);
	g_signal_emit (dialog, signals[SIGNAL_CHANGED], 0);
}

static void
month_changed (GtkWidget *widget, GncalGoto *dialog)
{
	GncalGotoPriv *priv;
	GDate date;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_GOTO (dialog));

	priv = dialog->priv;

	g_signal_handler_block (priv->day_chooser, priv->day_signal);

	calendar_month_get_date (CALENDAR_MONTH (priv->day_chooser), &date);
	date.month = gtk_combo_box_get_active (GTK_COMBO_BOX (priv->month_chooser)) + 1;
	calendar_month_set_date (CALENDAR_MONTH (priv->day_chooser), &date);

	g_signal_handler_unblock (priv->day_chooser, priv->day_signal);

	g_signal_emit (dialog, signals[SIGNAL_MONTH_CHANGED], 0);
	g_signal_emit (dialog, signals[SIGNAL_CHANGED], 0);
}

static void
day_changed (GncalGoto *dialog, guint year, guint month, guint day, GtkWidget *widget)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_GOTO (dialog));

	g_signal_emit (dialog, signals[SIGNAL_DAY_CHANGED], 0);
	g_signal_emit (dialog, signals[SIGNAL_CHANGED], 0);
}

static void
today_clicked (GtkWidget *widget, GncalGoto *dialog)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_GOTO (dialog));

	gncal_goto_set_today (dialog);
}

/*
 * Setup
 */

static GtkWidget *
gncal_goto_month_cb_new (GncalGoto *dialog)
{
	GtkWidget *cb;

	gint i;

	static gchar *months[] = {
		N_("January"),
		N_("February"),
		N_("March"),
		N_("April"),
		N_("May"),
		N_("June"),
		N_("July"),
		N_("August"),
		N_("September"),
		N_("October"),
		N_("November"),
		N_("December")
	};

	cb = gtk_combo_box_new_text ();

	for (i = 0; i < G_N_ELEMENTS (months); i++)
		gtk_combo_box_append_text (GTK_COMBO_BOX (cb), _(months[i]));

	return cb;
}

static void
setup (GncalGoto *dialog)
{
	GncalGotoPriv	*priv;
	GtkWidget	*widget;
	GtkWidget	*box, *hbox;
	GtkWidget	*label;
	GnomeCanvasItem	*citem;

	time_t t;
	struct tm tm;

	priv = dialog->priv;

	/* Get Current Date */

	t = time (NULL);
	localtime_r (&t, &tm);

	/* Widget Setup */

	gtk_window_set_title (GTK_WINDOW (dialog), _("Go to date"));

	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				GTK_STOCK_HELP,  GTK_RESPONSE_HELP,
				GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
				NULL);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CLOSE);

	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (response), NULL);

	box = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (box), 6);
	gtk_widget_show (box);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), box);

	/* Label */

	label = gtk_label_new (_("Please choose the date to jump to:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);

	/* Year/Month */

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (box), hbox, FALSE, FALSE, 0);

	label = gtk_label_new_with_mnemonic (_("_Year:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	priv->year_chooser = gtk_spin_button_new_with_range (1900.0, 2199.0, 1.0);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (priv->year_chooser), tm.tm_year + 1900);
	gtk_widget_set_size_request (priv->year_chooser, 60, -1);
	priv->year_signal =
		g_signal_connect (G_OBJECT (priv->year_chooser), "value-changed",
				  G_CALLBACK (year_changed), dialog);
	gtk_widget_show (priv->year_chooser);
	gtk_box_pack_start (GTK_BOX (hbox), priv->year_chooser,
			    FALSE, FALSE, 0);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->year_chooser);

	label = gtk_label_new_with_mnemonic (_("_Month:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	priv->month_chooser = gncal_goto_month_cb_new (dialog);
	gtk_combo_box_set_active (GTK_COMBO_BOX (priv->month_chooser), tm.tm_mon);
	priv->month_signal =
		g_signal_connect (G_OBJECT (priv->month_chooser), "changed",
				  G_CALLBACK (month_changed), dialog);
	gtk_widget_show (priv->month_chooser);
	gtk_box_pack_start (GTK_BOX (hbox), priv->month_chooser, FALSE, FALSE, 0);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), priv->month_chooser);

	/* Calendar */

	priv->day_chooser = calendar_month_new ();
	priv->day_signal =
		g_signal_connect_swapped (G_OBJECT (priv->day_chooser),
					  "day-changed",
					  G_CALLBACK (day_changed), dialog);
	gtk_widget_show (priv->day_chooser);
	gtk_box_pack_start (GTK_BOX (box), priv->day_chooser, TRUE, TRUE, 0);

	citem = calendar_month_get_month_item (CALENDAR_MONTH (priv->day_chooser));
	g_object_set (G_OBJECT (citem),
		      "show-week-numbers", FALSE,
		      NULL);

	/* Today button */

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (box), hbox, FALSE, FALSE, 0);

	widget = gtk_button_new_with_mnemonic (_("_Today"));
	g_signal_connect (G_OBJECT (widget), "clicked",
			  G_CALLBACK (today_clicked), dialog);
	gtk_widget_show (widget);
	gtk_box_pack_start (GTK_BOX (hbox), widget, FALSE, FALSE, 0);
}

/*
 * Exported Methods
 */

GtkWidget *
gncal_goto_new (GtkWindow *parent)
{
	GncalGoto *dialog;

	g_return_val_if_fail (parent == NULL || GTK_IS_WINDOW (parent), NULL);

	dialog = g_object_new (GNCAL_TYPE_GOTO, NULL);

	if (parent)
		gtk_window_set_transient_for (GTK_WINDOW (dialog), parent);

	return GTK_WIDGET (dialog);
}

void
gncal_goto_set_date (GncalGoto *dialog, const GDate *date)
{
	GncalGotoPriv *priv;
	const GDate olddate;
	guint changed;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_GOTO (dialog));
	g_return_if_fail (date != NULL);
	g_return_if_fail (g_date_valid (date));

	priv = dialog->priv;

	g_signal_handler_block (priv->year_chooser,  priv->year_signal);
	g_signal_handler_block (priv->month_chooser, priv->month_signal);
	g_signal_handler_block (priv->day_chooser,   priv->day_signal);

	calendar_month_get_date (CALENDAR_MONTH (priv->day_chooser), &olddate);

	changed = 0;

	if (olddate.year != date->year) {
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (priv->year_chooser), date->year);
		changed |= 0x1;
	}
	if (olddate.month != date->month) {
		gtk_combo_box_set_active (GTK_COMBO_BOX (priv->month_chooser), date->month - 1);
		changed |= 0x2;
	}
	if (olddate.day != date->day)
		changed |= 0x4;

	if (changed)
		calendar_month_set_date (CALENDAR_MONTH (priv->day_chooser), date);

	g_signal_handler_unblock (priv->year_chooser,  priv->year_signal);
	g_signal_handler_unblock (priv->month_chooser, priv->month_signal);
	g_signal_handler_unblock (priv->day_chooser,   priv->day_signal);

	if (changed & 0x1)
		g_signal_emit (dialog, signals[SIGNAL_YEAR_CHANGED], 0);
	if (changed & 0x2)
		g_signal_emit (dialog, signals[SIGNAL_MONTH_CHANGED], 0);
	if (changed & 0x4)
		g_signal_emit (dialog, signals[SIGNAL_DAY_CHANGED], 0);
	if (changed)
		g_signal_emit (dialog, signals[SIGNAL_CHANGED], 0);
}

void
gncal_goto_set_today (GncalGoto *dialog)
{
	time_t t;
	struct tm tm;
	GDate date;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_GOTO (dialog));

	t = time (NULL);
	localtime_r (&t, &tm);
	g_date_set_dmy (&date, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);

	gncal_goto_set_date (dialog, &date);
}

void
gncal_goto_get_date (GncalGoto *dialog, GDate *date)
{
	GncalGotoPriv *priv;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GNCAL_IS_GOTO (dialog));
	g_return_if_fail (date != NULL);

	priv = dialog->priv;

	calendar_month_get_date (CALENDAR_MONTH (priv->day_chooser), date);
}
