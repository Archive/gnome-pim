#ifndef __CALENDAR_YEAR_ITEM_H__
#define __CALENDAR_YEAR_ITEM_H__

/* Calendar Year Item Object
 * Copyright (C) 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib-object.h>

#include <libgnomecanvas/gnome-canvas.h>

#define CALENDAR_TYPE_YEAR_ITEM			(calendar_year_item_get_type())
#define CALENDAR_YEAR_ITEM(obj)			(GTK_CHECK_CAST ((obj), CALENDAR_TYPE_YEAR_ITEM, CalendarYearItem))
#define CALENDAR_YEAR_ITEM_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_YEAR_ITEM))
#define CALENDAR_IS_YEAR_ITEM(obj)		(GTK_CHECK_TYPE ((obj), CALENDAR_TYPE_YEAR_ITEM))
#define CALENDAR_IS_YEAR_ITEM_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_YEAR_ITEM))

typedef struct _CalendarYearItem	CalendarYearItem;
typedef struct _CalendarYearItemClass	CalendarYearItemClass;
typedef struct _CalendarYearItemPriv	CalendarYearItemPriv;

struct _CalendarYearItem
{
	GnomeCanvasGroup parent;

	CalendarYearItemPriv *priv;
};

struct _CalendarYearItemClass
{
	GnomeCanvasGroupClass parent_class;

	void (* day_changed)  (CalendarYearItem *yitem, guint year, guint month, guint day);
	void (* day_selected) (CalendarYearItem *yitem, guint year, guint month, guint day);
	void (* day_menu)     (CalendarYearItem *yitem, guint year, guint month, guint day);
};

GType		 calendar_year_item_get_type	(void);

GnomeCanvasItem *
		calendar_year_item_new		(GnomeCanvasGroup *parent,
						 const gchar *arg1, ...);
void		calendar_year_item_construct	(CalendarYearItem *yitem);

void		calendar_year_item_set_date	(CalendarYearItem *yitem, const GDate *date);
void		calendar_year_item_get_date	(CalendarYearItem *yitem, GDate *date);

void		calendar_year_item_mark_day	(CalendarYearItem *yitem, GDateMonth month, GDateDay day);
void		calendar_year_item_unmark_day	(CalendarYearItem *yitem, GDateMonth month, GDateDay day);
void		calendar_year_item_clear_marks	(CalendarYearItem *yitem);

#endif /* __CALENDAR_YEAR_ITEM_H__ */
