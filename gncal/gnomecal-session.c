/* GNOME Calendar Session Handling
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include "gnomecal-session.h"


static gboolean
gncal_session_save (GnomeClient *client,
		    gint arg1, gint arg2, gboolean arg3, gint arg4, gboolean arg5,
		    gpointer data)
		    /* FIXME
 phase, GnomeRestartStyle save_style, gint shutdown,
		    GnomeInteractStyle  interact_style,
 gint fast,
 gpointer client_data)
*/
{
#if 0 /* FIXME */
	char **argv = (char **) g_malloc (sizeof (char *) * ((g_list_length (all_calendars) * 6) + 3));
	GList *l, *free_list = 0;
	int   i;

	argv [0] = client_data;
	for (i = 1, l = all_calendars; l; l = l->next) {
		GnomeCalendar *gcal = GNOME_CALENDAR (l->data);
		gchar *geometry;

		geometry = ui_gtk_window_get_geometry (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (gcal))));

		if (strcmp (gcal->cal->filename, user_calendar_file) == 0)
			argv [i++] = "--userfile";
		else {
			argv [i++] = "--file";
			argv [i++] = gcal->cal->filename;
		}
		argv [i++] = "--geometry";
		argv [i++] = geometry;
		argv [i++] = "--view";
		argv [i++] = gnome_calendar_get_current_view_name (gcal);
		free_list = g_list_append (free_list, geometry);
		my_save_calendar (gcal, gcal->cal->filename);
	}
	argv [i] = NULL;
	gnome_client_set_clone_command (client, i, argv);
	gnome_client_set_restart_command (client, i, argv);

	for (l = free_list; l; l = l->next)
		g_free (l->data);
	g_list_free (free_list);

#endif

	return TRUE;
}

static void
gncal_session_die (GnomeClient *client, gpointer data)
{
	g_main_loop_quit ((GMainLoop *) data);
}

void
gncal_session_init (GMainLoop *main_loop)
{
	GnomeClient *client;

	client = gnome_master_client ();
	if (client) {
		g_signal_connect (G_OBJECT (client), "save_yourself",
				  G_CALLBACK (gncal_session_save), NULL);
		g_signal_connect (G_OBJECT (client), "die",
				  G_CALLBACK (gncal_session_die), main_loop);
	}

}
