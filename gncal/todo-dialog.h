#ifndef __TODO_DIALOG_H__
#define __TODO_DIALOG_H__

/* To Do Dialog Object
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <gtk/gtkdialog.h>
#include <gtk/gtkwidget.h>

#include <mimedir/mimedir-vtodo.h>


#define TODO_TYPE_DIALOG		(todo_dialog_get_type())
#define TODO_DIALOG(obj)		(GTK_CHECK_CAST ((obj), TODO_TYPE_DIALOG, TodoDialog))
#define TODO_DIALOG_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), TODO_TYPE_DIALOG))
#define TODO_IS_DIALOG(obj)		(GTK_CHECK_TYPE ((obj), TODO_TYPE_DIALOG))
#define TODO_IS_DIALOG_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), TODO_TYPE_DIALOG))

typedef struct _TodoDialog	TodoDialog;
typedef struct _TodoDialogClass	TodoDialogClass;
typedef struct _TodoDialogPriv	TodoDialogPriv;

struct _TodoDialog
{
	GtkDialog       parent;

	TodoDialogPriv *priv;
};

struct _TodoDialogClass
{
	GtkDialogClass parent_class;
};

GType		 todo_dialog_get_type	(void);
GtkWidget	*todo_dialog_new	(GtkWindow *parent, MIMEDirVTodo *item);

void		 todo_dialog_set_item	(TodoDialog *dialog, MIMEDirVTodo *item);

/* The following function returns the TodoItem that's currently being
 * edited by this dialog. It will not by ref'ed, so you have to do this
 * yourself if you want to use it even after it's unbref'ed by the dialog.
 */
MIMEDirVTodo	*todo_dialog_get_item	(TodoDialog *dialog);

#endif /* __TODO_DIALOG_H__ */
