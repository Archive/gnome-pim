/* To Do Categories Object
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * Based on the original to-do dialog by Federico Mena.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>

#include <glib.h>
#include <gnome.h>

#include "todo-categories.h"
#include "utils-ui.h"


static void todo_categories_class_init	(TodoCategoriesClass *klass);
static void todo_categories_init	(TodoCategories *dialog);
static void todo_categories_finalize	(GObject *object);
static void todo_categories_response	(GtkDialog *dialog, gint response_id, gpointer data);


enum {
	COLUMN_CHECK,
	COLUMN_TEXT
};

enum {
	SIGNAL_CHANGED,
	SIGNAL_LAST
};

struct _TodoCategoriesPriv {
	GtkWidget *list;
};


static GtkDialogClass	*parent_class = NULL;
static guint		 cat_signals[SIGNAL_LAST] = { 0 };


GType
todo_categories_get_type (void)
{
	static GType todo_categories_type = 0;

	if (!todo_categories_type) {
		static const GTypeInfo todo_categories_info = {
			sizeof (TodoCategoriesClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) todo_categories_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (TodoCategories),
			1,    /* n_preallocs */
			(GInstanceInitFunc) todo_categories_init,
		};

		todo_categories_type = g_type_register_static (GTK_TYPE_DIALOG,
							       "TodoCategories",
							       &todo_categories_info,
							       0);
	}

	return todo_categories_type;
}

static void
todo_categories_class_init (TodoCategoriesClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (TODO_IS_CATEGORIES_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->finalize = todo_categories_finalize;

	parent_class = g_type_class_peek_parent (klass);

	cat_signals[SIGNAL_CHANGED] =
		g_signal_new ("changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (TodoCategoriesClass,
					       changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
}

/*
 * Utility Methods
 */

/*
 * Callbacks
 */

static void
todo_categories_modify_clicked (GtkWidget *widget, gpointer data)
{
    /* FIXME: implement */
}

static void
todo_categories_cell_toggled (GtkCellRendererToggle *renderer,
			      gchar *path_str,
			      gpointer data)
{
	TodoCategories	*dialog;
	GtkTreeModel	*model;
	GtkTreePath	*path;
	GtkTreeIter	 iter;

	gboolean b;

	g_return_if_fail (data != NULL);
	g_return_if_fail (TODO_IS_CATEGORIES (data));

	dialog = TODO_CATEGORIES (data);
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (dialog->priv->list));

	path = gtk_tree_path_new_from_string (path_str);
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_path_free (path);

	gtk_tree_model_get (model, &iter, COLUMN_CHECK, &b, -1);
	b = b ? FALSE : TRUE;
	gtk_list_store_set (GTK_LIST_STORE (model), &iter, COLUMN_CHECK, b, -1);

	g_signal_emit (G_OBJECT (dialog), cat_signals[SIGNAL_CHANGED], 0);
}

/*
 * Setup
 */

static void
todo_categories_populate_list_with_defaults (GtkListStore *store)
{
	const gchar **cat;

	/* FIXME: non-static */
	static const gchar *stock_categories[] = {
		N_("Church"),
		N_("Friends"),
		N_("Guadec"),
		N_("Hacking"),
		N_("Hobby"),
		N_("Housekeeping"),
		N_("Love"),
		N_("Meeting"),
		N_("Money"),
		N_("Music"),
		N_("Paperwork"),
		N_("Relatives"),
		N_("School"),
		N_("Shopping"),
		N_("Travel"),
		N_("Work"),
		NULL
	};

	gtk_list_store_clear (store);

	for (cat = stock_categories; *cat != NULL; cat++) {
		GtkTreeIter iter;
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,
				    COLUMN_CHECK, FALSE,
				    COLUMN_TEXT,  _(*cat),
				    -1);
	}
}

GtkWidget *
todo_categories_new_list (TodoCategories *dialog)
{
	GtkWidget         *list;
	GtkTreeViewColumn *column;
	GtkTreeSelection  *selection;
	GtkListStore      *store;
	GtkCellRenderer   *renderer;

	store = gtk_list_store_new (2, G_TYPE_BOOLEAN, G_TYPE_STRING);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (store),
					      COLUMN_TEXT, GTK_SORT_ASCENDING);

	list = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
	gtk_widget_set_size_request (list, 200, 200);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (list), FALSE);

	g_object_unref (G_OBJECT (store));

	renderer = gtk_cell_renderer_toggle_new ();
	g_signal_connect (G_OBJECT (renderer), "toggled",
			  G_CALLBACK (todo_categories_cell_toggled), dialog);
	column = gtk_tree_view_column_new_with_attributes (NULL,
							   renderer,
							   "active", COLUMN_CHECK,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);
							   
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (NULL,
							   renderer,
							   "text", COLUMN_TEXT,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_NONE);

	return list;
}

static void
todo_categories_init (TodoCategories *dialog)
{
	TodoCategoriesPriv	*priv;
	GtkWidget		*mainbox;
	GtkWidget		*label;
	GtkWidget		*button;
	GtkWidget		*sw;
	GtkTreeModel		*model;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (TODO_IS_CATEGORIES (dialog));

	priv = g_new0 (TodoCategoriesPriv, 1);
	dialog->priv = priv;

	gtk_window_set_title (GTK_WINDOW (dialog), _("To Do Item Categories"));
	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				GTK_STOCK_HELP,  GTK_RESPONSE_HELP,
				GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
				NULL);
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (todo_categories_response), NULL);

	mainbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (mainbox), 6);
	gtk_widget_show (mainbox);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), mainbox);

	label = gtk_label_new (_("Please select the categories of the current item:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (mainbox), label, FALSE, FALSE, 0);

	/* Selected list */

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
					     GTK_SHADOW_IN);
	gtk_widget_show (sw);
	gtk_box_pack_start (GTK_BOX (mainbox), sw, TRUE, TRUE, 0);

	priv->list = todo_categories_new_list (dialog);
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (priv->list));
	todo_categories_populate_list_with_defaults (GTK_LIST_STORE (model));
	gtk_widget_show (priv->list);
	gtk_container_add (GTK_CONTAINER (sw), priv->list);

	/* Modify button */

	button = gtk_button_new_with_mnemonic (_("C_onfigure Categories..."));
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (todo_categories_modify_clicked), dialog);
	/* FIXME: gtk_widget_show (button); */
	gtk_box_pack_start (GTK_BOX (mainbox), button, FALSE, FALSE, 0);
}

static void
todo_categories_finalize (GObject *object)
{
	TodoCategories *dialog;

	g_return_if_fail (object != NULL);
	g_return_if_fail (TODO_IS_CATEGORIES (object));

	dialog = TODO_CATEGORIES (object);

	g_free (dialog->priv);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
todo_categories_response (GtkDialog *dialog, gint response_id, gpointer data)
{
	g_return_if_fail (dialog != NULL);
	g_return_if_fail (GTK_IS_WINDOW (dialog));

	switch (response_id) {
	case GTK_RESPONSE_HELP:
		ui_show_help(GTK_WINDOW (dialog), "categories");
		break;
	case GTK_RESPONSE_CLOSE:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;
	case GTK_RESPONSE_DELETE_EVENT:
		break;
	default:
		g_return_if_reached ();
	}
}

GtkWidget *
todo_categories_new (GtkWindow *parent)
{
	TodoCategories *dialog;

	g_return_val_if_fail (parent == NULL || GTK_IS_WINDOW (parent), NULL);

	dialog = g_object_new (TODO_TYPE_CATEGORIES, NULL);

	if (parent)
		gtk_window_set_transient_for (GTK_WINDOW (dialog), parent);

	return GTK_WIDGET (dialog);
}

static gboolean
todo_categories_select_item_cb (GtkTreeModel *model,
				GtkTreePath  *path,
				GtkTreeIter  *iter,
				gpointer      data)
{
	const gchar *string;

        g_return_val_if_fail (model != NULL, FALSE);
        g_return_val_if_fail (GTK_IS_LIST_STORE (model), FALSE);

	gtk_tree_model_get (model, iter, COLUMN_TEXT, &string, -1);

	if (!data || !strcmp (string, (const gchar *) data)) {
		gtk_list_store_set (GTK_LIST_STORE (model), iter,
				    COLUMN_CHECK, TRUE,
				    -1);
		return TRUE;
	}

	return FALSE;
}

static gboolean
todo_categories_unselect_item_cb (GtkTreeModel *model,
				  GtkTreePath  *path,
				  GtkTreeIter  *iter,
				  gpointer      data)
{
	const gchar *string;

        g_return_val_if_fail (model != NULL, FALSE);
        g_return_val_if_fail (GTK_IS_LIST_STORE (model), FALSE);

	gtk_tree_model_get (model, iter, COLUMN_TEXT, &string, -1);

	if (!data || !strcmp (string, (const gchar *) data)) {
		gtk_list_store_set (GTK_LIST_STORE (model), iter,
				    COLUMN_CHECK, FALSE,
				    -1);
		return TRUE;
	}

	return FALSE;
}

void
todo_categories_unselect_all (TodoCategories *dialog)
{
	GtkTreeModel *model;

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (dialog->priv->list));

	gtk_tree_model_foreach (model, todo_categories_unselect_item_cb, NULL);
}

void
todo_categories_set_list (TodoCategories *dialog, GList *list)
{
	GList *current;
	GtkTreeModel *model;

	/* Unselect all */

	todo_categories_unselect_all (dialog);

	/* Select items in list */

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (dialog->priv->list));

	current = g_list_first (list);
	while (current) {
		gtk_tree_model_foreach (model,
					todo_categories_select_item_cb,
					current->data);
		current = g_list_next (current);
	}
}

static gboolean
todo_categories_get_entries (GtkTreeModel	*model,
			     GtkTreePath	*path,
			     GtkTreeIter	*iter,
			     gpointer		 data)
{
	GList		**list;
	gboolean	  enable;
	gchar		 *text;

	g_return_val_if_fail (data != NULL, FALSE);

	list = (GList **) data;

	gtk_tree_model_get (model, iter,
			    COLUMN_CHECK, &enable,
			    COLUMN_TEXT,  &text,
			    -1);
	if (enable)
		*list = g_list_append (*list, text);

	return FALSE;
}

GList *
todo_categories_get_list (TodoCategories *dialog)
{
	GList *list = NULL;
	GtkTreeModel *model;

	g_return_val_if_fail (dialog != NULL, NULL);
	g_return_val_if_fail (TODO_IS_CATEGORIES (dialog), NULL);

	model = gtk_tree_view_get_model (GTK_TREE_VIEW (dialog->priv->list));

	gtk_tree_model_foreach (model, todo_categories_get_entries, &list);

	return list;
}

static void
todo_categories_free_element (gpointer item, gpointer data)
{
	g_return_if_fail (item != NULL);

	g_free (item);
}

void
todo_categories_free_list (TodoCategories *dialog, GList *list)
{
	g_list_foreach (list, todo_categories_free_element, NULL);
	g_list_free (list);
}
