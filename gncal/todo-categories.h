#ifndef __TODO_CATEGORIES_H__
#define __TODO_CATEGORIES_H__

/* To Do Categories Object
 * Copyright (C) 2002  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * Based on the original to-do dialog by Federico Mena.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <gtk/gtkdialog.h>
#include <gtk/gtkwidget.h>


#define TODO_TYPE_CATEGORIES		(todo_categories_get_type())
#define TODO_CATEGORIES(obj)		(GTK_CHECK_CAST ((obj), TODO_TYPE_CATEGORIES, TodoCategories))
#define TODO_CATEGORIES_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), TODO_TYPE_CATEGORIES))
#define TODO_IS_CATEGORIES(obj)		(GTK_CHECK_TYPE ((obj), TODO_TYPE_CATEGORIES))
#define TODO_IS_CATEGORIES_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), TODO_TYPE_CATEGORIES))

typedef struct _TodoCategories		TodoCategories;
typedef struct _TodoCategoriesClass	TodoCategoriesClass;
typedef struct _TodoCategoriesPriv	TodoCategoriesPriv;

struct _TodoCategories
{
	GtkDialog           parent;

	TodoCategoriesPriv *priv;
};

struct _TodoCategoriesClass
{
	GtkDialogClass parent_class;

	void (* changed) (TodoCategories *dialog);
};

GType		 todo_categories_get_type	(void);
GtkWidget	*todo_categories_new		(GtkWindow *parent);
void		 todo_categories_unselect_all	(TodoCategories *dialog);
void		 todo_categories_set_list	(TodoCategories *dialog, GList *list);
GList           *todo_categories_get_list	(TodoCategories *dialog);
void		 todo_categories_free_list	(TodoCategories *dialog, GList *list);

#endif /* __TODO_CATEGORIES_H__ */
