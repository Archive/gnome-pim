#ifndef __GNOMECAL_GOTO_H__
#define __GNOMECAL_GOTO_H__

/* Gnome Calendar Go To Dialog Object
 * Copyright (C) 2002, 2003  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * Based on the original go-to dialog by Federico Mena.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>

#include <gtk/gtkdialog.h>
#include <gtk/gtkwidget.h>


#define GNCAL_TYPE_GOTO			(gncal_goto_get_type())
#define GNCAL_GOTO(obj)			(GTK_CHECK_CAST ((obj), GNCAL_TYPE_GOTO, GncalGoto))
#define GNCAL_GOTO_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GNCAL_TYPE_GOTO))
#define GNCAL_IS_GOTO(obj)		(GTK_CHECK_TYPE ((obj), GNCAL_TYPE_GOTO))
#define GNCAL_IS_GOTO_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GNCAL_TYPE_GOTO))

typedef struct _GncalGoto	GncalGoto;
typedef struct _GncalGotoClass	GncalGotoClass;
typedef struct _GncalGotoPriv	GncalGotoPriv;

struct _GncalGoto
{
	GtkDialog       parent;

	GncalGotoPriv *priv;
};

struct _GncalGotoClass
{
	GtkDialogClass parent_class;

	void (* changed)       (GncalGoto *dialog);
	void (* year_changed)  (GncalGoto *dialog);
	void (* month_changed) (GncalGoto *dialog);
	void (* day_changed)   (GncalGoto *dialog);
};

GType		 gncal_goto_get_type	(void);
GtkWidget	*gncal_goto_new		(GtkWindow *parent);
void		 gncal_goto_set_date	(GncalGoto *dialog, const GDate *date);
void		 gncal_goto_set_today	(GncalGoto *dialog);
void		 gncal_goto_get_date	(GncalGoto *dialog, GDate *date);

#endif /* __GNCAL_GOTO_H__ */
