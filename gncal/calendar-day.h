#ifndef __CALENDAR_DAY_H__
#define __CALENDAR_DAY_H__

/* Calendar Day Object
 * Copyright (C) 2003-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* This class provides a widget that contains a complete day view. */

#include <time.h>

#include <gtk/gtkvbox.h>

#include "calendar-manager.h"


#define CALENDAR_TYPE_DAY		(calendar_day_get_type())
#define CALENDAR_DAY(obj)		(GTK_CHECK_CAST ((obj), CALENDAR_TYPE_DAY, CalendarDay))
#define CALENDAR_DAY_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), CALENDAR_TYPE_DAY))
#define CALENDAR_IS_DAY(obj)		(GTK_CHECK_TYPE ((obj), CALENDAR_TYPE_DAY))
#define CALENDAR_IS_DAY_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), CALENDAR_TYPE_DAY))

typedef struct _CalendarDay		CalendarDay;
typedef struct _CalendarDayClass	CalendarDayClass;
typedef struct _CalendarDayPriv		CalendarDayPriv;

struct _CalendarDay
{
	GtkVBox parent;

	CalendarDayPriv *priv;
};

struct _CalendarDayClass
{
	GtkVBoxClass parent_class;

	void (* day_changed)  (CalendarDay *cd, guint year, guint month, guint day);
};

GType		 calendar_day_get_type		(void);
GtkWidget	*calendar_day_new		(void);

void		 calendar_day_set_calendar	(CalendarDay *cd, CalendarManager *cal);

void		 calendar_day_set_date		(CalendarDay *cd, const GDate *date);
void		 calendar_day_get_date		(CalendarDay *cd, GDate *date);

#endif /* __CALENDAR_DAY_H__ */
