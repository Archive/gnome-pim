/* GNOME Calendar Alarm Sink
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <libintl.h>

#include <glib-object.h>

#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include <gtk/gtkmessagedialog.h>
#include <gtk/gtkstock.h>

#include <mimedir/mimedir-attendee.h>
#include <mimedir/mimedir-valarm.h>
#include <mimedir/mimedir-vcomponent.h>

#include "gnomecal-alarm-sink.h"
#include "gnomecal-config.h"
#include "gnomecal-ui.h"


#ifndef _
#define _(x) (gettext(x))
#endif

#define DEBUG /* FIXME */


struct _AlarmItem {
	MIMEDirVComponent	*component;
	gulong			 handler;
};
typedef struct _AlarmItem AlarmItem;

struct _DueItem {
	time_t			 time;
	AlarmItem		*ai;
	MIMEDirVAlarm		*alarm;
};
typedef struct _DueItem DueItem;


static void	gncal_alarm_sink_class_init		(GncalAlarmSinkClass	*klass);
static void	gncal_alarm_sink_init			(GncalAlarmSink		*sink);
static void	gncal_alarm_sink_dispose		(GObject		*object);

static void	component_alarms_changed_cb		(GncalAlarmSink *sink, MIMEDirVComponent *component);

static gboolean	timeout					(gpointer data);
static void	remove_all_alarms			(GncalAlarmSink *sink);


struct _GncalAlarmSinkPriv {
	GList		*list;
	GHashTable	*hash;

	GList		*alarm_list;

	guint		 timeout_tag;
};


static GObjectClass *parent_class = NULL;

/*
 * Class and Object Management
 */

GType
gncal_alarm_sink_get_type (void)
{
	static GType gncal_alarm_sink_type = 0;

	if (!gncal_alarm_sink_type) {
		static const GTypeInfo gncal_alarm_sink_info = {
			sizeof (GncalAlarmSinkClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) gncal_alarm_sink_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (GncalAlarmSink),
			1,    /* n_preallocs */
			(GInstanceInitFunc) gncal_alarm_sink_init,
		};

		gncal_alarm_sink_type = g_type_register_static (G_TYPE_OBJECT,
								"GncalAlarmSink",
								&gncal_alarm_sink_info,
								0);
	}

	return gncal_alarm_sink_type;
}


static void
gncal_alarm_sink_class_init (GncalAlarmSinkClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (GNCAL_IS_ALARM_SINK_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->dispose = gncal_alarm_sink_dispose;

	parent_class = g_type_class_peek_parent (klass);
}

static void
gncal_alarm_sink_init (GncalAlarmSink *sink)
{
	g_return_if_fail (sink != NULL);
	g_return_if_fail (GNCAL_IS_ALARM_SINK (sink));

#ifdef DEBUG
	printf ("alarm sink %p: initializing\n", sink);
#endif

	sink->priv = g_new0 (GncalAlarmSinkPriv, 1);

	sink->priv->hash = g_hash_table_new (g_direct_hash, NULL);

	sink->priv->timeout_tag =
		g_timeout_add (1000, timeout, sink);
}

static void
gncal_alarm_sink_dispose (GObject *object)
{
	GncalAlarmSink *sink;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNCAL_IS_ALARM_SINK (object));

	sink = GNCAL_ALARM_SINK (object);

#ifdef DEBUG
	printf ("alarm sink %p: disposing\n", sink);
#endif

	if (sink->priv) {
		remove_all_alarms (sink);

		g_hash_table_destroy (sink->priv->hash);

		g_source_remove (sink->priv->timeout_tag);

		g_free (sink->priv);
		sink->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

/*
 * Private Methods
 */

static AlarmItem *
setup_alarm_item (GncalAlarmSink *sink, MIMEDirVComponent *component)
{
	AlarmItem *ai;

	g_object_ref (G_OBJECT (component));

	ai = g_new (AlarmItem, 1);

	ai->component = component;
	ai->handler   = g_signal_connect_swapped (G_OBJECT (component), "alarms-changed", G_CALLBACK (component_alarms_changed_cb), sink);

	return ai;
}

static void
delete_alarm_item (GncalAlarmSink *sink, AlarmItem *ai)
{
	g_signal_handler_disconnect (G_OBJECT (ai->component), ai->handler);
	g_object_unref (G_OBJECT (ai->component));
	g_free (ai);
}

static gint
gncal_alarm_sink_due_list_compare_func (gconstpointer a, gconstpointer b)
{
	const DueItem *dia, *dib;

	dia = (const DueItem *) a;
	dib = (const DueItem *) b;

	if (dia->time < dib->time)
		return -1;
	else if (dia->time == dib->time)
		return 0;
	else
		return 1;
}


static void
setup_alarms (GncalAlarmSink *sink, AlarmItem *ai)
{
	MIMEDirDateTime *now, *next;
	const GList *list;

	now = mimedir_datetime_new_now ();

	list = mimedir_vcomponent_get_alarm_list (ai->component);
	for (; list != NULL; list = g_list_next (list)) {
		MIMEDirVAlarm *alarm;

		g_assert (list->data != NULL && MIMEDIR_IS_VALARM (list->data));
		alarm = MIMEDIR_VALARM (list->data);

#ifdef DEBUG
		printf ("alarm sink %p: setting up alarm %p\n", sink, alarm);
#endif

		next = mimedir_valarm_get_next_occurence (alarm, ai->component, now);
		if (next) {
			DueItem *di;

#ifdef DEBUG
			printf ("next occurence at %s\n", mimedir_datetime_to_string (next));
#endif

			di = g_new (DueItem, 1);
			di->time  = mimedir_datetime_get_time_t (next);
			di->ai    = ai;
			di->alarm = alarm;

			sink->priv->alarm_list = g_list_insert_sorted (sink->priv->alarm_list, di, gncal_alarm_sink_due_list_compare_func);

			g_object_unref (G_OBJECT (next));
		}

#ifdef DEBUG
		else
			printf ("alarm has no next occurence\n");
#endif

	}

	g_object_unref (G_OBJECT (now));
}


static void
remove_alarms (GncalAlarmSink *sink, AlarmItem *ai)
{
	GList *list;

	for (list = sink->priv->alarm_list; list != NULL;) {
		GList *next;
		DueItem *di;

		di = (DueItem *) list->data;

		next = g_list_next (list);

		if (di->ai == ai)
			sink->priv->alarm_list = g_list_delete_link (sink->priv->alarm_list, list);

		list = next;
	}
}


/*
 * Callbacks
 */

static void
component_alarms_changed_cb (GncalAlarmSink *sink, MIMEDirVComponent *component)
{
	AlarmItem *ai;

	ai = (AlarmItem *) g_hash_table_lookup (sink->priv->hash, component);
	g_assert (ai != NULL);

	remove_alarms (sink, ai);
	setup_alarms (sink, ai);
}


static void
gncal_alarm_sink_reminder_response (GncalAlarmSink *sink, gint response, GtkDialog *dialog)
{
	switch (response) {
	case GTK_RESPONSE_OK:
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;
	case GTK_RESPONSE_DELETE_EVENT:
		/* dummy */
		break;
	case 1:
		/* FIXME: snooze */
		gtk_widget_destroy (GTK_WIDGET (dialog));
		break;
	default:
		g_return_if_reached ();
	}
}

/*
 * Alarm Loop
 */

static void
gncal_alarm_sink_handle_audio_alarm (GncalAlarmSink *sink, DueItem *di)
{
	/* FIXME */
}


static void
gncal_alarm_sink_handle_display_alarm (GncalAlarmSink *sink, DueItem *di)
{
	GConfClient *gconf;
	MIMEDirVComponent *comp;
	MIMEDirVAlarm *alarm;
	GtkWidget *dialog;
	gchar *msg;

	comp  = di->ai->component;
	alarm = di->alarm;

	msg = mimedir_valarm_get_reminder_string (alarm, comp);

	dialog = gtk_message_dialog_new (NULL, 0,
					 GTK_MESSAGE_INFO,
					 GTK_BUTTONS_NONE,
					 msg);
	gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				_("Snooze"),  1,
				GTK_STOCK_OK, GTK_RESPONSE_OK,
				NULL);
	g_signal_connect_swapped (G_OBJECT (dialog), "response", G_CALLBACK (gncal_alarm_sink_reminder_response), sink);
	gtk_widget_show (dialog);

	g_free (msg);

	gconf = gconf_client_get_default ();
	if (gconf_client_get_bool (gconf, CONFIG_ALARM_BEEP, NULL))
		gdk_beep ();
	g_object_unref (G_OBJECT (gconf));
}


static void
gncal_alarm_sink_handle_email_alarm (GncalAlarmSink *sink, DueItem *di)
{
	MIMEDirVAlarm *alarm;
	MIMEDirVComponent *comp;
	const GList *attendees;
	gchar *msg, *summary;

	alarm = di->alarm;
	comp  = di->ai->component;

	/* Prepare mail */

	g_object_get (G_OBJECT (alarm),
		      "summary",   &summary,
		      NULL);
	g_assert (summary != NULL);

	msg = mimedir_valarm_get_reminder_string (alarm, comp);

	attendees = mimedir_vcomponent_get_attendee_list (MIMEDIR_VCOMPONENT (alarm));

	/* Send mail */

	{
		GError *err = NULL;
		const GList *l;
		GString *receivers;
		gint in;

		gchar *command_array[] = {
			"/usr/lib/sendmail",
			NULL,
			NULL
		};

		receivers = g_string_new ("");
		for (l = attendees; l != NULL; l = g_list_next (l)) {
			MIMEDirAttendee *att = l->data;

			g_assert (att != NULL && MIMEDIR_IS_ATTENDEE (att));

			if (!g_ascii_strncasecmp (att->uri, "mailto:", 7)) {
				g_string_append (receivers, att->uri + 7);
				if (l->next)
					g_string_append_c (receivers, ' ');
			}
		}
		command_array[1] = receivers->str;

		if (!g_spawn_async_with_pipes (NULL,
					       command_array,
					       NULL, 0,
					       NULL, NULL,
					       NULL,
					       &in, NULL, NULL,
					       &err)) {
			gncal_ui_show_error (NULL, err);
			g_error_free (err);
			return;
		}

		g_string_free (receivers, TRUE);

		write (in, "To: ", 4);
		for (l = attendees; l != NULL; l = g_list_next (l)) {
			MIMEDirAttendee *att = l->data;

			g_assert (att != NULL && MIMEDIR_IS_ATTENDEE (att));

			if (!g_ascii_strncasecmp (att->uri, "mailto:", 7)) {
				write (in, att->uri + 7, strlen (att->uri + 7));
				if (l->next)
					write (in, ", ", 2);
			}
		}
		write (in, "\nSubject: ", 10);
		write (in, summary, strlen (summary));
		write (in, "\n\n", 2);
		write (in, msg, strlen (msg));

		close (in);
	}

	/* Cleanup */

	g_free (msg);
	g_free (summary);
}


static void
gncal_alarm_sink_handle_procedure_alarm (GncalAlarmSink *sink, DueItem *di)
{
	/* FIXME */
}


static void
gncal_alarm_sink_handle_alarm (GncalAlarmSink *sink, DueItem *di)
{
	switch (mimedir_valarm_get_alarm_type (di->alarm)) {
	case MIMEDIR_VALARM_AUDIO:
		gncal_alarm_sink_handle_audio_alarm (sink, di);
		break;
	case MIMEDIR_VALARM_DISPLAY:
		gncal_alarm_sink_handle_display_alarm (sink, di);
		break;
	case MIMEDIR_VALARM_EMAIL:
		gncal_alarm_sink_handle_email_alarm (sink, di);
		break;
	case MIMEDIR_VALARM_PROCEDURE:
		gncal_alarm_sink_handle_procedure_alarm (sink, di);
		break;
	case MIMEDIR_VALARM_UNKNOWN:
		/* dummy */
		break;
	}
}


static gboolean
timeout (gpointer data)
{
	GncalAlarmSink *sink;
	time_t t;

	sink = GNCAL_ALARM_SINK (data);

	t = time (NULL);

	for (;;) {
		DueItem *di;
		MIMEDirDateTime *current, *next;

		if (!sink->priv->alarm_list)
			break;

		di = (DueItem *) sink->priv->alarm_list->data;

		if (t < di->time)
			break;

#ifdef DEBUG
		printf ("alarm sink %p: alarm %p due\n", sink, di->alarm);
#endif

		sink->priv->alarm_list = g_list_delete_link (sink->priv->alarm_list, sink->priv->alarm_list);

		gncal_alarm_sink_handle_alarm (sink, di);

		current = mimedir_datetime_new_from_time_t (t + 1);
		next = mimedir_valarm_get_next_occurence (di->alarm, di->ai->component, current);
		g_object_unref (G_OBJECT (current));

		if (next) {
			di->time = mimedir_datetime_get_time_t (next);
			sink->priv->alarm_list = g_list_insert_sorted (sink->priv->alarm_list, di, gncal_alarm_sink_due_list_compare_func);
			g_object_unref (G_OBJECT (next));
		}
	}

	return TRUE;
}

static void
remove_all_alarms (GncalAlarmSink *sink)
{
	GList *item;

	for (item = sink->priv->list; item != NULL; item = g_list_next (item))
		delete_alarm_item (sink, (AlarmItem *) item->data);
	g_list_free (sink->priv->list);
	sink->priv->list = NULL;

	for (item = sink->priv->alarm_list; item != NULL; item = g_list_next (item))
		g_free ((DueItem *) item->data);
	g_list_free (sink->priv->alarm_list);
	sink->priv->alarm_list = NULL;
}

/*
 * Public Methods
 */

GncalAlarmSink *
gncal_alarm_sink_new (void)
{
	GncalAlarmSink *sink;

	sink = g_object_new (GNCAL_TYPE_ALARM_SINK, NULL);

	return sink;
}


void
gncal_alarm_sink_add (GncalAlarmSink *sink, MIMEDirVComponent *component)
{
	AlarmItem *ai;

	g_return_if_fail (sink != NULL);
	g_return_if_fail (GNCAL_IS_ALARM_SINK (sink));
	g_return_if_fail (component != NULL);
	g_return_if_fail (MIMEDIR_IS_VCOMPONENT (component));

#ifdef DEBUG
	printf ("alarm sink %p: adding component %p\n", sink, component);
#endif

	/* Check for dupes */

	g_return_if_fail (!g_hash_table_lookup (sink->priv->hash, component));

	/* Add alarm to list */

	ai = setup_alarm_item (sink, component);

	sink->priv->list = g_list_prepend (sink->priv->list, ai);
	g_hash_table_insert (sink->priv->hash, component, ai);

	/* Setup alarm */

	setup_alarms (sink, ai);
}

void
gncal_alarm_sink_remove (GncalAlarmSink *sink, MIMEDirVComponent *component)
{
	GList *item;
	AlarmItem *ai;

	g_return_if_fail (sink != NULL);
	g_return_if_fail (GNCAL_IS_ALARM_SINK (sink));
	g_return_if_fail (component != NULL);
	g_return_if_fail (MIMEDIR_IS_VCOMPONENT (component));

#ifdef DEBUG
	printf ("alarm sink %p: removing component %p\n", sink, component);
#endif

	for (item = sink->priv->list; item != NULL; item = g_list_next (item)) {
		ai = (AlarmItem *) item->data;

		if (ai->component == component) {
			sink->priv->list = g_list_delete_link (sink->priv->list, item);
			remove_alarms (sink, ai);
			g_hash_table_remove (sink->priv->hash, ai->component);
			delete_alarm_item (sink, ai);
			return;
		}
	}

	g_return_if_reached ();
}

void
gncal_alarm_sink_clear (GncalAlarmSink *sink)
{
#ifdef DEBUG
	printf ("alarm sink %p: clearing\n", sink);
#endif

	g_return_if_fail (sink != NULL);
	g_return_if_fail (GNCAL_IS_ALARM_SINK (sink));

	remove_all_alarms (sink);

	g_hash_table_destroy (sink->priv->hash);
	sink->priv->hash = g_hash_table_new (g_direct_hash, NULL);
}
