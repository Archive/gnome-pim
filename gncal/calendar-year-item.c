/* Calendar Year Item Object
 * Copyright (C) 2003-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * This code is based on the original year-view.c by
 * Arturo Espinosa <arturo@nuclecu.unam.mx> and
 * Federico Mena <federico@nuclecu.unam.mx>, which is
 * Copyright (C) 1998 The Free Software Foundation.
 */

/* TODO:
 *  o a11y
 */

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <gnome.h>

#include "calendar-marshal.h"
#include "calendar-month-item.h"
#include "calendar-year-item.h"
#include "gnomecal-config.h"

static void calendar_year_item_init		(CalendarYearItem	*item);
static void calendar_year_item_finalize		(GObject		*object);
static void calendar_year_item_class_init	(CalendarYearItemClass	*klass);
static void calendar_year_item_set_property	(GObject		*object,
						 guint			 property_id,
						 const GValue		*value,
						 GParamSpec		*pspec);
static void calendar_year_item_get_property	(GObject		*object,
						 guint			 property_id,
						 GValue			*value,
						 GParamSpec		*pspec);

static void queue_reshape			(CalendarYearItem *yitem);
static void update_months			(CalendarYearItem *yitem);


#define MONTHS_IN_YEAR		(12)


enum {
	SIGNAL_DAY_CHANGED,
	SIGNAL_DAY_SELECTED,
	SIGNAL_DAY_MENU,
	SIGNAL_LAST
};


static GnomeCanvasGroupClass *parent_class = NULL;
static guint signals[SIGNAL_LAST] = { 0 };


struct _CalendarYearItemPriv {
	/* Dimensions */

	gdouble			 x;
	gdouble			 y;
	gdouble			 width;
	gdouble			 height;

	/* Date */

	GDateYear		 year;
	GDateYear		 month;
	GDateDay		 day;

	/* Items */

	GnomeCanvasItem		*labels[MONTHS_IN_YEAR];
	GnomeCanvasItem		*months[MONTHS_IN_YEAR];

	/* Signals IDs etc. */

	guint			 idle;
};

enum {
	ARG_X = 1,
	ARG_Y,
	ARG_WIDTH,
	ARG_HEIGHT,
	ARG_YEAR,
	ARG_MONTH,
	ARG_DAY
};

/*
 * Class and Instance Setup
 */

GType
calendar_year_item_get_type (void)
{
	static GType calendar_year_item_type = 0;

	if (!calendar_year_item_type) {
		static const GTypeInfo calendar_year_item_info = {
			sizeof (CalendarYearItemClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_year_item_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarYearItem),
			0,    /* n_preallocs */
			(GInstanceInitFunc) calendar_year_item_init,
		};

		calendar_year_item_type = g_type_register_static (GNOME_TYPE_CANVAS_GROUP,
								  "CalendarYearItem",
								  &calendar_year_item_info,
								  0);
	}

	return calendar_year_item_type;
}

static void
calendar_year_item_class_init (CalendarYearItemClass *klass)
{
	GObjectClass	*gobject_class;
	GParamSpec	*pspec;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_ITEM_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->finalize     = calendar_year_item_finalize;
	gobject_class->set_property = calendar_year_item_set_property;
	gobject_class->get_property = calendar_year_item_get_property;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_DAY_CHANGED] =
		g_signal_new ("day-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarYearItemClass, day_changed),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	signals[SIGNAL_DAY_SELECTED] =
		g_signal_new ("day-selected",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarYearItemClass, day_selected),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	signals[SIGNAL_DAY_MENU] =
		g_signal_new ("day-menu",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarYearItemClass, day_menu),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	/* Properties */

	pspec = g_param_spec_double ("x",
				     _("X Position"),
				     _("Canvas item x position"),
				     -1E+307, 1E+307, 0.0,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_X, pspec);
	pspec = g_param_spec_double ("y",
				     _("Y Position"),
				     _("Canvas item y position"),
				     -1E+307, 1E+307, 0.0,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_Y, pspec);
	pspec = g_param_spec_double ("width",
				     _("Width"),
				     _("Canvas item width"),
				     0.0, 1E+307, 150.0,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_WIDTH, pspec);
	pspec = g_param_spec_double ("height",
				     _("Height"),
				     _("Canvas item height"),
				     0.0, 1E+307, 0.0,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_HEIGHT, pspec);
	pspec = g_param_spec_uint ("year",
				   _("Year"),
				   _("The displayed year"),
				   1, 2999, 1970,
				   G_PARAM_READWRITE);
	g_object_class_install_property (gobject_class, ARG_YEAR, pspec);
	pspec = g_param_spec_uint ("month",
				   _("Month"),
				   _("The displayed month (1-12)"),
				   1, 12, 1,
				   G_PARAM_READWRITE);
	g_object_class_install_property (gobject_class, ARG_MONTH, pspec);
	pspec = g_param_spec_uint ("day",
				   _("Day"),
				   _("The displayed day of month (1-31)"),
				   1, 31, 1,
				   G_PARAM_READWRITE);
	g_object_class_install_property (gobject_class, ARG_DAY, pspec);
}

static void
calendar_year_item_init (CalendarYearItem *yitem)
{
	CalendarYearItemPriv *priv;
	time_t t;
	struct tm tm;

	g_return_if_fail (yitem != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_ITEM (yitem));

	priv = g_new0 (CalendarYearItemPriv, 1);
	yitem->priv = priv;

	/* Initialize to the current date by default */

	t = time (NULL);
	tm = *localtime (&t);

	priv->year  = tm.tm_year + 1900;
	priv->month = tm.tm_mon + 1;
	priv->day   = tm.tm_mday;

	priv->idle  = 0;
}

static void
calendar_year_item_finalize (GObject *object)
{
	CalendarYearItem *yitem;
	CalendarYearItemPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_ITEM (object));

	yitem = CALENDAR_YEAR_ITEM (object);
	priv = yitem->priv;

	if (priv) {
		g_idle_remove_by_data (yitem);

		g_free (priv);
		yitem->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
calendar_year_item_set_property (GObject      *object,
				 guint         property_id,
				 const GValue *value,
				 GParamSpec   *pspec)
{
	CalendarYearItem *yitem;
	CalendarYearItemPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_ITEM (object));

	yitem = CALENDAR_YEAR_ITEM (object);
	priv = yitem->priv;

	switch (property_id) {
	case ARG_X:
		priv->x = g_value_get_double (value);

		if (!GNOME_CANVAS_ITEM (yitem)->canvas)
			break;

		queue_reshape (yitem);

		break;

	case ARG_Y:
		priv->y = g_value_get_double (value);

		if (!GNOME_CANVAS_ITEM (yitem)->canvas)
			break;

		queue_reshape (yitem);

		break;

	case ARG_WIDTH:
		priv->width = g_value_get_double (value);

		if (!GNOME_CANVAS_ITEM (yitem)->canvas)
			break;

		queue_reshape (yitem);

		break;

	case ARG_HEIGHT:
		priv->height = g_value_get_double (value);

		if (!GNOME_CANVAS_ITEM (yitem)->canvas)
			break;

		queue_reshape (yitem);

		break;

	case ARG_YEAR:
		priv->year = g_value_get_uint (value);

		if (!priv->months[0])
			break;

		update_months (yitem);

		break;

	case ARG_MONTH: {
		GDateMonth oldmonth = priv->month;

		priv->month = g_value_get_uint (value);

		if (!priv->months[0])
			break;

		g_object_set (G_OBJECT (priv->months[oldmonth]),
			      "day", G_DATE_BAD_DAY,
			      NULL);
		g_object_set (G_OBJECT (priv->months[priv->month]),
			      "day", priv->day,
			      NULL);

		break;
	}

	case ARG_DAY:
		priv->day = g_value_get_uint (value);

		if (!priv->months[0])
			break;

		g_object_set (G_OBJECT (priv->months[priv->month]),
			      "day", priv->day,
			      NULL);

		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
calendar_year_item_get_property (GObject    *object,
				 guint       property_id,
				 GValue     *value,
				 GParamSpec *pspec)
{
	CalendarYearItemPriv *priv;

	priv = CALENDAR_YEAR_ITEM (object)->priv;

	switch (property_id) {
	case ARG_X:
		g_value_set_double (value, priv->x);
		break;

	case ARG_Y:
		g_value_set_double (value, priv->y);
		break;

	case ARG_WIDTH:
		g_value_set_double (value, priv->width);
		break;

	case ARG_HEIGHT:
		g_value_set_double (value, priv->height);
		break;

	case ARG_YEAR:
		g_value_set_uint (value, priv->year);
		break;

	case ARG_MONTH:
		g_value_set_uint (value, priv->month);
		break;

	case ARG_DAY:
		g_value_set_uint (value, priv->day);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

/*
 * Callbacks
 */

static void
month_day_changed (CalendarYearItem *yitem, guint year, guint month, guint day, CalendarMonthItem *mitem)
{
	CalendarYearItemPriv *priv = yitem->priv;

	if (month != priv->month) {
		g_object_set (G_OBJECT (priv->months[priv->month - 1]),
			      "day", G_DATE_BAD_DAY,
			      NULL);
		priv->month = month;
	}

	priv->day = day;

	g_signal_emit (G_OBJECT (yitem), signals[SIGNAL_DAY_CHANGED], 0, year, month, day);
}

static void
month_day_selected (CalendarYearItem *yitem, guint year, guint month, guint day, CalendarMonthItem *mitem)
{
	g_signal_emit (G_OBJECT (yitem), signals[SIGNAL_DAY_SELECTED], 0, year, month, day);
}

static void
month_day_menu (CalendarYearItem *yitem, guint year, guint month, guint day, CalendarMonthItem *mitem)
{
	month_day_changed (yitem, year, month, day, mitem);

	g_signal_emit (G_OBJECT (yitem), signals[SIGNAL_DAY_MENU], 0, year, month, day);
}

/*
 * Private Methods
 */

#define SPACING (4.0)

static void
reshape (CalendarYearItem *yitem)
{
	CalendarYearItemPriv *priv = yitem->priv;
	gdouble label_height;
	gdouble month_width, month_height;
	gint i;

	/* Get the heights of the labels */

	g_object_get (GTK_OBJECT (priv->labels[0]),
		      "text_height", &label_height,
		      NULL);

	/* Month item dimensions */

	month_width  = priv->width / 3.0 - 2 * SPACING;
	month_height = (priv->height - label_height * 4.0) / 4.0 - 3 * SPACING;

	for (i = 0; i < G_N_ELEMENTS (priv->months); i++) {
		gdouble w, h;

		gnome_canvas_item_set (priv->months[i],
				       "width",  MAX (month_width,  0.0),
				       "height", MAX (month_height, 0.0),
				       NULL);

		g_object_get (G_OBJECT (priv->months[i]),
			      "width",  &w,
			      "height", &h,
			      NULL);

		if (w > month_width)
			month_width = w;
		if (h > month_height)
			month_height = h;
	}

	/* Set real width and height */

	priv->width  = month_width * 3 + SPACING * 2;
	priv->height = (month_height + label_height * 4) + SPACING * 3;

	/* Adjust titles and months */

	for (i = 0; i < G_N_ELEMENTS (priv->months); i++) {
		gdouble x, y;

		x = (i % 3) * (month_width + SPACING);
		y = (i / 3) * (month_height + label_height + SPACING);

		gnome_canvas_item_set (priv->labels[i],
				       "x", x + month_width / 2.0,
				       "y", y,
				       NULL);

		gnome_canvas_item_set (priv->months[i],
				       "x", x,
				       "y", y + label_height,
				       "width",  MAX (month_width,  0.0),
				       "height", MAX (month_height, 0.0),
				       NULL);
	}
}

static gboolean
idle_reshape (gpointer data)
{
	CalendarYearItem *yitem = CALENDAR_YEAR_ITEM (data);
	CalendarYearItemPriv *priv = yitem->priv;

	reshape (yitem);

	priv->idle = 0;

	return FALSE;
}

static void
queue_reshape (CalendarYearItem *yitem)
{
	CalendarYearItemPriv *priv = yitem->priv;

	if (!priv->idle)
		priv->idle = g_idle_add (&idle_reshape, yitem);
}

static void
create_headings (CalendarYearItem *yitem)
{
	CalendarYearItemPriv *priv = yitem->priv;
	gint i;

	for (i = 0; i < G_N_ELEMENTS (priv->labels); i++) {
		PangoAttrList *attrs;
		PangoAttribute *attr;
		GDate date;
		gchar *s;
		gchar buf[100];

		/* Title */

		g_date_set_dmy (&date, 1, i + 1, 2000);
		g_date_strftime (buf, sizeof (buf), "%B", &date);
		s = g_locale_to_utf8 (buf, -1, NULL, NULL, NULL);

		attrs = pango_attr_list_new ();
		attr = pango_attr_scale_new (PANGO_SCALE_LARGE);
		attr->start_index = 0;
		attr->end_index = G_MAXINT;
		pango_attr_list_insert (attrs, attr);
		attr = pango_attr_weight_new (PANGO_WEIGHT_BOLD);
		attr->start_index = 0;
		attr->end_index = G_MAXINT;
		pango_attr_list_insert (attrs, attr);

		priv->labels[i] =
			gnome_canvas_item_new (GNOME_CANVAS_GROUP (yitem),
					       GNOME_TYPE_CANVAS_TEXT,
					       "attributes", attrs,
					       "anchor",     GTK_ANCHOR_N,
					       "fill_color", "#000",
					       NULL);

		/* FIXME: "text" can't be set at construction time due to a
		 * bug in libgnomecanvas. Should be fixed in 2.9.2. */
		gnome_canvas_item_set (priv->labels[i],
				       "text", s,
				       NULL);

		g_free (s);
	}
}

static void
create_days (CalendarYearItem *yitem)
{
	CalendarYearItemPriv *priv = yitem->priv;
	gint i;

	for (i = 0; i < G_N_ELEMENTS (priv->months); i++) {
		guint day = G_DATE_BAD_DAY;

		if (priv->month == i + 1)
			day = priv->day;

		priv->months[i] =
			calendar_month_item_new (GNOME_CANVAS_GROUP (yitem),
						 "year",  (guint) priv->year,
						 "month", (guint) i + 1,
						 "day",   day,
						 NULL);

		g_signal_connect_swapped (G_OBJECT (priv->months[i]), "day-changed",
					  G_CALLBACK (month_day_changed), yitem);
		g_signal_connect_swapped (G_OBJECT (priv->months[i]), "day-selected",
					  G_CALLBACK (month_day_selected), yitem);
		g_signal_connect_swapped (G_OBJECT (priv->months[i]), "day-menu",
					  G_CALLBACK (month_day_menu), yitem);
	}
}

static void
update_months (CalendarYearItem *yitem)
{
	CalendarYearItemPriv *priv = yitem->priv;
	gint i;

	for (i = 0; i < G_N_ELEMENTS (priv->months); i++) {
		guint day = G_DATE_BAD_DAY;

		if (i == priv->month - 1)
			day = priv->day;

		g_object_set (G_OBJECT (priv->months[i]),
			      "year", (guint) priv->year,
			      "day",  day,
			      NULL);
	}
}

/*
 * Public Methods
 */

GnomeCanvasItem *
calendar_year_item_new (GnomeCanvasGroup *parent, const gchar *arg1, ...)
{
	GObject *yitem;
	va_list ap;

	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_CANVAS_GROUP (parent), NULL);

	yitem = g_object_new (CALENDAR_TYPE_YEAR_ITEM, "parent", parent, NULL);
	calendar_year_item_construct (CALENDAR_YEAR_ITEM (yitem));

	va_start (ap, arg1);
	g_object_set_valist (yitem, arg1, ap);
	va_end (ap);

	return GNOME_CANVAS_ITEM (yitem);
}

void
calendar_year_item_construct (CalendarYearItem *yitem)
{
	g_return_if_fail (yitem != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_ITEM (yitem));

	/* Create items */

	create_headings (yitem);
	create_days (yitem);
}

void
calendar_year_item_set_date (CalendarYearItem *yitem, const GDate *date)
{
	CalendarYearItemPriv *priv;

	g_return_if_fail (yitem != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_ITEM (yitem));
	g_return_if_fail (date != NULL);
	g_return_if_fail (g_date_valid (date));

	priv = yitem->priv;

	priv->year  = date->year;
	priv->month = date->month;
	priv->day   = date->day;

	update_months (yitem);
}

void
calendar_year_item_get_date (CalendarYearItem *yitem, GDate *date)
{
	CalendarYearItemPriv *priv;

	g_return_if_fail (yitem != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_ITEM (yitem));
	g_return_if_fail (date != NULL);

	priv = yitem->priv;

	g_date_set_dmy (date, priv->day, priv->month, priv->year);
}

void
calendar_year_item_mark_day (CalendarYearItem *yitem, GDateMonth month, GDateDay day)
{
	CalendarYearItemPriv *priv;

	g_return_if_fail (yitem != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_ITEM (yitem));
	g_return_if_fail (g_date_valid_month (month));
	g_return_if_fail (g_date_valid_day (day));

	priv = yitem->priv;

	calendar_month_item_mark_day (CALENDAR_MONTH_ITEM (priv->months[month - 1]), day);
}

void
calendar_year_item_unmark_day (CalendarYearItem *yitem, GDateMonth month, GDateDay day)
{
	CalendarYearItemPriv *priv;

	g_return_if_fail (yitem != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_ITEM (yitem));
	g_return_if_fail (g_date_valid_month (month));
	g_return_if_fail (g_date_valid_day (day));

	priv = yitem->priv;

	calendar_month_item_unmark_day (CALENDAR_MONTH_ITEM (priv->months[month - 1]), day);
}

void
calendar_year_item_clear_marks (CalendarYearItem *yitem)
{
	CalendarYearItemPriv *priv;
	int i;

	g_return_if_fail (yitem != NULL);
	g_return_if_fail (CALENDAR_IS_YEAR_ITEM (yitem));

	priv = yitem->priv;

	for (i = 0; i < G_N_ELEMENTS (priv->months); i++) {
		calendar_month_item_clear_marks (CALENDAR_MONTH_ITEM (priv->months[i]));
	}
}
