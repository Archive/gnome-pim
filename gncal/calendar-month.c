/* Calendar Month Object
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <time.h>

#include <glib.h>
#include <gnome.h>
#include <libgnomecanvas/gnome-canvas.h>

#include "calendar-marshal.h"
#include "calendar-month.h"
#include "calendar-month-item.h"

static void calendar_month_class_init	(CalendarMonthClass	*klass);
static void calendar_month_init		(CalendarMonth		*cm);
static void calendar_month_finalize	(GObject		*object);

static void setup			(CalendarMonth		*cm);


struct _CalendarMonthPriv {
	GtkWidget *label;
	GtkWidget *canvas;
	GnomeCanvasItem *month_item;

	gboolean show_label;
};

enum {
	SIGNAL_DAY_CHANGED,
	SIGNAL_DAY_SELECTED,
	SIGNAL_DAY_MENU,
	SIGNAL_LAST
};


static GtkVBoxClass *parent_class = NULL;

static guint signals[SIGNAL_LAST] = { 0 };

/*
 * Class and Instance Setup
 */

GType
calendar_month_get_type (void)
{
	static GType calendar_month_type = 0;

	if (!calendar_month_type) {
		static const GTypeInfo calendar_month_info = {
			sizeof (CalendarMonthClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_month_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarMonth),
			1,    /* n_preallocs */
			(GInstanceInitFunc) calendar_month_init,
		};

		calendar_month_type = g_type_register_static (GTK_TYPE_VBOX,
							      "CalendarMonth",
							      &calendar_month_info,
							      0);
	}

	return calendar_month_type;
}

static void
calendar_month_class_init (CalendarMonthClass *klass)
{
	GObjectClass *gobject_class;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->finalize = calendar_month_finalize;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_DAY_CHANGED] =
		g_signal_new ("day-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarMonthClass, day_changed),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	signals[SIGNAL_DAY_SELECTED] =
		g_signal_new ("day-selected",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarMonthClass, day_selected),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	signals[SIGNAL_DAY_MENU] =
		g_signal_new ("day-menu",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarMonthClass, day_menu),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);
}

static void
calendar_month_init (CalendarMonth *cm)
{
	g_return_if_fail (cm != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH (cm));

	cm->priv = g_new0 (CalendarMonthPriv, 1);
	cm->priv->show_label = FALSE;

	setup (cm);
}

static void
calendar_month_finalize (GObject *object)
{
	CalendarMonth *cm;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH (object));

	cm = CALENDAR_MONTH (object);

	g_free (cm->priv);
	cm->priv = NULL;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*
 * Callbacks
 */

static void
day_changed_cb (CalendarMonth *cm, guint year, guint month, guint day, CalendarMonthItem *mitem)
{
	g_signal_emit (G_OBJECT (cm), signals[SIGNAL_DAY_CHANGED], 0, year, month, day);
}

static void
day_selected_cb (CalendarMonth *cm, guint year, guint month, guint day, CalendarMonthItem *mitem)
{
	g_signal_emit (G_OBJECT (cm), signals[SIGNAL_DAY_SELECTED], 0, year, month, day);
}

static void
day_menu_cb (CalendarMonth *cm, guint year, guint month, guint day, CalendarMonthItem *mitem)
{
	g_signal_emit (G_OBJECT (cm), signals[SIGNAL_DAY_MENU], 0, year, month, day);
}

/*
 * Private Methods
 */

static void
update_label (CalendarMonth *cm)
{
	CalendarMonthPriv *priv = cm->priv;
	GDate date;
	guint year, month, day;
	gchar buffer[100];

	g_object_get (G_OBJECT (priv->month_item),
		      "year",  &year,
		      "month", &month,
		      "day",   &day,
		      NULL);
	g_date_set_dmy (&date, day, month, year);

	/* Translators: This is "MONTH YEAR". */
	g_date_strftime (buffer, sizeof (buffer), _("%B %Y"), &date);
	gtk_label_set_text (GTK_LABEL (priv->label), buffer);
}

/*
 * Setup
 */

/* Sets the scrolling region of the canvas to the allocation size */
static void
set_scroll_region (CalendarMonth *cm, GtkAllocation *allocation, GtkWidget *canvas)
{
	CalendarMonthPriv *priv = cm->priv;

	if (GTK_WIDGET_CLASS (parent_class)->size_allocate)
		(* GTK_WIDGET_CLASS (parent_class)->size_allocate) (GTK_WIDGET (canvas), allocation);

        gnome_canvas_item_set (GNOME_CANVAS_ITEM (priv->month_item),
                               "width",  (double) (allocation->width - 1),
                               "height", (double) (allocation->height - 1),
                               NULL);

        gnome_canvas_set_scroll_region (GNOME_CANVAS (priv->canvas),
                                        0, 0,
                                        allocation->width, allocation->height);
}

static void
setup (CalendarMonth *cm)
{
	CalendarMonthPriv *priv;
	PangoAttrList *attrs;
	PangoAttribute *attr;

	priv = cm->priv;

	gtk_widget_set_size_request (GTK_WIDGET (cm), 150, 120);
	gtk_box_set_spacing (GTK_BOX (cm), 12);

	/* Label */

	attrs = pango_attr_list_new ();
	attr = pango_attr_scale_new (PANGO_SCALE_X_LARGE);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);
	attr = pango_attr_weight_new (PANGO_WEIGHT_BOLD);
	attr->start_index = 0;
	attr->end_index = G_MAXINT;
	pango_attr_list_insert (attrs, attr);

	priv->label = gtk_label_new ("");
	gtk_label_set_attributes (GTK_LABEL (priv->label), attrs);
	gtk_box_pack_start (GTK_BOX (cm), priv->label, FALSE, FALSE, 0);

	pango_attr_list_unref (attrs);

	/* Canvas */

	priv->canvas = gnome_canvas_new ();
	gtk_box_pack_start (GTK_BOX (cm), priv->canvas, TRUE, TRUE, 0);
	gtk_widget_show (priv->canvas);

	priv->month_item = calendar_month_item_new (gnome_canvas_root (GNOME_CANVAS (priv->canvas)), NULL);
	g_signal_connect_swapped (G_OBJECT (priv->month_item), "day-changed",
				  G_CALLBACK (day_changed_cb), cm);
	g_signal_connect_swapped (G_OBJECT (priv->month_item), "day-selected",
				  G_CALLBACK (day_selected_cb), cm);
	g_signal_connect_swapped (G_OBJECT (priv->month_item), "day-menu",
				  G_CALLBACK (day_menu_cb), cm);

	/* Connect to size_allocate so that we can change the size of the
	 * month item and the scrolling region appropriately.
	 */

	/* FIXME: size request */
	g_signal_connect_swapped (G_OBJECT (priv->canvas), "size_allocate",
				  G_CALLBACK (set_scroll_region), cm);

	update_label (cm);
}

/*
 * Public Methods
 */

GtkWidget *
calendar_month_new (void)
{
	CalendarMonth *cm;

	cm = g_object_new (CALENDAR_TYPE_MONTH, NULL);

	return GTK_WIDGET (cm);
}

void
calendar_month_set_calendar (CalendarMonth *cm, CalendarManager *calendar)
{
	g_return_if_fail (cm != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH (cm));
	g_return_if_fail (calendar != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (calendar));

	calendar_month_item_set_calendar (CALENDAR_MONTH_ITEM (cm->priv->month_item), calendar);
}

GnomeCanvasItem *
calendar_month_get_month_item (CalendarMonth *cm)
{
	g_return_val_if_fail (cm != NULL, NULL);
	g_return_val_if_fail (CALENDAR_IS_MONTH (cm), NULL);

	return cm->priv->month_item;
}

void
calendar_month_set_date (CalendarMonth *cm, const GDate *date)
{
	CalendarMonthPriv *priv;

	g_return_if_fail (cm != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH (cm));
	g_return_if_fail (date != NULL);
	g_return_if_fail (g_date_valid (date));

	priv = cm->priv;

	calendar_month_item_set_date (CALENDAR_MONTH_ITEM (priv->month_item), date);
	update_label (cm);

	g_signal_emit (G_OBJECT (cm), signals[SIGNAL_DAY_SELECTED], 0,
		       (guint) date->year, (guint) date->month, (guint) date->day);
}

void
calendar_month_get_date (CalendarMonth *cm, GDate *date)
{
	CalendarMonthPriv *priv;

	g_return_if_fail (cm != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH (cm));
	g_return_if_fail (date != NULL);

	priv = cm->priv;

	calendar_month_item_get_date (CALENDAR_MONTH_ITEM (priv->month_item), date);
}

void
calendar_month_set_show_label (CalendarMonth *cm, gboolean show)
{
	CalendarMonthPriv *priv;

	g_return_if_fail (cm != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH (cm));

	priv = cm->priv;

	priv->show_label = show ? TRUE : FALSE;

	if (show)
		gtk_widget_show (priv->label);
	else
		gtk_widget_hide (priv->label);
}

gboolean
calendar_month_get_show_label (CalendarMonth *cm)
{
	CalendarMonthPriv *priv;

	g_return_val_if_fail (cm != NULL, FALSE);
	g_return_val_if_fail (CALENDAR_IS_MONTH (cm), FALSE);

	priv = cm->priv;

	return priv->show_label;
}
