/* Calendar Month Item Object
 * Copyright (C) 2002-2005  Sebastian Rittau <srittau@jroger.in-berlin.de>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * This code is based on the original gnome-month-item.c by
 * Federico Mena <federico@nuclecu.unam.mx>, which is
 * Copyright (C) 1998 Red Hat Software, Inc.
 */

/* TODO:
 *  o a11y
 */

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <gnome.h>

#include "calendar-manager.h"
#include "calendar-marshal.h"
#include "calendar-month-item.h"
#include "gnomecal-config.h"

static void calendar_month_item_init		(CalendarMonthItem	*item);
static void calendar_month_item_finalize	(GObject		*object);
static void calendar_month_item_class_init	(CalendarMonthItemClass	*klass);
static void calendar_month_item_set_property	(GObject		*object,
						 guint			 property_id,
						 const GValue		*value,
						 GParamSpec		*pspec);
static void calendar_month_item_get_property	(GObject		*object,
						 guint			 property_id,
						 GValue			*value,
						 GParamSpec		*pspec);

static gint day_to_index			(CalendarMonthItem *mitem, GDateDay day);
static GDateDay index_to_day			(CalendarMonthItem *mitem, gint index);

static void remove_gconf_notifications		(CalendarMonthItem *mitem);
static void reorder				(CalendarMonthItem *mitem);
static void reshape				(CalendarMonthItem *mitem);
static void set_label_color			(CalendarMonthItem *mitem, gint idx);
static void set_box_color			(CalendarMonthItem *mitem, gint idx);
static void update_head_font			(CalendarMonthItem *mitem);
static void update_day_font			(CalendarMonthItem *mitem);
static void update_outline_color		(CalendarMonthItem *mitem);
static void update_day_box_color		(CalendarMonthItem *mitem);
static void update_head_color			(CalendarMonthItem *mitem);
static void update_day_color			(CalendarMonthItem *mitem);


enum {
	SIGNAL_DAY_CHANGED,
	SIGNAL_DAY_SELECTED,
	SIGNAL_DAY_MENU,
	SIGNAL_LAST
};


static GnomeCanvasGroupClass *parent_class = NULL;
static guint signals[SIGNAL_LAST] = { 0 };


#define DAYS_IN_WEEK		7
#define DAYS_IN_MONTH		31
#define WEEKS_IN_MONTH		6
#define DAYS_IN_CALENDAR	(DAYS_IN_WEEK * WEEKS_IN_MONTH)

#define DEFAULT_FONT		"fixed"
#define DEFAULT_OUTLINE_COLOR	"#483D8B"
#define DEFAULT_DAY_BOX_COLOR   "#FFF1B2"
#define DEFAULT_HEAD_COLOR	"#FFFFFF"
#define DEFAULT_DAY_COLOR	"#000000"

#define DAY_MARKED		(0x01)
#define DAY_HIGHLIGHTED		(0x02)

struct _CalendarMonthItemPriv {
	CalendarManager		*calendar;

	GList			*appointments;

	/* Dimensions */

	gdouble			 x;
	gdouble			 y;
	gdouble			 width;
	gdouble			 height;

	/* Date */

	GDateYear		 year;
	GDateMonth		 month;
	GDateDay		 day;

	/* Settings */

	gboolean		 start_on_monday;
	gboolean		 week_numbers;

	PangoFontDescription	*head_font;
	PangoFontDescription	*day_font;
	gchar			*outline_color;
	gchar			*day_box_color;
	gchar			*head_color;
	gchar			*day_color;

	gdouble			 head_padding;
	gdouble			 day_padding;

	/* Items */

	GnomeCanvasItem		*pad_group;
	GnomeCanvasItem		*pad_box;
	GnomeCanvasItem		*head_groups[DAYS_IN_WEEK];
	GnomeCanvasItem		*head_boxes[DAYS_IN_WEEK];
	GnomeCanvasItem		*head_labels[DAYS_IN_WEEK];
	GnomeCanvasItem		*week_rows[WEEKS_IN_MONTH];
	GnomeCanvasItem		*week_groups[WEEKS_IN_MONTH];
	GnomeCanvasItem		*week_boxes[WEEKS_IN_MONTH];
	GnomeCanvasItem		*week_labels[WEEKS_IN_MONTH];
	GnomeCanvasItem		*day_groups[DAYS_IN_CALENDAR];
	GnomeCanvasItem		*day_boxes[DAYS_IN_CALENDAR];
	GnomeCanvasItem		*day_labels[DAYS_IN_CALENDAR];

	/* State */

	guchar			 day_state[DAYS_IN_MONTH];

	/* Signal IDs */

	guint			 color_changed_notify;
};

enum {
	ARG_X = 1,
	ARG_Y,
	ARG_WIDTH,
	ARG_HEIGHT,
	ARG_YEAR,
	ARG_MONTH,
	ARG_DAY,
	ARG_HEAD_FONT,
	ARG_HEAD_FONT_DESC,
	ARG_DAY_FONT,
	ARG_DAY_FONT_DESC,
	ARG_HEAD_PADDING,
	ARG_DAY_PADDING,
	ARG_SHOW_WEEK_NUMBERS,
};

/*
 * Class and Instance Setup
 */

GType
calendar_month_item_get_type (void)
{
	static GType calendar_month_item_type = 0;

	if (!calendar_month_item_type) {
		static const GTypeInfo calendar_month_item_info = {
			sizeof (CalendarMonthItemClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) calendar_month_item_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (CalendarMonthItem),
			1,    /* n_preallocs */
			(GInstanceInitFunc) calendar_month_item_init,
		};

		calendar_month_item_type = g_type_register_static (GNOME_TYPE_CANVAS_GROUP,
								   "CalendarMonthItem",
								   &calendar_month_item_info,
								   0);
	}

	return calendar_month_item_type;
}

static void
calendar_month_item_class_init (CalendarMonthItemClass *klass)
{
	GObjectClass	*gobject_class;
	GParamSpec	*pspec;

	g_return_if_fail (klass != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM_CLASS (klass));

	gobject_class = G_OBJECT_CLASS (klass);

	gobject_class->finalize     = calendar_month_item_finalize;
	gobject_class->set_property = calendar_month_item_set_property;
	gobject_class->get_property = calendar_month_item_get_property;

	parent_class = g_type_class_peek_parent (klass);

	/* Signal setup */

	signals[SIGNAL_DAY_CHANGED] =
		g_signal_new ("day-changed",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarMonthItemClass, day_changed),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	signals[SIGNAL_DAY_SELECTED] =
		g_signal_new ("day-selected",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarMonthItemClass, day_selected),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	signals[SIGNAL_DAY_MENU] =
		g_signal_new ("day-menu",
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
			      G_STRUCT_OFFSET (CalendarMonthItemClass, day_menu),
			      NULL, NULL,
			      calendar_marshal_VOID__UINT_UINT_UINT,
			      G_TYPE_NONE, 3, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_UINT);

	/* Properties */

	pspec = g_param_spec_double ("x",
				     _("X Position"),
				     _("Canvas item x position"),
				     -1E+307, 1E+307, 0.0,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_X, pspec);
	pspec = g_param_spec_double ("y",
				     _("Y Position"),
				     _("Canvas item y position"),
				     -1E+307, 1E+307, 0.0,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_Y, pspec);
	pspec = g_param_spec_double ("width",
				     _("Width"),
				     _("Canvas item width"),
				     0.0, 1E+307, 150.0,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_WIDTH, pspec);
	pspec = g_param_spec_double ("height",
				     _("Height"),
				     _("Canvas item height"),
				     0.0, 1E+307, 0.0,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_HEIGHT, pspec);
	pspec = g_param_spec_uint ("year",
				   _("Year"),
				   _("The displayed year"),
				   1, 2999, 1970,
				   G_PARAM_READWRITE);
	g_object_class_install_property (gobject_class, ARG_YEAR, pspec);
	pspec = g_param_spec_uint ("month",
				   _("Month"),
				   _("The displayed month (1-12)"),
				   1, 12, 1,
				   G_PARAM_READWRITE);
	g_object_class_install_property (gobject_class, ARG_MONTH, pspec);
	pspec = g_param_spec_uint ("day",
				   _("Day"),
				   _("The displayed day of month (1-31)"),
				   0, 31, 0,
				   G_PARAM_READWRITE);
	g_object_class_install_property (gobject_class, ARG_DAY, pspec);
	pspec = g_param_spec_string ("heading_font",
				     _("Heading font"),
				     _("Font to use for the heading"),
				     DEFAULT_FONT,
				     G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_HEAD_FONT, pspec);
	pspec = g_param_spec_pointer ("heading_font_desc",
				      _("Heading font description"),
				      _("PangoFontDescription of the font to use for the heading"),
				      G_PARAM_READWRITE);
	g_object_class_install_property (gobject_class, ARG_HEAD_FONT_DESC, pspec);
	pspec = g_param_spec_string ("day_font",
				     _("Day font"),
				     _("Font to use for the day items"),
				     DEFAULT_FONT,
				     G_PARAM_WRITABLE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_DAY_FONT, pspec);
	pspec = g_param_spec_pointer ("day_font_desc",
				      _("Day font description"),
				      _("PangoFontDescription of the font to use for the day items"),
				      G_PARAM_READWRITE);
	g_object_class_install_property (gobject_class, ARG_DAY_FONT_DESC, pspec);
	pspec = g_param_spec_double ("heading_padding",
				     _("Heading padding"),
				     _("Size of the heading's padding"),
				     0.0, 1E+307, 0.0,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_HEAD_PADDING, pspec);
	pspec = g_param_spec_double ("day_padding",
				     _("Day padding"),
				     _("Size of the padding around day items"),
				     0.0, 1E+307, 2.0,
				     G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_DAY_PADDING, pspec);
	pspec = g_param_spec_boolean ("show_week_numbers",
				      _("Show week numbers"),
				      _("Whether week numbers are displayed"),
				      TRUE,
				      G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
	g_object_class_install_property (gobject_class, ARG_SHOW_WEEK_NUMBERS, pspec);
}

static void
calendar_month_item_init (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv;
	time_t t;
	struct tm tm;
	gint i;

	g_return_if_fail (mitem != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (mitem));

	priv = g_new0 (CalendarMonthItemPriv, 1);
	mitem->priv = priv;

	priv->start_on_monday = strcmp (_("calendar:week_start:0"), "calendar:week_start:1") == 0;

	/* Initialize to the current month by default */

	t = time (NULL);
	tm = *localtime (&t);

	priv->year  = tm.tm_year + 1900;
	priv->month = tm.tm_mon + 1;
	priv->day   = tm.tm_mday;

	for (i = 0; i < G_N_ELEMENTS (priv->day_state); i++)
		priv->day_state[i] = 0x00;
}

static void
calendar_month_item_finalize (GObject *object)
{
	CalendarMonthItem *mitem;
	CalendarMonthItemPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (object));

	mitem = CALENDAR_MONTH_ITEM (object);
	priv = mitem->priv;

	if (priv) {
		GList *item;

		if (priv->calendar)
			g_object_unref (G_OBJECT (priv->calendar));

		for (item = priv->appointments; item != NULL; item = g_list_next (item))
			g_object_unref (G_OBJECT (item->data));
		g_list_free (priv->appointments);

		pango_font_description_free (priv->head_font);
		pango_font_description_free (priv->day_font);
		g_free (priv->outline_color);
		g_free (priv->day_box_color);
		g_free (priv->head_color);
		g_free (priv->day_color);

		remove_gconf_notifications (mitem);

		g_free (priv);
		mitem->priv = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
calendar_month_item_set_property (GObject      *object,
				  guint         property_id,
				  const GValue *value,
				  GParamSpec   *pspec)
{
	CalendarMonthItem *mitem;
	CalendarMonthItemPriv *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (object));

	mitem = CALENDAR_MONTH_ITEM (object);
	priv = mitem->priv;

	switch (property_id) {
	case ARG_X:
		priv->x = g_value_get_double (value);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		reshape (mitem);

		break;

	case ARG_Y:
		priv->y = g_value_get_double (value);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		reshape (mitem);

		break;

	case ARG_WIDTH:
		priv->width = g_value_get_double (value);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		reshape (mitem);

		break;

	case ARG_HEIGHT:
		priv->height = g_value_get_double (value);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		reshape (mitem);

		break;

	case ARG_YEAR:
		priv->year = g_value_get_uint (value);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		reorder (mitem);

		break;

	case ARG_MONTH:
		priv->month = g_value_get_uint (value);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		reorder (mitem);

		break;

	case ARG_DAY:
	{
		gint oldday;

		oldday = priv->day;
		priv->day = g_value_get_uint (value);
		set_label_color (mitem, day_to_index (mitem, oldday));
		set_label_color (mitem, day_to_index (mitem, priv->day));
		break;
	}

	case ARG_HEAD_FONT:
		pango_font_description_free (priv->head_font);
		priv->head_font = pango_font_description_from_string (g_value_get_string (value));

		g_return_if_fail (priv->head_font != NULL);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		update_head_font (mitem);

		break;

	case ARG_HEAD_FONT_DESC:
		pango_font_description_free (priv->head_font);
		priv->head_font = pango_font_description_copy (g_value_get_pointer (value));

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		update_head_font (mitem);

		break;

	case ARG_DAY_FONT:
		pango_font_description_free (priv->day_font);
		priv->day_font = pango_font_description_from_string (g_value_get_string (value));

		g_return_if_fail (priv->day_font != NULL);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		update_day_font (mitem);

		break;

	case ARG_DAY_FONT_DESC:
		pango_font_description_free (priv->day_font);
		priv->day_font = pango_font_description_copy (g_value_get_pointer (value));

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		update_day_font (mitem);

		break;

	case ARG_HEAD_PADDING:
		priv->head_padding = g_value_get_double (value);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		reshape (mitem);

		break;

	case ARG_DAY_PADDING:
		priv->day_padding = g_value_get_double (value);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		reshape (mitem);

		break;

	case ARG_SHOW_WEEK_NUMBERS:
		priv->week_numbers = g_value_get_boolean (value);

		if (!GNOME_CANVAS_ITEM (mitem)->canvas)
			break;

		reshape (mitem);

		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
calendar_month_item_get_property (GObject    *object,
				  guint       property_id,
				  GValue     *value,
				  GParamSpec *pspec)
{
	CalendarMonthItemPriv *priv;

	priv = CALENDAR_MONTH_ITEM (object)->priv;

	switch (property_id) {
	case ARG_X:
		g_value_set_double (value, priv->x);
		break;

	case ARG_Y:
		g_value_set_double (value, priv->y);
		break;

	case ARG_WIDTH:
		g_value_set_double (value, priv->width);
		break;

	case ARG_HEIGHT:
		g_value_set_double (value, priv->height);
		break;

	case ARG_YEAR:
		g_value_set_uint (value, priv->year);
		break;

	case ARG_MONTH:
		g_value_set_uint (value, priv->month);
		break;

	case ARG_DAY:
		g_value_set_uint (value, priv->day);
		break;

	case ARG_HEAD_FONT_DESC:
		g_value_set_pointer (value, priv->head_font);
		break;

	case ARG_DAY_FONT_DESC:
		g_value_set_pointer (value, priv->day_font);
		break;

	case ARG_HEAD_PADDING:
		g_value_set_double (value, priv->head_padding);
		break;

	case ARG_DAY_PADDING:
		g_value_set_double (value, priv->day_padding);
		break;

	case ARG_SHOW_WEEK_NUMBERS:
		g_value_set_boolean (value, priv->week_numbers);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

/*
 * Callbacks
 */

static void
color_changed_cb (GConfClient	*gconf,
		  guint		 cnxn_id,
		  GConfEntry	*entry,
		  gpointer	 data)
{
	CalendarMonthItem     *mitem;
	CalendarMonthItemPriv *priv;
	const gchar *key;
	const gchar *value;

	g_return_if_fail (entry != NULL);
	g_return_if_fail (data != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (data));

	mitem = CALENDAR_MONTH_ITEM (data);
	priv = mitem->priv;

	key   = gconf_entry_get_key (entry);
	value = gconf_value_get_string (gconf_entry_get_value (entry));

	if (strcmp (key, CONFIG_COLOR_OUTLINE) == 0) {
		g_free (priv->outline_color);
		priv->outline_color = g_strdup (value);
		update_outline_color (mitem);
	} else if (strcmp (key, CONFIG_COLOR_EMPTY_DAYS) == 0) {
		g_free (priv->day_box_color);
		priv->day_box_color = g_strdup (value);
		update_day_box_color (mitem);
	} else if (strcmp (key, CONFIG_COLOR_HEADINGS) == 0) {
		g_free (priv->head_color);
		priv->head_color = g_strdup (value);
		update_head_color (mitem);
	} else if (strcmp (key, CONFIG_COLOR_DAY_NUMBERS) == 0) {
		g_free (priv->day_color);
		priv->day_color = g_strdup (value);
		update_day_color (mitem);
	}
}

/* Handles EnterNotify and LeaveNotify events from the month item's day
 * groups, and performs appropriate prelighting.
 */
static gboolean
day_enter_leave (CalendarMonthItem *mitem, GdkEvent *event, GnomeCanvasItem *item)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	gint day, idx;

	/* Get index information */

	day = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (item), "day_of_month"));
	if (day == 0)
		return TRUE; /* it was a day outside the month's range */
	idx = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (item), "index"));

	/* Change state */

	switch (event->type) {
	case GDK_ENTER_NOTIFY:
		priv->day_state[day - 1] |= DAY_HIGHLIGHTED;
		break;

	case GDK_LEAVE_NOTIFY:
		priv->day_state[day - 1] &= ~DAY_HIGHLIGHTED;
		break;

	default:
		g_return_val_if_reached (FALSE);
	}

	set_box_color (mitem, idx);

	return TRUE;
}

static gboolean
day_button_press (CalendarMonthItem *mitem, GdkEvent *event, GnomeCanvasItem *item)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	gint day;

	if (event->button.button != 1 && event->button.button != 3)
		return FALSE;

	day = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (item), "day_of_month"));
	if (day == 0)
		return FALSE;

	if (day != priv->day) {
		GDateDay oldday = priv->day;

		priv->day = day;

		set_label_color (mitem, day_to_index (mitem, oldday));
		set_label_color (mitem, day_to_index (mitem, day));

		g_signal_emit (G_OBJECT (mitem),
			       signals[SIGNAL_DAY_CHANGED],
			       0,
			       (guint) priv->year, (guint) priv->month, (guint) day);
	}

	g_signal_emit (G_OBJECT (mitem),
		       signals[event->button.button == 1 ? SIGNAL_DAY_SELECTED : SIGNAL_DAY_MENU],
		       0,
		       (guint) priv->year, (guint) priv->month, (guint) day);

	return TRUE;
}

static gboolean
day_event_cb (CalendarMonthItem *mitem, GdkEvent *event, GnomeCanvasItem *item)
{
	switch (event->type) {
	case GDK_ENTER_NOTIFY:
	case GDK_LEAVE_NOTIFY:
		return day_enter_leave (mitem, event, item);
	case GDK_BUTTON_PRESS:
		return day_button_press (mitem, event, item);
	default:
		return FALSE;
	}
}

/*
 * Private Methods
 */

static gint
get_first_day_index (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GDate date;
	GDateWeekday weekday;

	g_date_set_dmy (&date, 1, priv->month, priv->year);
	weekday = g_date_get_weekday(&date) - 1;
	if (!priv->start_on_monday)
		weekday = (weekday + 1) % 7;

	return weekday;
}

/* The weird month of September 1752, where 3 Sep through 13 Sep were
 * eliminated due to the Gregorian reformation.
 */
static const guchar
sept_1752[DAYS_IN_CALENDAR] = {
	 0,  0,  1,  2, 14, 15, 16,
	17, 18, 19, 20, 21, 22, 23,
	24, 25, 26, 27, 28, 29, 30,
	 0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0
};

/* Fills the 42-element days array with the day numbers for the specified month.  Slots outside the
 * bounds of the month are filled with zeros.  The starting and ending indexes of the days are
 * returned in the start and end arguments.
 */
static void
calculate_month (CalendarMonthItem *mitem, guchar *days)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GDateWeekday weekday;
	guint8 days_in_month;
	guint i;

	/* Special handling for September 1752 */

	if (priv->year == 1752 && priv->month == G_DATE_SEPTEMBER) {
		memcpy (days, sept_1752, sizeof(sept_1752));
		return;
	}

	/* Initialize array */

	for (i = 0; i < DAYS_IN_CALENDAR; i++)
		days[i] = 0;

	weekday = get_first_day_index (mitem);
	days_in_month = g_date_get_days_in_month (priv->month, priv->year);
	for (i = 0; i < days_in_month; i++)
		days[weekday + i] = i + 1;
}

static gint
day_to_index (CalendarMonthItem *mitem, GDateDay day)
{
	if (day == G_DATE_BAD_DAY)
		return -1;
	else
		return get_first_day_index (mitem) + day - 1;
}

static GDateDay
index_to_day (CalendarMonthItem *mitem, gint index)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GDateDay day;

	day = index - get_first_day_index (mitem) + 1;
	if (day < 1 || day > g_date_get_days_in_month (priv->month, priv->year))
		return G_DATE_BAD_DAY;
	else
		return day;
}

static void
load_gconf (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GConfClient *gconf;

	gconf = gconf_client_get_default ();

	gconf_client_add_dir (gconf, CONFIG_COLOR_DIR,
			      GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);

	priv->outline_color =
		gncal_config_get_color (gconf, CONFIG_COLOR_OUTLINE,     DEFAULT_COLOR_OUTLINE);
	priv->day_box_color =
		gncal_config_get_color (gconf, CONFIG_COLOR_EMPTY_DAYS,  DEFAULT_COLOR_EMPTY_DAYS);
	priv->head_color =
		gncal_config_get_color (gconf, CONFIG_COLOR_HEADINGS,    DEFAULT_COLOR_HEADINGS);
	priv->day_color =
		gncal_config_get_color (gconf, CONFIG_COLOR_DAY_NUMBERS, DEFAULT_COLOR_DAY_NUMBERS);

	priv->color_changed_notify =
		gconf_client_notify_add (gconf, CONFIG_COLOR_DIR,
					 color_changed_cb, mitem,
					 NULL, NULL);

	g_object_unref (G_OBJECT (gconf));
}

static void
remove_gconf_notifications (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GConfClient *gconf;

	gconf = gconf_client_get_default ();

	if (priv->color_changed_notify) {
		gconf_client_notify_remove (gconf, priv->color_changed_notify);
		priv->color_changed_notify = 0;
	}

	g_object_unref (G_OBJECT (gconf));
}

static void
create_pad (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	const gchar *outline_color, *head_color;

	outline_color = pango_color_parse (NULL, priv->outline_color) ? priv->outline_color : DEFAULT_OUTLINE_COLOR;
	head_color    = pango_color_parse (NULL, priv->head_color)    ? priv->head_color    : DEFAULT_HEAD_COLOR;

	priv->pad_group = gnome_canvas_item_new (GNOME_CANVAS_GROUP (mitem),
						 GNOME_TYPE_CANVAS_GROUP,
						 NULL);

	priv->pad_box = gnome_canvas_item_new (GNOME_CANVAS_GROUP (priv->pad_group),
					       GNOME_TYPE_CANVAS_RECT,
					       "outline_color", outline_color,
					       "fill_color",    outline_color,
					       NULL);
}

static void
create_headings (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	const gchar *outline_color, *head_color;
	gint i;

	outline_color = pango_color_parse (NULL, priv->outline_color) ? priv->outline_color : DEFAULT_OUTLINE_COLOR;
	head_color    = pango_color_parse (NULL, priv->head_color)    ? priv->head_color    : DEFAULT_HEAD_COLOR;

	for (i = 0; i < G_N_ELEMENTS (priv->head_labels); i++) {
		GnomeCanvasItem *item;
		GnomeCanvasGroup *group;

		/* Group */

		item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (mitem),
					      GNOME_TYPE_CANVAS_GROUP,
					      NULL);
		priv->head_groups[i] = item;
		group = GNOME_CANVAS_GROUP (item);

		/* Box */

		item = gnome_canvas_item_new (group,
					      GNOME_TYPE_CANVAS_RECT,
					      "outline_color", outline_color,
					      "fill_color",    outline_color,
					      NULL);
		priv->head_boxes[i] = item;

		/* Label */

		item = gnome_canvas_item_new (group,
					      GNOME_TYPE_CANVAS_TEXT,
					      "font_desc", priv->head_font,
					      "fill_color", head_color,
					      NULL);
		priv->head_labels[i] = item;
	}
}

static void
create_week_rows (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	gint i;

	for (i = 0; i < G_N_ELEMENTS (priv->week_rows); i++) {
		GnomeCanvasItem *item;

		item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (mitem),
					      GNOME_TYPE_CANVAS_GROUP,
					      NULL);
		priv->week_rows[i] = item;
	}
}

static void
create_week_numbers (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	const gchar *outline_color, *head_color;
	gint i;

	outline_color = pango_color_parse (NULL, priv->outline_color) ? priv->outline_color : DEFAULT_OUTLINE_COLOR;
	head_color    = pango_color_parse (NULL, priv->head_color)    ? priv->head_color    : DEFAULT_HEAD_COLOR;

	g_assert (G_N_ELEMENTS (priv->week_labels) == G_N_ELEMENTS (priv->week_rows));

	for (i = 0; i < G_N_ELEMENTS (priv->week_rows); i++) {
		GnomeCanvasItem *item;
		GnomeCanvasGroup *group;

		/* Group */

		item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (priv->week_rows[i]),
					      GNOME_TYPE_CANVAS_GROUP,
					      NULL);
		priv->week_groups[i] = item;
		group = GNOME_CANVAS_GROUP (item);

		/* Box */

		item = gnome_canvas_item_new (group,
					      GNOME_TYPE_CANVAS_RECT,
					      "outline_color", outline_color,
					      "fill_color",    outline_color,
					      NULL);
		priv->week_boxes[i] = item;

		/* Label */

		item = gnome_canvas_item_new (group,
					      GNOME_TYPE_CANVAS_TEXT,
					      "font_desc", priv->head_font,
					      "fill_color", head_color,
					      NULL);
		priv->week_labels[i] = item;
	}
}

static void
create_days (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	const gchar *outline_color, *day_box_color, *day_color;
	gint i;

	day_color     = pango_color_parse (NULL, priv->day_color)     ? priv->day_color     : DEFAULT_DAY_COLOR;
	day_box_color = pango_color_parse (NULL, priv->day_box_color) ? priv->day_box_color : DEFAULT_DAY_BOX_COLOR;
	outline_color = pango_color_parse (NULL, priv->outline_color) ? priv->outline_color : DEFAULT_OUTLINE_COLOR;

	for (i = 0; i < G_N_ELEMENTS (priv->day_labels); i++) {
		GnomeCanvasItem *item;
		GnomeCanvasGroup *group;

		/* Group */

		item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (priv->week_rows[i / 7]),
					      GNOME_TYPE_CANVAS_GROUP,
					      NULL);
		g_object_set_data (G_OBJECT (item), "day_of_month", GINT_TO_POINTER (0));
		g_object_set_data (G_OBJECT (item), "index", GINT_TO_POINTER (i));
		g_signal_connect_swapped (G_OBJECT (item), "event",
					  G_CALLBACK (day_event_cb), mitem);
		priv->day_groups[i] = item;
		group = GNOME_CANVAS_GROUP (item);

		/* Box */

		item = gnome_canvas_item_new (group,
					      GNOME_TYPE_CANVAS_RECT,
					      "outline_color", outline_color,
					      "fill_color", day_box_color,
					      NULL);
		priv->day_boxes[i] = item;

		/* Label */

		item = gnome_canvas_item_new (group,
					      GNOME_TYPE_CANVAS_TEXT,
					      "font_desc", priv->day_font,
					      "fill_color", day_color,
					      NULL);
		priv->day_labels[i] = item;
	}
}

static void
reorder_week_numbers (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GDate date;
	gint i;

	g_date_set_dmy (&date, 1, priv->month, priv->year);

	for (i = 0; i < G_N_ELEMENTS (priv->week_labels); i++) {
		gchar buffer[40];

		g_date_strftime (buffer, sizeof (buffer), "%V", &date);

		gnome_canvas_item_set (priv->week_labels[i],
				       "text", buffer,
				       NULL);

		g_date_add_days (&date, 7);
	}
}

/* Reorder the items according to the current month and year. */
static void
reorder (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GnomeCanvasItem *label;
	guchar days[DAYS_IN_CALENDAR];
	gint i;

	reorder_week_numbers (mitem);

	calculate_month (mitem, days);

	/* Clear days before start of month */

	for (i = 0; days[i] == 0; i++) {
		gnome_canvas_item_set (priv->day_labels[i],
				       "text", "",
				       NULL);
		g_object_set_data (G_OBJECT (priv->day_groups[i]), "day_of_month", GINT_TO_POINTER (0));
	}

	/* Set days of month */

	for (; days[i] > 0; i++) {
		gchar *tmp;

		tmp = g_strdup_printf ("%d", days[i]);
		gnome_canvas_item_set (priv->day_labels[i],
				       "text", tmp,
				       NULL);
		g_free (tmp);
		g_object_set_data (G_OBJECT (priv->day_groups[i]), "day_of_month", GINT_TO_POINTER ((gint) days[i]));

		set_label_color (mitem, i);
		set_box_color (mitem, i);
	}

	/* Clear days after end of month */

	for (; i < G_N_ELEMENTS (days); i++) {
		gnome_canvas_item_set (priv->day_labels[i],
				       "text", "",
				       NULL);
		g_object_set_data (G_OBJECT (priv->day_groups[i]), "day_of_month", GINT_TO_POINTER (0));
	}

	/* Show/hide last week of month, depending on whether it has days in
	 * this month or not.
	 */

	label = priv->week_groups[G_N_ELEMENTS (priv->week_groups) - 1];

	if (days[G_N_ELEMENTS (days) - 7] == 0)
		gnome_canvas_item_hide (priv->week_rows[G_N_ELEMENTS (priv->week_rows) - 1]);
	else
		gnome_canvas_item_show (priv->week_rows[G_N_ELEMENTS (priv->week_rows) - 1]);
}

/* This updates all day labels in case the current day has changed. */
static void
update_current_day (CalendarMonthItem *mitem)
{
	gint i;

	for (i = 0; i < G_N_ELEMENTS (mitem->priv->day_labels); i++)
		set_label_color (mitem, i);
}

/* Calculates the minimum heading height based on the heading font size and
 * padding. It also calculates the minimum width of the month item based on
 * the width of the headings.
 */
static void
calculate_heading_size (CalendarMonthItem *mitem, gdouble *width, gdouble *height)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GnomeCanvas *canvas = GNOME_CANVAS_ITEM (mitem)->canvas;
	gint i;

	/* Go through each heading and remember the widest one */

	*width  = 0;
	*height = 0;

	for (i = 0; i < G_N_ELEMENTS (priv->head_labels); i++) {
		PangoLayout *layout;
		gchar *text;
		gint w, h;

		g_object_get (G_OBJECT (priv->head_labels[i]),
			      "text", &text,
			      NULL);

		layout = gtk_widget_create_pango_layout (GTK_WIDGET (canvas),
							 text);
		pango_layout_set_font_description (layout, priv->head_font);

		pango_layout_get_pixel_size (layout, &w, &h);
		if (*width < w)
			*width = w;
		if (*height < h)
			*height = h;
	}

	/* Calculate minimum width and height */

	*width  = DAYS_IN_WEEK * (*width + 2 * priv->head_padding);
	*height = *height + 2 * priv->head_padding;
}

/* Calculates the minimum width and height of the week numbers column
 * based on the day font size and padding.
 */
static void
calculate_week_numbers_size (CalendarMonthItem *mitem, gdouble *width, gdouble *height)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GnomeCanvas *canvas = GNOME_CANVAS_ITEM (mitem)->canvas;
	PangoLayout *layout;
	gint max_width, max_height;
	gint i;

	if (!priv->week_numbers) {
		*width  = 0.0;
		*height = 0.0;
		return;
	}

	layout = gtk_widget_create_pango_layout (GTK_WIDGET (canvas), NULL);
	pango_layout_set_font_description (layout, priv->day_font);

	max_width  = 0;
	max_height = 0;

	for (i = 1; i < 53; i++) {
		gchar buf[3];
		gint w, h;

		sprintf (buf, "%d", i);
		pango_layout_set_text (layout, buf, -1);
		pango_layout_get_pixel_size (layout, &w, &h);

		if (max_width < w)
			max_width = w;
		if (max_height < h)
			max_height = h;
	}

	*width  = max_width  + 2 * priv->day_padding;
	*height = WEEKS_IN_MONTH * (max_height + 2 * priv->day_padding);
}

/* Calculates the minimum width and height of the month item based on the
 * day font size and padding.
 */
static void
calculate_day_size (CalendarMonthItem *mitem, gdouble *width, gdouble *height)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GnomeCanvas *canvas = GNOME_CANVAS_ITEM (mitem)->canvas;
	PangoLayout *layout;
	gint max_width, max_height;
	gint i;

	layout = gtk_widget_create_pango_layout (GTK_WIDGET (canvas), NULL);
	pango_layout_set_font_description (layout, priv->day_font);

	max_width  = 0;
	max_height = 0;

	for (i = 1; i < 32; i++) {
		gchar buf[3];
		gint w, h;

		sprintf (buf, "%d", i);
		pango_layout_set_text (layout, buf, -1);
		pango_layout_get_pixel_size (layout, &w, &h);

		if (max_width < w)
			max_width = w;
		if (max_height < h)
			max_height = h;
	}

	*width  = DAYS_IN_WEEK   * (max_width  + 2 * priv->day_padding);
	*height = WEEKS_IN_MONTH * (max_height + 2 * priv->day_padding);
}

/* Resets the position of the head padding. */
static void
reshape_pad (CalendarMonthItem *mitem, gdouble x, gdouble y, gdouble width, gdouble height)
{
	CalendarMonthItemPriv *priv = mitem->priv;

	/* Group */

	gnome_canvas_item_set (priv->pad_group,
			       "x", x,
			       "y", y,
			       NULL);

	/* Box */

	gnome_canvas_item_set (priv->pad_box,
			       "x1", 0.0,
			       "y1", 0.0,
			       "x2", width,
			       "y2", height,
			       NULL);
}

/* Resets the position of the day name headings in the calendar. */
static void
reshape_headings (CalendarMonthItem *mitem, gdouble x, gdouble y, gdouble width, gdouble height)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	int i;

	width = width / DAYS_IN_WEEK;

	for (i = 0; i < G_N_ELEMENTS (priv->head_labels); i++) {

		/* Group */

		gnome_canvas_item_set (priv->head_groups[i],
				       "x", x + width * i,
				       "y", y,
				       NULL);

		/* Box */

		gnome_canvas_item_set (priv->head_boxes[i],
				       "x1", 0.0,
				       "y1", 0.0,
				       "x2", width,
				       "y2", height,
				       NULL);

		/* Label */

		gnome_canvas_item_set (priv->head_labels[i],
				       "x", priv->head_padding,
				       "y", priv->head_padding,
				       "anchor", GTK_ANCHOR_NW,
				       NULL);
	}
}

/* Resets the position of the week numbers in the calendar. */
static void
reshape_week_numbers (CalendarMonthItem *mitem, gdouble x, gdouble y, gdouble width, gdouble height)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	int i;

	height = height / WEEKS_IN_MONTH;

	for (i = 0; i < G_N_ELEMENTS (priv->week_labels); i++) {

		/* Group */

		gnome_canvas_item_set (priv->week_groups[i],
				       "x", x,
				       "y", y + height * i,
				       NULL);

		/* Box */

		gnome_canvas_item_set (priv->week_boxes[i],
				       "x1", 0.0,
				       "y1", 0.0,
				       "x2", width,
				       "y2", height,
				       NULL);

		/* Label */

		gnome_canvas_item_set (priv->week_labels[i],
				       "x", priv->head_padding,
				       "y", priv->head_padding,
				       "anchor", GTK_ANCHOR_NW,
				       NULL);

		/* Hide/show week numbers, depending on settings */

		if (priv->week_numbers)
			gnome_canvas_item_show (priv->week_groups[i]);
		else
			gnome_canvas_item_hide (priv->week_groups[i]);
	}
}

/* Resets the position of the days in the calendar. */
static void
reshape_days (CalendarMonthItem *mitem, gdouble x, gdouble y, gdouble width, gdouble height)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	gint row, col;
	gint i;

	width  = width  / DAYS_IN_WEEK;
	height = height / WEEKS_IN_MONTH;

	i = 0;

	for (row = 0; row < WEEKS_IN_MONTH; row++) {
		for (col = 0; col < DAYS_IN_WEEK; col++, i++) {

			/* Group */

			gnome_canvas_item_set (priv->day_groups[i],
					       "x", x + width * col,
					       "y", y + height * row,
					       NULL);

			/* Box */

			gnome_canvas_item_set (priv->day_boxes[i],
					       "x1", 0.0,
					       "y1", 0.0,
					       "x2", width,
					       "y2", height,
					       NULL);

			/* Label */

			gnome_canvas_item_set (priv->day_labels[i],
					       "x", priv->day_padding,
					       "y", priv->day_padding,
					       "anchor", GTK_ANCHOR_NW,
					       NULL);
		}
	}
}

static void
reshape (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	gdouble width, height;
	gdouble head_width, head_height;
	gdouble week_width, week_height;
	gdouble body_width, body_height;

	/* Calculate sizes */

	calculate_heading_size (mitem, &head_width, &head_height);
	calculate_week_numbers_size (mitem, &week_width, &week_height);
	calculate_day_size (mitem, &body_width, &body_height);

	width  = week_width + body_width;
	width  = MAX (head_width, body_width);
	height = MAX (week_height, body_height);
	height += head_height;

	if (width > priv->width)
		priv->width = width;
	if (height > priv->height)
		priv->height = height;

	body_width  = priv->width  - week_width;
	body_height = priv->height - head_height;

	/* Reshape */

	reshape_pad (mitem, priv->x, priv->y, week_width, head_height);
	reshape_headings (mitem, priv->x + week_width, priv->y, body_width, head_height);
	reshape_week_numbers (mitem, priv->x, priv->y + head_height, week_width, body_height);
	reshape_days (mitem, priv->x + week_width, priv->y + head_height, body_width, body_height);
}

static void
set_label_color (CalendarMonthItem *mitem, gint idx)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GConfClient *gconf;
	GDateDay day;
	const gchar *key;
	const gchar *def;
	gchar *color;

	if (idx == -1)
		return;

	/* Get the color */

	day = index_to_day (mitem, idx);
	if (day == G_DATE_BAD_DAY)
		return;

	if (day == priv->day) {
		key = CONFIG_COLOR_CURRENT_DAY;
		def = DEFAULT_COLOR_CURRENT_DAY;
	} else {
		key = CONFIG_COLOR_DAY_NUMBERS;
		def = DEFAULT_COLOR_DAY_NUMBERS;
	}

	/* Set the color of the item */

	gconf = gconf_client_get_default ();
	color = gncal_config_get_color (gconf, key, def);
	g_object_unref (G_OBJECT (gconf));

	gnome_canvas_item_set (priv->day_labels[idx],
			       "fill_color", color,
			       NULL);

	g_free (color);
}

static void
set_box_color (CalendarMonthItem *mitem, gint idx)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GConfClient *gconf;
	GDateDay day;
	const gchar *key;
	const gchar *def;
	gchar *color;

	if (idx == -1)
		return;

	/* Get the color */

	day = index_to_day (mitem, idx);
	if (day == G_DATE_BAD_DAY)
		return;

	if (priv->day_state[day - 1] & DAY_HIGHLIGHTED) {
		key = CONFIG_COLOR_DAY_HIGHLIGHT;
		def = DEFAULT_COLOR_DAY_HIGHLIGHT;
	} else if (priv->day_state[day - 1] & DAY_MARKED) {
		key = CONFIG_COLOR_APPOINTMENTS;
		def = DEFAULT_COLOR_APPOINTMENTS;
	} else {
		key = CONFIG_COLOR_EMPTY_DAYS;
		def = DEFAULT_COLOR_EMPTY_DAYS;
	}

	/* Set the color of the item */

	gconf = gconf_client_get_default ();
	color = gncal_config_get_color (gconf, key, def);
	g_object_unref (G_OBJECT (gconf));

	gnome_canvas_item_set (priv->day_boxes[idx],
			       "fill_color", color,
			       NULL);

	g_free (color);
}

static void
update_all_box_colors (CalendarMonthItem *mitem)
{
	gint i;

	for (i = 0; i < G_N_ELEMENTS (mitem->priv->day_boxes); i++)
		set_box_color (mitem, i);
}

static void
update_head_font (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	gint i;

	for (i = 0; i < G_N_ELEMENTS (priv->head_labels); i++) {
		if (priv->head_labels[i]) {
			gnome_canvas_item_set (priv->head_labels[i],
					       "font_desc", priv->head_font,
					       NULL);
		}
	}

	for (i = 0; i < G_N_ELEMENTS (priv->week_labels); i++) {
		if (priv->week_labels[i]) {
			gnome_canvas_item_set (priv->week_labels[i],
					       "font_desc", priv->head_font,
					       NULL);
		}
	}

	reshape (mitem);
}

static void
update_day_font (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	gint i;

	for (i = 0; i < G_N_ELEMENTS (priv->day_labels); i++) {
		if (priv->day_labels[i]) {
			gnome_canvas_item_set (priv->day_labels[i],
					       "font_desc", priv->day_font,
					       NULL);
		}
	}

	reshape (mitem);
}

static void
update_outline_color (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	const gchar *color;
	gint i;

	color = pango_color_parse (NULL, priv->outline_color) ? priv->outline_color : DEFAULT_OUTLINE_COLOR;

	gnome_canvas_item_set (priv->pad_box,
			       "fill_color", color,
			       NULL);

	for (i = 0; i < G_N_ELEMENTS (priv->head_boxes); i++) {
		gnome_canvas_item_set (priv->head_boxes[i],
				       "fill_color", color,
				       NULL);
	}

	for (i = 0; i < G_N_ELEMENTS (priv->week_boxes); i++) {
		gnome_canvas_item_set (priv->week_boxes[i],
				       "fill_color", color,
				       NULL);
	}

	for (i = 0; i < G_N_ELEMENTS (priv->day_boxes); i++) {
		gnome_canvas_item_set (priv->head_boxes[i],
				       "outline_color", color,
				       NULL);
	}
}

static void
update_day_box_color (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	const gchar *color;
	gint i;

	color = pango_color_parse (NULL, priv->day_box_color) ? priv->day_box_color : DEFAULT_DAY_BOX_COLOR;

	for (i = 0; i < G_N_ELEMENTS (priv->day_boxes); i++) {
		gnome_canvas_item_set (priv->day_boxes[i],
				       "fill_color", color,
				       NULL);
	}
}

static void
update_head_color (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	const gchar *color;
	gint i;

	color = pango_color_parse (NULL, priv->head_color) ? priv->head_color : DEFAULT_HEAD_COLOR;

	for (i = 0; i < G_N_ELEMENTS (priv->week_labels); i++) {
		gnome_canvas_item_set (priv->week_labels[i],
				       "fill_color", color,
				       NULL);
	}

	for (i = 0; i < G_N_ELEMENTS (priv->head_labels); i++) {
		gnome_canvas_item_set (priv->head_labels[i],
				       "fill_color", color,
				       NULL);
	}
}

static void
update_day_color (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	const gchar *color;
	gint i;

	color = pango_color_parse (NULL, priv->day_color) ? priv->day_color : DEFAULT_DAY_COLOR;

	for (i = 0; i < G_N_ELEMENTS (priv->day_labels); i++) {
		gnome_canvas_item_set (priv->day_labels[i],
				       "fill_color", color,
				       NULL);
	}
}

static void
update_day_names (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	gint i;

	for (i = 0; i < G_N_ELEMENTS (priv->head_labels); i++) {
		gchar buffer[40];
		gchar *tmp;
		struct tm tm;

		memset (&tm, 0, sizeof(tm));
		tm.tm_wday = priv->start_on_monday ? ((i + 1) % 7) : i;

		strftime (buffer, sizeof (buffer), "%a", &tm);
		tmp = g_locale_to_utf8 (buffer, -1, NULL, NULL, NULL);

		gnome_canvas_item_set (priv->head_labels[i],
				       "text", tmp,
				       NULL);

		g_free (tmp);
	}
}

/*
 * Appointment Handling
 */

static void
update_day_marks (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GList *item;
	gint i;

	/* Clear all day markers */

	for (i = 0; i < G_N_ELEMENTS (priv->day_state); i++)
		priv->day_state[i] &= ~DAY_MARKED;

	/* Set appropriate day markers */

	for (item = mitem->priv->appointments; item != NULL; item = g_list_next (item)) {
		/* FIXME: get days in this month where this is active */
	}

	update_all_box_colors (mitem);
}

static void
add_appointment (CalendarMonthItem *mitem, MIMEDirVEvent *appointment)
{
	CalendarMonthItemPriv *priv = mitem->priv;

	g_log (NULL, G_LOG_LEVEL_DEBUG, "adding appointment %p to month", appointment);

	g_object_ref (G_OBJECT (appointment));
	priv->appointments = g_list_prepend (priv->appointments, appointment);
}

static void
remove_appointment (CalendarMonthItem *mitem, MIMEDirVEvent *appointment)
{
	CalendarMonthItemPriv *priv = mitem->priv;

	g_log (NULL, G_LOG_LEVEL_DEBUG, "removing appointment %p from month", appointment);

	priv->appointments = g_list_remove (priv->appointments, appointment);
	g_object_unref (G_OBJECT (appointment));
}

static void
remove_all_appointments (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;

	while (priv->appointments)
		remove_appointment (mitem, MIMEDIR_VEVENT (priv->appointments->data));
}

static void
add_appointments_of_this_month (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv = mitem->priv;
	GList *list, *item;
	GDate start, end;

	g_assert (priv->calendar != NULL);

	/* Load appointments */

	g_date_clear (&start, 1);
	g_date_clear (&end, 1);
	g_date_set_dmy (&start, 1, priv->month, priv->year);
	g_date_set_dmy (&end, g_date_get_days_in_month (priv->month, priv->year), priv->month, priv->year);

	list = calendar_manager_get_appointments_intersecting (priv->calendar, &start, &end);

	for (item = list; item != NULL; item = g_list_next (item))
		add_appointment (mitem, MIMEDIR_VEVENT (item->data));

	g_list_free (list);
}

/*
 * Public Methods
 */

GnomeCanvasItem *
calendar_month_item_new (GnomeCanvasGroup *parent, const gchar *arg1, ...)
{
	GObject *mitem;
	va_list ap;

	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_CANVAS_GROUP (parent), NULL);

	mitem = g_object_new (CALENDAR_TYPE_MONTH_ITEM, "parent", parent, NULL);
	calendar_month_item_construct (CALENDAR_MONTH_ITEM (mitem));

	va_start (ap, arg1);
	g_object_set_valist (mitem, arg1, ap);
	va_end (ap);

	return GNOME_CANVAS_ITEM (mitem);
}

void
calendar_month_item_construct (CalendarMonthItem *mitem)
{
	g_return_if_fail (mitem != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (mitem));

	/* Load preferences */

	load_gconf (mitem);

	/* Create items */

	create_pad (mitem);
	create_headings (mitem);
	create_week_rows (mitem);
	create_week_numbers (mitem);
	create_days (mitem);

	update_day_names (mitem);
	reorder (mitem);
	reshape (mitem);
}

void
calendar_month_item_set_calendar (CalendarMonthItem *mitem, CalendarManager *calendar)
{
	g_return_if_fail (mitem != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (mitem));
	g_return_if_fail (calendar != NULL);
	g_return_if_fail (CALENDAR_IS_MANAGER (calendar));

	g_return_if_fail (mitem->priv->calendar == NULL);

	mitem->priv->calendar = calendar;
	g_object_ref (G_OBJECT (calendar));

	add_appointments_of_this_month (mitem);

#if 0 /* FIXME */
	priv->todo_add_sig = g_signal_connect_swapped (G_OBJECT (cal), "todo-added",
		G_CALLBACK (todo_added_cb), todo);
	priv->todo_remove_sig = g_signal_connect_swapped (G_OBJECT (cal), "todo-removed",
		G_CALLBACK (todo_removed_cb), todo);
	priv->todo_changed_sig = g_signal_connect_swapped (G_OBJECT (cal), "todo-changed",
		G_CALLBACK (todo_changed_cb), todo);
#endif

	update_day_marks (mitem);
}

void
calendar_month_item_set_date (CalendarMonthItem *mitem, const GDate *date)
{
	CalendarMonthItemPriv *priv;
	gboolean changed = FALSE;

	g_return_if_fail (mitem != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (mitem));
	g_return_if_fail (date != NULL);
	g_return_if_fail (g_date_valid (date));

	priv = mitem->priv;

	if (priv->year != date->year || priv->month != date->month)
		changed = TRUE;

	priv->year  = date->year;
	priv->month = date->month;
	priv->day   = date->day;

	if (changed) {
		reorder (mitem);

		remove_all_appointments (mitem);
		add_appointments_of_this_month (mitem);
		update_day_marks (mitem);
	} else {
		update_current_day (mitem);
	}
}

void
calendar_month_item_get_date (CalendarMonthItem *mitem, GDate *date)
{
	CalendarMonthItemPriv *priv;

	g_return_if_fail (mitem != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (mitem));
	g_return_if_fail (date != NULL);

	priv = mitem->priv;

	g_date_set_dmy (date, priv->day, priv->month, priv->year);
}

void
calendar_month_item_mark_day (CalendarMonthItem *mitem, GDateDay day)
{
	CalendarMonthItemPriv *priv;
	gint idx;

	g_return_if_fail (mitem != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (mitem));
	g_return_if_fail (g_date_valid_day (day));

	priv = mitem->priv;

	idx = day_to_index (mitem, day);

	priv->day_state[day - 1] |= DAY_MARKED;

	set_box_color (mitem, idx);
}

void
calendar_month_item_unmark_day (CalendarMonthItem *mitem, GDateDay day)
{
	CalendarMonthItemPriv *priv;
	gint idx;

	g_return_if_fail (mitem != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (mitem));
	g_return_if_fail (g_date_valid_day (day));

	priv = mitem->priv;

	idx = day_to_index (mitem, day);

	priv->day_state[day - 1] &= ~DAY_MARKED;

	set_box_color (mitem, idx);
}

void
calendar_month_item_clear_marks (CalendarMonthItem *mitem)
{
	CalendarMonthItemPriv *priv;
	int i;

	g_return_if_fail (mitem != NULL);
	g_return_if_fail (CALENDAR_IS_MONTH_ITEM (mitem));

	priv = mitem->priv;

	for (i = 0; i < G_N_ELEMENTS (priv->day_state); i++) {
		priv->day_state[i] &= ~DAY_MARKED;
		set_box_color (mitem, day_to_index (mitem, i + 1));
	}
}
